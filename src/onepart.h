#ifndef ONEPART_H
#define ONEPART_H

#include "common.h"


#ifdef QT4
#include <QtGui/QMainWindow>
#include <QtGui/QFileDialog>
#include <QtGui/QStandardItemModel>
#include <QtGui/QMessageBox>
#include <QItemDelegate>
#endif

#ifdef QT5
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QFileDialog>
#include <QStandardItemModel>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QItemDelegate>
#endif


#include "askpotential.h"
#include "delegate.h"
#include "multiple.h"
#include <vector>
#include <QDate>



#ifdef UNIX
#include <qwt6.1.0/qwt_plot.h>
#include <qwt6.1.0/qwt_scale_map.h>
#include <qwt6.1.0/qwt_color_map.h>
#include <qwt6.1.0/qwt_symbol.h>
#include <qwt6.1.0/qwt.h>
#include <qwt6.1.0/qwt_plot_curve.h>
#include <qwt6.1.0/qwt_color_map.h>
#include <qwt6.1.0/qwt_scale_widget.h>
#include <qwt6.1.0/qwt_scale_draw.h>
#include <qwt6.1.0/qwt_plot_zoomer.h>
#include <qwt6.1.0/qwt_plot_panner.h>
#include <qwt6.1.0/qwt_plot_layout.h>
#endif

#ifdef WINDOWS
#include "qwt6.1.0/qwt_plot.h"
#include "qwt6.1.0/qwt_scale_map.h"
#include "qwt6.1.0/qwt_color_map.h"
#include "qwt6.1.0/qwt_symbol.h"
#include "qwt6.1.0/qwt.h"
#include "qwt6.1.0/qwt_plot_curve.h"
#include "qwt6.1.0/qwt_color_map.h"
#include "qwt6.1.0/qwt_scale_widget.h"
#include "qwt6.1.0/qwt_scale_draw.h"
#include "qwt6.1.0/qwt_plot_zoomer.h"
#include "qwt6.1.0/qwt_plot_panner.h"
#include "qwt6.1.0/qwt_plot_layout.h"
#endif



#include "VDL.h"
#include "4algos.h"
#include "spectre.h"
#include "plotmainscreen.h"
#include <iostream>

using namespace std;






namespace Ui {
class OnePart;
}



struct Message {
    Message();
    void append(QString);
    void comment(QString);
    void clear();
    QString content();
    QString listOperations;
};




    class OnePart : public QWidget
    {
        Q_OBJECT
private:
    Ui::OnePart *ui;

public:
    explicit OnePart(QWidget *parent = 0);
    ~OnePart();

    void Reset();
    void updateScreen();    // for the message/macro

    //void update_checks();
    // void Compute();this one has bee n promoted to 'slot'
    void ComputeAndUpdateWall();
    //void PlotResultsOnly(); this one has bee n promoted to 'slot'
    void updateModel();
    void updateModel_interactions();
    void updateModel_data();
    void updateXPoints(int new_size);
    void PreComputeImpactWall();

    // out of the class ... void convert_to_occupancy(vector<double> & to_read, vector<double> & to_fill, int Vex, int starting_filling = 0);

protected:


        Multiple* multiple;

        int type_particles;
        int viewing_window;
        int position;
        int impact_wall;

        int range;
        int rangepoint;


        SpinBoxDelegate* delegate;
        QStandardItemModel* model;

        SpinBoxDelegate* delegate_interactions;
        QStandardItemModel* model_interactions;

        SpinBoxDelegate* delegate_data;
        QStandardItemModel* model_data;

        int Vex_1;
        int Vex_2;
        double Mu1;
        double Mu2;

        double kT;



        vector<double> Potential1;
        vector<double> Potential2;

        vector<double> Interaction11;
        vector<double> Interaction12;
        vector<double> Interaction22;

        vector<double> loaded_data;
        vector<double> converted_to_occupancy_data;

        vector<double> local_density_p1;
        vector<double> local_density_p2;
        vector<double> local_occupancy_p1;
        vector<double> local_occupancy_p2;
        vector<double> local_dyades_p1;
        vector<double> local_dyades_p2;


        vector<double> xpoints;

        QwtPlotCurve* curveInteraction11;
        QwtPlotCurve* curveInteraction12;
        QwtPlotCurve* curveInteraction22;
        QwtPlotCurve* curveDensity1;
        QwtPlotCurve* curveDensity2;
        QwtPlotCurve* curveOccupancy1;
        QwtPlotCurve* curveOccupancy2;
        QwtPlotCurve* curveDyade1;
        QwtPlotCurve* curveDyade2;
        QwtPlotCurve* curveData;
        QwtPlotCurve* curvePotential1;
        QwtPlotCurve* curvePotential2;

        QwtPlotCurve* curveOccupancyData;
        QwtPlotCurve* curveOnePair11;
        QwtPlotCurve* curveOnePair12;
        QwtPlotCurve* curveOnePair21;
        QwtPlotCurve* curveOnePair22;

        // going to qwt 6.0.1 ...
        Spectre* spectrogrammes;
        vector< vector<double> > PairData11;
        vector< vector<double> > PairData12;
        vector< vector<double> > PairData21;
        vector< vector<double> > PairData22;
        vector<double> revertedPair11;
        vector<double> revertedPair12;
        vector<double> revertedPair21;
        vector<double> revertedPair22;

        //bool show_data;
        bool show_our_algo;
        //bool show_pair_function;
        //bool show_all_pair_functions;
        bool show_potential;
        bool dont_plot;
        bool convertDataToOccupancy;
        bool convertOurAlgoToOccupancy;
        //bool manualimpact;

        QString currentUserDirectory;

public slots:
        void PlotResultsOnly();
        void Compute();

        void nbParticlesChanged(int new_one);
        void viewingWindowChanged(int new_one);
        void maximizeWindowSize();
        void Vex1Changed(int new_one);
        void Vex2Changed(int new_one);
        void Mu1ChangedFromSlider(int new_one);
        void Mu2ChangedFromSlider(int new_one);
        void Mu1ChangedFromBox(double new_one);
        void Mu2ChangedFromBox(double new_one);
        void positionChangedFromSlider(int new_one);
        void positionChangedFromBox(int new_one);

        void LoadPotential1();
        void LoadPotential2();

        void PotentialChangedFromTable(QModelIndex, QModelIndex);
        void DisplacementInTable(int new_position);
        void DataChangedFromTable(QModelIndex Q1, QModelIndex Q2);


        void kTChangedFromBox(double new_one);
        void ImpactChangedFromBox(int new_one);

        void upCheckData(int i = 2);

        void upCheckShowPotential(int i);

        void upCheckPairFunction(int i);
        void upCheckOnePoint(int i);

        void upCheckConvertData(int i);

        void upCheckConvertOurAlgo(int i);
        void upCheckManualImpact(int i);

        void rangeChanged(int new_one);

        void LoadInteraction11();
        void LoadInteraction12();
        void LoadInteraction22();

        void InteractionsChangedFromTable(QModelIndex Q1, QModelIndex Q2);

        void ResetFromButton();

        void FullScreenMainPlot();
        void SaveMainPlot();
        void MainPlotToR();
        void CreateSumFile();
        void scanParameters();

        void SaveOnePointPairFunction();
        void SaveAllPointsPairFunctions();
        void pairPointChangedFromSlide(int new_one);
        void pairPointChangedFromBox(int new_one);
};



#endif // ONEPART_H
