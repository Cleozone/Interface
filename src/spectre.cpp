#include "spectre.h"
#include "ui_spectre.h"

#include <qwt6.1.0/qwt_color_map.h>
#include <qwt6.1.0/qwt_raster_data.h>
#include <qwt6.1.0/qwt_plot_spectrogram.h>
#include <qwt6.1.0/qwt_scale_widget.h>
#include <qwt6.1.0/qwt_scale_draw.h>
#include <qwt6.1.0/qwt_plot_zoomer.h>
#include <qwt6.1.0/qwt_plot_panner.h>
#include <qwt6.1.0/qwt_plot_layout.h>
#include <qwt6.1.0/qwt_plot.h>
#include <qwt6.1.0/qwt_plot_spectrogram.h>

#include <vector>
#include <iostream>
using namespace std;

#define DBG 0








#include "spectre.h"

SpectrogramData::SpectrogramData(vector< vector<double> >* _content)
{
    size_I = 0;
    size_J = 0;
    myContent = _content;
    update();
}
void SpectrogramData::update(){
    if(DBG) cerr << "SpectrogramData::update()" << endl;
    size_I = 0;
    size_J = 0;

    if(myContent){
       size_I = myContent->size();
       if(size_I == 0) {
           //cerr << "EMPTY Vector (I) !!" << endl;
           return;
       }
       if(size_I > 0) size_J = (*myContent)[0].size();
       if(size_J == 0) {
           cerr << "EMPTY Vector (J) !!" << endl;
           return;
       }
       if(DBG) cerr << "SpectroData Updated with size " << size_I << "\t" << size_J << endl;
       //this->setBoundingRect(QwtDoubleRect(+0.5, +0.5, size_J, size_I));
       setInterval( Qt::XAxis, QwtInterval( 0.5, size_J-0.05 ) );
       setInterval( Qt::YAxis, QwtInterval( 0.5, size_I-0.05 ) );
       setInterval( Qt::ZAxis, QwtInterval( 0.0, 1.0 ) );
    } else {
        cerr << "SpectrogramData : Empty Data !" << endl;
        //this->setBoundingRect(QwtDoubleRect(+0.5, +0.5, size_J, size_I));
        setInterval( Qt::XAxis, QwtInterval( 0.5, 1.0 ) );
        setInterval( Qt::YAxis, QwtInterval( 0.5, 1.0 ) );
        setInterval( Qt::ZAxis, QwtInterval( 0.0, 1.0 ) );
        return;
    }
}
QwtRasterData * SpectrogramData::copy() const
{
    //cerr << "Copie " << endl;
    SpectrogramData* to_fill = new SpectrogramData(myContent);
    //cerr << "End Copie " << endl;
    return to_fill;
}

double SpectrogramData::value(double x1, double y1) const
{
    //if(DBG) cerr << "SpectrogramData::value(" << x1 << "," << y1 << ")" << endl;
    if(!myContent) return 1.0;
    /*cerr << "Check size" << (*myContent).size() << "," << (*myContent)[0].size() << " ex (0,0) " << (int) (*myContent)[0][0] << endl;
    for(int i = 0; i < myContent->size(); ++i){
        for(int j = 0; j < (*myContent)[i].size(); ++j){
            cerr << ((*myContent)[i][j]) << "\t";
        }
        cerr << endl;
    }*/

    int x = (int) (x1 + 0.51) ;
    int y = (int) (y1 + 0.51) ; /// §§§§
    if((x > 0) && (x <= size_J) && (y > 0) && (y <= size_I))
    {
        //if(DBG && (size_I * size_J < 1000)) cerr << "asks " << y1 << "(" << y-1 << ")," << x1 << "(" << x-1 << ") value=" << (*myContent)[y-1][x-1] << endl;
        return (*myContent)[y-1][x-1];
    }
    else {
        //if(DBG) cerr << "OUTs (y in table" << y-1 << "," << x-1 << endl;
        return 0.0;
    }
}


class MyZoomer: public QwtPlotZoomer{
public:
    MyZoomer( QWidget *canvas ): QwtPlotZoomer( canvas ) {
        setTrackerMode( AlwaysOn );
    }
    virtual QwtText trackerTextF( const QPointF &pos ) const {
        QColor bg( Qt::white );
        bg.setAlpha( 200 );
        QwtText text = QwtPlotZoomer::trackerTextF( pos );
        text.setBackgroundBrush( QBrush( bg ) );
        return text;
    }
};

class ColorMap: public QwtLinearColorMap
{
public:
    ColorMap(float _contrast):
        QwtLinearColorMap( Qt::darkBlue, Qt::red )
    {
        addColorStop( 0.1 * _contrast, Qt::blue );
        addColorStop( 0.6 * _contrast, Qt::green );
        addColorStop( 0.999 * _contrast, Qt::yellow );
        if(DBG) cerr << "Contraste " << _contrast << endl;
    }

};

Spectre::Spectre(QWidget *parent) : QWidget(parent), ui(new Ui::Spectre)
{
    ui->setupUi(this);
    reset();

    Dessin = new QwtPlot(ui->showingWidget);
    Dessin->resize(parent->size());
    Dessin->setGeometry(0, 20, parent->width(), parent->height() - 20);
    Dessin->show();
    //d_plot->setGeometry(0, 20, parent->width(), parent->height() - 20); //resize(parent->size());

    Spectro = new QwtPlotSpectrogram();
    //Spectro->attach(Dessin);
    QObject::connect(ui->checkBoxContours, SIGNAL(stateChanged(int)), this, SLOT(contours(int)));
    QObject::connect(ui->checkBoxFilled, SIGNAL(stateChanged(int)), this, SLOT(fill(int)));
    QObject::connect(ui->pushButtonExport, SIGNAL(released()), this, SLOT(exporting()));
    QObject::connect(ui->comboBox, SIGNAL(activated(int)), this, SLOT(choiceChanged(int)));
    QObject::connect(ui->horizontalSlider, SIGNAL(sliderMoved(int)), this, SLOT(contrastChanged(int)));

    cerr << "Sizes : " << parent->size().height() << "," << parent->size().width() << endl;
    Dessin->resize(parent->width() - 33, parent->height() - 60);

    update();
}

void Spectre::reset(){
    ui->comboBox->clear();
    ui->checkBoxContours->setChecked(false);
    ui->checkBoxFilled->setChecked(true);
    ui->labelNoData->show();
    ui->horizontalSlider->setValue(50);
    ui->horizontalSlider->setMinimum(0);
    ui->horizontalSlider->setMaximum(100);
    Contrast =  ((float) ui->horizontalSlider->value()) / 100.0;
    Donnees.clear();// seg fault ?
    Names.clear();
    currentDataShowed = NULL;
    //Spectro->init();
}

void Spectre::addData(vector< vector<double> >* _content, QString name){
    if(Names.size() != Donnees.size()) {cerr << "Spectre::choiceChanged : Problem : not same size between names and data number : Names (" << Names.size() << "), Donnees(" << Donnees.size() << ")" << endl;}
    Names.push_back(name);
    SpectrogramData* SpD = new SpectrogramData(_content);
    //test intermédiaire
    //cerr << "Interm " << (*_content)[1][2] << endl;
    //SpD->update(); done in the constructor
    Donnees.push_back(SpD);
    ui->comboBox->clear();
    for(int i = 0; i < Names.size(); ++i){
        ui->comboBox->addItem(Names[i]);
    }
    choiceChanged(Donnees.size() - 1);
}

void Spectre::update(){
    if(!currentDataShowed) {cerr << "Spectre can not update : no data" << endl; return;}
    currentDataShowed->update();
    //showContour(true);
    //showSpectrogram(true);
    //d_spectrogram->attach(this);
    Dessin->replot();

}

void Spectre::fill(int on){
    if(!currentDataShowed) return;
    if(!Spectro) {cerr << "No Spectro\n"; return;}
    //cerr << "Fill" << on << endl;
    Spectro->setDisplayMode(QwtPlotSpectrogram::ImageMode, (on != 0));
    Spectro->setDefaultContourPen(on ? QPen() : QPen(Qt::NoPen));
    Dessin->replot();
}

void Spectre::contours(int on){
    if(!currentDataShowed) return;
    if(!Spectro) {cerr << "No Spectro\n"; return;}
    //cerr << "Contours" << on << endl;
    Spectro->setDisplayMode(QwtPlotSpectrogram::ContourMode, (on != 0)); // WTF !! renderd
    Dessin->replot();
}

void Spectre::contrastChanged(int f){
    if(!currentDataShowed) return;
    if(!Spectro) {cerr << "No Spectro\n"; return;}
    Contrast = ((float) f ) / 100.0;
    if(DBG) cerr << "new Contrast : " << Contrast << endl;
    choiceChanged(ui->comboBox->currentIndex()); //not optimal !!!
}

void Spectre::choiceChanged(int nb){
    if(nb < 0) return;
    if(DBG) cerr << "New choice " << nb << endl;
    if(Names.size() != Donnees.size()) {cerr << "Spectre::choiceChanged : Problem : not same size between names and data number : Names (" << Names.size() << "), Donnees(" << Donnees.size() << ")" << endl;}
    if((nb < 0) || (nb >= Names.size())) {
        cerr << "Spectre::choiceChanged (from the combo box), invalid index" << nb << " but should be in 0 .. " << Names.size() - 1 << endl;
        return;
    }
    currentDataShowed = Donnees[nb];

    //cerr << " value : " << currentDataShowed->value(1.0,1.0) << endl;
    if(!currentDataShowed) {
        cerr << "No data for selected choice (?)" << endl;
        return;
    }
    //currentDataShowed->update(); // no need as we will give a copy --> new one --> update done at first



    Spectro->setColorMap(new ColorMap(Contrast));

    Spectro->setData(currentDataShowed->copy());    // ATTENTION, set DATA implicitement détruit le contenu...
    Spectro->attach(Dessin);

    Spectro->plot();

    QList<double> contourLevels;
    for ( double level = 0.0; level < 10.0 ; level += 0.1 )
        contourLevels += level;
    Spectro->setContourLevels(contourLevels);

    const QwtInterval zInterval = Spectro->data()->interval( Qt::ZAxis );
    QwtScaleWidget *rightAxis = Dessin->axisWidget(QwtPlot::yRight);
    rightAxis->setTitle("Intensity");
    rightAxis->setColorBarEnabled(true);
    rightAxis->setColorMap(zInterval, new ColorMap(Contrast));
    Dessin->setAxisScale(QwtPlot::yRight,       //ici on parle de l'axe des intensités
        zInterval.minValue(),
        zInterval.maxValue() );
    Dessin->enableAxis(QwtPlot::yRight);
    Dessin->plotLayout()->setAlignCanvasToScales(true);
    rightAxis->setFont(QFont(QString("Arial"),7));
    Dessin->axisWidget(QwtPlot::yLeft)->setFont(QFont(QString("Arial"),7));
    Dessin->axisWidget(QwtPlot::xBottom)->setFont(QFont(QString("Arial"),7));
    Dessin->replot();


    // LeftButton for the zooming
    // MidButton for the panning
    // RightButton: zoom out by 1
    // Ctrl+RighButton: zoom out to full size

    QwtPlotZoomer* zoomer = new MyZoomer(Dessin->canvas());
    zoomer->setMousePattern( QwtEventPattern::MouseSelect2,
        Qt::RightButton, Qt::ControlModifier );
    zoomer->setMousePattern( QwtEventPattern::MouseSelect3,
        Qt::RightButton );

    QwtPlotPanner *panner = new QwtPlotPanner(Dessin->canvas());
    panner->setAxisEnabled(QwtPlot::yRight, false);
    panner->setMouseButton(Qt::MidButton);

    // Avoid jumping when labels with more/less digits
    // appear/disappear when scrolling vertically
    // en français, pour que les zooms soient corrects.

    const QFontMetrics fm(Dessin->axisWidget(QwtPlot::yLeft)->font());
    QwtScaleDraw *sd = Dessin->axisScaleDraw(QwtPlot::yLeft);
    sd->setMinimumExtent( fm.width("100.00") );

    const QColor c(Qt::darkBlue);
    zoomer->setRubberBandPen(c);
    zoomer->setTrackerPen(c);
}

void Spectre::exporting(){
    if(!currentDataShowed) return;
    if(DBG) cerr << "You want to export ..." << endl;
}

Spectre::~Spectre(){
    /*for(int i = 0; i < Donnees.size(); ++i){
        delete Donnees[i];
    }*/
    delete ui;
}





