#include "onepart.h"
#include "ui_onepart.h"

#include "scanparameters.h"
#include "plotmainscreen.h"
#include "delegate.h"
#include "spectro.h"
#include "viewsum.h"
#include "multiple.h"

#include <iostream>
#include <sstream>
#include <cmath>
#include <string>
using namespace std;



// remark : dont_plot and show_our_algo sont des astuces pour modifier des truc sans recalculer à chaque modif
// ça aurait été plus facile avec un bool 'données modifiées' ...

#define show_data ui->DataCheckBox->isChecked()
#define show_pair_function ui->checkBoxShowOnePair->isChecked()
#define show_all_pair_functions ui->checkBoxShowPair->isChecked()
#define manualimpact ui->checkBoxManualImpact->isChecked()
#define convertDataToOccupancy ui->checkBoxConvertData->isChecked()
#define show_potential ui->checkBoxShowPotentials->isChecked()

OnePart::OnePart(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OnePart),
    currentUserDirectory(HOME)
{
    ui->setupUi(this);

    // ---------  Defines the tables (size, type)

    model = new QStandardItemModel(2, 1);
    model_interactions = new QStandardItemModel(2, 1);
    model_data = new QStandardItemModel(1, 1);

    delegate = new SpinBoxDelegate(this);
    delegate_interactions = new SpinBoxDelegate(this);
    delegate_data = new SpinBoxDelegate(this);

    ui->TablePotentials->setItemDelegate(delegate);
    ui->TableInteractions->setItemDelegate(delegate_interactions);
    ui->tableViewData->setItemDelegate(delegate_data);

    // ---------  Defines all the curves in the upper plot

    ui->qwtPlotMainPlot->axisWidget(QwtPlot::yLeft)->setFont(QFont(QString("Arial"),7));
    ui->qwtPlotMainPlot->axisWidget(QwtPlot::xBottom)->setFont(QFont(QString("Arial"),7));

    curveInteraction11 = new QwtPlotCurve("Interaction 1-1");
    curveInteraction12 = new QwtPlotCurve("Interaction 1-2");
    curveInteraction22 = new QwtPlotCurve("Interaction 2-2");
    curveInteraction11->setPen(QColor(Qt::darkBlue));
    curveInteraction11->setStyle(QwtPlotCurve::Lines);
    curveInteraction11->setRenderHint(QwtPlotItem::RenderAntialiased);
    curveInteraction12->setPen(QColor(Qt::darkBlue));
    curveInteraction12->setStyle(QwtPlotCurve::Lines);
    curveInteraction12->setRenderHint(QwtPlotItem::RenderAntialiased);
    curveInteraction22->setPen(QColor(Qt::darkBlue));
    curveInteraction22->setStyle(QwtPlotCurve::Lines);
    curveInteraction22->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveDensity1 = new QwtPlotCurve("Density1");
    curveDensity2 = new QwtPlotCurve("Density2");
    curveDensity1->setPen(QColor(Qt::darkBlue));
    curveDensity1->setStyle(QwtPlotCurve::Lines);
    curveDensity1->setRenderHint(QwtPlotItem::RenderAntialiased);
    curveDensity2->setPen(QColor(Qt::darkRed));
    curveDensity2->setStyle(QwtPlotCurve::Lines);
    curveDensity2->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveData = new QwtPlotCurve("Data");
    curveData->setPen(QColor(Qt::darkGreen));
    curveData->setStyle(QwtPlotCurve::Lines);
    curveData->setRenderHint(QwtPlotItem::RenderAntialiased);

    curvePotential1 = new QwtPlotCurve("Potential1");
    curvePotential1->setPen(QColor(Qt::darkCyan));
    curvePotential1->setStyle(QwtPlotCurve::Lines);
    curvePotential1->setRenderHint(QwtPlotItem::RenderAntialiased);
    curvePotential2 = new QwtPlotCurve("Potential2");
    curvePotential2->setPen(QColor(Qt::gray));
    curvePotential2->setStyle(QwtPlotCurve::Lines);
    curvePotential2->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveOccupancy1 = new QwtPlotCurve("Occupancy1");
    curveOccupancy1->setPen(QColor(Qt::blue));
    curveOccupancy1->setStyle(QwtPlotCurve::Lines);
    curveOccupancy1->setRenderHint(QwtPlotItem::RenderAntialiased);
    curveOccupancy2 = new QwtPlotCurve("Occupancy2");
    curveOccupancy2->setPen(QColor(Qt::red));
    curveOccupancy2->setStyle(QwtPlotCurve::Lines);
    curveOccupancy2->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveDyade1 = new QwtPlotCurve("Dyade1");
    curveDyade1->setPen(QColor(Qt::blue));
    curveDyade1->setStyle(QwtPlotCurve::Lines);
    curveDyade1->setRenderHint(QwtPlotItem::RenderAntialiased);
    curveDyade2 = new QwtPlotCurve("Dyade2");
    curveDyade2->setPen(QColor(Qt::red));
    curveDyade2->setStyle(QwtPlotCurve::Lines);
    curveDyade2->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveOccupancyData = new QwtPlotCurve("DataConverted");
    curveOccupancyData->setPen(QColor(Qt::green));
    curveOccupancyData->setStyle(QwtPlotCurve::Lines);
    curveOccupancyData->setRenderHint(QwtPlotItem::RenderAntialiased);

    // ---------  Defines all the curves in the bottom plot (one point pair functions)

    ui->qwtPlotOnePair->axisWidget(QwtPlot::yLeft)->setFont(QFont(QString("Arial"),7));
    ui->qwtPlotOnePair->axisWidget(QwtPlot::xBottom)->setFont(QFont(QString("Arial"),7));

    curveOnePair11 = new QwtPlotCurve("Paire 1-1");
    curveOnePair11->setPen(QColor(Qt::darkBlue));
    curveOnePair11->setStyle(QwtPlotCurve::Lines);
    curveOnePair11->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveOnePair12 = new QwtPlotCurve("Paire 1-2");
    curveOnePair12->setPen(QColor(Qt::darkGreen));
    curveOnePair12->setStyle(QwtPlotCurve::Lines);
    curveOnePair12->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveOnePair21 = new QwtPlotCurve("Paire 2-1");
    curveOnePair21->setPen(QColor(Qt::darkRed));
    curveOnePair21->setStyle(QwtPlotCurve::Lines);
    curveOnePair21->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveOnePair22 = new QwtPlotCurve("Paire 2-2");
    curveOnePair22->setPen(QColor(Qt::darkYellow));
    curveOnePair22->setStyle(QwtPlotCurve::Lines);
    curveOnePair22->setRenderHint(QwtPlotItem::RenderAntialiased);


    // --------- Define the spectrogram (to see all pair functions in 2D)

    spectrogrammes = new Spectre(ui->widgetSpectrePairs);
    spectrogrammes->addData(&(PairData11), QString("Pair 1 - 1"));
    spectrogrammes->addData(&(PairData12), QString("Pair 1 - 2"));
    spectrogrammes->addData(&(PairData21), QString("Pair 2 - 1"));
    spectrogrammes->addData(&(PairData22), QString("Pair 2 - 2"));


    /* ----- Connects signals to functions ----- */

    QObject::connect(ui->DataCheckBox, SIGNAL(stateChanged(int)), this, SLOT(upCheckData(int)));
    QObject::connect(ui->pushButtonLoadData, SIGNAL(released()), this, SLOT(upCheckData()));
    QObject::connect(ui->checkBoxConvertData, SIGNAL(stateChanged(int)), this, SLOT(upCheckConvertData(int)));
    QObject::connect(ui->checkBoxLeftDensities, SIGNAL(stateChanged(int)), this, SLOT(PlotResultsOnly()));
    QObject::connect(ui->checkBoxOccupancy, SIGNAL(stateChanged(int)), this, SLOT(PlotResultsOnly()));
    QObject::connect(ui->checkBoxDyades, SIGNAL(stateChanged(int)), this, SLOT(PlotResultsOnly()));
    QObject::connect(ui->checkBoxShowPair, SIGNAL(stateChanged(int)), this, SLOT(upCheckPairFunction(int)));
    QObject::connect(ui->checkBoxShowOnePair, SIGNAL(stateChanged(int)), this, SLOT(upCheckOnePoint(int)));
    QObject::connect(ui->checkBoxShowPotentials, SIGNAL(stateChanged(int)), this, SLOT(upCheckShowPotential(int)));
    QObject::connect(ui->checkBoxManualImpact, SIGNAL(stateChanged(int)), this, SLOT(upCheckManualImpact(int)));
    QObject::connect(ui->spinBoxNumberImpact, SIGNAL(valueChanged(int)), this, SLOT(ImpactChangedFromBox(int)));
    QObject::connect(ui->doubleSpinBoxDyade1, SIGNAL(valueChanged(double)), this, SLOT(Compute()));
    QObject::connect(ui->doubleSpinBoxDyade2, SIGNAL(valueChanged(double)), this, SLOT(Compute()));
    QObject::connect(ui->checkBoxWithInteraction, SIGNAL(stateChanged(int)), this, SLOT(Compute()));


    QObject::connect(ui->BoxNbTypeParticules, SIGNAL(valueChanged(int)), this, SLOT(nbParticlesChanged(int)));
    QObject::connect(ui->spinBoxVex1, SIGNAL(valueChanged(int)), this, SLOT(Vex1Changed(int)));
    QObject::connect(ui->spinBoxVex2, SIGNAL(valueChanged(int)), this, SLOT(Vex2Changed(int)));
    QObject::connect(ui->Mu1horizontalSlider, SIGNAL(sliderMoved(int)), this, SLOT(Mu1ChangedFromSlider(int)));
    QObject::connect(ui->Mu2horizontalSlider, SIGNAL(sliderMoved(int)), this, SLOT(Mu2ChangedFromSlider(int)));
    QObject::connect(ui->Mu1doubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(Mu1ChangedFromBox(double)));
    QObject::connect(ui->Mu2doubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(Mu2ChangedFromBox(double)));
    QObject::connect(ui->kTdoubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(kTChangedFromBox(double)));

    QObject::connect(ui->pushButtonLoadPotential1, SIGNAL(released()), this, SLOT(LoadPotential1()));
    QObject::connect(ui->pushButtonLoadPotential2, SIGNAL(released()), this, SLOT(LoadPotential2()));
    QObject::connect(ui->pushButtonLoadInteraction11, SIGNAL(released()), this, SLOT(LoadInteraction11()));
    QObject::connect(ui->pushButtonLoadInteraction12, SIGNAL(released()), this, SLOT(LoadInteraction12()));
    QObject::connect(ui->pushButtonLoadInteraction22, SIGNAL(released()), this, SLOT(LoadInteraction22()));

    QObject::connect(this->model, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(PotentialChangedFromTable(QModelIndex, QModelIndex)));
    QObject::connect(this->model_interactions, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(InteractionsChangedFromTable(QModelIndex, QModelIndex)));
    QObject::connect(this->model_data, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(DataChangedFromTable(QModelIndex, QModelIndex)));

    QObject::connect(ui->spinBoxPosition, SIGNAL(valueChanged(int)), this, SLOT(positionChangedFromBox(int)));
    QObject::connect(ui->horizontalSliderMainPlot, SIGNAL(sliderMoved(int)), this, SLOT(positionChangedFromSlider(int)));
    QObject::connect(ui->BoxLength, SIGNAL(valueChanged(int)), this, SLOT(viewingWindowChanged(int)));
    QObject::connect(ui->ButtonMaximizeLength, SIGNAL(released()), this, SLOT(maximizeWindowSize()));

    QObject::connect(ui->spinBoxRangePair, SIGNAL(valueChanged(int)), this, SLOT(rangeChanged(int)));
    QObject::connect(ui->spinBoxOnePair, SIGNAL(valueChanged(int)), this, SLOT(pairPointChangedFromBox(int)));
    QObject::connect(ui->horizontalSliderOnePair, SIGNAL(sliderMoved(int)), this, SLOT(pairPointChangedFromSlide(int)));

    QObject::connect(ui->pushButtonFullScreen, SIGNAL(released()), this, SLOT(FullScreenMainPlot()));
    QObject::connect(ui->pushButtonScanParameters, SIGNAL(released()), this, SLOT(scanParameters()));
    QObject::connect(ui->pushButtonReset, SIGNAL(released()), this, SLOT(ResetFromButton()));
    QObject::connect(ui->pushButtonCreateSum, SIGNAL(released()), this, SLOT(CreateSumFile()));

    //QObject::connect(ui->centralWidget , SIGNAL(resized(int,int)), this, SLOT(resizing(int,int)));
    //QObject::connect(ui-, SIGNAL((int)), this, SLOT((int)));

    Reset();                // Reset values, vectors and update box/sliders values
            // at the end, because it may cause computation
}

OnePart::~OnePart(){
    delete ui;
}

void OnePart::ResetFromButton(){
    QMessageBox msgBox;
    msgBox.setText("This will reset all values.");
    msgBox.setInformativeText("Do you really want to erase current values and parameters ?");
    msgBox.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    int ret = msgBox.exec();
    switch (ret) {
       case QMessageBox::Ok:
            Reset();

            break;
       case QMessageBox::Cancel:
           break;
       default:
           // should never be reached
           break;
    }
}


void OnePart::Reset(){
    show_our_algo = false;  // keep it false until the end of the function ...
    ui->progressBarOverMainPlot->hide();

    ui->comboBoxAlgo->hide();
    ui->labelAlgo->hide();

    ui->DataCheckBox->setChecked(false);
    ui->checkBoxShowOnePair->setChecked(false);
    ui->checkBoxShowPair->setChecked(false);
    ui->checkBoxManualImpact->setChecked(false);
    ui->checkBoxConvertData->setChecked(false);
    ui->checkBoxShowPotentials->setChecked(false);

    dont_plot = false;

    Potential1.clear();
    Potential2.clear();
    Interaction11.clear();
    Interaction12.clear();
    Interaction22.clear();
    loaded_data.clear();
    local_density_p1.clear();
    local_density_p2.clear();
    local_occupancy_p1.clear();
    local_occupancy_p2.clear();
    local_dyades_p1.clear();
    local_dyades_p2.clear();
    converted_to_occupancy_data.clear();
    xpoints.clear();
    updateXPoints(100);

    viewing_window = 1000;
    type_particles = 1;
    Vex_1 = 2;
    Vex_2 = 2;
    Mu1 = 0.0;
    Mu2 = 0.0;
    range = 49;
    rangepoint = 1;
    position = 1;
    kT = 1.0;

    impact_wall = 10;

    Potential1.resize(100, 0.0);
    Potential2.resize(100, 0.0);
    Interaction11.resize(1, 0.0);
    Interaction12.resize(1, 0.0);
    Interaction22.resize(1, 0.0);
    loaded_data.resize(1,0.0);
    updateModel_data();
    updateModel();
    updateModel_interactions();

    revertedPair11.clear();
    revertedPair12.clear();
    revertedPair21.clear();
    revertedPair22.clear();

    updateScreen();
    show_our_algo = true;       // after updating the screen ...
}

void OnePart::updateScreen(){

    // Be aware the signals will be emitted each time, that's why show_our_algo is first put to false ...
    ui->BoxNbTypeParticules->setValue(type_particles);
    ui->BoxLength->setValue(viewing_window);
    ui->spinBoxVex1->setValue(Vex_1);
    ui->spinBoxVex2->setValue(Vex_2);
    ui->Mu1doubleSpinBox->setValue(Mu1);
    ui->Mu2doubleSpinBox->setValue(Mu2);
    ui->spinBoxPosition->setValue(position);
    ui->spinBoxRangePair->setValue(range);
    ui->spinBoxOnePair->setValue(rangepoint);
    ui->horizontalSliderOnePair->setValue(rangepoint);
    ui->spinBoxNumberImpact->setValue(impact_wall);
    ui->lcdNumberImpact->display(impact_wall);
    ui->progressBarOverMainPlot->hide();

}

void OnePart::Compute(){

    int Ltot = 0;                           // total number of positions where potential(s) is(are) defined

    // Window of computation (large view_window + impact_wall in both directions
    int start = 0;                      // computation window start
    int fin = 0;                        // computation window end
    int Lcompute = 1;                   // computation window length

    // Plotting window :
    // this->position                   // start of the plotting window
    // this->viewing_window             // length of the viewing window
    int posMax = 0;                     // maximum position for starting the plotting wndow of length viewing_window

    if(type_particles == 1){
        Ltot = max(1, (int) Potential1.size());
    } else if(type_particles == 2){
        Ltot = max(1, (int) min(Potential1.size(), Potential2.size()));
    }

    posMax = max(1, Ltot - viewing_window + 1);
    start = max(0, position - 1 - impact_wall);
    fin = min(Ltot - 1, position - 2 + viewing_window + impact_wall);
    Lcompute = fin - start + 1;

    if((start < 0)||(start >= (int) Potential1.size()))       cerr << "WRONG POSITION P1" << start << endl;
    if((fin  < 0) ||(fin   >= (int) Potential1.size()))       cerr << "WRONG POSITION P1 + window " << fin << endl;
    if(type_particles == 2){
        if((start < 0)|| (start >= (int) Potential2.size()))  cerr << "WRONG POSITION P2" << start << endl;
        if((fin  < 0) || (fin   >= (int) Potential2.size()))  cerr << "WRONG POSITION P2 + window "<< fin << endl;
    }

    ui->spinBoxPosition->setMaximum(posMax);
    ui->horizontalSliderMainPlot->setMaximum(posMax);
    // note that in spin box / slider, the values are positions and start at 1
    ui->spinBoxOnePair->setMaximum(fin+1);
    ui->spinBoxOnePair->setMinimum(start+1);
    ui->spinBoxOnePair->setValue(start+1);
    ui->horizontalSliderOnePair->setMinimum(start+1);
    ui->horizontalSliderOnePair->setMaximum(fin+1);
    ui->horizontalSliderOnePair->setValue(start+1);

    /* ----- Ask the grandcanonique algorithm and takes the density back ----- */
    if(show_our_algo){

        if(type_particles == 1){
            int portee = Interaction11.size() + Vex_1;                                  // Maximum interaction range (1 de trop mais pas grave)

            vector<valeur> localPotential1(fin - start + 1);                            //vector<double> localPotential1(&Potential1[start],&Potential1[fin]);
            for(int i = start; i <= fin; ++i)
                localPotential1[i-start] = (valeur) Potential1[i];
            vector<valeur> copie_interaction11(Interaction11.size() + Vex_1, 0.0);      // Interaction starts AFTER excluded volume
            if(ui->checkBoxWithInteraction->isChecked()){
                for(int i = Vex_1-1; i < (int) Interaction11.size() + Vex_1 - 1; ++i){
                    copie_interaction11[i] = Interaction11[i - Vex_1 + 1];
                }
            }


            FastGrandCanonique oneparticule(copie_interaction11, localPotential1, (valeur) Mu1, max(1, Lcompute - Vex_1 + 1), ui->checkBoxWithInteraction->isChecked() ? portee : 1, Vex_1, kT);
            oneparticule.Init();

            local_density_p1.resize(min(viewing_window,Lcompute));            
            local_density_p2.clear();

            for(int i = 1; i <= min(viewing_window,Lcompute); ++i){
                local_density_p1[i-1] = oneparticule.get_density(i + min(impact_wall, position-1));
            }
            //if(this->convertOurAlgoToOccupancy){ //compute in every case
                vector<double> large_local_density_p1(min(viewing_window + impact_wall,Lcompute), 0.0);
                for(int i = 1; i <= min(viewing_window + impact_wall,Lcompute); ++i)
                    large_local_density_p1[i-1] = oneparticule.get_density(i);
                convert_to_occupancy(large_local_density_p1, local_occupancy_p1, Vex_1, min(impact_wall, position-1));
                convert_to_dyades(large_local_density_p1, local_dyades_p1, ui->doubleSpinBoxDyade1->value());
                double mean_density = 0;
                int losz = local_occupancy_p1.size();
                for(int i = 0; i < losz; ++i){
                    mean_density += local_occupancy_p1[i];
                    ////////local_occupancy_p1[i] = log((local_occupancy_p1[i] == 0 ? 0.01 : local_occupancy_p1[i] )) / log(2.0);
                }
                mean_density /= local_occupancy_p1.size();
                ui->lcdNumberDensitySimul->display(mean_density);
                local_occupancy_p2.clear();
                local_dyades_p2.clear();
            //} else {
            //    local_occupancy_p1.clear();
            //    local_occupancy_p2.clear();
            //}

        } else if(type_particles == 2){
            int portee = max(Vex_1 + Interaction11.size(), max(Vex_1, Vex_2) + max(Interaction12.size(), Vex_2 + Interaction22.size()));            // the portee will be 1 more than necessary. Don't worry, zeros added if necessary

            Table Potentiels(2);
                #define localPotential1 Potentiels[0]
                #define localPotential2 Potentiels[1]
            localPotential1.resize(fin - start + 1);
            for(int i = start; i <= fin; ++i){
                localPotential1[i-start] = Potential1[i];
            }
            localPotential2.resize(fin - start + 1);
            for(int i = start; i <= fin; ++i){
                localPotential2[i-start] = Potential2[i];
            }
            vector<valeur> Mus(2, 0.0);
            Mus[0] = (valeur) Mu1; Mus[1] = (valeur) Mu2;
            vector<int> Volumes(2, 0);
            Volumes[0] = Vex_1; Volumes[1] = Vex_2;

            Table Interactions(4);
                #define I11 Interactions[0]
                #define I12 Interactions[1]
                #define I21 Interactions[2]
                #define I22 Interactions[3]

            I11.resize(portee, 0.0);
            I12.resize(portee, 0.0);
            I21.resize(portee, 0.0);
            I22.resize(portee, 0.0);
            if(ui->checkBoxWithInteraction->isChecked()){
                for(int i = Vex_1-1; i < Vex_1-1 + (int) Interaction11.size(); ++i)
                    I11[i] = Interaction11[i - Vex_1+1];
                for(int i = Vex_1-1; i < Vex_1-1 + (int) Interaction12.size(); ++i)
                    I12[i] = Interaction12[i - Vex_1+1];
                for(int i = Vex_2-1; i < Vex_2-1 + (int) Interaction12.size(); ++i)
                    I21[i] = Interaction12[i - Vex_2+1];
                for(int i = Vex_2-1; i < Vex_2-1 + (int) Interaction22.size(); ++i)
                    I22[i] = Interaction22[i - Vex_2+1];
            }

            // VRRAAAAOOOOOOUUUUUUMMMMMM
            GeneralFast twoparticules(Lcompute, 2, ui->checkBoxWithInteraction->isChecked() ? portee : 1, Interactions, Potentiels, Mus, Volumes);
            twoparticules.Init();

            // And gets densities
            local_density_p1.resize(min(viewing_window,Lcompute));
            for(int i = 1; i <= min(viewing_window,Lcompute) ; ++i){
                local_density_p1[i-1] = twoparticules.density(1, i + min(impact_wall, position-1));
            }
            local_density_p2.resize(min(viewing_window,Lcompute));
            for(int i = 1; i <= min(viewing_window,Lcompute); ++i){
                local_density_p2[i-1] = twoparticules.density(2,i + min(impact_wall, position-1));
            }

            //if(this->convertOurAlgoToOccupancy){
                vector<double> large_local_density_p1(min(viewing_window + impact_wall,Lcompute), 0.0);
                for(int i = 1; i <= min(viewing_window + impact_wall,Lcompute); ++i)
                    large_local_density_p1[i-1] = twoparticules.density(1,i);
                convert_to_occupancy( large_local_density_p1, local_occupancy_p1, Vex_1, min(impact_wall, position-1));
                convert_to_dyades(large_local_density_p1, local_dyades_p1, ui->doubleSpinBoxDyade1->value());
                vector<double> large_local_density_p2(min(viewing_window + impact_wall,Lcompute), 0.0);
                for(int i = 1; i <= min(viewing_window + impact_wall,Lcompute); ++i)
                    large_local_density_p2[i-1] = twoparticules.density(2,i);
                convert_to_occupancy(large_local_density_p2, local_occupancy_p2, Vex_2, min(impact_wall, position-1));
                convert_to_dyades(large_local_density_p2, local_dyades_p2, ui->doubleSpinBoxDyade2->value());

                double mean_density = 0;
                int losz = local_occupancy_p1.size();
                for(int i = 0; i < losz; ++i){
                    mean_density += local_occupancy_p1[i];
                }
                mean_density /= local_occupancy_p1.size();
                ui->lcdNumberDensitySimul->display(mean_density);
                mean_density = 0;
                int losz2 = local_occupancy_p2.size();
                for(int i = 0; i < losz2; ++i){
                    mean_density += local_occupancy_p2[i];
                }
                mean_density /= local_occupancy_p2.size();
                ui->lcdNumberDensitySimulP2->display(mean_density);


            /*} else {
                local_occupancy_p1.clear();
                local_occupancy_p2.clear();
            }*/

            if((this->show_all_pair_functions) || (show_pair_function)){
                cerr << "... computing pair function ..." << endl;
                cerr << "... computing pair function ..." << endl;
                PairData11.clear();
                twoparticules.compute_pair(max(1, range), 1, 1, &(PairData11));
                PairData12.clear();
                twoparticules.compute_pair(max(1, range), 1, 2, &(PairData12));
                PairData21.clear();
                twoparticules.compute_pair(max(1, range), 2, 1, &(PairData21));
                PairData22.clear();
                twoparticules.compute_pair(max(1, range), 2, 2, &(PairData22));
                spectrogrammes->update();
            }

        }

        if(loaded_data.size() > 0){
            double mean_density = 0;
            int lds = loaded_data.size();
            for(int i = 0; i < lds; ++i){
                mean_density += loaded_data[i];
            }
            mean_density /= loaded_data.size();
            ui->lcdNumberDensityData->display(mean_density);
        }

        if(type_particles == 1){
            ui->widgetPairs->hide();
        }

    }   // end if our algo (i.e. si on veut vraiment simuler)


    PlotResultsOnly();
}




void OnePart::PreComputeImpactWall(){
    if((manualimpact) || (!show_our_algo)){
        return;
    }
    impact_wall = 10;
    // estimation : 100 * volume  exculs should be enough. Compute pair function from a 0 potential, and with the same : Mus, Interactions,
    // Principe : simule avec E constant (pour les deux particules si 2), et regarde a  partir de quelle distance
    // les densités respectives se stabilisent sur leur moyenne respective. Attention, la portée peut être très longue.
    // ici, test sur min(20 fois volume exclus max, 10000). Si n'est pas stabilisé a  la fin, renvoie une erreur.
        if(type_particles == 1){
            int portee = Interaction11.size() + Vex_1;                                  // Maximum interaction range (1 de trop mais pas grave)
            int Ltest = min(20000, min(max(200, 40 * portee), (int) Potential1.size()));
            if(Ltest < 21) {
                impact_wall = 10;
                return ;
            }
            vector<valeur> fakePotential1(Ltest, (valeur) 0.0);                                 //vector<double> localPotential1(&Potential1[start],&Potential1[fin]);
            vector<valeur> copie_interaction11(Interaction11.size() + Vex_1, (valeur) 0.0);      // Interaction starts AFTER excluded volume

            for(int i = Vex_1-1; i < (int) Interaction11.size() + Vex_1 - 1; ++i)
                copie_interaction11[i] = (valeur) Interaction11[i - Vex_1 + 1];

            FastGrandCanonique oneparticule(copie_interaction11, fakePotential1, (valeur) Mu1, max(1, Ltest - Vex_1 + 1), Interaction11.size(), Vex_1, kT);
            oneparticule.Init();

            vector<double> fake_result(Ltest/2, 0.0);
            for(int i = 1; i <= Ltest / 2; ++i)
                fake_result[i-1] = oneparticule.get_density(i);

            double moyenne_stabilisee = 0;
            for(int i = Ltest/2 - 11; i < Ltest/2 - 1; ++i){
                moyenne_stabilisee += fake_result[i];
            }
            moyenne_stabilisee /= 10;
            int cpt = 0;
            int j = Ltest/2 - 1;
            while(j > 0 && (fabs((fake_result[j] - moyenne_stabilisee) / moyenne_stabilisee) < 0.01)){
                cpt++;
                j--;
            }
            if(cpt < 2*portee){
                impact_wall = min(10000, (int) Potential1.size() / 2);
                cerr << "WARNING ! (1 particle) stabilisation not reached because of walls in a range of " << Ltest/2 << " positions. take maximal value : " << impact_wall << endl;
            } else {
                impact_wall = max(1, Ltest/2 - cpt);
            }

        } else if(type_particles == 2){
            int portee = max(Vex_1 + Interaction11.size(), max(Vex_1, Vex_2) + max(Interaction12.size(), Vex_2 + Interaction22.size()));            // the portee will be 1 more than necessary. Don't worry, zeros added if necessary
            int Ltest = min(20000, min(max(200, 40 * portee), min((int) Potential1.size(), (int) Potential2.size())));
            Table Potentiels(2);
                #define localPotential1 Potentiels[0]
                #define localPotential2 Potentiels[1]
            localPotential1.resize(Ltest, 0.0);
            localPotential2.resize(Ltest, 0.0);
            vector<valeur> Mus(2, 0.0);
            Mus[0] = (valeur) Mu1; Mus[1] = (valeur) Mu2;
            vector<int> Volumes(2, 0);
            Volumes[0] = Vex_1-1; Volumes[1] = Vex_2-1;

            Table Interactions(4);
                #define I11 Interactions[0]
                #define I12 Interactions[1]
                #define I21 Interactions[2]
                #define I22 Interactions[3]

            I11.resize(portee, 0.0);
            I12.resize(portee, 0.0);
            I21.resize(portee, 0.0);
            I22.resize(portee, 0.0);
            for(int i = Vex_1-1; i < Vex_1-1 + (int) Interaction11.size(); ++i)
                 I11[i] = Interaction11[i - Vex_1+1];
            for(int i = Vex_1-1; i < Vex_1-1 + (int) Interaction12.size(); ++i)
                 I12[i] = Interaction12[i - Vex_1+1];
            for(int i = Vex_2-1; i < Vex_2-1 + (int) Interaction12.size(); ++i)
                 I21[i] = Interaction12[i - Vex_2+1];
            for(int i = Vex_2-1; i < Vex_2-1 + (int) Interaction22.size(); ++i)
                 I22[i] = Interaction22[i - Vex_2+1];

            // VRRAAAAOOOOOOUUUUUUMMMMMM
            GeneralFast twoparticules(Ltest, 2, portee, Interactions, Potentiels, Mus, Volumes);
            twoparticules.Init();

            // And gets densities
            local_density_p1.resize(Ltest / 2);
            for(int i = 1; i <= Ltest / 2 ; ++i){
                local_density_p1[i-1] = twoparticules.density(1, i + min(impact_wall, position-1));
            }
            local_density_p2.resize(Ltest / 2);
            for(int i = 1; i <= Ltest / 2; ++i){
                local_density_p2[i-1] = twoparticules.density(2,i + min(impact_wall, position-1));
            }
            vector<double> fake_result1(Ltest/2, 0.0);
            vector<double> fake_result2(Ltest/2, 0.0);

            for(int i = 1; i <= Ltest / 2; ++i){
                fake_result1[i-1] = twoparticules.density(1, i);
                fake_result2[i-1] = twoparticules.density(2, i);
            }
            double moyenne_stabilisee1 = 0;
            double moyenne_stabilisee2 = 0;
            for(int i = Ltest/2 - 11; i < Ltest/2 - 1; ++i){
                moyenne_stabilisee1 += fake_result1[i];
                moyenne_stabilisee2 += fake_result2[i];
            }
            moyenne_stabilisee1 /= 10;
            moyenne_stabilisee2 /= 10;
            int cpt1 = 0;
            int cpt2 = 0;
            int j = Ltest/2 - 1;
            while(j > 0 && (fabs((fake_result1[j] - moyenne_stabilisee1) / moyenne_stabilisee1) < 0.01)){
                cpt1++;
                j--;
            }
            j = Ltest/2 - 1;
            while(j > 0 && (fabs((fake_result2[j] - moyenne_stabilisee2) / moyenne_stabilisee2) < 0.01)){
                cpt2++;
                j--;
            }
            if(min(cpt1, cpt2) < 2*portee){
                impact_wall = min(10000, min((int) Potential1.size(), (int)Potential2.size()) / 2);
                cerr << "WARNING ! (1 particle) stabilisation not reached because of walls in a range of " << Ltest/2 << " positions. take maximal value : " << impact_wall << endl;
            } else {
                impact_wall = max(1, Ltest/2 - max(cpt1, cpt2));
            }
        }
       // end if our algo
       ui->lcdNumberImpact->display(impact_wall);

}

// xpoints is the list of abscisses values (i.e. positions to display). Here we make it start at 1 and be sufficiently long, so a subvector of xpoints can be used to draw.
void OnePart::updateXPoints(int new_size){
    if(new_size > (int) xpoints.size()){
        xpoints.resize(new_size);
        for(int i = 0; i < (int) new_size; ++i){
            xpoints[i] = i+1;
        }
    }
}




void OnePart::PlotResultsOnly(){

    if(manualimpact) {ui->spinBoxNumberImpact->setValue(this->impact_wall);}
    ui->spinBoxNumberImpact->setVisible(manualimpact);
    ui->lcdNumberImpact->setVisible(!manualimpact);
    ui->tableViewData->setVisible(show_data);
    ui->pushButtonLoadData->setVisible(show_data);
    ui->widgetPairs->setVisible(type_particles == 2);
    ui->spinBoxRangePair->setVisible(show_all_pair_functions);
    ui->groupBoxAllPairs->setVisible(show_all_pair_functions);
    ui->groupBoxOnePair->setVisible(show_pair_function);

    ui->widgetOurAlgo->setEnabled(ui->checkBoxWithInteraction->isChecked());

    if(!dont_plot){

    // ------------------------- The big main plot --------------------------

    int maxLplot = max(local_density_p1.size(), local_density_p2.size());
    updateXPoints(range + max(Vex_1, Vex_2) + 10 + max(Potential1.size(), Potential2.size()));




    if(show_our_algo){

        if(ui->checkBoxOccupancy->isChecked()){
            curveOccupancy1->setSamples(&xpoints[position - 1], &local_occupancy_p1[0], local_occupancy_p1.size());
            curveOccupancy2->setSamples(&xpoints[position - 1], &local_occupancy_p2[0], local_occupancy_p2.size());
        } else {
            curveOccupancy1->setSamples(&xpoints[position - 1], &local_occupancy_p1[0], 0);
            curveOccupancy2->setSamples(&xpoints[position - 1], &local_occupancy_p2[0], 0);
        }

        if(ui->checkBoxLeftDensities->isChecked()){
            curveDensity1->setSamples(&xpoints[position - 1], &local_density_p1[0], local_density_p1.size());
            curveDensity2->setSamples(&xpoints[position - 1], &local_density_p2[0], local_density_p2.size());
        } else {
            curveDensity1->setSamples(&xpoints[position - 1], &local_density_p1[0], 0);
            curveDensity2->setSamples(&xpoints[position - 1], &local_density_p2[0], 0);
        }
        if(ui->checkBoxDyades->isChecked()){
            curveDyade1->setSamples(&xpoints[position - 1], &local_dyades_p1[0], local_dyades_p1.size());
            curveDyade2->setSamples(&xpoints[position - 1], &local_dyades_p2[0], local_dyades_p2.size());
        } else {
            curveDyade1->setSamples(&xpoints[position - 1], &local_dyades_p1[0], 0);
            curveDyade2->setSamples(&xpoints[position - 1], &local_dyades_p2[0], 0);
        }
    } else {
        curveDensity1->setSamples(&xpoints[position - 1], &local_density_p1[0], 0);
        curveDensity2->setSamples(&xpoints[position - 1], &local_density_p2[0], 0);
        curveOccupancy1->setSamples(&xpoints[position - 1], &local_occupancy_p1[0], 0);
        curveOccupancy2->setSamples(&xpoints[position - 1], &local_occupancy_p2[0], 0);
        curveDyade1->setSamples(&xpoints[position - 1], &local_dyades_p1[0], 0);
        curveDyade2->setSamples(&xpoints[position - 1], &local_dyades_p2[0], 0);
    }

    curveDensity1->attach(ui->qwtPlotMainPlot);
    curveDensity2->attach(ui->qwtPlotMainPlot);
    curveOccupancy1->attach(ui->qwtPlotMainPlot);
    curveOccupancy2->attach(ui->qwtPlotMainPlot);
    curveDyade1->attach(ui->qwtPlotMainPlot);
    curveDyade2->attach(ui->qwtPlotMainPlot);

    if(show_data){
        convert_to_occupancy(loaded_data, converted_to_occupancy_data, Vex_1, position - 1);
        if(!convertDataToOccupancy){
            curveData->setSamples(&xpoints[position - 1], &loaded_data[position-1], min(viewing_window, max(0, (int) loaded_data.size() - position + 1)));
            curveOccupancyData->setSamples(&xpoints[position - 1], &converted_to_occupancy_data[position-1], 0);
        } else {
            curveOccupancyData->setSamples(&xpoints[position - 1], &converted_to_occupancy_data[position-1], min(viewing_window, max(0, (int) converted_to_occupancy_data.size() - position + 1)));
            curveData->setSamples(&xpoints[position - 1], &loaded_data[position-1], 0);
        }
    } else {
        curveOccupancyData->setSamples(&xpoints[position - 1], &converted_to_occupancy_data[position-1], 0);
        curveData->setSamples(&xpoints[position - 1], &loaded_data[position-1], 0);
    }
    curveData->attach(ui->qwtPlotMainPlot);
    curveOccupancyData->attach(ui->qwtPlotMainPlot);

    if(this->show_potential){
        curvePotential1->setSamples(&xpoints[position - 1], &Potential1[position-1], min(viewing_window, max(0, (int) Potential1.size() - position + 1)));
    } else {
        curvePotential1->setSamples(&xpoints[position - 1], &Potential1[position-1], 0);
    }
    if(this->show_potential && (type_particles > 1)){
        curvePotential2->setSamples(&xpoints[position - 1], &Potential2[position-1], min(viewing_window, max(0, (int) Potential2.size() - position + 1)));
    } else {
        curvePotential2->setSamples(&xpoints[position - 1], &Potential2[position-1], 0);
    }
    curvePotential1->attach(ui->qwtPlotMainPlot);
    curvePotential2->attach(ui->qwtPlotMainPlot);

    //ui->qwtPlotMainPlot->setAxisScale(0, 0, 1, 0.2);
    //ui->qwtPlotMainPlot->setAxisScale(1, 0, 1, 0.2);
    ui->qwtPlotMainPlot->setAxisScale(2, 1 + position - 1, maxLplot + position - 1, maxLplot / 10);
    ui->qwtPlotMainPlot->setAxisScale(3, 1 + position - 1, maxLplot + position - 1, maxLplot / 10);


    if(show_pair_function && (type_particles == 2)){
        int start = max(0, position - 1 - impact_wall);
        int virtualRangepoint = rangepoint-start;
        // the table PairData is in the wrong orientatin (PairData[delta][position])
        revertedPair11.resize(range,0.0);
        revertedPair12.resize(range,0.0);
        revertedPair21.resize(range,0.0);
        revertedPair22.resize(range,0.0);

        if((virtualRangepoint >= 1) && (virtualRangepoint <= (int) PairData11[0].size())){
            ui->qwtPlotOnePair->show();

            int rankFinish11 = range;
            int rankFinish12 = range;
            int rankFinish21 = range;
            int rankFinish22 = range;

            for(int i = 0; i < range; ++i){
                revertedPair11[i] = PairData11[i][virtualRangepoint-1];
                if((rankFinish11 == range) && (revertedPair11[i] < -0.5)) rankFinish11 = i;
                revertedPair12[i] = PairData12[i][virtualRangepoint-1];
                if((rankFinish12 == range) && (revertedPair12[i] < -0.5)) rankFinish12 = i;
                revertedPair21[i] = PairData21[i][virtualRangepoint-1];
                if((rankFinish21 == range) && (revertedPair21[i] < -0.5)) rankFinish21 = i;
                revertedPair22[i] = PairData22[i][virtualRangepoint-1];
                if((rankFinish22 == range) && (revertedPair22[i] < -0.5)) rankFinish22 = i;
            }
            revertedPair11.resize(rankFinish11);
            revertedPair12.resize(rankFinish12);
            revertedPair21.resize(rankFinish21);
            revertedPair22.resize(rankFinish22);

            curveOnePair11->setSamples(&xpoints[0], &revertedPair11[0], (int) revertedPair11.size());
            curveOnePair12->setSamples(&xpoints[0], &revertedPair12[0], (int) revertedPair12.size());
            curveOnePair21->setSamples(&xpoints[0], &revertedPair21[0], (int) revertedPair21.size());
            curveOnePair22->setSamples(&xpoints[0], &revertedPair22[0], (int) revertedPair22.size());
        } else {
            curveOnePair11->setSamples(&xpoints[0], &revertedPair11[0], 0);
            curveOnePair12->setSamples(&xpoints[0], &revertedPair12[0], 0);
            curveOnePair21->setSamples(&xpoints[0], &revertedPair21[0], 0);
            curveOnePair22->setSamples(&xpoints[0], &revertedPair22[0], 0);
            ui->qwtPlotOnePair->hide();
        }
    }

    curveOnePair11->attach(ui->qwtPlotOnePair);
    curveOnePair12->attach(ui->qwtPlotOnePair);
    curveOnePair21->attach(ui->qwtPlotOnePair);
    curveOnePair22->attach(ui->qwtPlotOnePair);
    ui->qwtPlotOnePair->replot();
    //ui->qwtPlotOnePair>setAxisScale(,,,);

    ui->qwtPlotMainPlot->replot();
    spectrogrammes->update();
    }
}


/* -------------- Signal Handling ------------------ */

// When data changed , updates the data displayed in the screen
void OnePart::updateModel(){
    int L1 = Potential1.size();
    int L2 = Potential2.size();
    int maxL = max(L1, L2);
    model->clear();
    model->insertColumns(0, maxL);
    model->insertRows(0,2);
    ui->TablePotentials->setModel(model);

    for (int column = 0; column < L1; ++column) {
         QModelIndex index = model->index(0, column, QModelIndex());
         model->setData(index, QVariant(Potential1[column]));
    }
    for (int column = 0; column < L2; ++column) {
         QModelIndex index = model->index(1, column, QModelIndex());
         model->setData(index, QVariant(Potential2[column]));
    }

    model->setHeaderData(0, Qt::Vertical, QVariant("E1[x]"));
    model->setHeaderData(1, Qt::Vertical, QVariant("E2[x]"));
    ui->TablePotentials->show();
}

// When data changed in the Interaction vectors, updates the display in the screen
void OnePart::updateModel_interactions(){
    int L1 = Interaction11.size();
    int L2 = Interaction12.size();
    int L3 = Interaction22.size();
    int maxL = max(L1, max(L2,L3));
    model_interactions->clear();
    model_interactions->insertColumns(0, maxL);
    model_interactions->insertRows(0,3);
    ui->TableInteractions->setModel(model_interactions);

    for (int column = 0; column < L1; ++column) {
         QModelIndex index = model_interactions->index(0, column, QModelIndex());
         model_interactions->setData(index, QVariant(Interaction11[column]));
    }
    for (int column = 0; column < L2; ++column) {
         QModelIndex index = model_interactions->index(1, column, QModelIndex());
         model_interactions->setData(index, QVariant(Interaction12[column]));
    }
    for (int column = 0; column < L3; ++column) {
         QModelIndex index = model_interactions->index(2, column, QModelIndex());
         model_interactions->setData(index, QVariant(Interaction22[column]));
    }

    model_interactions->setHeaderData(0, Qt::Vertical, QVariant("I 1-1[dist]"));
    model_interactions->setHeaderData(1, Qt::Vertical, QVariant("I 1-2[dist]"));
    model_interactions->setHeaderData(2, Qt::Vertical, QVariant("I 2-2[dist]"));
}

// When data changed in the data table, updates what's displayed
void OnePart::updateModel_data(){
    int L1 = loaded_data.size();
    model_data->clear();
    model_data->insertColumns(0, L1);
    model_data->insertRows(0,1);
    ui->tableViewData->setModel(model_data);

    for (int column = 0; column < L1; ++column) {
         QModelIndex index = model_data->index(0, column, QModelIndex());
         model_data->setData(index, QVariant(loaded_data[column]));
    }
    model_data->setHeaderData(0, Qt::Vertical, QVariant("Data[x]"));
}





void OnePart::nbParticlesChanged(int new_one){
    show_our_algo = false;
    if(new_one == 2){
        ui->checkBoxShowPair->setChecked(true);
        ui->checkBoxShowOnePair->setChecked(true);
    }
    type_particles = new_one;
    show_our_algo = true;
    PreComputeImpactWall();
    Compute();
}
void OnePart::viewingWindowChanged(int new_one){
    viewing_window = new_one;
    Compute();
}
void OnePart::maximizeWindowSize(){
    int L1 = Potential1.size();
    int L2 = Potential2.size();
    int maxL = max(L1, L2);
    viewing_window = maxL;
    ui->BoxLength->setValue(viewing_window);
    Compute();
}
void OnePart::Vex1Changed(int new_one){
    Vex_1 = new_one;
    PreComputeImpactWall();
    Compute();
}
void OnePart::Vex2Changed(int new_one){
    Vex_2 = new_one;
    if(type_particles > 1){
        PreComputeImpactWall();
        Compute();
    }
}
void OnePart::Mu1ChangedFromSlider(int new_value){
    Mu1 = (double) new_value / 10.0;
    ui->Mu1doubleSpinBox->setValue(Mu1);
    PreComputeImpactWall();
    Compute();
}
void OnePart::Mu2ChangedFromSlider(int new_value){
    Mu2 = (double) new_value / 10.0;
    ui->Mu2doubleSpinBox->setValue(Mu2);
    if(type_particles > 1) {
        PreComputeImpactWall();
        Compute();
    }
}
void OnePart::Mu1ChangedFromBox(double new_one){
    Mu1 = new_one;
    ui->Mu1horizontalSlider->setValue((int) (Mu1 * 10));
    PreComputeImpactWall();
    Compute();
}
void OnePart::Mu2ChangedFromBox(double new_one){
    Mu2 = new_one;
    ui->Mu2horizontalSlider->setValue((int) (Mu2 * 10));
    if(type_particles > 1){
        PreComputeImpactWall();
        Compute();
    }
}
void OnePart::positionChangedFromSlider(int new_one){
    position = new_one;
    ui->spinBoxPosition->setValue(position);
    Compute();
}
void OnePart::positionChangedFromBox(int new_one){
    position = new_one;
    ui->horizontalSliderMainPlot->setValue(position);
    Compute();
}

void OnePart::kTChangedFromBox(double new_one){
    kT = new_one;
    PreComputeImpactWall();
    Compute();
}

// useless now. Can be used to move potential and data table simultaneously for instance
void OnePart::DisplacementInTable(int new_position){
    cerr << "DisplacementInTable : " << new_position << endl;
}

// when the user modified values in the table, the values are updated in the vectors in memory.
void OnePart::PotentialChangedFromTable(QModelIndex Q1, QModelIndex Q2){
    //cerr << "Potentials modified, from : (" << Q1.column() << "," << Q1.row() << ") to (" << Q2.column() << "," << Q2.row() << ")" <<  endl;
    int L1 = Potential1.size();
    int L2 = Potential2.size();
    bool problem = false;
    for(int row = Q1.row(); row <= Q2.row(); ++row){
        for(int column = Q1.column(); column <= Q2.column(); ++column){
            if((row == 0) && (column < L1)) Potential1[column] = model->data(model->index(row, column, QModelIndex())).toDouble();
            if((row == 0) && (column >= L1)) problem = true;
            if((row == 1) && (column < L2)) Potential2[column] = model->data(model->index(row, column, QModelIndex())).toDouble();
            if((row == 1) && (column >= L2)) problem = true;
        }
    }
    if(problem) updateModel();
    //PreComputeImpactWall(); the impact wall is computed with a 0 constant potential, so no impact of changing the potential to this evaluation of the impact
    Compute();
}

void OnePart::DataChangedFromTable(QModelIndex Q1, QModelIndex Q2){
    //cerr << "Data modified, from : (" << Q1.column() << "," << Q1.row() << ") to (" << Q2.column() << "," << Q2.row() << ")" <<  endl;
    int L = loaded_data.size();
    bool problem = false;
    for(int row = Q1.row(); row <= Q2.row(); ++row){
        for(int column = Q1.column(); column <= Q2.column(); ++column){
            if((row == 0) && (column < L)) loaded_data[column] = model_data->data(model_data->index(row, column, QModelIndex())).toDouble();
            if((row == 0) && (column >= L)) problem = true;
        }
    }
    if(problem) updateModel_data();
    PlotResultsOnly();
}

void OnePart::InteractionsChangedFromTable(QModelIndex Q1, QModelIndex Q2){
    //cerr << "Interactions modified, from : (" << Q1.column() << "," << Q1.row() << ") to (" << Q2.column() << "," << Q2.row() << ")" <<  endl;
    int L1 = Interaction11.size();
    int L2 = Interaction12.size();
    int L3 = Interaction22.size();
    bool problem = false;
    for(int row = Q1.row(); row <= Q2.row(); ++row){
        for(int column = Q1.column(); column <= Q2.column(); ++column){
            if((row == 0)  && (column < L1)) Interaction11[column] = model_interactions->data(model_interactions->index(row, column, QModelIndex())).toDouble();
            if((row == 0) && (column >= L1)) problem = true;
            if((row == 1)  && (column < L2)) Interaction12[column] = model_interactions->data(model_interactions->index(row, column, QModelIndex())).toDouble();
            if((row == 1) && (column >= L2)) problem = true;
            if((row == 2)  && (column < L3)) Interaction22[column] = model_interactions->data(model_interactions->index(row, column, QModelIndex())).toDouble();
            if((row == 2) && (column >= L3)) problem = true;
        }
    }
    if(problem) updateModel_interactions();
    PreComputeImpactWall();
    Compute();
}




void OnePart::upCheckPairFunction(int i){
    if(i > 0){
        //show_all_pair_functions = true;

        Compute();
    } else {
        //show_all_pair_functions = false;

        PlotResultsOnly(); // the pair option is removed, so need to compute anything
    }
}

void OnePart::upCheckOnePoint(int i){
    if(i > 0){
        //show_pair_function = true;

        Compute();
    } else {
        //show_pair_function = false;

        PlotResultsOnly();  // the pair option is removed, so need to compute anything
    }
}

void OnePart::upCheckShowPotential(int i){
    if(i > 0){
        // show_potential = true;
        //  // nothing to show/hide
        Compute();
    } else {
        // show_potential = false;
        //
        PlotResultsOnly();
    }
}

void OnePart::upCheckConvertData(int i){
    if(i > 0){
        //convertDataToOccupancy = true;
        // nothing to hide/show
        Compute();
    } else {
        //this->convertDataToOccupancy = false;
        // nothing to hide/show
        PlotResultsOnly();
    }
}

void OnePart::upCheckConvertOurAlgo(int i){
    if(i > 0){
        //this->convertOurAlgoToOccupancy = true;
        // nothing to hide/show
        Compute();
    } else {
        //this->convertOurAlgoToOccupancy = false;
        // nothing to hide/show
        PlotResultsOnly();
    }
}

void OnePart::upCheckManualImpact(int i){
    if(i > 0){
        //this->manualimpact = true;

        Compute();
    } else {
        //this->manualimpact = false;
        PreComputeImpactWall();

        Compute();
        //PlotResultsOnly();
    }
}





void OnePart::upCheckData(int i){
    if(i > 0){  //new state = checked
        //show_data = true;
        int save_show_our_algo = show_our_algo;
        dont_plot = true;
        show_our_algo = false;  // to not compute every time a value is changed in the table ...

        AskPotential choseData(this->loaded_data, 100, this);
        choseData.exec();
        updateModel_data();

        show_our_algo = save_show_our_algo;
        dont_plot = false;

        PlotResultsOnly();

    } else {    // unchecked
        //show_data = false;

        PlotResultsOnly();
    }
}

void OnePart::LoadPotential1(){
    int save_show_our_algo = show_our_algo;
    dont_plot = true;
    show_our_algo = false;  // to not compute every time a value is changed in the table ...
    AskPotential chosePotential(this->Potential1, 100, this, true);
    chosePotential.exec();
    updateModel();
    show_our_algo = save_show_our_algo;
    dont_plot = false;
    PreComputeImpactWall();
    Compute();
}
void OnePart::LoadPotential2(){
    int save_show_our_algo = show_our_algo;
    dont_plot = true;
    show_our_algo = false;  // to not compute every time a value is changed in the table ...
    AskPotential chosePotential(this->Potential2, 100, this, true);
    chosePotential.exec();
    updateModel();
    show_our_algo = save_show_our_algo;
    dont_plot = false;
    PreComputeImpactWall();
    Compute();
}

void OnePart::LoadInteraction11(){
    int save_show_our_algo = show_our_algo;
    dont_plot = true;
    show_our_algo = false;  // to not compute every time a value is changed in the table ...
    AskPotential choseInteraction(this->Interaction11, 10, this);
    choseInteraction.exec();
    updateModel_interactions();
    show_our_algo = save_show_our_algo;
    dont_plot = false;
    PreComputeImpactWall();
    Compute();
}
void OnePart::LoadInteraction12(){
    int save_show_our_algo = show_our_algo;
    dont_plot = true;
    show_our_algo = false;  // to not compute every time a value is changed in the table ...
    AskPotential choseInteraction(this->Interaction12, 10, this);
    choseInteraction.exec();
    updateModel_interactions();
    show_our_algo = save_show_our_algo;
    dont_plot = false;
    PreComputeImpactWall();
    Compute();
}
void OnePart::LoadInteraction22(){
    int save_show_our_algo = show_our_algo;
    dont_plot = true;
    show_our_algo = false;  // to not compute every time a value is changed in the table ...
    AskPotential choseInteraction(this->Interaction22, 10, this);
    choseInteraction.exec();
    updateModel_interactions();
    show_our_algo = save_show_our_algo;
    dont_plot = false;
    PreComputeImpactWall();
    Compute();
}
void OnePart::rangeChanged(int new_one){
    range = new_one;
    Compute();      // the box to display the range is supposed to be shown only if pairs are displayed so we need to compute in all cases
}
void OnePart::ImpactChangedFromBox(int new_one){
    impact_wall = new_one;
    ui->lcdNumberImpact->display(new_one);
    Compute();
}
void OnePart::pairPointChangedFromSlide(int new_one){
    ui->spinBoxOnePair->setValue(new_one);
    rangepoint=new_one;
    PlotResultsOnly();
}
void OnePart::pairPointChangedFromBox(int new_one){
    ui->horizontalSliderOnePair->setValue(new_one);
    rangepoint=new_one;
    PlotResultsOnly();
}






void OnePart::FullScreenMainPlot(){
    int maxL = ((type_particles == 1) ? Potential1.size() : min(Potential1.size(), Potential2.size()));
    QString chooseDirectory = QFileDialog::getExistingDirectory(this, tr("Please choose a working directory"), currentUserDirectory.toStdString().c_str(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(chooseDirectory.size() > 1) {
        currentUserDirectory = chooseDirectory;
    }
    PlotMainScreen z(maxL, 1, chooseDirectory);

    z.updateDATA(0, loaded_data);
    z.updateDATAocc(0, converted_to_occupancy_data);
    z.updateP1(0, Potential1);
    z.updateP2(0, Potential2);

    //int saveVV = this->viewing_window;
    //int savePos = this->position;
    PreComputeImpactWall();

    ui->progressBarOverMainPlot->show();
    ui->progressBarOverMainPlot->setValue(0);

    for(int i = 0; i < maxL; i = i + 10000){
        ui->progressBarOverMainPlot->setValue((int) 100 * i / maxL);
        cerr << "part " << i << " ... " << i + 10000 << endl;
        position = i+1;
        viewing_window = 10010; // it could be 10000. it may be a way to check that the 10 values that overlap each time are not nonsense
        Compute();
        z.updateGP1(i, this->local_density_p1);
        z.updateGP2(i, this->local_density_p2);
        z.updateGP1occ(i, this->local_occupancy_p1);
        z.updateGP2occ(i, this->local_occupancy_p2);
        //z.updateVDL(i, this->local_density_VDL);
        //z.updateVDLocc(i, this->local_occupancy_VDL);
    }

    z.doScript();
    ui->progressBarOverMainPlot->hide();
    z.exec();
}


void OnePart::SaveMainPlot(){
    // never done ...
}
void OnePart::MainPlotToR(){
    // never done ...
}


void OnePart::CreateSumFile(){
    // Will write a .txt file for each parameter table / result in a folder to choose
    QString chooseDirectory = QFileDialog::getExistingDirectory(this, tr("Please choose a directory for saving files"), currentUserDirectory.toStdString().c_str(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(chooseDirectory.size() > 1) {
        currentUserDirectory = chooseDirectory;
    }

    // 1 - List of parameters
    QStringList files;
    files << "densities.txt" << "Pots.txt" << "Inter11.txt" << "Inter12.txt" << "Inter22.txt" << "Pair11.txt" << "Pair12.txt" << "Pair21.txt" << "Pair22.txt" << "PairOnePoint.txt" ;

    chooseDirectory.append("/");
    for (int i = 0; i < files.size(); ++i){
        QString location1(chooseDirectory);
        location1.append(files[i]);
        ofstream fichier(location1.toStdString().c_str(), ios::out);
        if(fichier){
            switch(i) {
                case 0 : {
                    int L1 = local_density_p1.size();
                    if(type_particles > 1) L1 = min(L1, (int) local_density_p2.size());
                    fichier << "Position\tPotential1\tPotential2\tData\tDataToOccopancy\tGraphP1\tGraphP2\tGP1occupancy\tGP2occupancy\tVDL\tVDLOccupancy" << endl;
                     /*bool show_VanDerLick;
                    bool keep_same_values_for_VDL;
                    bool show_our_algo;
                    bool show_pair_function;
                    bool show_all_pair_functions;
                    */

                    int Ldata = loaded_data.size();
                    int Loccdata = converted_to_occupancy_data.size();
                    //int LVDL = local_density_VDL.size();
                    //int LVDL2 =  local_occupancy_VDL.size();

                    for(int i = 0; i < L1; ++i){
                        fichier << i + 1 << "\t";
                        fichier << setprecision (10) << Potential1[i] << "\t";
                        if(type_particles > 1) fichier << setprecision (10) << Potential2[i] << "\t";                                                       else fichier << "\t";
                        if(show_data && (i < Ldata)) fichier << setprecision (10) << loaded_data[i] << "\t";                                                else fichier << "\t";
                        if(show_data && convertDataToOccupancy && (i < Loccdata))fichier << setprecision (10) << converted_to_occupancy_data[i] << "\t";    else fichier << "\t";
                        fichier << setprecision (10) << local_density_p1[i] << "\t";
                        if (type_particles > 1) fichier << setprecision (10) << local_density_p2[i] << "\t";                                                else fichier << "\t";
                        if(convertOurAlgoToOccupancy) fichier << setprecision (10) << local_occupancy_p1[i] << "\t";                                        else fichier << "\t";
                        if((type_particles > 1) && convertOurAlgoToOccupancy) fichier << setprecision (10) << local_occupancy_p2[i] << "\t";                else fichier << "\t";
                        fichier << endl;
                    }
                    break;
                }
                case 1 : {
                    int L1 = Potential1.size();
                    int L2 = Potential2.size();
                    int L3 = min(L1, L2);

                    for(int i = 0; i < L3; ++i){
                        fichier << setprecision (10) << Potential1[i] << "\t";
                    }
                    if(type_particles == 2){
                        fichier << "\n";
                        for(int i = 0; i < L3; ++i){
                            fichier << setprecision (10) << Potential1[i] << "\t";
                        }
                    }
                    break;
                }
                case 2 : {
                    int L1 = Interaction11.size();
                    for(int i = 0; i < L1; ++i){
                        fichier << setprecision (10) << Interaction11[i] << "\t";
                    }
                    break;
                }
                case 3 : {
                    int L1 = Interaction12.size();
                    for(int i = 0; i < L1; ++i){
                        fichier << setprecision (10) << Interaction12[i] << "\t";
                    }
                    break;
                }
                case 4 : {
                    int L1 = Interaction22.size();
                    for(int i = 0; i < L1; ++i){
                        fichier << setprecision (10) << Interaction22[i] << "\t";
                    }
                    break;
                }
                case 5 : {
                    int L1 = PairData11.size();
                    if(L1 > 0){
                        int L2 = PairData11[0].size();
                        for(int i = 0; i < L1; ++i){
                            for(int j = 0; j < L2; ++j){
                                fichier << setprecision (10) << PairData11[i][j] << "\t";
                            }
                            fichier << endl;
                        }
                    }
                    break;
                }
                case 6 : {
                    int L1 = PairData12.size();
                    if(L1 > 0){
                        int L2 = PairData12[0].size();
                        for(int i = 0; i < L1; ++i){
                            for(int j = 0; j < L2; ++j){
                                fichier << setprecision (10) << PairData12[i][j] << "\t";
                            }
                            fichier << endl;
                        }
                    }
                    break;
                }
                case 7 : {
                    int L1 = PairData21.size();
                    if(L1 > 0){
                        int L2 = PairData21[0].size();
                        for(int i = 0; i < L1; ++i){
                            for(int j = 0; j < L2; ++j){
                                fichier << setprecision (10) << PairData21[i][j] << "\t";
                            }
                            fichier << endl;
                        }
                    }
                    break;
                }
                case 8 : {
                    int L1 = PairData22.size();
                    if(L1 > 0){
                        int L2 = PairData22[0].size();
                        for(int i = 0; i < L1; ++i){
                            for(int j = 0; j < L2; ++j){
                                fichier << setprecision (10) << PairData22[i][j] << "\t";
                            }
                            fichier << endl;
                        }
                    }
                    break;
                }
                case 9 : {
                    int L1 = revertedPair11.size();
                    int L2 = revertedPair12.size();
                    int L3 = revertedPair21.size();
                    int L4 = revertedPair22.size();
                    int Lmax = max(max(L1,L2),max(L3,L4));
                    fichier << "AA\tAB\tBA\tBB\n";
                    for(int i = 0; i < Lmax; ++i){
                        if((i>=Vex_1-1) && (i < L1)) fichier << setprecision (10) <<revertedPair11[i] << "\t";
                        else fichier << "NaN\t";
                        if((i>=Vex_1-1) && (i < L2))  fichier << setprecision (10) <<revertedPair12[i] << "\t";
                        else fichier << "NaN\t";
                        if((i>=Vex_2-1) && (i < L3)) fichier << setprecision (10) <<revertedPair21[i] << "\t";
                        else fichier << "NaN\t";
                        if((i>=Vex_2-1) && (i < L4)) fichier << setprecision (10) <<revertedPair22[i];
                        else fichier << "NaN";
                        fichier << endl;
                    }
                    break;
                }
             }
            fichier.close();
        } else cerr << "ERR cannot open(" << location1.toStdString() << ")" << endl;
    }

    QString location2(chooseDirectory);
    location2.append("index.html");
    ofstream fichier(location2.toStdString().c_str(), ios::out);
    if(fichier){

        fichier << "<h2> Sum up of experiment with Nucleozone </h2>" << endl;

        QDate date(QDate::currentDate());
        fichier << "Done : " << (date.toString().toStdString()) << ", ";
        QTime time = QTime::currentTime();
        fichier << time.toString().toStdString() << ". <br>" << endl;

        fichier << "<h3> Parameters : </h3>" << endl;
        fichier << "Types of particles :" << type_particles << "<br>\n";
        fichier << "Non-specific affinity of particles A (mu1) : " << Mu1 <<  "<br>\n";
        fichier << "Volume of particles A (Vex1) : " << Vex_1 << "<br>\n";
        if(type_particles> 1){
        fichier << "Non-specific affinity of particles B (mu2) : " << Mu2 << "<br>\n";
        fichier << "Volume of particles B (Vex2) : " << Vex_2 << "<br>\n";
        }
        fichier << "Temperature (kT) ; " << kT << "<br>\n";
        fichier << "The impact of borders is estimated at a distance of " << impact_wall << " each side before it vanishes" <<  "<br>\n";

        fichier << "Potentials defined along with the positions : <br>\n";
        //fichier << "<img src=\"figures/pot1.gif\" alt=\"Potentials particles A\" height=\"42\" width=\"42\"> <br>\n";
        //fichier << "<img src=\"figures/pot2.gif\" alt=\"Potentials particles B\" height=\"42\" width=\"42\"> <br>\n";
        fichier << "The considered window is : " << position << " to " << position + viewing_window -1 << " ( length = " << viewing_window << ")<br>\n";
        //if(show_VanDerLick) fichier << "For comparison, the Vanderlick algorithm is also simulated for one particle only, with parameters : Mu1=" << VDLMu1 << ", Mu2=" << VDLMu2 << ", Vex1=" << VDLVex_1 << ", Vex2=" << VDLVex_1 << "<br>\n";

        fichier << "<h3> Results : </h3>" << endl;

        fichier << "<a href=\"file://localhost/" << (chooseDirectory.toStdString()) << "densities.txt\"> Table of predicted densities </a> <br>\n";

        if((type_particles > 1) && show_all_pair_functions){
        fichier << "Pair function between both kind of particles, computed from distance 1 to " << range << "<br>\n";
        fichier << "  <a href=\"file://localhost/" << (chooseDirectory.toStdString()) << "Pair11.txt\"> Table of pair densities A-A </a> <br> \n";
        fichier << "  <a href=\"file://localhost/" << (chooseDirectory.toStdString()) << "Pair12.txt\"> Table of pair densities A-B </a> <br> \n";
        fichier << "  <a href=\"file://localhost/" << (chooseDirectory.toStdString()) << "Pair21.txt\"> Table of pair densities B-A </a> <br> \n";
        fichier << "  <a href=\"file://localhost/" << (chooseDirectory.toStdString()) << "Pair22.txt\"> Table of pair densities B-B </a> <br>\n";
        }
        if((type_particles > 1) && show_pair_function){
        fichier << "Detail of pair function taken with a particle at position " << rangepoint << ": <a href=\"file://localhost/" << (chooseDirectory.toStdString()) << "PairOnePoint.txt\"> Table of pair densities at position" << rangepoint << "</a> <br>\n";
        }
        /*int L = local_density_p1.size();
        if(type_particles > 1) L = min(L, (int) local_density_p2.size());
        for(int i = 0; i <= L; ++i){
            fichier << i+1 << "\t";
            fichier << setprecision (10) << local_occupancy_p1[i];
            if(type_particles > 1) fichier << "\t" << setprecision (10) << local_occupancy_p2[i];
            fichier << endl;
        }*/
        fichier.close();
    } else cerr << "ERR before saving window(" << location2.toStdString() << ") : Impossible d'ouvrir le fichier " << endl;

    viewsum VS(location2);
    VS.exec();
    cerr << "Done." << endl;
}

void OnePart::SaveOnePointPairFunction(){
    // never done ...
}
void OnePart::SaveAllPointsPairFunctions(){
    // never done ...
}



void OnePart::scanParameters(){
    vector<double> Recup(8,0.0);
    ScanParameters Ask(type_particles, Recup);
    Ask.exec();

    int maxL = ((type_particles == 1) ? Potential1.size() : min(Potential1.size(), Potential2.size()));
    QString chooseDirectory = QFileDialog::getExistingDirectory(this, tr("Please choose a working directory"), currentUserDirectory.toStdString().c_str(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(chooseDirectory.size() > 1) {
        currentUserDirectory = chooseDirectory;
    } else return;


    QString param = QString("");
    switch ((int) Recup[0]){
        case 0: {
            param = QString("ERR : No Parameter");
            return;
            break;
        }
        case 1: {
            param = QString("Mu1");
            break;
        }
        case 2: {
            param = QString("Vex1");
            break;
        }
        case 3: {
            param = QString("kT");
            break;
        }
        case 4: {
            param = QString("I11");
            break;
        }
        case 5: {
            param = QString("Mu2");
            break;
        }
        case 6: {
            param = QString("Vex2");
            break;
        }
        case 7: {
            param = QString("I12");
            break;
        }
        case 8: {
            param = QString("I21");
            break;
        }
        case 9: {
            param = QString("I22");
            break;
        }
    }

    QString location(chooseDirectory);
    location.append("/Sim");
    location.append(param);

    if(Recup[4] == 0.0){
        cerr << "Scanning One parameter only (" << Recup[1] << "->" << Recup[2] << ") pitch=" << Recup[3] << endl;
        vector< vector<double>* > stockP1;
        vector< vector<double>* > stockP2;
        vector< vector<double>* > stockOccP1;
        vector< vector<double>* > stockOccP2;

        ui->progressBarOverMainPlot->show();
        ui->progressBarOverMainPlot->setValue(0);
        double oneStep = (100.0 / (Recup[2] - Recup[1]));
        for(double v = Recup[1]; v <= Recup[2]; v += Recup[3]){
            ui->progressBarOverMainPlot->setValue((int) (v - Recup[1]) * oneStep);
            switch ((int) Recup[0]){
                case 0: {break;}
                case 1: {Mu1 = v;   break;}
                case 2: {Vex_1 = v; break;}
                case 3: {kT=v;      break;}
                case 4: {           break;} //I11
                case 5: {Mu2=v;     break;}
                case 6: {Vex_2=v;   break;}
                case 7: {           break;} //I12
                case 8: {           break;} //I21
                case 9: {           break;} //I22
            }

            cerr << "value :" << v << endl;
            PlotMainScreen z(maxL, 1, chooseDirectory);
            z.updateDATA(0, loaded_data);
            z.updateDATAocc(0, converted_to_occupancy_data);
            z.updateP1(0, Potential1);
            z.updateP2(0, Potential2);
            //int saveVV = this->viewing_window;
            //int savePos = this->position;
            PreComputeImpactWall();
                for(int i = 0; i < maxL; i = i + 10000){
                position = i+1;
                viewing_window = 10010; // it could be 10000. it may be a way to check that the 10 values that overlap each time are not nonsense
                Compute();
                z.updateGP1(i, this->local_density_p1);
                z.updateGP2(i, this->local_density_p2);
                z.updateGP1occ(i, this->local_occupancy_p1);
                z.updateGP2occ(i, this->local_occupancy_p2);
                //z.updateVDL(i, this->local_density_VDL);
                //z.updateVDLocc(i, this->local_occupancy_VDL);
            }
            QString loctemp(location);
            loctemp.append("=");
            std::ostringstream sstream;
            sstream << v << ".txt";                   // subterfuge pour transforler un double en string
            loctemp.append(QString(sstream.str().c_str()));

            z.writeToFile(loctemp);
            stockP1.push_back(new vector<double>(z.density_p1));
            stockP2.push_back(new vector<double>(z.density_p2));
            stockOccP1.push_back(new vector<double>(z.occupancy_p1));
            stockOccP2.push_back(new vector<double>(z.occupancy_p2));
        }
        QString locationP1(location);
        locationP1.append("AllP1.txt");
        cerr << "location :" << locationP1.toStdString() << endl;
        ofstream fichier3(locationP1.toStdString().c_str(), ios::out);
        if(fichier3){
            for(double v = Recup[1]; v <= Recup[2]; v += Recup[3]){
                 fichier3 << param.toStdString() << "=" << v << "\t";
            }
            fichier3 << endl;
            for(int j = 0; j < maxL; ++j){
                int pt = 0;
                for(double v = Recup[1]; v <= Recup[2]; v += Recup[3]){
                    fichier3 << setprecision(10) << (*stockP1[pt])[j] << "\t";
                    ++pt;
                }
                fichier3 << endl;
            }
            fichier3.close();
        } else cerr << "ERR opening(" << locationP1.toStdString() << ") : Impossible d'ouvrir le fichier " << endl;

        QString locationOccP1(location);
        locationOccP1.append("AllOccP1.txt");
        cerr << "location :" << locationOccP1.toStdString() << endl;
        ofstream fichier4(locationOccP1.toStdString().c_str(), ios::out);
        if(fichier4){
            for(double v = Recup[1]; v <= Recup[2]; v += Recup[3]){
                 fichier4 << param.toStdString() << "=" << v << "\t";
            }
            fichier4 << endl;
            for(int j = 0; j < maxL; ++j){
                int pt = 0;
                for(double v = Recup[1]; v <= Recup[2]; v += Recup[3]){
                    fichier4 << setprecision(10) << (*stockOccP1[pt])[j] << "\t";
                    ++pt;
                }
                fichier4 << endl;
            }
            fichier4.close();
        } else cerr << "ERR opening(" << locationOccP1.toStdString() << ") : Impossible d'ouvrir le fichier " << endl;

        if(type_particles > 1){
            QString locationP2(location);
            locationP1.append("AllP2.txt");
            cerr << "location :" << locationP2.toStdString() << endl;
            ofstream fichier5(locationP2.toStdString().c_str(), ios::out);
            if(fichier5){
                for(double v = Recup[1]; v <= Recup[2]; v += Recup[3]){
                     fichier5 << param.toStdString() << "=" << v << "\t";
                }
                fichier5 << endl;
                for(int j = 0; j < maxL; ++j){
                    int pt = 0;
                    for(double v = Recup[1]; v <= Recup[2]; v += Recup[3]){
                        fichier5 << setprecision(10) << (*stockP2[pt])[j] << "\t";
                        ++pt;
                    }
                    fichier5 << endl;
                }
                fichier5.close();
            } else cerr << "ERR opening(" << locationP2.toStdString() << ") : Impossible d'ouvrir le fichier " << endl;

            QString locationOccP2(location);
            locationOccP2.append("AllOccP1.txt");
            cerr << "location :" << locationOccP2.toStdString() << endl;
            ofstream fichier6(locationOccP2.toStdString().c_str(), ios::out);
            if(fichier6){
                for(double v = Recup[1]; v <= Recup[2]; v += Recup[3]){
                     fichier6 << param.toStdString() << "=" << v << "\t";
                }
                fichier6 << endl;
                for(int j = 0; j < maxL; ++j){
                    int pt = 0;
                    for(double v = Recup[1]; v <= Recup[2]; v += Recup[3]){
                        fichier6 << setprecision(10) << (*stockOccP2[pt])[j] << "\t";
                        ++pt;
                    }
                    fichier6 << endl;
                }
                fichier6.close();
            } else cerr << "ERR opening(" << locationOccP2.toStdString() << ") : Impossible d'ouvrir le fichier " << endl;

        }




        ui->progressBarOverMainPlot->hide();
    }


}



// not used yet, a way to accumulate messages or log file ...

Message::Message(){
    listOperations.clear();
}
void Message::append(QString z){
    listOperations.append(z);
    listOperations.append("\n");
}
void Message::comment(QString z){
    listOperations.append("/*   ");
    listOperations.append(z);
//    listOperations.append("\n");
    listOperations.append("   */\n");
}
void Message::clear(){
    listOperations.clear();
}
QString Message::content(){
    return listOperations;
}

