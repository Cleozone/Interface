#include "scanparameters.h"
#include "ui_scanparameters.h"
#include <iostream>

ScanParameters::ScanParameters(int _nb_part, vector<double> &_chosenParameters, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ScanParameters), nb_part(_nb_part), chosenParameters(&_chosenParameters)
{
    ui->setupUi(this);

    ui->scanComboBox->addItem(QString("Choose"));
    ui->scanComboBox->addItem(QString("Mu1"));
    ui->scanComboBox->addItem(QString("Vex1"));
    ui->scanComboBox->addItem(QString("kT"));
    ui->scanComboBox->addItem(QString("I11 Coeff"));
    if(nb_part > 1){
        ui->scanComboBox->addItem(QString("Mu2"));
        ui->scanComboBox->addItem(QString("Vex2"));
        ui->scanComboBox->addItem(QString("I12 Coeff"));
        ui->scanComboBox->addItem(QString("I21 Coeff"));
        ui->scanComboBox->addItem(QString("I22 Coeff"));
    }

    ui->scanComboBox2->addItem(QString("Only One"));
    ui->scanComboBox2->addItem(QString("Mu1"));
    ui->scanComboBox2->addItem(QString("Vex1"));
    ui->scanComboBox2->addItem(QString("kT"));
    ui->scanComboBox2->addItem(QString("I11 Coeff"));
    if(nb_part > 1){
        ui->scanComboBox2->addItem(QString("Mu2"));
        ui->scanComboBox2->addItem(QString("Vex2"));
        ui->scanComboBox2->addItem(QString("Choose"));
        ui->scanComboBox2->addItem(QString("I12 Coeff"));
        ui->scanComboBox2->addItem(QString("I21 Coeff"));
        ui->scanComboBox2->addItem(QString("I22 Coeff"));
    }

    QObject::connect(ui->scanStartSpinBox, SIGNAL(valueChanged(double)), this, SLOT(check_values(double)));
    QObject::connect(ui->scanStartSpinBox2, SIGNAL(valueChanged(double)), this, SLOT(check_values(double)));
    QObject::connect(ui->endScanSpinBox, SIGNAL(valueChanged(double)), this, SLOT(check_values(double)));
    QObject::connect(ui->endScanSpinBox2, SIGNAL(valueChanged(double)), this, SLOT(check_values(double)));
    QObject::connect(ui->pitchScanSpinBox, SIGNAL(valueChanged(double)), this, SLOT(check_values(double)));
    QObject::connect(ui->pitchScanSpinBox2, SIGNAL(valueChanged(double)), this, SLOT(check_values(double)));
    QObject::connect(ui->scanComboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(param1Changed(QString)));
    QObject::connect(ui->scanComboBox2, SIGNAL(currentIndexChanged(QString)), this, SLOT(param2Changed(QString)));

    QObject::connect(ui->scanButton, SIGNAL(released()), this, SLOT(go()));

    ui->scanButton->setChecked(false);
    ui->secondWidget->hide();

}

void ScanParameters::check_values(double nv){
    cerr << "ScanParam :: check_values(" << nv << ") - nothing to do" << endl;
}

void ScanParameters::param1Changed(QString ns){
    ns.append(QString("")); // to avoid warning !
    int nv_val = ui->scanComboBox->currentIndex();
    if((nv_val != 2) && (nv_val != 6)){
        ui->endScanSpinBox->setMinimum(     0.0);
        ui->scanStartSpinBox->setMinimum(   -1e10);
        ui->pitchScanSpinBox->setMinimum(   1e-10);
        ui->endScanSpinBox->setMaximum(     1e10);
        ui->scanStartSpinBox->setMaximum(   1e10);
        ui->pitchScanSpinBox->setMaximum(   1e10);
        ui->endScanSpinBox->setSingleStep(  0.1);
        ui->scanStartSpinBox->setSingleStep(0.1);
        ui->pitchScanSpinBox->setSingleStep(0.1);
    } else {
        ui->endScanSpinBox->setMinimum(     2.0);
        ui->scanStartSpinBox->setMinimum(   2.0);
        ui->pitchScanSpinBox->setMinimum(   1.0);
        ui->endScanSpinBox->setMaximum(     1e10);
        ui->scanStartSpinBox->setMaximum(   1e10);
        ui->pitchScanSpinBox->setMaximum(   1e10);
        ui->endScanSpinBox->setSingleStep(  1.0);
        ui->scanStartSpinBox->setSingleStep(1.0);
        ui->pitchScanSpinBox->setSingleStep(1.0);
        ui->endScanSpinBox->setValue((double) (int) ui->endScanSpinBox->value());
        ui->scanStartSpinBox->setValue((double) (int) ui->scanStartSpinBox->value());
        ui->pitchScanSpinBox->setValue((double) (int) ui->pitchScanSpinBox->value());
    }

}

void ScanParameters::param2Changed(QString ns){
    ns.append(QString("")); // to avoid warning !

    int nv_val = ui->scanComboBox2->currentIndex();
    if((nv_val != 2) && (nv_val != 6)){
        ui->endScanSpinBox2->setMinimum(     0.0);
        ui->scanStartSpinBox2->setMinimum(   0.0);
        ui->pitchScanSpinBox2->setMinimum(   1e-10);
        ui->endScanSpinBox2->setMaximum(     1e10);
        ui->scanStartSpinBox2->setMaximum(   1e10);
        ui->pitchScanSpinBox2->setMaximum(   1e10);
        ui->endScanSpinBox2->setSingleStep(  0.1);
        ui->scanStartSpinBox2->setSingleStep(0.1);
        ui->pitchScanSpinBox2->setSingleStep(0.1);
    } else {
        ui->endScanSpinBox2->setMinimum(     2.0);
        ui->scanStartSpinBox2->setMinimum(   2.0);
        ui->pitchScanSpinBox2->setMinimum(   1.0);
        ui->endScanSpinBox2->setMaximum(     1e10);
        ui->scanStartSpinBox2->setMaximum(   1e10);
        ui->pitchScanSpinBox2->setMaximum(   1e10);
        ui->endScanSpinBox2->setSingleStep(  1.0);
        ui->scanStartSpinBox2->setSingleStep(1.0);
        ui->pitchScanSpinBox2->setSingleStep(1.0);
        ui->endScanSpinBox2->setValue((double) (int) ui->endScanSpinBox2->value());
        ui->scanStartSpinBox2->setValue((double) (int) ui->scanStartSpinBox2->value());
        ui->pitchScanSpinBox2->setValue((double) (int) ui->pitchScanSpinBox2->value());
    }

}

void ScanParameters::go(){
    if(chosenParameters->size() < 8) chosenParameters->resize(8);
    (*chosenParameters)[0] = ui->scanComboBox->currentIndex();
    (*chosenParameters)[1] = ui->scanStartSpinBox->value();
    (*chosenParameters)[2] = ui->endScanSpinBox->value();
    (*chosenParameters)[3] = ui->pitchScanSpinBox->value();

    (*chosenParameters)[4] = ui->scanComboBox2->currentIndex();
    (*chosenParameters)[5] = ui->scanStartSpinBox2->value();
    (*chosenParameters)[6] = ui->endScanSpinBox2->value();
    (*chosenParameters)[7] = ui->pitchScanSpinBox2->value();

    if(ui->scanStartSpinBox->value() > ui->endScanSpinBox->value()) return;
    if(ui->scanStartSpinBox2->value() > ui->endScanSpinBox2->value()) return;

    if(ui->scanComboBox->currentIndex() > 0) this->close();
}

ScanParameters::~ScanParameters()
{
    delete ui;
}
