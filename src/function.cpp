#include "function.h"
#include "ui_function.h"
#include <cmath>




QString functionName(FunctionT i){
    if(i == CONSTANT) return QString("Constant");
    if(i == SINUS) return QString("Sinus");
    if(i == TRIANGLE) return QString("Triangle");
    if(i == LENNARD) return QString("Lennard-Jones");

    if(i == NB_FUNCTIONS) return QString("<Select Function>");
    return QString("ERR !");
}




Function::Function(bool _need_plot, QWidget *parent) :
    QWidget(parent),
    m_ui(new Ui::Function),
    param1(0.0),
    param2(0.0),
    param3(0.0),
    min_slider1(-10),
    min_slider2(-10),
    min_slider3(-10),
    max_slider1(+10),
    max_slider2(+10),
    max_slider3(+10),
    needParam1(false),
    needParam2(false),
    needParam3(false),
    needPlot(_need_plot),
    size(0),
    values(0, 0.0)
{
    m_ui->setupUi(this);
    m_ui->doubleSpinBoxP1->setValue(param1);
    m_ui->doubleSpinBoxP2->setValue(param2);
    m_ui->doubleSpinBoxP3->setValue(param3);
    m_ui->horizontalSliderP1->setValue(param1);
    m_ui->horizontalSliderP2->setValue(param2);
    m_ui->horizontalSliderP3->setValue(param3);

    curve = new QwtPlotCurve("Function");
    curve->setPen(QColor(Qt::darkRed));
    curve->setStyle(QwtPlotCurve::Lines);
    curve->setRenderHint(QwtPlotItem::RenderAntialiased);

    m_ui->qwtPlot->axisWidget(QwtPlot::yLeft)->setFont(QFont(QString("Arial"),7));
    m_ui->qwtPlot->axisWidget(QwtPlot::xBottom)->setFont(QFont(QString("Arial"),7));

    updateShow();

    QObject::connect(m_ui->horizontalSliderP1, SIGNAL(sliderMoved(int)), this, SLOT(param1ChangedFromSlider(int)));
    QObject::connect(m_ui->doubleSpinBoxP1, SIGNAL(valueChanged(double)), this, SLOT(param1ChangedFromBox(double)));
    QObject::connect(m_ui->horizontalSliderP2, SIGNAL(sliderMoved(int)), this, SLOT(param2ChangedFromSlider(int)));
    QObject::connect(m_ui->doubleSpinBoxP2, SIGNAL(valueChanged(double)), this, SLOT(param2ChangedFromBox(double)));
    QObject::connect(m_ui->horizontalSliderP3, SIGNAL(sliderMoved(int)), this, SLOT(param3ChangedFromSlider(int)));
    QObject::connect(m_ui->doubleSpinBoxP3, SIGNAL(valueChanged(double)), this, SLOT(param3ChangedFromBox(double)));

}

Function::~Function()
{
    delete m_ui;
}

void Function::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Function::setSize(int new_one){
    if((new_one > 0) && (new_one < 10000000)){
        size = new_one;
        updateValues();
        replot();
    }
    else cerr << "Function::setSize, Wrong Size" << endl;
}

void Function::updateValues(){
    values.resize(size, 0.0);
    //cerr << "If you see this message, it means no subfunction has been chosen or the updateValues() function has not been iomplemented in the subclass." << endl;
}

void Function::replot(){
    vector<double> xpoints(size,0.0);
    for(int i = 0; i < size; ++i){
        xpoints[i] = i+1;
    }
    curve->setSamples(&xpoints[0], &values[0], values.size());
    curve->attach(m_ui->qwtPlot);
    m_ui->qwtPlot->setAxisScale(2, 1, values.size(), values.size() / 5 );
    m_ui->qwtPlot->setAxisScale(3, 1, values.size(), values.size() / 5);
    m_ui->qwtPlot->replot();
}

void Function::export_to_vector(vector<double>* to_fill){
    if((int) values.size() != size) cerr << "Function problem : size and values.size() different" << endl;
    size = values.size();
    to_fill->resize(values.size());
    for(int i = 0; i < size; ++i){
        (*to_fill)[i] = values[i];
    }
}

void Function::param1ChangedFromSlider(int value){
    param1 = ((double) value / 100.0 ) * (max_slider1 - min_slider1) + min_slider1;    //((double) value / 100.0) * (double) distortion_slider1;
    m_ui->doubleSpinBoxP1->setValue(param1);
    updateValues();
    replot();
}
void Function::param1ChangedFromBox(double value){
    if(max_slider1 - min_slider1 == 0) cerr << "ERR max_slider1 - min_slider1 == 0";
    param1 = value;
    m_ui->horizontalSliderP1->setValue((int) ((param1 - min_slider1) / (max_slider1 - min_slider1) * 100.0));
    updateValues();
    replot();
}
void Function::param2ChangedFromSlider(int value){
    param2 = ((double) value / 100.0 ) * (max_slider2 - min_slider2) + min_slider2;
    m_ui->doubleSpinBoxP2->setValue(param2);
    updateValues();
    replot();
}
void Function::param2ChangedFromBox(double value){
    if(max_slider2 - min_slider2 == 0) cerr << "ERR max_slider2 - min_slider2 == 0";
    param2 = value;
    m_ui->horizontalSliderP2->setValue((int) ((param2 - min_slider2) / (max_slider2 - min_slider2) * 100.0));
    updateValues();
    replot();
}
void Function::param3ChangedFromSlider(int value){
    param3 = ((double) value / 100.0 ) * (max_slider3 - min_slider3) + min_slider3;
    m_ui->doubleSpinBoxP3->setValue(param3);
    updateValues();
    replot();
}
void Function::param3ChangedFromBox(double value){
    if(max_slider3 - min_slider3 == 0) cerr << "ERR max_slider3 - min_slider3 == 0";
    param3 = value;
    m_ui->horizontalSliderP3->setValue((int) ((param3 - min_slider3) / (max_slider3 - min_slider3) * 100.0));
    updateValues();
    replot();
}

void Function::updateShow(){
    //cerr << "Update" << endl;
    if(needParam1){
        m_ui->doubleSpinBoxP1->show();
        m_ui->horizontalSliderP1->show();
        m_ui->labelP1->show();
        param1ChangedFromBox(param1);
    } else {
        m_ui->doubleSpinBoxP1->hide();
        m_ui->horizontalSliderP1->hide();
        m_ui->labelP1->hide();
    }
    if(needParam2){
        m_ui->doubleSpinBoxP2->show();
        m_ui->horizontalSliderP2->show();
        m_ui->labelP2->show();
        param2ChangedFromBox(param2);
    } else {
        m_ui->doubleSpinBoxP2->hide();
        m_ui->horizontalSliderP2->hide();
        m_ui->labelP2->hide();
    }
    if(needParam3){
        m_ui->doubleSpinBoxP3->show();
        m_ui->horizontalSliderP3->show();
        m_ui->labelP3->show();
        param3ChangedFromBox(param3);
    } else {
        m_ui->doubleSpinBoxP3->hide();
        m_ui->horizontalSliderP3->hide();
        m_ui->labelP3->hide();
    }

}












Constant::Constant(bool _need_plot, QWidget *parent): Function(_need_plot, parent)
{
    needParam1 = true;
    m_ui->labelP1->setText(QString("Value"));
    updateShow();

}

void Constant::updateValues(){
    // here, size and param are defined, just do your work on values. Don't forget to resize values before
    values.resize(size);
    for(int i = 0; i < size; ++i){
        values[i] = param1;
    }
}




Sinus::Sinus(bool _need_plot, QWidget *parent): Function(_need_plot, parent)
{
    needParam1 = true;
    needParam2 = true;
    needParam3 = true;
    min_slider1 = 0;
    min_slider2 = -10;
    min_slider3 = 0;
    max_slider1 = 1000;
    max_slider2 = +10;
    max_slider3 = 1000;
    m_ui->labelP1->setText(QString("Period"));
    m_ui->labelP2->setText(QString("Amplitude"));
    m_ui->labelP3->setText(QString("Add Phase"));
    updateShow();

}

void Sinus::updateValues(){
    // here, size and param are defined, just do your work on values. Don't forget to resize values before
    values.resize(size);
    for(int i = 0; i < size; ++i){
        values[i] = param2 * sin(2 * M_PI *(((double) i) / param1 + param3));
    }
}



double Ent(double v){
    return (double) ((int) v);
}

double triangleSimple(double x){
    return ((x - Ent(x)) - (-x - Ent(-x))) / 2.0;
}

Triangle::Triangle(bool _need_plot, QWidget *parent): Function(_need_plot, parent)
{
    needParam1 = true;
    needParam2 = true;
    needParam3 = true;
    m_ui->labelP1->setText(QString("Period"));
    m_ui->labelP2->setText(QString("Amplitude"));
    m_ui->labelP3->setText(QString("Add Phase"));
    updateShow();

}

void Triangle::updateValues(){
    // here, size and param are defined, just do your work on values. Don't forget to resize values before
    values.resize(size);
    for(int i = 0; i < size; ++i){
        values[i] = param2 * triangleSimple(((double) i) / param1 + param3);
    }
}


Lennard::Lennard(bool _need_plot, QWidget *parent): Function(_need_plot, parent)
{
    needParam1 = true;
    needParam2 = true;
    needParam3 = false;
    min_slider1 = 1;
    min_slider2 = 0;
    max_slider1 = 20;
    max_slider2 = 1000;
    m_ui->labelP1->setText(QString("Amplitude"));
    m_ui->labelP2->setText(QString("Radius"));
    updateShow();

}

void Lennard::updateValues(){
    // here, size and param are defined, just do your work on values. Don't forget to resize values before
    values.resize(size);
    for(int i = 0; i < size; ++i){
        values[i] = param1 * (   pow(param2 / (((double) i + 1) + param2), 12) - pow(param2 / (((double) i + 1) + param2), 6)) ;
    }
}





FunctionT functionID(QString str){
    for(int i = 0; i < NB_FUNCTIONS; ++i){
        if(!str.compare(functionName((FunctionT) i))) return (FunctionT) i;
    }
    return NB_FUNCTIONS;
}

Function* CreateFunctionAndPlot(FunctionT type, QWidget *parent){
    if(type == CONSTANT){return new Constant(true, parent);};
    if(type == SINUS){return new Sinus(true, parent);};
    if(type == TRIANGLE){return new Triangle(true, parent);};
    if(type == LENNARD){return new Lennard(true, parent);};

    if(type == NB_FUNCTIONS){return new Function(true, parent);};
    cerr << "ERR : Undefined type of function";
    return new Function(true, parent);
}

