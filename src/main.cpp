#include "common.h"
#ifdef QT4
#include <QtGui/QApplication>
#endif
#ifdef QT5
#include <QtWidgets/QApplication>
#endif
#include "mainwindow.h"
#include "4algos.h"

void Examples();
void teste_occup_dyades();

int main(int argc, char *argv[])
{
    //Examples();
    //teste_occup_dyades();
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}

void teste_occup_dyades() {
    vector <vector <double> > to_read;
    to_read.resize(2);
    to_read[0].resize(10, 0.0);
    to_read[1].resize(11, 0.0);
    to_read[0][2] = 0.5;
    to_read[1][3] = 0.7;

    vector< vector<double> > to_fill;

    vector<double> dyades(2, 0.0);
    dyades[0] = 1.5;
    dyades[1] = 2.0;

    //convert_to_dyades(to_read, to_fill, dyades);
    to_fill.resize(2);
    convert_to_dyades(to_read[0], to_fill[0], dyades[0]);
    convert_to_dyades(to_read[1], to_fill[1], dyades[1]);

    for(int i = 0; i < (int) to_fill.size(); ++i){
        for(int j = 0; j < (int) to_fill[i].size(); ++j){
            cerr << to_fill[i][j] << "\t";
        }
        cerr << endl;
    }

    vector<int> Vexs(2, 0);
    Vexs[0] = 2;
    Vexs[1] = 3;

    convert_to_occupancy(to_read, to_fill, Vexs);



    for(int i = 0; i < (int) to_fill.size(); ++i){
        for(int j = 0; j < (int) to_fill[i].size(); ++j){
            cerr << to_fill[i][j] << "\t";
        }
        cerr << endl;
    }

    //void convert_to_occupancy(vector<double> & to_read, vector<double> & to_fill, int Vex, int starting_fill);
    //void convert_to_dyades(vector< vector <double> > & to_read, vector< vector <double> > & to_fill, vector<double> dyades, int starting_fill);


}

void Examples(){
    int Vex = 3;
    int L = 9;
    int N = L - Vex + 1;
    int portee = 3;


    vector<valeur> z(7, 0.0);
    z[0] = 0.0;
    z[1] = 0.0;
    z[2] = 0.0;
    z[3] = 0.0;
    z[4] = 0.0;
    z[5] = 0.0;
    z[6] = 0.0;

    vector<valeur> I(7,0.0);
    I[0] = 0.5;
    I[1] = 0.2;
    I[2] = 0.1;
    I[3] = 0.05;
    I[4] = 0.025;
    I[5] = 0.0125;


    valeur mu = -2.0;
    Microcanonique m(I, z, mu, N, portee, Vex);
    m.Init();

    /*m.compute_density_from_all_paths();
    m.show_all_best_solutions();

    m.show_density();*/
    cerr << "done " << endl;
    for(int i = 0; i < 10; ++i){cout << i << endl;}




}
