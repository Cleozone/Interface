#include "potcomputedialog.h"
#include "ui_potcomputedialog.h"
#include "potCompute.h"

#include <QFileDialog>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <QComboBox>

potComputeDialog::potComputeDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::potComputeDialog)
{
    ui->setupUi(this);

    ui->doubleSpinBoxA->setValue(50);
    ui->doubleSpinBoxC->setValue(75);
    ui->spinBoxL->setValue(146);
    ui->doubleSpinBoxTwist->setValue(10.25);
    ui->doubleSpinBoxPhi->setValue(0);


    QObject::connect(ui->pushButtonLoad, SIGNAL(released()), this, SLOT(load()));
    QObject::connect(ui->doubleSpinBoxA, SIGNAL(valueChanged(int)), this, SLOT(recompute()));
    QObject::connect(ui->doubleSpinBoxC, SIGNAL(valueChanged(int)), this, SLOT(recompute()));
    QObject::connect(ui->spinBoxL, SIGNAL(valueChanged(int)), this, SLOT(recompute()));
    QObject::connect(ui->doubleSpinBoxPhi, SIGNAL(valueChanged(int)), this, SLOT(recompute()));
    QObject::connect(ui->doubleSpinBoxTwist, SIGNAL(valueChanged(int)), this, SLOT(recompute()));

    cerr << "Launch PotComputeDialog" << endl;

}

void potComputeDialog::clear(){
    ui->plainTextEditProfile->clear();
    ui->plainTextEditSource->clear();
    currentSequence.clear();
    currentProfile.clear();
}

void potComputeDialog::load(){
    string FileName = QFileDialog::getOpenFileName().toStdString();
    if(FileName.size() == 0) return;

    vector< string > listFasta;
    vector< string > listID;

    // code for reading FASTA - inspired from https://stackoverflow.com/questions/35251635/fasta-reader-written-in-c
    std::ifstream myFile(FileName);
    if (!myFile.good()) {
        cerr << "Could not open: " << FileName << endl;
        clear();
        return;
    }

    string currentLine, id, DNA_sequence;
    while (std::getline(myFile, currentLine)){
        if(currentLine.empty()) continue;
        if(currentLine[0] == '>'){
            if(!id.empty()) {
                listFasta.push_back(DNA_sequence);
                listID.push_back(id);
            }
            id = currentLine.substr(1);
            DNA_sequence.clear();
        }
        else{
            DNA_sequence += currentLine;
        }
    }
    if(!id.empty()) {
        listFasta.push_back(DNA_sequence);
        listID.push_back(id);
    }

    if(listID.size() == 1){ // if fasta contains one sequence, easy
        currentSequence = listFasta[0];
        ui->plainTextEditSource->setPlainText(QString(string(">" + listID[0] + "\n" + currentSequence).c_str()));
    } else { // if more sequences
        QDialogButtonBox* Choser = new QDialogButtonBox(QDialogButtonBox::Ok,this);
        QObject::connect(Choser, SIGNAL(accepted()), Choser, SLOT(accept()));
        Choser->setGeometry(100,100,500,200);
        QComboBox* QCB = new QComboBox(Choser);
        QStringList options;
        for(int i = 0; i < (int) listID.size(); ++i){
            stringstream oneLine;
            oneLine << listID[i] << "  -  " << listFasta[i].substr(0, min(listFasta[i].size(), 40 - listID[i].size()));
            options << QString(oneLine.str().c_str());
        }
        QCB->addItems(options);
        QCB->setGeometry(70,50,300,40);
        Choser->show();

        QEventLoop loop;
        QObject::connect(Choser, SIGNAL(accepted()), &loop, SLOT(quit()));
        loop.exec();
        Choser->hide();
        if(QCB->currentIndex() >= (int) listFasta.size()) return;
        currentSequence = listFasta[QCB->currentIndex()];
        ui->plainTextEditSource->setPlainText(QString(string(">" + listID[QCB->currentIndex()] + "\n" + currentSequence).c_str()));
    }
    recompute();
}

void potComputeDialog::recompute(){
    currentProfile = comp_pot(currentSequence, ui->doubleSpinBoxA->value(), ui->doubleSpinBoxC->value(), ui->doubleSpinBoxTwist->value(), ui->spinBoxL->value(), ui->doubleSpinBoxPhi->value());
    stringstream tablePot;
    int L = currentProfile.size();
    for(int i = 0; i < L; ++i){
        tablePot << i << "\t" << currentProfile[i] << "\n";
    }
    ui->plainTextEditProfile->setPlainText(QString(tablePot.str().c_str()));
}

potComputeDialog::~potComputeDialog()
{
    delete ui;
}
