#ifndef FUNCTION_H
#define FUNCTION_H

#ifdef QT4
#include <QtGui/QWidget>
#endif
#ifdef QT5
#include <QtWidgets/QWidget>
#endif

#include <vector>

#include "common.h"

#ifdef UNIX
#include <qwt6.1.0/qwt_plot.h>
#include <qwt6.1.0/qwt_scale_map.h>
#include <qwt6.1.0/qwt_scale_widget.h>
#include <qwt6.1.0/qwt_plot_curve.h>
#endif

#ifdef WINDOWS
#include "qwt6.1.0/qwt_plot.h"
#include "qwt6.1.0/qwt_scale_map.h"
#include "qwt6.1.0/qwt_scale_widget.h"
#include "qwt6.1.0/qwt_plot_curve.h"
#endif

#include <iostream>

using namespace std;

/* To ADD A new function, 5 steps :

   1 - add an arbitrary name(ID) for your function in the enum declaration, but you must let NB_FUNCTIONS to be the LAST ONE !!! : */

enum FunctionT {CONSTANT, SINUS, TRIANGLE, LENNARD, NB_FUNCTIONS};

/* 2 - add (in function.cpp in QString functionName(FunctionT i);) the text that will be displayed as name for your function (in function.cpp):
    if(i == YOUR_FUNCTION_ID) return QString("The Display Name");
*/

QString functionName(FunctionT i);

/* 3 - Define here (in the end of this file (function.h))  a new class for your function : (replace Sinus by a class name) */

//   example :
/*        class Sinus : public Function {
            Q_OBJECT
        public:
            Sinus(bool _need_plot, QWidget *parent = 0);
            void updateValues();
        };
*/


/* 4 - In Function.cc file, you have to implement :
   - the constructor, where you say how many aprameters you need and the label for each parameter.
     Don't say anything for parameters you don't need, they will be hidden. */

//   example :
/*        Sinus::Sinus(bool _need_plot, QWidget *parent): Function(_need_plot, parent)
        {
            needParam1 = true;
            needParam2 = true;
            needParam3 = true;
            m_ui->labelP1->setText(QString("Period"));
            m_ui->labelP2->setText(QString("Amplitude"));
            m_ui->labelP3->setText(QString("Add Phase"));
            updateShow();           // to hide/show parameter selection in the window
        }*/

//   - the function to compute the vector of points for this function :

//    example :
/*        void Sinus::updateValues(){
            // here, size and param are defined, just do your work on values. Don't forget to resize values before
            values.resize(size);
            for(int i = 0; i < size; ++i){
                values[i] = param3 * sin(2 * M_PI *(((double) i) / param1 + param3));
            }
        }
*/


/* 5 - in function.cpp, update CreateFunctionAndPlot function to be able to return your function : */

//    example : adding
//         if(type == TRIANGLE){return new Triangle(true, parent);}







FunctionT functionID(QString);

namespace Ui {
    class Function;
}

class Function : public QWidget {
    Q_OBJECT

protected:
    Ui::Function *m_ui;

public:
    Function(bool _need_plot, QWidget *parent = 0);
    ~Function();

    void setSize(int new_one);
    void replot();
    void export_to_vector(vector<double> * to_fill);

    virtual void updateValues();
    void updateShow();

protected:
    void changeEvent(QEvent *e);
    FunctionT myType;
    double param1;
    double param2;
    double param3;
    double min_slider1;
    double min_slider2;
    double min_slider3;
    double max_slider1;
    double max_slider2;
    double max_slider3;

public:
    bool needParam1;
    bool needParam2;
    bool needParam3;
    bool needPlot;

protected:
    int size;
    vector<double> values;
    QwtPlotCurve* curve; 

public slots:
    void param1ChangedFromSlider(int value);
    void param1ChangedFromBox(double value);
    void param2ChangedFromSlider(int value);
    void param2ChangedFromBox(double value);
    void param3ChangedFromSlider(int value);
    void param3ChangedFromBox(double value);


};

class Constant : public Function {
    Q_OBJECT
public:
    Constant(bool _need_plot, QWidget *parent = 0);
    void updateValues();
};


class Sinus : public Function {
    Q_OBJECT
public:
    Sinus(bool _need_plot, QWidget *parent = 0);
    void updateValues();

};


class Triangle : public Function {
    Q_OBJECT
public:
    Triangle(bool _need_plot, QWidget *parent = 0);
    void updateValues();

};

class Lennard : public Function {
    Q_OBJECT
public:
    Lennard(bool _need_plot, QWidget *parent = 0);
    void updateValues();
};





Function* CreateFunctionAndPlot(FunctionT type, QWidget *parent = 0);

#endif // FUNCTION_H
