/*!
 *		Algorithm to compute densities of K particles competing to access a discrete grid
 *		Created by Philippe Robert on 03/11-07/11.
 *
 !*/


#ifndef ALGOS_H
#define ALGOS_H

/// ##################################### Pre-definitions, not really interesting ###################################

/*! Define : Ways of using this program :
 *
 *		1 - In command line (#define COMMAND_LINE). Compile and launch it to have a small user-help
 *		2 - To include it as a lastwave function (#define LAST_WAVE)
 *
 *		3 - To include it in an other C++ program/environment (#define USE_AS_LIBRARY)
 *				(if you want to include it from a C environment, please use " extern "C++" { #include "4algos.h" }")
!*/

//    #define COMMAND_LINE
//	#define LAST_WAVE
    #define USE_AS_LIBRARY

#ifdef USE_AS_LIBRARY
#include "common.h"
#endif

#ifdef LAST_WAVE
    extern "C" {
        //#include "xx_system.h" /* ANSI changed */
        #include "lastwave.h"
        #include "signals.h"
    }
#endif

using namespace std;
#include <iostream>			// cin cout
#include <iomanip>			// << setprecision() <<
#include <cmath>			// min(x,y)
#include <vector>			// tout est en vecteur car tailles grandes -> alloc dynamiques
#include <queue>			// pour dijkstra
#include <string>			// lecture des arguments du main
#include <fstream>			// i/o fichiers par << et >>
#include <cstdlib>			// for linux, atoi ...
#include <ctime>			// for srand(time NULL)

/// If you want to know what happens (a lot !), please put DBG = 1 ...
#define DBG 0

/// In the microcanonical choice (optimal energy), an optimal path is defined as going through optimall edge of the graph
/// and an optimal vertex is an edge (s,e), such dist(s) + weight(edge(s,e)) = dist(e). To check this equality, because of
/// floating point imprecisions, 'a = b' is defined as : 'abs(a-b) < precision', where precision is defined just here.
#define my_precision 0.000001

/// The type of values for computing inside the program is 'long double' to avoid at maximum the 'nan' effect of big numbers.
/// please feel free to change it to other type like only double or more sophisticated type.
typedef long double valeur;



void convert_to_occupancy(vector<double> & to_read, vector<double> & to_fill, int Vex, int starting_fill);
void convert_to_occupancy(vector< vector <double> > & to_read, vector< vector <double> > & to_fill, vector<int> Vexs);
void convert_to_dyades(vector <double> & to_read,vector <double> & to_fill, double dyade);
void convert_to_dyades(vector< vector <double> > & to_read, vector< vector <double> > & to_fill, vector<double> dyades);





/// ################################################ Definition of a graph ####################################################


/// A - First, definition of internal classes (to store a graph in memory).

    /*!		a graph is a set of nodes (also called vertices) and edges (weighted arrows).
            In memory,																										*/

    /*!		1:nodes - a node is an integer : let say a graph is of size S, we will call the nodes 1, 2, 3 ..., S.		*/

    /*!		2:edges - first, an edge is defined as:
                a destination node,
                a weight.
                (the starting node is omitted because each starting node will be given its edges from it).				*/

struct edge {
    int dest;
    valeur weight;
    /// Constructor (int destination_node, valeur weight)
    edge(int d, valeur w): dest(d), weight(w) {};
};


    /*!		3:graph - the graph is stored in a 'adjacence list', meaning : for each node, we associate the list of edges starting from it.
                \image html FiguresDoc/Fig1Adjacence.jpg																*/

typedef vector<edge> liste;
/// structure to store graphes in memory
struct liste_adjacence {
    /// adjacency list (vector of list of edges)
    vector<liste *> table;
    /// number of nodes in the graph
    int taille;

    /// get the number of nodes
    int size();
    /// void constructor
    liste_adjacence();
    /// constructor of a new graph with _taille nodes
    liste_adjacence(int _taille);
    /// rebuild the graph with an other number of nodes
    void resize(int _taille);
    /// to access the list of edges starting from node i
    liste & operator [](int i);
    /// to add an edge, from node i to node j with weight w
    void add(int i, int j, valeur w);
    /// to destruct
    ~liste_adjacence();
};


    /*!		4:paths - In order to manipulate optimal pathes in the microcanonical problem, a path (chemin) is defined as a table of the nodes it goes through */

struct chemin {
    /// Table of the nodes the path goes through
    vector<int> ch;
    /// to erase and copy from an other path
    void copy(chemin ch2);
    /// to access the ith node of the path
    int operator [] (int i);
    /// Constructor copying an existing path
    chemin(vector<int> ch2);
    /// void constructor (then, size = 0).
    chemin();
    /// get the size (number of nodes in the path)
    int size();
    /// add a new node to the end of the path
    void push_back(int z);
    /// display the path
    void show();
};

    /*!		5:set of pathes - In order to manipulate the set of all optimal paths and to display it,   */

    /// ths structure stores all the pathes from the source (or dhe sink), to the node 'destination'.
struct ens_chemins {
    /// destination of all the paths
    int destination;
    /// table of all paths in the set
    vector<chemin> table;
    /// nb of paths in the set
    int size;
    /// add a new path in the set
    void add(chemin &ch);
    /// access the path number i of the set
    chemin operator[](int i);
    /// constructor (empty set)
    ens_chemins();
    /// set the destination destination to _destination (h�h�)
    void set_destination(int _destination);
    /// go back to empty set (without going through start case and getting 20 000$)
    void clear();
    /// add all the paths from an other set to me
    void add(struct ens_chemins &ens);
    ///
    void set_to_source();
    /// display all the paths of the set.
    void show();
};


/// B - Then, more user oriented classes,

    /*!		6:grid-graphs -	Since we will manipulate graph that look like 2D grids, here is a structure to convert
            from (i,j) positions in the grid to the name (number) of the corresponding node.	*/

struct grille {
    /// with of the 2D grid
    int largeur;
    /// constructor for a new grid of width largeur (nb of columns).
    grille(int largeur);
    /// get the node ID (number) sitting at position (i, j) on the grid (i=row, j=column)
    int ID(int i, int j);
    /// get the x position (column) on the grid of node ID
    int pos_x(int ID);
    /// get the y position (row) on the grid of node ID
    int pos_y(int ID);
    /// displays  cout << "$ID ($x,$y) >/t"
    void showpos(int ID);
    /// ID of the special node that will hold the source (ex : grand-canonical = ID(K+1, 1))
    int source;
    /// ID of the special node that will hold the sink  (ex : grand-canonical = ID(K+1, 2))
    int arrivee;
    /// convert a vector of IDs to corresponding x positions (filling an already defined vector)
    void convert_to_positions_x(vector<int> &IDs, vector<int> &pos_to_fill);
    /// trick (or cheating ...), to say 'I want to display the node id, but with a static function'.
    static void showpostatic(int id);
};


    /*!		7:Big General Class of graphs with nice functions  to operate on them			*/

class Graphe {

private:
    int size;									/// Nombre de sommets
public:
    liste_adjacence t;							/// LE GRAPHE   : Liste d'adjacence (tableau de vecteur<edge>)
    liste_adjacence reverse;					/// SON INVERSE : Liste d'adjacence du graphe inverse
private:
    int cpt_interne;
public:
    Graphe(int size);							/// to clear t and reverse
    void add_to_graph(int i, int j, valeur w);	/// to add i--(w)-->j to t and j--(w)-->i to reverse
    int nb_sommets();
private:
    struct vert{ int num; valeur dist; vert(int n, valeur d): num(n), dist(d) { }; };
    struct comp { bool operator() (vert &v1, vert &v2){ return (v1.dist > v2.dist); }; };
    void DFS(int start);
    void DFS_reverse(int start);



private:
    vector<int> Dpred;							/// Pr�c�cesseur de chaque sommet (apr�s dijkstra)
    vector<valeur> Ddist;						/// Distance de chaque sommet � la source (apr�s dijkstra)
    vector<bool> Dvisited;						/// Table des sommets visit�s par dijkstra au cours de son ex�cution
    bool dijkstra_done;
    int dijkstra_source;
    int dijkstra_dest;
public:
    bool dijkstra(int start, int dest);			/// Calcule dist et pred � partir du sommet start. D�s que toutes les mani�res optimales de relier destination ont été trouvées, l'algorithme s'arrête.
    int pred(int sommet);
    valeur dist(int sommet);



private:
    vector<int> Dpred_reverse;					/// Pr�c�cesseur de chaque sommet (apr�s dijkstra)
    vector<valeur> Ddist_reverse;				/// Distance de chaque sommet � la source (apr�s dijkstra)
    vector<bool> Dvisited_reverse;				/// Table des sommets visit�s par dijkstra au cours de son ex�cution
    bool dijkstra_reverse_done;
    int dijkstra_reverse_source;
    int dijkstra_reverse_dest;
public:
    bool dijkstra_reverse(int start, int dest);	/// Calcule dist et pred � partir du sommet start. D�s que toutes les mani�res optimales de relier destination ont �t� trouv�es, l'algorithme s'arr�te.
    int pred_reverse(int sommet);
    valeur dist_reverse(int sommet);



private:
    vector<int> Tpred_topo;
    int current_index_topo;
    vector<int> Tordre;
    bool tri_topologique_done;
    int tri_topologique_source;
public:
    void tri_topologique(int start);
    int pred_topo(int);
    int nb_sommets_ordonnes();
    int ordre(int nb);
    int number(int sommet);



private:
    vector<int> Tpred_topo_reverse;
    int current_index_topo_reverse;
    vector<int> Tordre_reverse;
    bool tri_topologique_reverse_done;
    int tri_topologique_reverse_source;
public:
    void tri_topologique_reverse(int start);
    int pred_topo_reverse(int);
    int nb_sommets_ordonnes_reverse();
    int ordre_reverse(int nb);
    int number_reverse(int sommet);

    void show_graph(void (* conv)(int ID) = NULL);

};









class Probleme {
protected:
    valeur Mu;				/// Energie par fixation d'une particule ; PAR RAPPORT A LA MOYENNE DU POTENTIEL EXTERIEUR landscape
    int N;					/// Nombre de positions �  considérer
    int portee;				/// Portee du potentiel (apr�?s le volume exclus)
    int vol_exclus;				/// Volume exculs ENTRE DEUX NUCLEOSOMES CONSECUTIFS !!!
    int K;
public:
    valeur _Mu(){return Mu;}
    int _N(){return N;}
    int _vol_exclus(){return vol_exclus;}
    int _portee(){return portee;}
    int _K(){return K;}

private:
    void load_landscape_from_function(valeur (* _function_landscape) (int));
    valeur (* function_interaction) (int);				// Potentiel d'interaction, soit stocké sous forme de fonction
    vector<valeur> table_interaction;					//							soit sous forme de tableau de valeurs (pour dist de 1 �  N)
    void load_landscape_from_vector(vector<valeur> _landscape);
    void load_landscape_from_file(char* file_landscape);
    valeur correct_to_interaction_function_only;		// A Ajouter �  interaction pour être sûr qu'il est positif. ATTENTION, CECI SERA SOUSTRAIT A MU POUR COMPENSER
    int size_table_interaction;							//								et sa taille (pour ne pas la recalculer sans arrêt)
public:
    valeur interaction(int);							// Fonction qui renvoie le potentiel d'interaction, �  partir d'un des deux stockages ci-dessus ; -1:=chargé comme fonction et pas comme tableau. 0:pas de potentiel chargé

private:
    void load_interaction_from_function(valeur (* _function_interaction) (int));
    valeur (* function_landscape)(int);					// Potentiel Extérieur, soit en fonction
    vector<valeur> table_landscape;						//						soit en tableau
    void load_interaction_from_vector(vector<valeur> _interaction);
    void load_interaction_from_file(char* file_interaction);
    valeur correct_to_landscape_function_only;			// Constante �  ajouter au potentiel pour être sûr qu'il est strictement positif.
    int size_table_landscape;							//							et sa taille ; -1:=chargé comme fonction et pas comme tableau. 0:pas de potentiel chargé
protected:
    valeur mean_landscape;
public:
    valeur _mean_landscape(){return mean_landscape;}
    valeur landscape(int i);							// Fonction �  appeler pour avoir le potentiel extérieur

    virtual void Init(); //int argc, char* argv[]);

    /* Position (
     - Interaction	(sous forme de fonction ou de fichier de valeurs)
     - Landscape		(potentiel extérieur) (sous forme de fonction ou de fichier de valeurs)
     - Mu			énergie de liaison par rapport �  la moyenne du landscape.
     - N				nombre de positions
     // - K				nombre de particules max sera calculé comme (N - 1) / (vol_exclus + 1) + 1
     - Volume exclus	ENTRE deux nucléosomes. Ex : 2 => N..N pas plus pr�?s (i.e. distance de 3 min) */
protected:
    vector<valeur> density;

public:
    Probleme(valeur (* _interaction)(int),	valeur (* _pot_ext)(int),	valeur _mu, int _N, int _portee, int _vol_exclus);
    Probleme(valeur (* _interaction)(int),	char* fichier_pot_ext,		valeur _mu, int _N, int _portee, int _vol_exclus);
    Probleme(char* fichier_interaction,		valeur (* _pot_ext)(int),	valeur _mu, int _N, int _portee, int _vol_exclus);
    Probleme(char* fichier_interaction,		char* fichier_pot_ext,		valeur _mu, int _N, int _portee, int _vol_exclus);
    Probleme(vector<valeur> _interaction,	vector<valeur> _pot_ext,	valeur _mu, int _N, int _portee, int _vol_exclus);

    void show_interaction();
    void show_landscape();
    void show_density();
    void SumUp();
        valeur get_density(int i){ if((i >= 0) && (i < (int) density.size())) return density[i]; else return 0.0;};

    void export_graph();
};


class Microcanonique : public Probleme {
public:
    Microcanonique(char* fichier_interaction,		char* fichier_pot_ext,		valeur _mu, int _N, int _portee, int _vol_exclus) : Probleme(fichier_interaction,		fichier_pot_ext,		_mu, _N, _portee, _vol_exclus), G2D(N+1), graphe((N+1) * (K) + 2)  {};
    Microcanonique(vector<valeur> _interaction,		vector<valeur> _pot_ext,		valeur _mu, int _N, int _portee, int _vol_exclus) : Probleme(_interaction,		_pot_ext,		_mu, _N, _portee, _vol_exclus), G2D(N+1), graphe((N+1) * (K) + 2)  {};

    void Init();
private:
    grille G2D;
    Graphe graphe;
    vector<valeur> density_graph;			// Pourcentage de chemins (source-dest) qui passent par chaque sommet
    valeur CstAntiNeg;

    typedef vector<int> solution;			// Une solution sera un ensemble de positions de nucléosomes
    solution une_solution_optimale;			// Pour stocker une solution optimale
    void trace_path(int dest);				// Pour calculer une solution optimale

    vector<ens_chemins> big_table;
    vector<solution> ensemble_solutions;	// Pour stocker l'ensemble des solutions optimales
    bool egal(valeur a, valeur b);			// sous-fonction qui teste l'égalité �  precision pr�?s. Sinon, probl�?mes d'arrondi avec les flottants. (cf. #define my_precision)
    void trace_all_paths(int dest);			// Calcule touts les chemins optimaux et les stocke dans ensemble_solutions
    bool all_paths_computed;

    vector<int> nb_paths;					// Mémorise le nombre de chemins de source �  chaque point
    vector<int> nb_paths_reverse;			// Mémorise le nombre de chemins de arrivee �  chaque point

public:
    void compute_density_from_all_paths();	// Prend tous les chemins optimaux et regarde le nombre de chemins optimaux qui passent par chaque sommet.
    void compute_density_from_nb_paths();	// Prend le nombre de chemins calculé dans nb_paths, entre la source et chaque point, et divise par le nombre total de chemins (source->dest).

    void show_one_best_solution();
    void show_all_best_solutions();

    void export_graph();
};


class GrandCanonique : public Probleme {
public:
    GrandCanonique(char* fichier_interaction,		char* fichier_pot_ext,		valeur _mu, int _N, int _portee, int _vol_exclus, valeur _kT = 1.0) :
        Probleme(fichier_interaction,		fichier_pot_ext,		_mu, _N, _portee, _vol_exclus), G2D(N+1), graphe((N+1) * (2*K) + 2) {Z = 0.0 ;	kT = _kT;};
    GrandCanonique(vector<valeur> _interaction,		vector<valeur> _pot_ext,		valeur _mu, int _N, int _portee, int _vol_exclus, valeur _kT = 1.0) :
        Probleme(_interaction,		_pot_ext,		_mu, _N, _portee, _vol_exclus), G2D(N+1), graphe((N+1) * (2*K) + 2) {Z = 0.0 ;	kT = _kT;};
    void Init();
    valeur kT;
    valeur poids(valeur);

private:
    grille G2D;
    Graphe graphe;
    vector<valeur> density_graph;			// Pourcentage de chemins (source-dest) qui passent par chaque sommet

    vector<valeur> partition;				// Mémorise la fonction de partition entre source et chaque point
    vector<valeur> partition_reverse;		// Mémorise la fonction de partition entre source et chaque point
public:
    void compute_density_from_recursion();
    void show_partitions();
    void export_graph();

    valeur compute_canonical_density_from_recursion(int nb_part);
    void show_all_canonical_density_from_recursion(int compare_to_discernables = 1);

    valeur Z;
};


class FastGrandCanonique : public Probleme {
public:
    FastGrandCanonique(char* fichier_interaction,		char* fichier_pot_ext,		valeur _mu, int _N, int _portee, int _vol_exclus, valeur _kT = 1.0) :
        Probleme(fichier_interaction,		fichier_pot_ext,		_mu, _N, _portee, _vol_exclus), G2D(N), graphe((N) * (2) + 2)  {	kT = _kT;};
    FastGrandCanonique(vector<valeur> _interaction,	vector<valeur> _pot_ext,		valeur _mu, int _N, int _portee, int _vol_exclus, valeur _kT = 1.0) :
        Probleme(_interaction,		_pot_ext,		_mu, _N, _portee, _vol_exclus), G2D(N), graphe((N) * (2) + 2)  {	kT = _kT;};
    void Init();
    valeur kT;
    valeur poids(valeur);

private:
    grille G2D;
    Graphe graphe;
    vector<valeur> density_graph;			// Pourcentage de chemins (source-dest) qui passent par chaque sommet

    vector<valeur> partition;				// Mémorise la fonction de partition entre source et chaque point
    vector<valeur> partition_reverse;		// Mémorise la fonction de partition entre source et chaque point
public:
    void compute_density_from_recursion();
    //void compute_canonical_density_from_recursion();
    void show_partitions();
    void export_graph();
};


class TeifGrandCanonique : public Probleme {
public:
    TeifGrandCanonique(char* fichier_interaction,		char* fichier_pot_ext,		valeur _mu, int _N, int _portee, int _vol_exclus, valeur _kT = 1.0) :
        Probleme(fichier_interaction,		fichier_pot_ext,		_mu, _N, _portee, _vol_exclus) {	kT = _kT;};
    TeifGrandCanonique(vector<valeur> _interaction,		vector<valeur> _pot_ext,		valeur _mu, int _N, int _portee, int _vol_exclus, valeur _kT = 1.0) :
        Probleme(_interaction,		_pot_ext,		_mu, _N, _portee, _vol_exclus) {	kT = _kT;};
    void Init();
    valeur kT;
    valeur poids(valeur);
};

typedef vector <vector<valeur> > Table;




class GeneralProblem {
public:
    GeneralProblem(int _L, int _T, int _Dmax, Table &_interaction, Table &_pot_ext, vector<valeur> &_Mus, vector<int> _Volumes, vector<vector<int> > &cross_or_how_to_read_interactions);
    GeneralProblem(int _L, int _T, int _Dmax, Table &_interaction, Table &_pot_ext, vector<valeur> &_Mus, vector<int> _Volumes);
    void Parse(int _L, int _T, int _Dmax, Table &_interaction, Table &_pot_ext, vector<valeur> &_Mus, vector<int> _Volumes, vector<vector<int> > &_cross_or_how_to_read_interactions);

protected:
    int L;		// Total number of positions even inaccessible by start positions, but accessible to particles.
    int T;		// Number of particles species.
    vector<valeur> Mus;				// taille T		Energie par fixation d'une particule ; PAR RAPPORT A LA MOYENNE DU POTENTIEL EXTERIEUR pour ce type de particule
    vector<int> Volumes;			// taille T		Volume exculs ENTRE DEUX NUCLEOSOMES CONSECUTIFS !!!
    int Dmax;					// Portee maximum des interactions
    Table Interactions;				// taille T.T x Dmax
    Table Potentials;				// taille L x T
    vector< vector<int> > TCross;					// Taille T x T		says which line of Interactions has to be read when asking Interaction(species I, species J)
public:
    int Cross(int I, int J);
    valeur Interaction(int T1, int T2, int distance);
    valeur Potential(int T1, int position);
    valeur Mu(int T1){if((T1 < 1) || (T1 > T)) {cerr << "MuOutOfBounds\n"; return 0;} else return Mus[T1-1];}
    int Volume(int T1){if((T1 < 1) || (T1 > T)) {cerr << "VolumeOutOfBounds\n"; return 0;} else return Volumes[T1-1];}

protected:
    vector<valeur> mean_potentials;
public:
    valeur mean_potential(int T1){if((T1 < 1) || (T1 > T)) return 0; return mean_potentials[T1-1];}

//protected:
    vector<vector<valeur> > densities;		// Taille L x T

    void show_interactions();		// taille L x T
    void show_potentials();
    void show_density();
    void SumUp();
public:
        valeur density(int T1, int i){if((i >= 0) && (i <= (int) densities.size()) && (T1 > 0) && (T1 <= T)) return densities[i-1][T1-1]; else return (valeur) -1;};

    virtual void Init();
};


class GeneralFast : public GeneralProblem {
public:
    GeneralFast(int _L, int _T, int _Dmax, Table &_interaction, Table &_pot_ext, vector<valeur> &_Mus, vector<int> _Volumes, vector<vector<int> > &cross_or_how_to_read_interactions, valeur _kT = 1.0)
        : GeneralProblem(_L, _T, _Dmax, _interaction, _pot_ext, _Mus, _Volumes, cross_or_how_to_read_interactions), G2D(L), graphe((L) * (T+1) + 2) {kT = _kT; ZCreated = false;};
    GeneralFast(int _L, int _T, int _Dmax, Table &_interaction, Table &_pot_ext, vector<valeur> &_Mus, vector<int> _Volumes, valeur _kT = 1.0)
        : GeneralProblem(_L, _T, _Dmax, _interaction, _pot_ext, _Mus, _Volumes) , G2D(L), graphe((L) * (T+1) + 2) {kT = _kT; ZCreated = false;};

    valeur kT;
public:
    valeur poids(valeur);

private:
    grille G2D;
    Graphe graphe;
    vector<valeur> density_graph;			// Pourcentage de chemins (source-dest) qui passent par chaque sommet

    vector<valeur> partition;				// Mémorise la fonction de partition entre source et chaque point
    vector<valeur> partition_reverse;		// Mémorise la fonction de partition entre source et chaque point

public:
    void Init();
    void compute_densities();

    Table ZMatrix;	// Taille size(=L*T+1) x (Deltamax * T+1)
    bool ZCreated;
    int DeltaMax;
    valeur getZ(int ID1, int ID2);
    void setZ(int ID1, int ID2, valeur v);
        void compute_pair(int _DeltaMax, int T1 = 0, int T2 = 0, vector< vector<double> >* to_export = NULL);
};



class MonteCarloDiscret : public GeneralProblem {
public:
    MonteCarloDiscret(int _L, int _T, int _Dmax, Table &_interaction, Table &_pot_ext, vector<valeur> _Mus, vector<int> _Volumes, vector<vector<int> > &cross_or_how_to_read_interactions, valeur _kT = 1.0)
    : GeneralProblem(_L, _T, _Dmax, _interaction, _pot_ext, _Mus, _Volumes, cross_or_how_to_read_interactions) {kT = _kT;};
    MonteCarloDiscret(int _L, int _T, int _Dmax, Table &_interaction, Table &_pot_ext, vector<valeur> _Mus, vector<int> _Volumes, valeur _kT = 1.0)
    : GeneralProblem(_L, _T, _Dmax, _interaction, _pot_ext, _Mus, _Volumes) {kT = _kT;};
protected:
    valeur kT;
public:
    valeur poids(valeur);

    void Init();
    int Mult;
    int NSteps;
};

#endif
