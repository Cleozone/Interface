#ifndef MULTIPLE_H
#define MULTIPLE_H

#include <QWidget>
#include <vector>
#include "common.h"
#include <QStandardItem>
#include "spectre.h"
using namespace std;

// note that, qwt things included in the wrong order it doesn't work ??
#ifdef UNIX
#include <qwt6.1.0/qwt_plot.h>
#include <qwt6.1.0/qwt.h>
#include <qwt6.1.0/qwt_plot_curve.h>
#endif

#ifdef WINDOWS
#include "qwt6.1.0/qwt_plot.h"
#include "qwt6.1.0/qwt.h"
#include "qwt6.1.0/qwt_plot_curve.h"
#endif

// Note that 4Algos.cc is using 'long double' as called 'valeur' for computing, but it's
// easier to manage data in double afterwards
namespace Ui {
class Multiple;
}

class Multiple : public QWidget
{
    Q_OBJECT
    
public:
    explicit Multiple(QWidget *parent = 0);
    ~Multiple();
    void Compute();
    void updatePlotsOnly();

private:
    Ui::Multiple *ui;

    QString currentUserDirectory;

    // most of the information is stored directly into the widgets
    int NbPart;
    int L;

    // Memory / displaying for the settings
    QStandardItemModel *browseSettings;
    QStandardItemModel *browsePotentiels;
    QStandardItemModel *browseInteractions;
    vector<double> Mus;
    vector<int> Vexs;
    vector<double> dyades;
    vector< vector<double> > Interactions;
    vector< vector<double> > Potentiels;
    // each line of interaction can be larger than range (to keep values if changing the range)
    // note that if range is increased, zeros are added if the end of line is reached


    // Memory / displaying for the results
    QwtPlot* BigPlot;
    vector<QwtPlotCurve*> AllCurves;
    Spectre* BigSpectre;
    vector< vector<double> > currentDensities; // Je ne mets pas vector<vector<double> *> pour pouvoir le passer directement au spectrogram
    vector< vector<double> > currentOccupations;
    vector< vector<double> > currentDyades;
    vector<double> xpoints;    // abscisses des points à afficher



    void updateModelSettings();
    void updateModelInteractions();
    void updateModelPotentiels();
    void updateCombos();

public slots :
    void nbParticlesChanged(int);    // spinBoxNbParticles
    //void settingTableChanged();      // tableViewSettings
    //void interactionTableChanged();  // tableViewInteractions
    //void potentielTableChanged();    // tableViewPotentiels
    void resetFromButton();
    void withInteractionChanged(int);// checkBoxWithInteractions
    void kTchanged(double);          // doubleSpinBoxkT
    void loadInteraction();          // pushButtonLoadInter
    void loadPotentiel();            // pushButtonLoadPotentiel
    void clearInteraction();         // pushButtonClearInter
    void clearPotentiel();           // pushButtonClearPotentiel
    void loadInteractionFromFile();  // pushButtonLoadInterFromFile
    void loadPotentielFromFile();    // pushButtonLoadPotentielFromFile
    void firstInterChanged(int);     // comboBoxFirstInter
    void secondInterChanged(int);    // comboBoxSecondInter
    void showLeftChecked(bool);      // radioButtonLeft
    void showOccupancyChecked(bool); // radioButtonOccupancy
    void showDyadeChecked(bool);     // radioButtonDyade
    void exportSettings(bool export_results_as_well = false);           // pushButtonExportSettings
    void reset();                    // pushButtonResetSettings
    void rangeChanged(int);          // spinBoxRange
    void showTypeChangedCombo(int);  // comboBoxShowType
    void showTypeChangedSlider(int); // horizontalSliderType
    void sumUp();                    // pushButtonSumUp
    void windowChanged(int);         // spinBoxWindow
    void oneParamModifSlider(int);   // ui->horizontalSliderParam
    void oneParamModifBox(double);   // ui->doubleSpinBoxParam
    void param1changed(int);         // ui->comboBoxParam1
    void param2changed(int);         // ui->comboBoxParam2
    void Lchanged(int);              // ui->spinBoxL
    void positionChangedSlider(int); // ui->horizontalSliderMainPlot
    void positionChangedBox(int);    // ui->spinBoxPosition


    void SettingsChangedFromTable(QModelIndex, QModelIndex);       // his->browseSettings
    void PotentielChangedFromTable(QModelIndex, QModelIndex);      // this->browsePotentiels,
    void InteractionsChangedFromTable(QModelIndex, QModelIndex);   // this->browseInteractions

};

#endif // MULTIPLE_H
