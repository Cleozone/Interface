#ifndef VIEWSUM_H
#define VIEWSUM_H

#include "common.h"
#include <QDialog>

namespace Ui {
    class viewsum;
}

class viewsum : public QDialog
{
    Q_OBJECT

public:
    explicit viewsum(QString fichier, QWidget *parent = 0);
    ~viewsum();

private:
    Ui::viewsum *ui;
    QString fname;

public slots:
        void backToSum();
};

#endif // VIEWSUM_H
