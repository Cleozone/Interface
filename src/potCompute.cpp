#include <math.h>        // for bessel functions
#include <iostream>
#include <fstream>       // open files
#include <iomanip>       // for std::setprecision
#include <cstdlib>
//#include "helix_3d.h"
#include <string.h>
#include <vector>
//#include <direct.h> // to get current directory with getcwd. Linux might need unistd.h instead
using namespace std;

#define FLOAT float
#define pi M_PI
string home = string(""); // §§§§§§§ string("C:/Users/Philippe/Desktop/CedricPol/");
bool verbose = false;

// Re-programmed functions
vector<FLOAT> polCourb(string DNA);
vector<FLOAT> comp_pot(string DNA, int A, int C, float tw, int /*float*/ l, float phi);
void print(vector<float> v, const char* name);      // to display the content of a vector in cout
double BESSJ0 (double X), BESSI0 (double X);

// header for helix_3d whose source code is pasted at the end of this file
extern char index_2_nuc(int i);
extern int  nuc_2_index(char nuc);
extern int compute_all_angle(char *seq,FLOAT *Y,int size,int size_window);
extern int compute_all(char *seq,FLOAT *X,FLOAT *Y,FLOAT *Z,int size);
extern int change_tilt_file(const char *filename);
extern int change_roll_file(const char *filename);
extern int compute_courb_table(FLOAT tab[4][4][4]);



double BESSJ0 (double X);




void createMatRoll(){
    ofstream f1("roll_pnuc.dat");
    f1 << "0.0 2.3 2.7 4.2\n"
          "1.6 1.6 4.4 4.4\n"
          "2.4 3.0 3.1 4.9\n"
          "2.7 1.8 6.7 4.4\n"
          "0.6 0.6 4.7 4.7\n"
          "2.3 0.0 4.2 2.7\n"
          "4.3 3.0 4.4 6.1\n"
          "5.4 4.2 4.4 4.4\n"
          "4.2 5.4 4.4 4.4\n"
          "1.8 2.7 4.4 6.7\n"
          "4.4 5.3 4.9 6.1\n"
          "3.4 3.4 3.8 3.8\n"
          "3.0 4.3 6.1 4.4\n"
          "3.0 2.4 4.9 3.1\n"
          "4.4 4.4 8.1 8.1\n"
          "5.3 4.4 6.1 4.9\n";
    f1.close();
}
void createMatTwist(){
    ofstream f2("twist_pnuc.dat");
    f2 << "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n"
          "34.3 34.3 34.3 34.3\n";
    f2.close();

}






/*
int main(int argc, char **argv){
    // to get the current folder (where the matrices 'roll_pnuc.dat' and 'twist_pnuc.dat' should be)
    // char buffer[256];
    //getcwd(buffer, sizeof(buffer));
    //home = string(buffer) + string("/");
    //home = string("");  // enough to look in the folder

    // If argument problems -> displays an example
    if(argc != 7){
        cout << "Computes DNA binding energy.\n\n--> Incorrect number of arguments :\n Use : " << argv[0] << "  A  C  tw  L  phi  SEQ\n\n  A=\n  C= Twist (Torsion) constant\n  tw= \n  L= length (nb of nucleotides) covered by the binding protein\n  phi= \n  SEQ= either a DNA sequence, either a '.txt' file containing such sequence. Only [atgc] allowed.\n\nNote that the matrices 'roll_pnuc.dat' and 'twist_pnuc.dat' have to be in the same folder.\n\n";
        cout << "Here is an example using the following parameters :\n " << argv[0] << " 5  3  1.25  14   1.25   TCATAGCAAAGCGGCAACAGACAGTTTCTACTGAAGAGTTAGTATGCATACATACAGGGATCCCATGTTCTCAGAGTAC\n" << endl;
        verbose = true;
        string DNA = string("TCATAGCAAAGCGGCAACAGACAGTTTCTACTGAAGAGTTAGTATGCATACATACAGGGATCCCATGTTCTCAGAGTAC");
        vector<FLOAT> potentials = comp_pot(DNA,A,C,tw,  14,  1.25);
        print(potentials, "Potentials");
        cout << "------------ THE END ------------\n";
    }
    else {
        verbose    = true;              // to change to false when the program is running properly
        int A      = atoi(argv[1]);
        int C      = atoi(argv[2]);
        float tw   = atof(argv[3]);
        int l      = atoi(argv[4]);
        float phi  = atof(argv[5]);
        string txt = string(argv[6]);

        // if txt is the name of a .txt file, reads it (fasta files accepted, lines starting with > are ignored)
        string ending = txt.substr(max(0, (int) txt.size()-4), 4);
        if(! ending.compare(string(".txt"))){
            if(verbose) cout << "Reading sequence in file : " << txt << endl;
            ifstream f(argv[6], ios::in);
            if(!f){cout << "ERR: file not found : " << argv[6] << endl; return -1;}
            string bigseq = string("");
            char lineBuffer[(int) 1e6];
            while(f.getline((char*) lineBuffer, 1e6)) {if((lineBuffer[0] != '>')) bigseq.append(lineBuffer);}
            txt = bigseq;
        }

        // doing the job
        vector<FLOAT> potentials = comp_pot(txt, A, C, tw, l, phi);
        print(potentials, "");
    }

    return 0;
}

*/








vector<FLOAT> polCourb(string DNA){
    char* seqBuffer = (char*) DNA.c_str();
    int sizeSig = DNA.size()-2;
    vector<FLOAT> signal;
    signal.resize(sizeSig);

    //if((polSta = PolBufferToSignal(&seqBuffer,signal->Y,"courb")) != POLOK) ...
    // --> calls the function PolBuffer to Signal

    //int PolBufferToSignal(PolluxBuffer *polbuffer,FLOAT *signal,char *codage)
      int i,sizeb=DNA.size();
      //FLOAT a,c,g,t,r,y,s,w,m,k,flag;

      // if(!strcmp(codage,"Courb") || !strcmp(codage,"courb")){
      FLOAT *X,*Y,*Z;
      FLOAT dx,dy,dz;

      X = (FLOAT *) malloc(3*sizeb*sizeof(FLOAT));
      if(X == NULL){cout << "POLMEMERROR" << endl; return vector<FLOAT>();} //POLMEMERROR;
      Y = X+sizeb;
      Z = Y+sizeb;

      // This function is in helix_3D.c
      if(compute_all(seqBuffer,X,Y,Z,sizeb) != 0){
            free(X); cout << "POLERROR" << endl; return vector<FLOAT>();} // POLERROR;

      for(i=0;i<sizeb-2;i++){
          dx = X[i+1]-X[i];
          dy = Y[i+1]-Y[i];
          dz = Z[i+1]-Z[i];
          signal[i] = sqrt(dx*dx+dy*dy+dz*dz);
      }

      free(X);
      return signal; // POLOK;
    //}
}


vector<FLOAT> comp_pot(string DNA, int A, int C, float tw, int /*float*/ l, float phi){


    ifstream f1("roll_pnuc.dat");
    if(!f1) createMatRoll();
    else f1.close();
    ifstream f2("twist_pnuc.dat");
    if(!f2) createMatTwist();
    else f2.close();

    // parsing the sequence
    int sizeSeq = DNA.size();
    for(int i = 0; i < sizeSeq; ++i)
        if((DNA[i] != 'a') && (DNA[i] != 't') && (DNA[i] != 'g') && (DNA[i] != 'c') && (DNA[i] != 'A') && (DNA[i] != 'T') && (DNA[i] != 'G') && (DNA[i] != 'C')) {cout << "ERR : incorrect DNA sequence : presence of the character " << DNA[i] << ", parsing stopped.\n"; return vector<FLOAT>();}

    int sizes2 = DNA.size()-2;
    vector<FLOAT> temp(sizes2, 0.0);
    vector<FLOAT> s2(sizes2, 0.0),   s3(sizes2, 0.0);
    vector<FLOAT> sc(sizes2, 0.0),   sc2(sizes2, 0.0),    ss(sizes2, 0.0),     ss2(sizes2, 0.0);
    vector<FLOAT> sco(sizes2, 0.0),  sco2(sizes2, 0.0),   stw(sizes2, 0.0),    stw2(sizes2, 0.0);
    vector<FLOAT> sout1(sizes2, 0.0),sout2(sizes2, 0.0),  sout3(sizes2, 0.0),  sout4(sizes2, 0.0);

    /// dna = string qui contient la sequence
    /// On transfrome cetet sequnec en signal d'angle, ici selon le codage pn,uc (cf matrice plus haut)
    if(verbose){cout << "DNA Seq<size=" << DNA.size() << "> : " << DNA << endl;}

    ///pol helix_file roll '/home/cvaillan/codages/roll_pnuc.dat' ### les angles = roll_pnuc
    change_roll_file((home + string("roll_pnuc.dat")).c_str());
    ///no need for a tilt matrix ??

    ///§§§ LOOK HERE =================================================== !!
    //change_tilt_file((home + string("tilt.dat")).c_str());
    change_tilt_file((home + string("roll_pnuc.dat")).c_str()); // cheating ?

    ///pol courb dna s2   ### dans signal s2 les angles pnuc en radian assigne a chaque triplet de dna
    s2 = polCourb(DNA);
    if(verbose) print(s2, "s2");

    ///pol helix_file roll '/home/cvaillan/codages/twist_pnuc.dat'
    change_roll_file((home + string("twist_pnuc.dat")).c_str());

    ///pol courb dna s3
    s3 = polCourb(DNA);
    if(verbose) print(s3, "s3");

    /// ############ Renormalisation des valeurs de roll (ici pour "Pnuc") ###############
    for(int i = 0; i < sizes2; ++i)
        s2[i]=(s2[i]-0.159072256)/2.2;
    if(verbose) print(s2, "s2 Renormalized");

    /// ############ Energies de courbure

    for(int i = 0; i < sizes2; ++i){
        sco[i]=1/(4.3*4.3)+(s2[i]*s2[i])/(0.34 * 0.34);
        sc[i]=2*(-1/0.34*s2[i]*cos(2*pi/tw*(float)i+phi))/4.3;
        ss[i]=2*( 1/0.34*s2[i]*sin(2*pi/tw*(float)i+phi))/4.3;
    }
    if(verbose) print(sco, "sco");
    if(verbose) print(sc, "sc");
    if(verbose) print(ss, "ss");

    for(int i = 0; i < sizes2-l-1; ++i){
        for(int j = 0; j < l; ++j){
            sco2[i] += sco[i + j];
            sc2[i]  += sc [i + j];
            ss2[i]  += ss [i + j];
        }
    }
    if(verbose) print(sco2, "sco2");
    if(verbose) print(sc2, "sc2");
    if(verbose) print(ss2, "ss2");

    for(int i = 0; i < sizes2; ++i)
        sout1[i]=0.5*A*sco2[i]*0.34;
    if(verbose) print(sout1, "sout1");

    /// ######### Energie de Twist

    stw.resize(sizes2);
    for(int i = 0; i < sizes2; ++i)
        stw[i]= pow(2*pi/(10.3*0.34)-s3[i]/0.34, 2.0);
    if(verbose) print(stw, "stw");

    stw2.resize(sizes2, 0.0);
    for(int i = 0; i < sizes2-l-1; ++i){
       for(int j = 0; j < l ; ++j){
            stw2[i] += stw[i + j];
        }
    }
    if(verbose) print(stw2, "stw2");

    sout2.resize(sizes2);
    for(int i = 0; i < sizes2; ++i)
        sout2[i]= 0.5*C*stw2[i]*0.34;
    if(verbose) print(sout2, "sout2");

    /// ########## Integration sur toutes les phases de l'energie de courbure
    temp.clear();
    temp.resize(sizes2);
    for(int i = 0; i < sizes2; ++i)
        temp[i]= (0.5*A*0.34*sqrt(ss2[i]*ss2[i]+sc2[i]*sc2[i]));
        //sout3[i]= sqrt(ss2[i]*ss2[i]+sc2[i]*sc2[i]);
    if(verbose) print(temp, "temp");

    // pour bess0 ca veut dire que je cree un signal qui est la fionction de bessel du signal mis en argument (ici temp)
    for(int i = 0; i < sizes2; ++i)
        sout4[i] = (FLOAT) BESSI0((double) temp[i]);
    if(verbose) print(sout4, "sout4");

    sout3.resize(sizes2);
    for(int i = 0; i < sizes2; ++i)
        sout3[i]= sout1[i]+sout2[i]-log(sout4[i]); // note that this 'log' from cmath is actually ln
    if(verbose) print(sout3, "sout3");

    // sout3=<One(floor(l/2))*sout3[0],sout3>
    // sout3=<sout3,One(dna.length-sout3.size)*sout3[sout3.size-1]>
    vector<FLOAT> final = vector<FLOAT>();
    vector<FLOAT> toInsert = vector<FLOAT>(min(l/2, (int) DNA.size()), sout3[0]);
    final.insert(final.end(), toInsert.begin(), toInsert.end());
    final.insert(final.end(), sout3.begin(), sout3.end());
    while(final.size() < DNA.size()){
        final.push_back(sout3[sout3.size()-1]);
    }
    if(verbose) print(final, "final");
    return final;
}

void print(vector<float> v, const char* name){
    cout << std::setprecision(10); // so that each numbers take 7 characters
    if(name) cout << "Vector[" << v.size() << "] : " << name << " = \t";
    for(int i = 0; i < (int) v.size(); ++i)
        cout << v[i] << "\t";
    cout << endl;
}









double BESSJ0 (double X) {
/***********************************************************************
* -------------------------------------------------------------------- *
*   Reference: From Numath Library By Tuan Dang Trong in Fortran 77.   *
*                                                                      *
*                               C++ Release 1.0 By J-P Moreau, Paris.  *
*                                        (www.jpmoreau.fr)             *
************************************************************************
  This subroutine calculates the First Kind Bessel Function of
  order 0, for any real number X. The polynomial approximation by
  series of Chebyshev polynomials is used for 0<X<8 and 0<8/X<1.
  REFERENCES:
  M.ABRAMOWITZ,I.A.STEGUN, HANDBOOK OF MATHEMATICAL FUNCTIONS, 1965.
  C.W.CLENSHAW, NATIONAL PHYSICAL LABORATORY MATHEMATICAL TABLES,
  VOL.5, 1962.
************************************************************************/
    const double
        P1=1.0, P2=-0.1098628627E-2, P3=0.2734510407E-4,
        P4=-0.2073370639E-5, P5= 0.2093887211E-6,
        Q1=-0.1562499995E-1, Q2= 0.1430488765E-3, Q3=-0.6911147651E-5,
        Q4= 0.7621095161E-6, Q5=-0.9349451520E-7,
        R1= 57568490574.0, R2=-13362590354.0, R3=651619640.7,
        R4=-11214424.18, R5= 77392.33017, R6=-184.9052456,
        S1= 57568490411.0, S2=1029532985.0, S3=9494680.718,
        S4= 59272.64853, S5=267.8532712, S6=1.0;
    double
        AX,FR,FS,Z,FP,FQ,XX,Y, TMP;

    if (X==0.0) return 1.0;
    AX = fabs(X);
    if (AX < 8.0) {
        Y = X*X;
        FR = R1+Y*(R2+Y*(R3+Y*(R4+Y*(R5+Y*R6))));
        FS = S1+Y*(S2+Y*(S3+Y*(S4+Y*(S5+Y*S6))));
        TMP = FR/FS;
    }
    else {
        Z = 8./AX;
        Y = Z*Z;
        XX = AX-0.785398164;
        FP = P1+Y*(P2+Y*(P3+Y*(P4+Y*P5)));
        FQ = Q1+Y*(Q2+Y*(Q3+Y*(Q4+Y*Q5)));
        TMP = sqrt(0.636619772/AX)*(FP*cos(XX)-Z*FQ*sin(XX));
    }
    return TMP;
}



double BESSI0(double X)
{
    double AX,TMP;
    double Y;

    if ((AX= (double) fabs(X)) < 3.75) {
        Y=X/3.75;
        Y*=Y;
        TMP=1.0+Y*((double) 3.5156229+Y*((double) 3.0899424+Y*((double) 1.2067492
            +Y*( (double) 0.2659732+Y*( (double) 0.360768e-1+Y* (double) 0.45813e-2)))));
    } else {
        Y= (double) 3.75/AX;
        TMP=((double) exp(AX)/((double) sqrt(AX)))*(0.39894228+Y*(0.1328592e-1
            +Y*(0.225319e-2+Y*(-0.157565e-2+Y*(0.916281e-2
            +Y*(-0.2057706e-1+Y*(0.2635537e-1+Y*(-0.1647633e-1
            +Y*0.392377e-2))))))));
    }
    return TMP;
}







/////// Source code for Helix3D

/*..........................................................................*/
/*                                                                          */
/*      Pollux 2.1                                                          */
/*                                                                          */
/*      Authors: Benjamin Audit, Emmanuel Bacry and Cedric Vaillant         */
/*      Copyright (C) 1997-2005.  All Right Reserved.                       */
/*                                                                          */
/*      Contact: Benjamin.Audit@ens-lyon.fr                                 */
/*                                                                          */
/*..........................................................................*/
/*                                                                          */
/*      This program is a free software, you can redistribute it and/or     */
/*      modify it under the terms of the GNU General Public License as      */
/*      published by the Free Software Foundation; either version 2 of the  */
/*      License, or (at your option) any later version                      */
/*                                                                          */
/*      This program is distributed in the hope that it will be useful,     */
/*      but WITHOUT ANY WARRANTY; without even the implied warranty of      */
/*      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       */
/*      GNU General Public License for more details.                        */
/*                                                                          */
/*      You should have received a copy of the GNU General Public License   */
/*      along with this program (in a file named COPYRIGHT);                */
/*      if not, write to the Free Software Foundation, Inc.,                */
/*      59 Temple Place, Suite 330, Boston, MA  02111-1307  USA             */
/*                                                                          */
/*..........................................................................*/

//#include "helix_3d.h"


#ifndef NOT_USED_WITH_LW

//extern void *Malloc(size_t size);
//extern void Free(void * ptr);
//extern void Printf(char *format,...);
//#define malloc Malloc
//#define free Free
//#define printf Printf

#endif /* NOT_USED_WITH_LW */


/* PRODUIT MATRICIEL 3*3 */
static void prod_mat(const double m1[3][3],const double m2[3][3],
             double m[3][3])
{
  int i,j,k;

  for(i=0;i<3;i++)
    for(j=0;j<3;j++)
      m[i][j] = 0;

  for(i=0;i<3;i++)
    for(j=0;j<3;j++)
      for(k=0;k<3;k++)
    m[i][j] += m1[i][k]*m2[k][j];
}

/* COPIE DE MATRICE */
static void mat_copy(const double in[3][3],double out[3][3])
{
  int i,j;

  for(i=0;i<3;i++)
    for(j=0;j<3;j++)
      out[i][j] = in[i][j];
}
/* MATRICE DE ROTATION AUTOUR DE X */
static void mat_rot_x(double m[3][3],double angle)
{
  m[2][2] = m[1][1] = cos(angle);
  m[2][1] = sin(angle);
  m[1][2] = -m[2][1];
  m[0][0] = 1.;
  m[0][1] = m[0][2] = m[1][0] = m[2][0] = 0.;
}
/* MATRICE DE ROTATION AUTOUR DE Y */
static void mat_rot_y(double m[3][3],double angle)
{
  m[2][2] = m[0][0] = cos(angle);
  m[0][2] = sin(angle);
  m[2][0] = -m[0][2];
  m[1][1] = 1.;
  m[1][0] = m[2][1] = m[0][1] = m[1][2] = 0.;
}
/* MATRICE DE ROTATION AUTOUR DE Z */
static void mat_rot_z(double m[3][3],double angle)
{
  m[1][1] = m[0][0] = cos(angle);
  m[0][1] = sin(angle);
  m[1][0] = -m[0][1];
  m[2][2] = 1.;
  m[2][0] = m[2][1] = m[0][2] = m[1][2] = 0.;
}


static void add_trans(double m[3][3],double twist,double roll,double tilt)
     /*
       Mat(B2 dans B0) = Mat(B1 dans B0)*Mat(B2 dans B1)
       Donc il faut multiplier a droite pour additionner une matrice
       */
{
  static double t[3][3];
  static double temp[3][3];

  /* LE TWIST */
  mat_rot_z(t,twist);
  prod_mat(m,t,temp);

  /* LE ROLL */
  mat_rot_x(t,roll);
  prod_mat(temp,t,m);

  /* LE TILT */
  mat_rot_y(t,tilt);
  prod_mat(m,t,temp);

  mat_copy(temp,m);

}

static void sous_trans(double m[3][3],double twist,double roll,double tilt)
     /*
       Mat(B2 dans B0) = Mat(B1 dans B0)*Mat(B2 dans B1)
       Donc il faut multiplier a gauche pour soustraire une matrice
       */
{
  static double t[3][3];
  static double temp[3][3];

  /* LE TWIST */
  mat_rot_z(t,-twist);
  prod_mat(t,m,temp);

  /* LE ROLL */
  mat_rot_x(t,-roll);
  prod_mat(t,temp,m);

  /* LE TILT */
  mat_rot_y(t,-tilt);
  prod_mat(t,m,temp);

  mat_copy(temp,m);

}



static double cos_angle(const double m[3][3])
{
  return m[2][2];
}

#define INDEXA 0
#define INDEXT 1
#define INDEXG 2
#define INDEXC 3
#define INDEXO 4

static double the_twist_mat[5][5][5];
static double the_roll_mat[5][5][5];
static double the_tilt_mat[5][5][5];
static int flag_are_init_the_mat = 0;

char index_2_nuc(int i)
{
  switch(i)
    {
    case INDEXA:
      return 'a';
    case INDEXT:
      return 't';
    case INDEXG:
      return 'g';
    case INDEXC:
      return 'c';
    default:
      break;
    }
  return '\0';
}

int nuc_2_index(char nuc)
{
  switch(nuc)
      {
      case 'a':
      case 'A':
    return INDEXA;
      case 't':
      case 'T':
    return INDEXT;
      case 'g':
      case 'G':
    return INDEXG;
      case 'c':
      case 'C':
    return INDEXC;
      default:
    break;
      }
  return INDEXO;
}

static int read_the_mat(char *filename,double the_mat[5][5][5])
{
  int i,j,k;
  FILE *fp;

  fp = fopen(filename,"r");
  if(fp ==NULL){
      cerr << "ERR:" << filename << "could not be opened" << endl;
    return -1;
  }

  for(k=0;k<4;k++)
    for(i=0;i<4;i++)
      for(j=0;j<4;j++)
    {
      if( fscanf(fp,"%lf",&the_mat[i][j][k]) != 1)
        {
          fclose(fp);
          return -1;
        }
    }

  fclose(fp);

  return 0;
}

static int init_the_roll_mat(char *filename)
{
  int i,j,k;

  if(read_the_mat(filename,the_roll_mat) != 0)
    return -1;

  for(i=0;i<5;i++)
    for(j=0;j<5;j++)
      {
    the_roll_mat[i][j][4] = 0.;
    the_roll_mat[i][4][j] = 0.;
    the_roll_mat[4][i][j] = 0.;
      }

  for(k=0;k<4;k++)
    for(i=0;i<4;i++)
      for(j=0;j<4;j++)
    the_roll_mat[i][j][k] *= M_PI/180.;

  return 0;
}

static int init_the_tilt_mat(char *filename)
{
  int i,j,k;

  if(read_the_mat(filename,the_tilt_mat) != 0)
    return -1;

  for(i=0;i<5;i++)
    for(j=0;j<5;j++)
      {
    the_tilt_mat[i][j][4] = 0.;
    the_tilt_mat[i][4][j] = 0.;
    the_tilt_mat[4][i][j] = 0.;
      }

  for(k=0;k<4;k++)
    for(i=0;i<4;i++)
      for(j=0;j<4;j++)
    the_tilt_mat[i][j][k] *= M_PI/180.;

  return 0;
}

static int init_the_twist_mat(void)
{
  int i,j,k;

  for(k=0;k<5;k++)
    for(i=0;i<5;i++)
      for(j=0;j<5;j++)
    the_twist_mat[i][j][k] = 34.3*M_PI/180.;

  return 0;
}


static void get_tw_r_ti(char *seq,int index,double *ptw,double *pr,double *pti)
{
  int k[3];
  int i;

  for(i=0;i<3;i++)
    k[i] = nuc_2_index(seq[index+i]);

  *ptw = the_twist_mat[k[0]][k[1]][k[2]];
  *pr = the_roll_mat[k[0]][k[1]][k[2]];
  *pti = the_tilt_mat[k[0]][k[1]][k[2]];
}

static void init_mat_identity(double m[3][3])
{
  int i,j;

  for(i=0;i<3;i++)
    for(j=0;j<3;j++)
      m[i][j] = 0.;

  for(i=0;i<3;i++)
    m[i][i] = 1.;

}

static char roll_file_default[] = "/user31/audit/MySofts/BEND/src/roll.dat";
static char *roll_file = roll_file_default;
int change_roll_file(const char *filename)
{
  /* if filename == NULL we want to use the default file */
  if(filename == NULL)
    {
      /* roll_file == roll_file_default there is nothing to do */
      if(roll_file == roll_file_default)
    return 0;
      else
    {
      free(roll_file);
      roll_file = roll_file_default;
      flag_are_init_the_mat = 0;
      return 0;
    }
    }
  else
    {
      /* if filename[] ==  roll_file[] there is nothing to do */
      if(strcmp(filename,roll_file) == 0)
    return 0;
      else
    {
      if(roll_file != roll_file_default)
        free(roll_file);

      roll_file = (char *) malloc((strlen(filename)+1)*sizeof(char));

      if(roll_file == NULL)
        {
          roll_file = roll_file_default;
          flag_are_init_the_mat = 0;
          return -1;
        }

      strcpy(roll_file,filename);
      flag_are_init_the_mat = 0;
      return 0;
    }
    }

  return 0;
}

static char tilt_file_default[] = "/user31/audit/MySofts/BEND/src/tilt.dat";
static char *tilt_file = tilt_file_default;
int change_tilt_file(const char *filename)
{

  /* if filename == NULL we want to use the default file */
  if(filename == NULL)
    {
      /* tilt_file == tilt_file_default there is nothing to do */
      if(tilt_file == tilt_file_default)
    return 0;
      else
    {
      free(tilt_file);
      tilt_file = tilt_file_default;
      flag_are_init_the_mat = 0;
      return 0;
    }
    }
  else
    {
      /* if filename[] ==  tilt_file[] there is nothing to do */
      if(strcmp(filename,tilt_file) == 0)
    return 0;
      else
    {
      if(tilt_file != tilt_file_default)
        free(tilt_file);

      tilt_file = (char *) malloc((strlen(filename)+1)*sizeof(char));

      if(tilt_file == NULL)
        {
          tilt_file = tilt_file_default;
          flag_are_init_the_mat = 0;
          return -1;
        }

      strcpy(tilt_file,filename);
      flag_are_init_the_mat = 0;
      return 0;
    }
    }

  return 0;
}

int compute_all_angle(char *seq,FLOAT *Y,int size,int size_window)
{
  double m[3][3];
  double tw,r,ti;
  int i;

  /* INITIALISATION */
  if(flag_are_init_the_mat != 1)
    {
      init_the_twist_mat();
      printf("roll file: %s\n",roll_file);
      if( init_the_roll_mat(roll_file) != 0)
    return -1;
      printf("tilt file: %s\n",roll_file);
      if( init_the_tilt_mat(tilt_file) != 0)
    return -1;
      flag_are_init_the_mat = 1;
    }
  /*  Printf("1");Flush(); */
  init_mat_identity(m);
  for(i=0;i<size_window;i++)
    {
      /*      Printf("o");Flush(); */
      get_tw_r_ti(seq,i,&tw,&r,&ti);
      add_trans(m,tw,r,ti);
    }
  /*  Printf("*");Flush(); */
  for(i=0;i<=size_window/2;i++)
    Y[i]=0.;

  /* PREMIER POINT */
  Y[size_window/2+1] = cos_angle(m);

  /* THE BOUCLE */
  for(i=1;i<=size-size_window-2;i++)
    {
      /*      Printf("*");Flush();*/
      get_tw_r_ti(seq,i+size_window-1,&tw,&r,&ti);
      add_trans(m,tw,r,ti);

      get_tw_r_ti(seq,i-1,&tw,&r,&ti);
      sous_trans(m,tw,r,ti);

      Y[i+size_window/2+1] = cos_angle(m);

    }

  /* LES DERNIERS POINTS */
  for(i=size-size_window+size_window/2;i<size;i++)
    Y[i] = 0.;

  return 0;
}

int compute_all(char *seq,FLOAT *X,FLOAT *Y,FLOAT *Z,int size)
{
  double m[3][3];
  double tw,r,ti;
  int i;

  /* INITIALISATION */
  if(flag_are_init_the_mat != 1)
    {
      init_the_twist_mat();
      printf("roll file: %s\n",roll_file);
      if( init_the_roll_mat(roll_file) != 0){
        cout << "Failed to init roll matrix\n"; return -1;}
      printf("tilt file: %s\n",tilt_file);
      if( init_the_tilt_mat(tilt_file) != 0){
        cout << "Failed to init tilt matrix\n"; return -1;}
      flag_are_init_the_mat =1;
    }
  /* PREMIER POINT */
  X[0] = 0.;
  Y[0] = 0.;
  Z[0] = 1.;

  init_mat_identity(m);
  for(i=0;i<size-2;i++)
    {
      get_tw_r_ti(seq,i,&tw,&r,&ti);
      add_trans(m,tw,r,ti);
      X[i+1] = m[0][2];
      Y[i+1] = m[1][2];
      Z[i+1] = m[2][2];
    }

  /* LE DERNIER POINT */
  X[size-1] = 0.;
  Y[size-1] = 0.;
  Z[size-1] = 0.;

  return 0;
}

int compute_courb_table(FLOAT tab[4][4][4])
{
  double m[3][3];
  double tw,r,ti;
  int i,j,k;
  double x,y,z;

  /* INITIALISATION */
  if(flag_are_init_the_mat != 1)
    {
      init_the_twist_mat();
      printf("roll file: %s\n",roll_file);
      if( init_the_roll_mat(roll_file) != 0)
    return -1;
      printf("tilt file: %s\n",tilt_file);
      if( init_the_tilt_mat(tilt_file) != 0)
    return -1;
      flag_are_init_the_mat =1;
    }

  for(i=0;i<4;i++)
    {
      for(j=0;j<4;j++)
    {
      for(k=0;k<4;k++)
        {
          /* LET'S GET THE ANGLES */
          tw = the_twist_mat[i][j][k];
          r = the_roll_mat[i][j][k];
          ti = the_tilt_mat[i][j][k];

          /* LET'S COMPUTE THE CURBATURE (|d2M/d2s|) */
          init_mat_identity(m);
          add_trans(m,tw,r,ti);
          x = m[0][2];
          y = m[1][2];
          z = m[2][2];
          tab[i][j][k] = sqrt(x*x+y*y+(z-1.)*(z-1.));

          /* Printf("%c%c%c %f\n",index_2_nuc(i),index_2_nuc(j),
         index_2_nuc(k),tab[i][j][k]); */
        }
    }
    }

  return 0;
}

