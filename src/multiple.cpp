#include "multiple.h"
#include "ui_multiple.h"
#include "askpotential.h"
#include <QMessageBox>
#include "4algos.h"
#include <QDate>
#include <QTime>
#include "viewsum.h"

#include <iostream>
using namespace std;

int max(int a, int b){
    if(a < b) return b; else return a;
}


void export_table(const char* file_to_write, vector< vector<double> > t){
    cerr << "    -> Write in file " << file_to_write << " from table ...\n";

    ofstream fichier(file_to_write);
    if(fichier){
        int L = t.size();
        for(int i =0; i < L; ++i){
            int S = t[i].size();
            for(int j = 0; j < S; ++j){
                fichier << t[i][j] << "\t";
            }
            fichier << "\n";
        }
        fichier.close();
    } else cerr << "ERR : Unable to open " << file_to_write << endl;
}
void export_vector_double (const char* file_to_write, vector<double> t){
    cerr << "    -> Write in file " << file_to_write << " from table ...\n";

    ofstream fichier(file_to_write);
    if(fichier){
        int L = t.size();
        for(int i =0; i < L; ++i){
            fichier << t[i] << "\t";
        }
        fichier << "\n";
        fichier.close();
    } else cerr << "ERR : Unable to open " << file_to_write << endl;
}
void export_vector_int (const char* file_to_write, vector<int> t){
    cerr << "    -> Write in file " << file_to_write << " from table ...\n";

    ofstream fichier(file_to_write);
    if(fichier){
        int L = t.size();
        for(int i =0; i < L; ++i){
            fichier << t[i] << "\t";
        }
        fichier << "\n";
        fichier.close();
    } else cerr << "ERR : Unable to open " << file_to_write << endl;
}

Multiple::Multiple(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Multiple),
    currentUserDirectory(HOME)
{
    ui->setupUi(this);

    NbPart = 2;
    L = 100;
    ui->checkBoxWithInteractions->setChecked(true);
    ui->checkBoxAfterInteractions->setChecked(true);
    ui->checkBoxCompareTotal->hide();   // what was it for ???
    ui->spinBoxNbPart->setValue(2);
    ui->spinBoxNbPart->setMinimum(1);
    ui->spinBoxNbPart->setMaximum(500);
    ui->spinBoxNbPart->setSingleStep(1);

    ui->spinBoxPosition->setMinimum(1);
    ui->spinBoxPosition->setMaximum(1e9);
    ui->spinBoxPosition->setSingleStep(1);

    ui->spinBoxRange->setValue(2); // starts at Vexmax
    ui->spinBoxRange->setMinimum(1);
    ui->spinBoxRange->setMaximum(10000);
    ui->spinBoxRange->setSingleStep(1);

    ui->spinBoxWindow->setMinimum(1);
    ui->spinBoxWindow->setMaximum(1e9);
    ui->spinBoxWindow->setSingleStep(1);

    ui->doubleSpinBoxkT->setMinimum(1e-6);
    ui->doubleSpinBoxkT->setMaximum(1000000);
    ui->doubleSpinBoxkT->setSingleStep(0.01);
    ui->doubleSpinBoxkT->setValue(1.0);

    ui->radioButtonOccupancy->setChecked(true);
    browseSettings = new QStandardItemModel(3, NbPart, ui->tableViewSettings);
    browseInteractions = new QStandardItemModel(NbPart * NbPart,ui->spinBoxRange->value(), ui->tableViewInteractions);
    browsePotentiels = new QStandardItemModel(NbPart, L, ui->tableViewPotentiels);


    BigPlot = new QwtPlot(ui->widgetBigPlot);

    BigPlot->axisWidget(QwtPlot::yLeft)->setFont(QFont(QString("Arial"),7));
    BigPlot->axisWidget(QwtPlot::xBottom)->setFont(QFont(QString("Arial"),7));

    BigPlot->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    BigPlot->setGeometry(0,0,580,200);/*
    BigPlot->setMinimumHeight(300);
    BigPlot->setMinimumWidth(1000);
    BigPlot->setMaximumHeight(300);
    BigPlot->setMaximumWidth(1000);*/

    BigSpectre = new Spectre(ui->widgetBigSpectro);
    reset();
    this->show();

    // really better to put the connect after everything is instanciated, because either it calls all the slots too early and cause seg faults ...

    QObject::connect(this->browseSettings, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(SettingsChangedFromTable(QModelIndex, QModelIndex)));
    QObject::connect(this->browsePotentiels, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(PotentielChangedFromTable(QModelIndex, QModelIndex)));
    QObject::connect(this->browseInteractions, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(InteractionsChangedFromTable(QModelIndex, QModelIndex)));


    // better to connect after settings (not to call functions everytime)

    //    QObject::connect(ui->, SIGNAL(valueChanged(int)), this, SLOT());

    QObject::connect(ui->spinBoxNbPart, SIGNAL(valueChanged(int)), this, SLOT(nbParticlesChanged(int)));

    //void settingTableChanged();      // tableViewSettings
    //void interactionTableChanged();  // tableViewInteractions

    QObject::connect(ui->checkBoxWithInteractions, SIGNAL(stateChanged(int)), this, SLOT(withInteractionChanged(int)));
    QObject::connect(ui->pushButtonLoadInter, SIGNAL(released()), this, SLOT(loadInteraction()));
    QObject::connect(ui->pushButtonLoadPot, SIGNAL(released()), this, SLOT(loadPotentiel()));
    QObject::connect(ui->pushButtonClearInter, SIGNAL(released()), this, SLOT(clearInteraction()));
    QObject::connect(ui->pushButtonClearPot, SIGNAL(released()), this, SLOT(clearPotentiel()));
    QObject::connect(ui->pushButtonLoadInterFromFile, SIGNAL(released()), this, SLOT(loadInteractionFromFile()));
    QObject::connect(ui->pushButtonLoadPotentielFromFile, SIGNAL(released()), this, SLOT(loadPotentielFromFile()));
    QObject::connect(ui->doubleSpinBoxkT, SIGNAL(valueChanged(double)), this, SLOT(kTchanged(double)));
    QObject::connect(ui->comboBoxFirstInter, SIGNAL(activated(int)), this, SLOT(firstInterChanged(int)));
    QObject::connect(ui->comboBoxSecondInter, SIGNAL(activated(int)), this, SLOT(secondInterChanged(int)));
    QObject::connect(ui->radioButtonLeft,       SIGNAL(clicked(bool)), this, SLOT(showLeftChecked(bool)));
    QObject::connect(ui->radioButtonOccupancy,  SIGNAL(clicked(bool)), this, SLOT(showOccupancyChecked(bool)));
    QObject::connect(ui->radioButtonDyade,      SIGNAL(clicked(bool)), this, SLOT(showDyadeChecked(bool)));
    QObject::connect(ui->pushButtonExportSettings, SIGNAL(released()), this, SLOT(exportSettings()));
    QObject::connect(ui->pushButtonResetSettings,  SIGNAL(released()), this, SLOT(resetFromButton()));
    QObject::connect(ui->pushButtonSumUp,  SIGNAL(released()), this, SLOT(sumUp()));
    QObject::connect(ui->spinBoxRange, SIGNAL(valueChanged(int)), this, SLOT(rangeChanged(int)));
    QObject::connect(ui->horizontalSliderType, SIGNAL(valueChanged(int)), this, SLOT(showTypeChangedSlider(int)));
    QObject::connect(ui->comboBoxShowType, SIGNAL(activated(int)), this, SLOT(showTypeChangedCombo(int)));
    QObject::connect(ui->spinBoxWindow, SIGNAL(valueChanged(int)), this, SLOT(windowChanged(int)));
    QObject::connect(ui->spinBoxL, SIGNAL(valueChanged(int)), this, SLOT(Lchanged(int)));

    QObject::connect(ui->comboBoxParam1, SIGNAL(activated(int)), this, SLOT(param1changed(int)));
    QObject::connect(ui->comboBoxParam2, SIGNAL(activated(int)), this, SLOT(param2changed(int)));
    QObject::connect(ui->horizontalSliderParam, SIGNAL(valueChanged(int)), this, SLOT(oneParamModifSlider(int)));
    QObject::connect(ui->doubleSpinBoxParam, SIGNAL(valueChanged(double)), this, SLOT(oneParamModifBox(double)));
    QObject::connect(ui->horizontalSliderMainPlot, SIGNAL(valueChanged(int)), this, SLOT(positionChangedSlider(int)));
    QObject::connect(ui->spinBoxPosition, SIGNAL(valueChanged(int)), this, SLOT(positionChangedBox(int)));
}

void Multiple::reset(){

    ui->pushButtonLoadInterFromFile->hide();
    ui->pushButtonLoadPotentielFromFile->hide();    // not done yet

    NbPart = 2;
    L = 100;
    ui->spinBoxWindow->setValue(L);
    ui->spinBoxNbPart->setValue(NbPart);
    ui->spinBoxWindow->setMinimum(2);
    ui->spinBoxWindow->setMaximum(L);
    ui->spinBoxL->setMinimum(2);
    ui->spinBoxL->setMaximum(1e9);
    ui->spinBoxL->setValue(L);


    Mus.clear();
    Mus.resize(NbPart, 0.0);
    Vexs.clear();
    Vexs.resize(NbPart, 2);
    dyades.clear();
    dyades.resize(NbPart, 1.0);
    Interactions.clear();
    Interactions.resize(NbPart * NbPart);
    Potentiels.clear();
    Potentiels.resize(NbPart);
    for(int i = 0; i < NbPart * NbPart; ++i){
        Interactions[i].resize(2, 0.0); // sinon ça appelle le slot !!
    }
    for(int i = 0; i < NbPart; ++i){
        Potentiels[i].resize(L, 0.0);
    }
    currentDensities.clear();
    currentDensities.resize(NbPart);
    for(int i = 0; i < NbPart; ++i){
        currentDensities[i].resize(L);
    }

    int Vmax = 0;
    for(int i = 0; i < NbPart; ++i){
        Vmax = max(Vmax, Vexs[i]);
    }
    ui->spinBoxRange->setValue(Vmax);


    //updateModelInteractions();
    updateModelSettings();
    updateModelPotentiels();
    updateCombos();
    //Compute();
    rangeChanged(Vmax);

    BigSpectre->reset();
    BigSpectre->addData(&(this->currentDensities), "All Densities");
    param1changed(ui->comboBoxParam1->currentIndex()); // to set the good boundaries for horizontal slider
}

void Multiple::resetFromButton(){
    int res = QMessageBox::warning(this, "Resetting settings", "Are you sure you want to reset all the settings ?", QMessageBox::Ok | QMessageBox::Cancel);
    if(res == QMessageBox::Cancel) return;
    reset();
}

/// ----------------- Updating the display ---------------------------

/* Displaying Mus, Vexs and Dyades in the table */
void Multiple::updateModelSettings(){
    // Q...(rows, cols)
    // QStandardItemModel* forDeleting = browseSettings;
    QObject::disconnect(this->browseSettings, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(SettingsChangedFromTable(QModelIndex, QModelIndex)));
    browseSettings->clear();
    browseSettings->setRowCount(3);
    browseSettings->setColumnCount(NbPart);

//    browseSettings = new QStandardItemModel(3, NbPart, ui->tableViewSettings);
    ui->tableViewSettings->setModel(browseSettings);
    //if(forDeleting) delete forDeleting; ça fait un seg fault !!!
    for(int i = 0; i < NbPart; ++i){
        // horizontal --> columns
        browseSettings->setHorizontalHeaderItem(i, new QStandardItem( QString("P") + QString::number(i+1)));
    }
    browseSettings->setVerticalHeaderItem(0, new QStandardItem(QString("Mu")));
    browseSettings->setVerticalHeaderItem(1, new QStandardItem(QString("Vex")));
    browseSettings->setVerticalHeaderItem(2, new QStandardItem(QString("Pos. Dyade")));
    ui->tableViewSettings->show();

    if((int) Mus.size() != NbPart) {cerr << "ERR (internal), Mus.size() is wrong (" << Mus.size() << ");\n"; return;}
    if((int) Vexs.size() != NbPart) {cerr << "ERR (internal), Vexs.size() is wrong (" << Vexs.size() << ");\n"; return;}
    if((int) dyades.size() != NbPart) {cerr << "ERR (internal), dyades.size() is wrong (" << dyades.size() << ");\n"; return;}

    for(int i = 0; i < NbPart; ++i){
        QStandardItem *info1 = new QStandardItem(QString::number(Mus[i]));
        browseSettings->setItem(0, i, info1);
        QStandardItem *info2 = new QStandardItem(QString::number(Vexs[i]));
        browseSettings->setItem(1, i, info2);
        QStandardItem *info3 = new QStandardItem(QString::number(dyades[i]));
        browseSettings->setItem(2, i, info3);
    }
    QObject::connect(this->browseSettings, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(SettingsChangedFromTable(QModelIndex, QModelIndex)));




}

/* Displaying Interactions (which is vector<vector<double>>, with NbPart * NbPart lines and all line has size > range)  */
void Multiple::updateModelInteractions(){
    QObject::disconnect(this->browseInteractions, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(InteractionsChangedFromTable(QModelIndex, QModelIndex)));

    browseInteractions->clear();
    browseInteractions->setRowCount(NbPart * NbPart);
    browseInteractions->setColumnCount(ui->spinBoxRange->value());

    ui->tableViewInteractions->setModel(browseInteractions);

    for(int i = 0; i < NbPart; ++i){
        for(int j = 0; j < NbPart; ++j){
            browseInteractions->setVerticalHeaderItem(i*NbPart+j, new QStandardItem( QString("P") + QString::number(i+1) + QString(" - ") + QString("P") + QString::number(j+1)));
        }
    }
    for(int i = 0; i < ui->spinBoxRange->value(); ++i){
        browseInteractions->setHorizontalHeaderItem(i, new QStandardItem(QString::number(i+1)));
    }

    // verifs
    if((int) Interactions.size() != NbPart * NbPart) {cerr << "ERR (internal), Interactions.size() is wrong (" << Interactions.size() << ");\n"; return;}

    for(int i = 0; i < NbPart * NbPart; ++i){
        if((int) Interactions[i].size() < ui->spinBoxRange->value()) {cerr << "ERR (internal), Interactions[" << i << "].size() is wrong (" << Interactions[i].size() << ");\n"; return;}
        for(int j = 0; j < ui->spinBoxRange->value(); ++j){
           QStandardItem *info4 = new QStandardItem(QString::number(Interactions[i][j]));
           if(j < (Vexs[i/NbPart]-1)) info4->setBackground(QBrush(QColor(Qt::lightGray)));
           browseInteractions->setItem(i, j, info4);
        }
    }
    QObject::connect(this->browseInteractions, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(InteractionsChangedFromTable(QModelIndex, QModelIndex)));

}

/* Displaying Potentiels (which is vector<vector<double>>, with NbPart lines and all line has size > L)  */
void Multiple::updateModelPotentiels(){
    QObject::disconnect(this->browsePotentiels, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(PotentielChangedFromTable(QModelIndex, QModelIndex)));


    // Q...(rows, cols)
    // QStandardItemModel* forDeleting = browseSettings;
    browsePotentiels->clear();
    browsePotentiels->setRowCount(NbPart);
    browsePotentiels->setColumnCount(L);

    ui->tableViewPotentiels->setModel(browsePotentiels);
    //if(forDeleting) delete forDeleting; ça fait un seg fault !!!
    for(int i = 0; i < L; ++i){
        // horizontal --> columns
        browsePotentiels->setHorizontalHeaderItem(i, new QStandardItem( QString::number(i+1)));
    }
    for(int i = 0; i < NbPart; ++i){
        browsePotentiels->setVerticalHeaderItem(i, new QStandardItem(QString("Particle ") + QString::number(i+1)));
    }
    ui->tableViewPotentiels->show();


    // verifs
    if((int) Potentiels.size() != NbPart) {cerr << "ERR (internal), Potentiels.size() is wrong (" << Potentiels.size() << ");\n"; return;}

    for(int i = 0; i < NbPart; ++i){
        if((int) Potentiels[i].size() < L) {cerr << "ERR (internal), Potentiels[" << i << "].size() is wrong (" << Potentiels[i].size() << ");\n"; return;}
        for(int j = 0; j < L; ++j){
            QStandardItem *info4 = new QStandardItem(QString::number(Potentiels[i][j]));
            browsePotentiels->setItem(i, j, info4);
            //cerr << i << "," << j << " ->" << Potentiels[i][j] << endl;
        }
    }
    QObject::connect(this->browsePotentiels, SIGNAL(dataChanged(QModelIndex, QModelIndex)), this, SLOT(PotentielChangedFromTable(QModelIndex, QModelIndex)));

}

/* updating all the combo boxes (when Nb Particles changed) */
void Multiple::updateCombos(){
    ui->comboBoxFirstInter->clear();
    ui->comboBoxSecondInter->clear();
    ui->comboBoxPotentiel->clear();
    ui->comboBoxShowType->clear();
    ui->comboBoxParam1->clear();
    ui->comboBoxParam2->clear();

    ui->comboBoxParam1->addItem(QString("Mu"));
    ui->comboBoxParam1->addItem(QString("Vex"));
    ui->comboBoxParam1->addItem(QString("Dyade"));
    for(int i = 0; i < NbPart; ++i){
        ui->comboBoxFirstInter->addItem(QString("P") + QString::number(i+1));
        ui->comboBoxSecondInter->addItem(QString("P") + QString::number(i+1));
        ui->comboBoxPotentiel->addItem(QString("P") + QString::number(i+1));
        ui->comboBoxShowType->addItem(QString("P") + QString::number(i+1));
        ui->comboBoxParam2->addItem(QString("P") + QString::number(i+1));
    }
    ui->comboBoxFirstInter->addItem(QString("All"));
    ui->comboBoxSecondInter->addItem(QString("All"));
    ui->comboBoxPotentiel->addItem(QString("All"));
    ui->comboBoxShowType->addItem(QString("All"));

    ui->horizontalSliderType->setMinimum(1);
    ui->horizontalSliderType->setMaximum(NbPart);
    ui->horizontalSliderType->setValue(1);
}







/// -------------------- Capturing Signals --------------------

void Multiple::nbParticlesChanged(int nb){
    if((nb < 1) || (nb > 300)){
        cerr << "ERR : when changing the number of particles kind, bad number : " << nb << ", should be in [1..300]\n";
        ui->spinBoxNbPart->setValue(NbPart);
    }
    if(nb != NbPart){
        cerr << NbPart << " -> " << nb << endl;
        int res = QMessageBox::warning(this, "Changing particles", "Are you sure you want to change the number of particles ? - you'll loose all the interaction settings", QMessageBox::Ok | QMessageBox::Cancel);
        if(res == QMessageBox::Cancel) return;
    }
    NbPart = nb;
    Potentiels.clear();
    Potentiels.resize(NbPart);
    for(int i = 0; i < NbPart; ++i){
        Potentiels[i].resize(L, 0.0);
    }

    Interactions.clear();
    Interactions.resize(NbPart * NbPart);
    for(int i = 0; i < NbPart * NbPart; ++i){
        Interactions[i].resize(ui->spinBoxRange->value(), 0.0);
    }
    Mus.resize(NbPart, 0.0);
    Vexs.resize(NbPart, 2);
    dyades.resize(NbPart, 1.0);

    updateModelSettings();
    //updateModelInteractions();
    updateModelPotentiels();
    updateCombos();
    int Vmax = 0;
    for(int i = 0; i < NbPart; ++i){
        Vmax = max(Vmax, Vexs[i]);
    }
    ui->spinBoxRange->setValue(Vmax);
    rangeChanged(Vmax);
    //Compute();
}

void Multiple::Lchanged(int newL){
    if((newL < 2) || (newL > 1e9)) {
        ui->spinBoxL->setValue(L);
        ui->horizontalSliderMainPlot->setValue(L);
        cerr << "New L is out of range " << newL << " Should be in 2..1e9" << endl;
        return;
    }
    L = newL;
    ui->spinBoxWindow->setValue(L); // est-ce que ça appelle le slot ?
    ui->spinBoxWindow->setMaximum(L);
    cerr << "new L " << newL << ui->spinBoxWindow->value() << endl;
    //    windowChanged(L);   //Problem : il ne faut pas afficher avant d'avoir fait compute ...
    // resizing the potentials
    for(int i = 0; i < NbPart; ++i){
        if((int) Potentiels[i].size() < L){
            Potentiels[i].resize(L, 0.0);
        }
    }
    ui->horizontalSliderMainPlot->setMinimum(1);
    ui->horizontalSliderMainPlot->setMaximum(L-ui->spinBoxWindow->value() +1);
    ui->horizontalSliderMainPlot->setValue(1);
    ui->spinBoxPosition->setMinimum(1);
    ui->spinBoxPosition->setMaximum(L-ui->spinBoxWindow->value() +1);
    updateModelPotentiels();
    Compute();
}

void Multiple::withInteractionChanged(int z){
    ui->widgetInteraction->setEnabled((z != 0));
    Compute();
}
void Multiple::kTchanged(double new_val){
    cerr << "KtChanged" << endl;
    new_val ++; //avoiding warning :-)
    Compute();
}





void Multiple::loadInteraction(){
    vector<double> stock = vector<double>(ui->spinBoxRange->value(), 0.0);
    AskPotential choseInteraction(stock, ui->spinBoxRange->value(), this);
    choseInteraction.exec();

    /*cerr << "Loaded : ";
    for(int i = 0; i < (int) stock.size(); ++i){
        cerr << stock[i] << "   ";
    }
    cerr << endl;*/

    int offset = ui->checkBoxAfterInteractions->isChecked() ? 1 : 0;

    if((int) Interactions.size() != NbPart * NbPart) cerr << "Intern ERR : wrong size for Interaction" << endl;
    // this interaction is to load for which particles ??
    if((ui->comboBoxFirstInter->currentIndex() == NbPart) && (ui->comboBoxSecondInter->currentIndex() == NbPart)){
        cerr << "All to All" << endl;
        for(int i = 0; i < NbPart; ++i){
            for(int k = 0; k < NbPart; ++k){
                Interactions[i*NbPart+k].resize(max(ui->spinBoxRange->value() + offset * (Vexs[i]-1), stock.size()), 0.0);
                int S = stock.size();
                for(int j = offset * (Vexs[i] - 1); j < S + offset * (Vexs[i] - 1); ++j){
                    Interactions[i*NbPart+k][j + offset*(Vexs[i]-1)] = stock[j];
                }
            }
        }
        updateModelInteractions();
        Compute();
        return;
    }
    if(ui->comboBoxFirstInter->currentIndex() == NbPart){
        cerr << "All to P" << 1+ui->comboBoxSecondInter->currentIndex() << endl;
        for(int i = 0; i < NbPart; ++i){
            int pos = (NbPart * i) + ui->comboBoxSecondInter->currentIndex();
            cerr << "position " << pos << endl;
            Interactions[pos].resize(max(ui->spinBoxRange->value() +  offset * (Vexs[i] - 1), stock.size()), 0.0);
            int S = stock.size();
            for(int j = 0; j < S; ++j){
                Interactions[pos][j + offset * (Vexs[i] - 1)] = stock[j];
            }
        }
        updateModelInteractions();
        Compute();
        return;
    }
    if(ui->comboBoxSecondInter->currentIndex() == NbPart){
        cerr << "P" << 1+ui->comboBoxFirstInter->currentIndex() << " to All " << endl;
        for(int i = 0; i < NbPart; ++i){
            int pos = i + (NbPart * ui->comboBoxFirstInter->currentIndex());
            cerr << "position " << pos << endl;
            Interactions[pos].resize(max(ui->spinBoxRange->value() +  offset * (Vexs[i] - 1), stock.size()), 0.0);
            int S = stock.size();
            for(int j = 0; j < S; ++j){
                Interactions[pos][j +  offset * (Vexs[i] - 1)] = stock[j];
            }
        }
        updateModelInteractions();
        Compute();
        return;
    } else {
        int i = ui->comboBoxFirstInter->currentIndex();
        cerr << "P" << 1+ i<< " to P" << 1+ui->comboBoxSecondInter->currentIndex() << endl;
        int pos = ui->comboBoxSecondInter->currentIndex() + (NbPart * ui->comboBoxFirstInter->currentIndex());
        Interactions[pos].resize(max(ui->spinBoxRange->value() +  offset * (Vexs[i] - 1), stock.size()), 0.0);
        int S = stock.size();
        for(int j = 0; j < S; ++j){
            Interactions[pos][j+  offset * (Vexs[i] - 1)] = stock[j];
        }
        updateModelInteractions();
        Compute();
        return;
    }
    cerr << "Bad project !" << endl;
}

void Multiple::clearInteraction(){
    if((int) Interactions.size() != NbPart*NbPart) cerr << "No Way Potentiels have bad size \n";
    for(int i = 0; i < NbPart*NbPart; ++i){
        Potentiels[i].clear();
        Potentiels[i].resize(ui->spinBoxRange->value(), 0.0);
    }
    updateModelPotentiels();
    Compute();
}

void Multiple::loadInteractionFromFile(){

}

void Multiple::loadPotentiel(){
    vector<double> stock = vector<double>(ui->spinBoxRange->value(), 0.0);
    AskPotential choseInteraction(stock, ui->spinBoxRange->value(), this, true);
    choseInteraction.exec();

    /*cerr << "Loaded : ";
    for(int i = 0; i < (int) stock.size(); ++i){
        cerr << stock[i] << "   ";
    }
    cerr << endl;*/

    if((int) Potentiels.size() != NbPart) cerr << "No Way Potentiels have bad size \n";

    if(ui->comboBoxPotentiel->currentIndex() == NbPart){
        cerr << "Load All pot" << endl;
        for(int i = 0; i < NbPart; ++i){
            Potentiels[i].resize(max(L, stock.size()), 0.0);
            int S = stock.size();
            for(int j = 0; j < S; ++j){
                Potentiels[i][j] = stock[j];
            }
        }
        updateModelPotentiels();
        Compute();
        return;
    }

    int i = ui->comboBoxPotentiel->currentIndex();
    cerr << "Load Pot for P" << i+1 << endl;
        Potentiels[i].resize(max(L, stock.size()), 0.0);
        int S = stock.size();
        for(int j = 0; j < S; ++j){
            Potentiels[i][j] = stock[j];
        }

    updateModelPotentiels();
    Compute();

}

void Multiple::clearPotentiel(){
    if((int) Potentiels.size() != NbPart) cerr << "No Way Potentiels have bad size \n";
    for(int i = 0; i < NbPart; ++i){
        Potentiels[i].clear();
        Potentiels[i].resize(L, 0.0);
    }
    updateModelPotentiels();
    Compute();
}

void Multiple::loadPotentielFromFile(){

}


void Multiple::firstInterChanged(int){
    // nothing to do !
}
void Multiple::secondInterChanged(int){
    // nothing to do !
}

void Multiple::showLeftChecked(bool){
    updatePlotsOnly();
}
void Multiple::showOccupancyChecked(bool){
    updatePlotsOnly();
}
void Multiple::showDyadeChecked(bool){
    updatePlotsOnly();
}

void Multiple::exportSettings(bool export_results_as_well){


    QString chooseDirectory = QFileDialog::getExistingDirectory(this, tr("Please choose a working directory for saving settings"), currentUserDirectory.toStdString().c_str(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if(chooseDirectory.size() < 1) return;
    cerr << "Saving settings to " << chooseDirectory.toStdString() << endl;

    QString fileP = chooseDirectory + "/Potentials.txt";
    export_table(fileP.toStdString().c_str(), Potentiels);

    QString fileI = chooseDirectory + "/Interactions.txt";
    export_table(fileI.toStdString().c_str(), Interactions);

    QString fileM = chooseDirectory + "/Mus.txt";
    export_vector_double(fileM.toStdString().c_str(), Mus);

    QString fileV = chooseDirectory + "/Vexs.txt";
    export_vector_int(fileV.toStdString().c_str(), Vexs);

    QString fileD = chooseDirectory + "/Dyades.txt";
    export_vector_double(fileD.toStdString().c_str(), dyades);

    vector< vector <double > > ReadInter;   // hoping it writes as intergers ...
    ReadInter.resize(NbPart);
    int k = 0;
    for(int i = 0; i < NbPart; ++i){
        ReadInter[i].resize(NbPart);
        for(int j = 0; j < NbPart; ++j){
            ReadInter[i][j] = k;
            k++;
        }
    }

    QString fileRI = chooseDirectory + "/ReadInter.txt";
    export_table(fileRI.toStdString().c_str(), ReadInter);

    QString fileInfo = chooseDirectory + "/Settings.txt";
    const char* file_to_write = fileInfo.toStdString().c_str();
    {ofstream fichier(file_to_write);
    if(fichier){
        fichier << "Experiment settings : \n";
        fichier << "      L      = " << L << "\n";
        fichier << "      NbPart = " << NbPart << "\n";
        fichier << "      kT     = " << ui->doubleSpinBoxkT << "\n";
        fichier << "      Potentials    -> " << fileP.toStdString() << "\n";
        fichier << "      Interactions  -> " << fileI.toStdString() << "\n";
        fichier << "      Mus           -> " << fileM.toStdString() << "\n";
        fichier << "      Vexs          -> " << fileV.toStdString() << "\n";
        fichier << "      ReadInter     -> " << fileRI.toStdString() << "\n";
        fichier.close();
    } else cerr << "ERR : Unable to open " << file_to_write << endl;
    }



        //vector< vector<double> > currentDensities; // Je ne mets pas vector<vector<double> *> pour pouvoir le passer directement au spectrogram
        //vector< vector<double> > currentOccupations;
        //vector< vector<double> > currentDyades;


        QString fileDens = chooseDirectory + "/AllDensities.txt";
        if(export_results_as_well)export_table(fileDens.toStdString().c_str(), currentDensities);

        QString fileOcc = chooseDirectory + "/AllOccupations.txt";
        if(export_results_as_well)export_table(fileOcc.toStdString().c_str(), currentDensities);

        QString fileDyad = chooseDirectory + "/AllDensityDyades.txt";
        if(export_results_as_well)export_table(fileDyad.toStdString().c_str(), currentDensities);





    QString location2(chooseDirectory);
    if(export_results_as_well) {
        location2.append("/SettingsAndResults.html");
    } else {
        location2.append("/Settings.html");
    }

    ofstream fichier(location2.toStdString().c_str(), ios::out);
    if(fichier){

        fichier << "<h2> Sum up of experiment with Nucleozone, any kinds of particles </h2>" << endl;

        QDate date(QDate::currentDate());
        fichier << "Done : " << (date.toString().toStdString()) << ", ";
        QTime time = QTime::currentTime();
        fichier << time.toString().toStdString() << ". <br>" << endl;

        fichier << "<h3> Parameters : </h3>" << endl;

        fichier << "Kinds of particles                          : " << NbPart << "<br>\n";
        fichier << "Length of sequence simulated                : " << L << "<br>\n";
        fichier << "Temperature (kT)                            : " << ui->doubleSpinBoxkT->value() << "<br>\n";
        fichier << "Non-specific affinities (Mus)               : ";
            fichier << "  <a href=\"file://localhost/" << (fileM.toStdString()) << "\"> here </a> <br> \n";
        fichier << "Volume of particles (Vexs)                  : ";
            fichier << "  <a href=\"file://localhost/" << (fileV.toStdString()) << "\"> here </a> <br> \n";
        fichier << "Potentials                                  : ";
            fichier << "  <a href=\"file://localhost/" << (fileP.toStdString()) << "\"> here </a> <br> \n";
        fichier << "Interactions                                : ";
            fichier << "  <a href=\"file://localhost/" << (fileI.toStdString()) << "\"> here </a> <br> \n";
        fichier << "Association Pi - Pj - line in Inter file    : ";
            fichier << "  <a href=\"file://localhost/" << (fileRI.toStdString()) << "\"> here </a> <br> \n";

        if(export_results_as_well) {

            fichier << "<h3> Results : </h3>" << endl;

            fichier << "<a href=\"file://localhost/" << (fileDens.toStdString()) << "\"> Table of predicted densities </a> <br>\n";
            fichier << "<a href=\"file://localhost/" << (fileOcc.toStdString()) << "\"> Table of predicted occupations </a> <br>\n";
            fichier << "<a href=\"file://localhost/" << (fileDyad.toStdString()) << "\"> Table of predicted dyade positions </a> <br>\n";

        }
        fichier.close();
    } else cerr << "ERR before saving window(" << location2.toStdString() << ") : Impossible d'ouvrir le fichier " << endl;

    viewsum VS(location2);
    VS.exec();
    cerr << "Done." << endl;


}

void Multiple::rangeChanged(int rg){
    cerr << "range Changed" << endl;
    int Vmax = 0;
    for(int i = 0; i < NbPart; ++i){
        Vmax = max(Vmax, Vexs[i]);
    }
        if((rg < Vmax) || (rg > 100000)){
            cerr << "ERR : when changing the range of interaction, bad number : " << rg << ", should be in [" << Vmax << "..100000]\n";

            ui->spinBoxRange->setValue(Vmax); // on s'auto-appelle pas ??
            //cerr << "Alors ?" << endl;
            //return;// pour voir non ça n'appelle pas ! donc pas de return
        }

//        cerr << "Appell" << endl;
        if((int) Interactions.size() == 0) cerr << "Empty Interactions !!! (internal - WTF - error)" << endl;
        if((int) Interactions.size() != NbPart * NbPart) cerr << "Wrong size for Interactions !!! (internal - WTF - error)" << Interactions.size() << endl;
        for(int i = 0; i < NbPart * NbPart; ++i){
            if((int) Interactions[i].size() < rg){
                Interactions[i].resize(rg, 0.0);    // should not replace already existing values
            }
        }
        updateModelInteractions();
        Compute();
}

void Multiple::showTypeChangedCombo(int index){
    ui->horizontalSliderType->setValue(index+1);
    updatePlotsOnly();
}
void Multiple::showTypeChangedSlider(int index){
    ui->comboBoxShowType->setCurrentIndex(index-1);
    updatePlotsOnly();
}
void Multiple::windowChanged(int z){
    cerr << "WindowChanged" << endl;

    ui->horizontalSliderMainPlot->setMinimum(1);
    ui->horizontalSliderMainPlot->setMaximum(L-ui->spinBoxWindow->value() +1);
    ui->horizontalSliderMainPlot->setValue(1);
    ui->spinBoxPosition->setMinimum(1);
    ui->spinBoxPosition->setMaximum(L-ui->spinBoxWindow->value() +1);

    if((int) Potentiels[0].size() >= z) {updatePlotsOnly();}
    else {cerr << "No plotting because resizing from inside ie computation not done yet" << endl;} // seems this case never happens but in case ...
}

void Multiple::positionChangedSlider(int z){
    // the maximum of the slider should have already been updated
    ui->spinBoxPosition->setValue(z);
    updatePlotsOnly();
}

void Multiple::positionChangedBox(int z){
    ui->horizontalSliderMainPlot->setValue(z);
    updatePlotsOnly();
}


void Multiple::oneParamModifSlider(int v){
    ui->doubleSpinBoxParam->setValue(((double) v) / 10000.0);
    //cerr << "Double to " << ((double) v) / 10000.0 << endl;
    if(ui->comboBoxParam1->currentIndex() == 0){//Mus
        Mus[ui->comboBoxParam2->currentIndex()] = (double) (v / 10000.0);
    }
    if(ui->comboBoxParam1->currentIndex() == 1){//Vex
        Vexs[ui->comboBoxParam2->currentIndex()] = v / 10000.0;
        dyades[ui->comboBoxParam2->currentIndex()] = ( (double) Vexs[ui->comboBoxParam2->currentIndex()]) / 2.0;
    }
    if(ui->comboBoxParam1->currentIndex() == 2){//Dyade
        dyades[ui->comboBoxParam2->currentIndex()] = (double) (v / 10000.0);
    }
    // cerr << "Updated : Vex P1 " << Vexs[0] << ", P2 : " << Vexs[1] << " Mu1 " << Mus[0] << " Mus[1] " << Mus[1] << endl;

    int Vmax = 0;
    for(int i = 0; i < NbPart; ++i){
        Vmax = max(Vmax, Vexs[i]);
    }

    // deux cas se posent : soit on change le Vmax et donc il suffit de faire ça :
    if(Vmax != ui->spinBoxRange->value()){
        ui->spinBoxRange->setValue(Vmax); // qui lui-même appellera compute
    } else {
        updateModelSettings();
        Compute();
    }
    //rangeChanged(Vmax); // super important !!!

}

void Multiple::oneParamModifBox(double d){
    int v = (int) (d * 10000.0);
    //cerr << "from Box " << v << " , max = " << ui->horizontalSliderParam->maximum() << endl;
    v = max(ui->horizontalSliderParam->minimum(), v);
    v = min(ui->horizontalSliderParam->maximum(), v);
    ui->horizontalSliderParam->setValue(v);
    // cerr << "after :" << v << endl;


    if(ui->comboBoxParam1->currentIndex() == 0){//Mus
        Mus[ui->comboBoxParam2->currentIndex()] = d;
    }
    if(ui->comboBoxParam1->currentIndex() == 1){//Vex
        Vexs[ui->comboBoxParam2->currentIndex()] = (int) d;
        ui->doubleSpinBoxParam->setValue((double) (v / 10000));
        dyades[ui->comboBoxParam2->currentIndex()] = ( (double) Vexs[ui->comboBoxParam2->currentIndex()]) / 2.0;
    }
    if(ui->comboBoxParam1->currentIndex() == 2){//Dyade
        dyades[ui->comboBoxParam2->currentIndex()] = d;
    }
    // cerr << "Updated : Vex P1 " << Vexs[0] << ", P2 : " << Vexs[1] << " Mu1 " << Mus[0] << " Mus[1] " << Mus[1] << endl;

    int Vmax = 0;
    for(int i = 0; i < NbPart; ++i){
        Vmax = max(Vmax, Vexs[i]);
    }
    // deux cas se posent : soit on change le Vmax et donc il suffit de faire ça :
    if(Vmax != ui->spinBoxRange->value()){
        ui->spinBoxRange->setValue(Vmax); // qui lui-même appellera compute
    } else {
        updateModelSettings();
        Compute();
    }
}

void Multiple::param1changed(int ind){
    if(ind == 0) {//Mus
        ui->horizontalSliderParam->setMinimum(-1e6);
        ui->horizontalSliderParam->setMaximum(1e6);
        ui->horizontalSliderParam->setSingleStep(100);
        ui->doubleSpinBoxParam->setValue(Mus[ui->comboBoxParam2->currentIndex()]);
        ui->doubleSpinBoxParam->setMinimum(-1e4);
        ui->doubleSpinBoxParam->setMaximum(1e4);
        ui->doubleSpinBoxParam->setSingleStep(0.01);
        ui->horizontalSliderParam->setValue((int) (Mus[ui->comboBoxParam2->currentIndex()] * 10000.0));
    }
    if(ind == 1) {//Vex
        ui->doubleSpinBoxParam->setMinimum(2.0);
        ui->doubleSpinBoxParam->setMaximum(1e4);
        ui->doubleSpinBoxParam->setSingleStep(1.0);
        ui->horizontalSliderParam->setMinimum(20000);
        ui->horizontalSliderParam->setMaximum(1e6);
        ui->horizontalSliderParam->setSingleStep(10000);
        ui->doubleSpinBoxParam->setValue(Vexs[ui->comboBoxParam2->currentIndex()]);
        ui->horizontalSliderParam->setValue((int) (Vexs[ui->comboBoxParam2->currentIndex()] * 10000.0));


    }
    if(ind == 2) {//Dyade
        ui->doubleSpinBoxParam->setMinimum(1.0);
        ui->doubleSpinBoxParam->setMaximum((double) Vexs[ui->comboBoxParam2->currentIndex()] + 1.0);
        ui->doubleSpinBoxParam->setSingleStep(0.5);
        ui->horizontalSliderParam->setMinimum(10000);
        ui->horizontalSliderParam->setMaximum(10000 * Vexs[ui->comboBoxParam2->currentIndex()] + 10000);
        ui->horizontalSliderParam->setSingleStep(5000);
        ui->horizontalSliderParam->setValue((int) (dyades[ui->comboBoxParam2->currentIndex()] * 10000.0));
        ui->doubleSpinBoxParam->setValue(dyades[ui->comboBoxParam2->currentIndex()]);


    }
    updateModelSettings();
}

void Multiple::param2changed(int){
    //nothing to do ??
}

void Multiple::sumUp(){
    exportSettings(true);
}







void Multiple::SettingsChangedFromTable(QModelIndex Q1, QModelIndex Q2){
    cerr << "settings changed" << endl;
    for(int row = Q1.row(); row <= Q2.row(); ++row){
        for(int column = Q1.column(); column <= Q2.column(); ++column){
            if((row == 0) && (column >= 0) && (column < (int) Mus.size())) Mus[column] = browseSettings->data(browseSettings->index(row, column, QModelIndex())).toDouble();
            if((row == 1) && (column < (int) Vexs.size())) {
                Vexs[column] = browseSettings->data(browseSettings->index(row, column, QModelIndex())).toDouble();
                dyades[column] = ((double) Vexs[column]) / 2.0;
            }
            if((row == 2) && (column < (int) dyades.size())) dyades[column] = browseSettings->data(browseSettings->index(row, column, QModelIndex())).toDouble();
        }
    }
    int Vmax = 0;
    for(int i = 0; i < NbPart; ++i){
        Vmax = max(Vmax, Vexs[i]);
    }
    // deux cas se posent : soit on change le Vmax et donc il suffit de faire ça :
    if(Vmax != ui->spinBoxRange->value()){
        ui->spinBoxRange->setValue(Vmax); // qui lui-même appellera compute
    } else {
        updateModelSettings();
        Compute();
    }
}

void Multiple::PotentielChangedFromTable(QModelIndex Q1, QModelIndex Q2){
    cerr << "Potentiels changed" << endl;
    for(int row = Q1.row(); row <= Q2.row(); ++row){
        for(int column = Q1.column(); column <= Q2.column(); ++column){
            if((row >= 0) && (row < NbPart) && (column >= 0) && (column < L))
                Potentiels[row][column] = browsePotentiels->data(browsePotentiels->index(row, column, QModelIndex())).toDouble();
        }
    }
    //cerr << "Modifie : Vex P1 " << Vexs[0] << ", P2 : " << Vexs[1] << " Mu1 " << Mus[0] << " Mus[1] " << Mus[1] << endl;
}

void Multiple::InteractionsChangedFromTable(QModelIndex Q1, QModelIndex Q2){
    //cerr << "Interactions changed" << endl;
    for(int row = Q1.row(); row <= Q2.row(); ++row){
        for(int column = Q1.column(); column <= Q2.column(); ++column){
            if((row >= 0) && (row < NbPart*NbPart) && (column >= 0) && (column < ui->spinBoxRange->value()))
                Interactions[row][column] = browseInteractions->data(browseInteractions->index(row, column, QModelIndex())).toDouble();
        }
    }
    Compute();
}












void Multiple::Compute(){
    cerr << "Compute" << endl;
    // cerr << "Compute : Vex P1 " << Vexs[0] << ", P2 : " << Vexs[1] << " Mu1 " << Mus[0] << " Mus[1] " << Mus[1] << endl;

    // for info,

    // data is stored here :
    // NbPart , L
    // vector<double> Mus;
    // vector<int> Vexs;
    // vector<double> dyades;
    // vector< vector<double> > Interactions;

    // result has to be put here
    // QwtPlot* BigPlot;
    // vector<QwtPlotCurve*> AllCurves;
    // Spectre* BigSpectre;
    // vector< vector<double> > currentDensities;


    // il faut tout transformer en long double (valeur) :'-(
    // this enable to give square tables to the algorithm

    //cerr << "before go" << endl;
    vector<valeur> Mus2;
    Mus2.clear();
    Mus2.resize(NbPart);
    for(int i = 0; i < NbPart; ++i){
        Mus2[i] = (valeur) Mus[i];
    }

    int Vmax = 0;
    for(int i = 0; i < NbPart; ++i){
        Vmax = max(Vmax, Vexs[i]);
    }

    Table Interactions2;
    Interactions2.clear();
    Interactions2.resize(NbPart*NbPart);
    if(ui->checkBoxWithInteractions->isChecked()){
        for(int i = 0; i < NbPart*NbPart; ++i){
            Interactions2[i].resize(ui->spinBoxRange->value());
            for(int j = 0; j < ui->spinBoxRange->value(); ++j){
                Interactions2[i][j] = (valeur) Interactions[i][j];
            }
        }
    } else {
        cerr << "without interactions " << endl;
        for(int i = 0; i < NbPart*NbPart; ++i){
            Interactions2[i].resize(Vmax);
            for(int j = 0; j < Vmax; ++j){
                Interactions2[i][j] = (valeur) 0;
            }
        }
    }

    Table Potentiels2;
    Potentiels2.clear();
    Potentiels2.resize(NbPart);
    for(int i = 0; i < NbPart; ++i){
        Potentiels2[i].resize(L);
        for(int j = 0; j < L; ++j){
            Potentiels2[i][j] = (valeur) Potentiels[i][j];
        }
    }

    //cerr << "GO !" << endl;


    GeneralFast firstway(L, NbPart, (ui->checkBoxWithInteractions->isChecked() ? ui->spinBoxRange->value() : Vmax), Interactions2, Potentiels2, Mus2, Vexs, ui->doubleSpinBoxkT->value());
    firstway.Init();

    //for(int i = 0; i < N

    // BE CAREFULL current densities is INVERTED !!! one line is one particle !
    //firstway.show_density();
    currentDensities.clear();
    currentDensities.resize(NbPart);
    for(int i = 0; i < NbPart; ++i){
        currentDensities[i].resize(L);
    }
    currentOccupations.clear();
    currentOccupations.resize(NbPart);
    for(int i = 0; i < NbPart; ++i){
        currentOccupations[i].resize(L, 0.0);
    }
    currentDyades.clear();
    currentDyades.resize(NbPart);
    for(int i = 0; i < NbPart; ++i){
        currentDyades[i].resize(L+1, 0.0); // carefull here !!
    }


    if((int) firstway.densities.size() != L) cerr << "ERR : the size of densities table returned by the algorithm has wrong size (" << firstway.densities.size() << ") instead of L=" << L << endl;
    for(int i = 0; i < L; ++i){
        int S = firstway.densities[i].size();
        if(S != NbPart) cerr << "ERR : the size of densities table returned by the algorithm has wrong size, at line " << i << " , size =" << firstway.densities[i].size() << " instead of NbPart=" << NbPart << endl;
        for(int j = 0; j < NbPart; ++j){
            currentDensities[j][i] = firstway.densities[i][j];
        }
    }

    convert_to_occupancy(currentDensities, currentOccupations, Vexs);
    /*for(int p = 0; p < NbPart; ++p){
        for(int i = 0; i < L - Vexs[p] + 1; ++i){
            for(int j = 0; j < Vexs[p]; ++j){
                currentOccupations[p][i] += NOOON (1.0 / (double) Vexs[p]) * currentDensities[p][i+j];
            }
        }
    }*/

    convert_to_dyades(currentDensities, currentDyades, dyades);
    /* ne marchait pas de toute façon
    for(int p = 0; p < NbPart; ++p){
        for(int i = 0; i < L - Vexs[p] + 1; ++i){
            for(int j = 0; j < Vexs[p]; ++j){
                if((((double) j+1) <= dyades[p]) && (dyades[p] <= (double) j+2)){
                    currentDyades[p][i+j+1] += (dyades[p] - ((double) j+1)) * currentDensities[p][i]; // tricky ...
                    currentDyades[p][i+j] += (((double) j+2) - dyades[p]) * currentDensities[p][i];
                }
            }
        }
    } */

    //cerr << "Ok Go !" << endl;
    updatePlotsOnly();
}
void Multiple::updatePlotsOnly(){
    BigPlot->update();

    xpoints.clear();
    int lwin = ui->spinBoxWindow->value();
    int xstart = ui->spinBoxPosition->value(); // starts at 1

    int stopping = L + (ui->radioButtonDyade->isChecked() ? 0 : 1);
    xpoints.resize(stopping); //L
    for(int i = 0; i < stopping; ++i){ // le +1 c'est pour les dyades
        xpoints[i] = i;
    }

    //cerr << L << " " << lwin << " " << xstart << endl;
    //cerr << this->currentDensities.size() << "," << this->currentDensities[0].size() << endl;
    BigSpectre->reset();
    if(ui->radioButtonLeft->isChecked()) {BigSpectre->addData(&(this->currentDensities), "Densities (Left)"); /*cerr << "Left" << endl;*/}
    if(ui->radioButtonOccupancy->isChecked()) {BigSpectre->addData(&(this->currentOccupations), "Densities (Occup)"); /* cerr << "Occup" << endl;*/}
    if(ui->radioButtonDyade->isChecked()) {BigSpectre->addData(&(this->currentDyades), "All Densities (Dyades)"); /* cerr << "Occup" << endl;*/}
    //BigSpectre->addData(&(this->currentDensities), "All Densities");

    //cerr << "spectre ok" << endl;

    if(ui->comboBoxShowType->currentIndex() == NbPart){
        cerr << "All " << endl;
        // whow all curves in the same plot
        ui->lcdNumber->display(0);
        ui->widgetShowOneKind->hide();
        AllCurves.clear();
        BigPlot->detachItems();
        for(int i = 0; i < NbPart; ++i){
            QwtPlotCurve* cv = new QwtPlotCurve("Density");
            cv->setPen(QColor(Qt::darkBlue));
            cv->setStyle(QwtPlotCurve::Lines);
            cv->setRenderHint(QwtPlotItem::RenderAntialiased);

            if(ui->radioButtonLeft->isChecked()) cv->setSamples(&xpoints[0], &(currentDensities[i][0]), L);
            if(ui->radioButtonOccupancy->isChecked()) cv->setSamples(&xpoints[0], &(currentOccupations[i][0]), L);
            if(ui->radioButtonDyade->isChecked()) cv->setSamples(&xpoints[0], &(currentDyades[i][0]), L+1);

            //cv->setSamples(&xpoints[0], &(currentDensities[i][0]), L);
            cv->attach(BigPlot);
            AllCurves.push_back(cv);
        }
        BigPlot->setAxisScale(2, xstart, xstart + lwin-1 + (ui->radioButtonDyade->isChecked() ? 0 : 1), lwin / 10);
        BigPlot->replot();
        BigPlot->show();

    } else {

        ui->lcdNumber->display(Vexs[ui->comboBoxShowType->currentIndex()]);
        ui->widgetShowOneKind->show();
        //cerr << "Stairway to hell" << endl;
        AllCurves.clear();
        BigPlot->detachItems();
        QwtPlotCurve* cv = new QwtPlotCurve("Density");
        cv->setPen(QColor(Qt::darkBlue));
        cv->setStyle(QwtPlotCurve::Lines);
        cv->setRenderHint(QwtPlotItem::RenderAntialiased);
        //cv->setSamples(&xpoints[0], &(currentDensities[ui->comboBoxShowType->currentIndex()][0]), L);

        if(ui->radioButtonLeft->isChecked()) cv->setSamples(&xpoints[0], &(currentDensities[ui->comboBoxShowType->currentIndex()][0]), L);
        if(ui->radioButtonOccupancy->isChecked()) cv->setSamples(&xpoints[0], &(currentOccupations[ui->comboBoxShowType->currentIndex()][0]), L);
        if(ui->radioButtonDyade->isChecked()) cv->setSamples(&xpoints[0], &(currentDyades[ui->comboBoxShowType->currentIndex()][0]), L+1);

        cv->attach(BigPlot);
        AllCurves.push_back(cv);
        BigPlot->setAxisScale(2, xstart, xstart + lwin-1 + (ui->radioButtonDyade->isChecked() ? 0 : 1), lwin / 10);
        BigPlot->replot();
        BigPlot->show();
        //cerr << "Lance-flammes OK" << endl;
    }

    cerr << "Ok Replot " << endl;
}





Multiple::~Multiple()
{
    delete ui;
}
