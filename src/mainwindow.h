#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "common.h"

#include "onepart.h"
#include "multiple.h"

using namespace std;

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    Ui::MainWindow *ui;

    Multiple* multiple;
    OnePart* onepart;

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

};

#endif // MAINWINDOW_H
