#ifndef VDL_H
#define VDL_H
#include <cmath>
#include <vector>
using namespace std;

#include "common.h"

void Vanderlick(vector<double>& pot, int Lgth, double sigma, double mu, vector<double>& to_fill);

#endif // VDL_H
