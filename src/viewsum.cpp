#include "viewsum.h"
#include "ui_viewsum.h"

#include <iostream>
using namespace std;
#include <QFile>
#include <QTextStream>

viewsum::viewsum(QString fichier, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::viewsum),
    fname(fichier)
{
    ui->setupUi(this);

    QObject::connect(ui->LinkButton, SIGNAL(released()), this, SLOT(backToSum()));
    ui->textBrowser->setSource(QUrl::fromLocalFile(fname));
   /* QFile file(fname);
    file.open(QFile::ReadOnly | QFile::Text);
    QTextStream ReadFile(&file);
    ui->textBrowser->setText(ReadFile.readAll());*/
}

viewsum::~viewsum()
{
    delete ui;
}

void viewsum::backToSum(){
    if(ui->textBrowser->isBackwardAvailable())
        ui->textBrowser->backward();
    ui->textBrowser->reload();
}
