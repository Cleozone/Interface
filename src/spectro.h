#define SPECTRO_H
#ifndef SPECTRO_H
#define SPECTRO_H

#include "common.h"

#ifdef QT4
#include <QtGui/QToolBar>
#include <QtGui/QToolButton>
#endif

#ifdef QT5
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#endif




#ifdef UNIX
#include <qwt6.1.0/qwt_color_map.h>
#include <qwt6.1.0/qwt_plot_spectrogram.h>
#include <qwt6.1.0/qwt_scale_widget.h>
#include <qwt6.1.0/qwt_scale_draw.h>
#include <qwt6.1.0/qwt_plot_zoomer.h>
#include <qwt6.1.0/qwt_plot_panner.h>
#include <qwt6.1.0/qwt_plot_layout.h>
#include <qwt6.1.0/qwt_plot.h>
#include <qwt6.1.0/qwt_plot_spectrogram.h>
#endif

#ifdef WINDOWS
#include "qwt6.1.0/qwt_color_map.h"
#include "qwt6.1.0/qwt_plot_spectrogram.h"
#include "qwt6.1.0/qwt_scale_widget.h"
#include "qwt6.1.0/qwt_scale_draw.h"
#include "qwt6.1.0/qwt_plot_zoomer.h"
#include "qwt6.1.0/qwt_plot_panner.h"
#include "qwt6.1.0/qwt_plot_layout.h"
#include "qwt6.1.0/qwt_plot.h"
#include "qwt6.1.0/qwt_plot_spectrogram.h"
#endif


#include <vector>
#include <iostream>
using namespace std;


class SpectrogramData: public QwtRasterData{

public:
    vector< vector<double> >* content;
    int size_I;
    int size_J;

    SpectrogramData(vector< vector<double> >* _content);
    virtual QwtRasterData *copy() const;
    //virtual QwtDoubleInterval range() const;
    virtual double value(double x, double y) const;
    //virtual QRectF pixelHint (const QRectF &area) const;
    virtual QSize rasterHint (const QRect & ) const ;
    void update();
};


class Plot: public QwtPlot{
//    Q_OBJECT
public:
    Plot(vector< vector<double> > *, QWidget * = NULL);
    void update();
    SpectrogramData* theData;

public slots:
    void showContour(bool _on);
    void showSpectrogram(bool _on);

private:
    QwtPlotSpectrogram *d_spectrogram;
};

class spectroWidget : public QWidget {
//        Q_OBJECT
    public:
        spectroWidget(vector< vector<double> >*, QWidget* parent = NULL);
        void update();
    private:
        Plot *d_plot;
};



#endif


