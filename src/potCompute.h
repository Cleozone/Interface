#ifndef POT_COMPUTE_H
#define POT_COMPUTE_H

#include <string>
void createMatRoll();
void createMatTwist();
vector<float> comp_pot(std::string DNA, int A, int C, float tw, int /*float*/ l, float phi);

#endif
