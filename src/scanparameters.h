#ifndef SCANPARAMETERS_H
#define SCANPARAMETERS_H

#include <QDialog>
#include <QComboBox>
#include <vector>
using namespace std;

namespace Ui {
    class ScanParameters;
}

class ScanParameters : public QDialog
{
    Q_OBJECT

public:
    explicit ScanParameters(int _nb_part, vector<double> &_chosenParameters, QWidget *parent = 0);
    ~ScanParameters();

private:
    Ui::ScanParameters *ui;
    int nb_part;
    vector<double> *chosenParameters;

public slots:
    void check_values(double);
    void param1Changed(QString);
    void param2Changed(QString);
    void go();
};

#endif // SCANPARAMETERS_H
