#ifndef PLOTMAINSCREEN_H
#define PLOTMAINSCREEN_H

#include <QDialog>
#include <QFileDialog>
#include <vector>
#include "common.h"



#ifdef UNIX
#include <qwt6.1.0/qwt_plot.h>
#include <qwt6.1.0/qwt.h>
#include <qwt6.1.0/qwt_plot_curve.h>
#endif

#ifdef WINDOWS
#include "qwt6.1.0/qwt_plot.h"
#include "qwt6.1.0/qwt.h"
#include "qwt6.1.0/qwt_plot_curve.h"
#endif

#include <iostream>
#include <fstream>
using namespace std;

namespace Ui {
    class PlotMainScreen;
}

class PlotMainScreen : public QDialog
{
    Q_OBJECT

private:
        Ui::PlotMainScreen *ui;

public:
    explicit PlotMainScreen(int _L, int _real_starting_position, QString dir, QWidget *parent = 0);
    ~PlotMainScreen();


    void updateP1(int start, vector<double> &source);
    void updateP2(int start, vector<double> &source);
    void updateDATA(int start, vector<double> &source);
    void updateDATAocc(int start, vector<double> &source);
    void updateGP1(int start, vector<double> &source);
    void updateGP2(int start, vector<double> &source);
    void updateGP1occ(int start, vector<double> &source);
    void updateGP2occ(int start, vector<double> &source);
    void updateVDL(int start, vector<double> &source);
    void updateVDLocc(int start, vector<double> &source);
    void writeToFile(QString location);

private:
    QwtPlot* BigPlot;
    QwtPlotCurve* curveDensity1;
    QwtPlotCurve* curveDensity2;
    QwtPlotCurve* curveDensityVDL;
    QwtPlotCurve* curveData;
    QwtPlotCurve* curvePotential1;
    QwtPlotCurve* curvePotential2;
    QwtPlotCurve* curveOccupancy1;
    QwtPlotCurve* curveOccupancy2;
    QwtPlotCurve* curveOccupancyVDL;
    QwtPlotCurve* curveOccupancyData;
    QwtPlotCurve* curveDataAtWish;

    QString directory;



    int L;
    int position;
    int viewing_window;
    int real_starting_position;

    // souce (inputs)
    vector<double> Potential1;
    vector<double> Potential2;
    vector<double> loaded_data;
    vector<double> converted_to_occupancy_data;
public:
    vector<double> density_p1;
    vector<double> density_p2;
    vector<double> occupancy_p1;
    vector<double> occupancy_p2;
private:
    vector<double> density_VDL;
    vector<double> occupancy_VDL;

    // May be modified by the script :
    vector<double> OutPotential1;
    vector<double> OutPotential2;
    vector<double> Outloaded_data;
    vector<double> Outconverted_to_occupancy_data;
    vector<double> Outdata_at_wish;
    vector<double> Outdensity_p1;
    vector<double> Outdensity_p2;
    vector<double> Outoccupancy_p1;
    vector<double> Outoccupancy_p2;
    vector<double> Outdensity_VDL;
    vector<double> Outoccupancy_VDL;

    vector<double> xpoints;



public slots:
    void textChanged();
    void doScript(int zz = 0);
    void replot(int zz = 0);
    void PositionChangedFromSlider(int new_one);
    void PositionChangedFromSpinBox(int new_one);
    void ViewingWindowChanged(int new_one);
};

#endif // PLOTMAINSCREEN_H
