#ifndef SPECTRE_H
#define SPECTRE_H

#include <QWidget>

#include <qwt6.1.0/qwt_color_map.h>
#include <qwt6.1.0/qwt_plot_spectrogram.h>
#include <qwt6.1.0/qwt_scale_widget.h>
#include <qwt6.1.0/qwt_scale_draw.h>
#include <qwt6.1.0/qwt_plot_zoomer.h>
#include <qwt6.1.0/qwt_plot_panner.h>
#include <qwt6.1.0/qwt_plot_layout.h>
#include <qwt6.1.0/qwt_plot.h>
#include <qwt6.1.0/qwt_plot_spectrogram.h>

#include <vector>
using namespace std;

namespace Ui {
    class Spectre;
}

class SpectrogramData: public QwtRasterData {
public:
    vector< vector<double> >* myContent;
    int size_I;
    int size_J;
    SpectrogramData(vector< vector<double> >* _content);
    virtual double value(double x, double y) const;
    void update();  // to update the borders ??
    virtual QwtRasterData *copy() const;
    // virtual QwtDoubleInterval range() const;
    // virtual QRectF pixelHint (const QRectF &area) const;
    // virtual QSize rasterHint (const QRect & ) const ;
};

/*  Principe : une classe pour afficher des données 3D (f(x,y) = z), comprises entre 0 et 1,
 *      utilisation :
 *          new Spectre();
 *          addData(vector< vector<double> >* , QString name)   // peut $etre employé plusieurs fois
 *          update();
 *          show() and hide() already implemented
 *          reset()
 **/

class Spectre : public QWidget {
    Q_OBJECT

public:
    explicit Spectre(QWidget *parent = 0);
    ~Spectre();
    void reset();
    void addData(vector< vector<double> >* , QString name);
    void update();

//private:
    Ui::Spectre *ui;
    QVector<SpectrogramData*> Donnees;
    SpectrogramData* currentDataShowed;
    QVector<QString> Names;
    QwtPlot* Dessin;
    QwtPlotSpectrogram* Spectro;
    float Contrast;

public slots :
    void fill(int);
    void contours(int);
    void contrastChanged(int);
    void choiceChanged(int);
    void exporting();
};



#endif // SPECTRE_H
