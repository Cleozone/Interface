#ifndef COMMON_H
#define COMMON_H

// common headers for all files of the project

// Note that this may be required : in linux
// sudo apt-get install build-essential g++

#define QT5

// WINDOWS / UNIX / MAC
#define WINDOWS

#define HOME "/home"

// if you want to change the location of R (for plotting main script)
// #define R_COMMAND "R"

// Note that, additionnaly, you have to check that #define COMMAND_LINE is uncommented in 4algos.h

#endif // COMMON_H

