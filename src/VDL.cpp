#include "VDL.h"

#define BETA 1      /*Temperature*/
#define LAMBDA 1 /*0.398942 normalisation 1/sqrt(2*pi)*/

void Vanderlick(vector<double>& pot, int Lgth, double sigma, double mu, vector<double>& to_fill)
{
    int j,n, L, k,ind;
    //double Somme(SIGNAL s, int a, int b);
    double S_0,S_1 = 0,S_2 = 0,S_3,S_1_new;

    /*Calcul des parametres*/
    L= max(0,min((int) pot.size(), Lgth));
    n = (int) L/sigma;
    vector<double> h(L, 0.0);
    vector<double> g(L, 0.0);

    L--; /*L=L-1 so that it makes things clearer*/

    /*First step: evaluating h_0*/
    S_0=0;

    
    for (k=1; k<=sigma;k++)
    {
        h[k] =exp(-BETA*(pot[k]))/(exp(-BETA*mu)+S_0);
        S_0=S_0+(exp(-BETA*pot[k])+exp(-BETA*pot[k+1]))/2;
    }

    /*Second step evaluating the rest of h*/
    for (j=1; j <n;j++)
    {

        S_1_new=(h[(j-1)*(int) sigma]+h[(j-1)*(int) sigma+1])/2;
        S_1=0;
        S_2=(exp(S_1-BETA*pot[j*(int)sigma])+exp(S_1_new-BETA*pot[j*(int) sigma+1]))/2;
        S_1=S_1_new;

        for (k=(int) j*sigma+1;k<=min((j+1)*(int) sigma,L-1); k++)
        {
            h[k] = exp(-BETA*pot[k]+S_1);
            h[k]/= exp(-BETA*pot[(int) (j*sigma)])/(h[(int) (j*sigma)])+S_2;


            S_1_new=S_1+(h[k-(int) sigma]+h[k-(int) sigma+1])/2;
            S_2+=(exp(S_1_new-BETA*pot[k+1])+exp(S_1-BETA*pot[k]))/2;
            S_1=S_1_new;
        }

    }
    /*The last index has to be treated a little bit differently or
    * else there might be out of domain problems*/
    j--;        /*The index j has been incremented before the test j<n!!*/
    h[L]=exp(-BETA*pot[L]+S_1);
    h[L]/=exp(-BETA*pot[(int) (j*sigma)])/(h[(int) (j*sigma)])+S_2;

    /*Et zou on repart dans l'autre sens pour calculer l*/
    /*First step: evaluating g_0*/
    S_0=0;
    for (k=L-1; k >=L-sigma; k--)
    {
        g[k]=exp(-S_0);
        S_0+=0.5*(h[k-1]+h[k]);
    }

    /*Second step rest of g*/
    for (j=1; j <n;j++)
    {
        /*Une petite variable en plus qui fait pas mal au seins*/
        ind=L-j*(int) sigma;
        S_1=0.5*(h[ind]+h[ind-1]);
        S_2=0.5*(g[ind+(int) sigma]*h[ind+(int) sigma]*exp(-S_1)+g[ind-1+(int) sigma]*h[ind-1+(int) sigma]);

        for (k=(int) (L-j*sigma-1); k >= max(L-(j+1)*(int) sigma,1); k--)
        {

            g[k]=g[(int) (L-j*sigma)]*exp(-S_1);
            g[k]+=S_2;

            S_1+=0.5*(h[k]+h[k-1]);
            S_3= 0.5*(h[k]+h[k-1]);
            S_2=exp(-S_3)*S_2+0.5*(g[k+(int) sigma]*h[k+(int) sigma]*exp(-S_3)+g[k-1+(int) sigma]*h[k-1+(int) sigma]);

        }
    }
    /*Again there need to be some specification for the last step*/
    j--;
    g[0]=g[(int) (L-j*sigma)]*exp(-S_1)+S_2;


    to_fill.resize(L+1);
    /*The very last thing: combining g and h*/
    for (k=0; k <= L;k++)
    {
        to_fill[k]=g[k]*h[k];
    }

}
