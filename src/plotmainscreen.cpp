#include "plotmainscreen.h"
#include "ui_plotmainscreen.h"
#include <iomanip>
#include <string>

PlotMainScreen::PlotMainScreen(int _L,int _real_starting_position, QString dir, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlotMainScreen),
    directory(dir)
    //QFileDialog::getExistingDirectory(this, tr("Open Directory"), "/home", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks))
{
    ui->setupUi(this);

    directory.append("/");
    L = _L;
    position = 1;
    viewing_window = 1000;
    real_starting_position = _real_starting_position;
    Potential1.clear();
    Potential2.clear();
    loaded_data.clear();
    converted_to_occupancy_data.clear();
    density_p1.clear();
    density_p2.clear();
    occupancy_p1.clear();
    occupancy_p2.clear();
    density_VDL.clear();
    occupancy_VDL.clear();

    OutPotential1.clear();
    OutPotential2.clear();
    Outloaded_data.clear();
    Outconverted_to_occupancy_data.clear();
    Outdata_at_wish.clear();
    Outdensity_p1.clear();
    Outdensity_p2.clear();
    Outoccupancy_p1.clear();
    Outoccupancy_p2.clear();
    Outdensity_VDL.clear();
    Outoccupancy_VDL.clear();

    Potential1.resize(L, 0.0);
    Potential2.resize(L, 0.0);
    loaded_data.resize(L, 0.0);
    converted_to_occupancy_data.resize(L, 0.0);
    density_p1.resize(L, 0.0);
    density_p2.resize(L, 0.0);
    occupancy_p1.resize(L, 0.0);
    occupancy_p2.resize(L, 0.0);
    density_VDL.resize(L, 0.0);
    occupancy_VDL.resize(L, 0.0);

    OutPotential1.resize(L, 0.0);
    OutPotential2.resize(L, 0.0);
    Outloaded_data.resize(L, 0.0);
    Outconverted_to_occupancy_data.resize(L, 0.0);
    Outdata_at_wish.resize(L, 0.0);
    Outdensity_p1.resize(L, 0.0);
    Outdensity_p2.resize(L, 0.0);
    Outoccupancy_p1.resize(L, 0.0);
    Outoccupancy_p2.resize(L, 0.0);
    Outdensity_VDL.resize(L, 0.0);
    Outoccupancy_VDL.resize(L, 0.0);

    xpoints.resize(L);
    for(int i = 0; i < L; ++i){
        xpoints[i] = i;// + real_starting_position;
    }

    ui->widgetBigPlot->setMinimumHeight(300);
    ui->widgetBigPlot->setMinimumWidth(1000);

    this->BigPlot = new QwtPlot(ui->widgetBigPlot);
    this->BigPlot->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    this->BigPlot->setGeometry(0,0,1000,280);
    this->BigPlot->setMinimumHeight(300);
    this->BigPlot->setMinimumWidth(1000);
    this->BigPlot->setMaximumHeight(300);
    this->BigPlot->setMaximumWidth(1000);

    curveDensity1 = new QwtPlotCurve("Density1");
    curveDensity2 = new QwtPlotCurve("Density2");
    curveDensity1->setPen(QColor(Qt::darkBlue));
    curveDensity1->setStyle(QwtPlotCurve::Lines);
    curveDensity1->setRenderHint(QwtPlotItem::RenderAntialiased);
    curveDensity2->setPen(QColor(Qt::darkRed));
    curveDensity2->setStyle(QwtPlotCurve::Lines);
    curveDensity2->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveDensityVDL = new QwtPlotCurve("density 1st particles kind (VanDerLick)");
    curveDensityVDL->setPen(QColor(Qt::darkYellow));
    curveDensityVDL->setStyle(QwtPlotCurve::Lines);
    curveDensityVDL->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveData = new QwtPlotCurve("Data");
    curveData->setPen(QColor(Qt::darkGreen));
    curveData->setStyle(QwtPlotCurve::Lines);
    curveData->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveDataAtWish = new QwtPlotCurve("DataAtWish");
    curveDataAtWish->setPen(QColor(Qt::darkCyan));
    curveDataAtWish->setStyle(QwtPlotCurve::Lines);
    curveDataAtWish->setRenderHint(QwtPlotItem::RenderAntialiased);

    curvePotential1 = new QwtPlotCurve("Potential1");
    curvePotential1->setPen(QColor(Qt::darkCyan));
    curvePotential1->setStyle(QwtPlotCurve::Lines);
    curvePotential1->setRenderHint(QwtPlotItem::RenderAntialiased);
    curvePotential2 = new QwtPlotCurve("Potential2");
    curvePotential2->setPen(QColor(Qt::gray));
    curvePotential2->setStyle(QwtPlotCurve::Lines);
    curvePotential2->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveOccupancy1 = new QwtPlotCurve("Occupancy1");
    curveOccupancy2 = new QwtPlotCurve("Occupancy2");
    curveOccupancy1->setPen(QColor(Qt::blue));
    curveOccupancy1->setStyle(QwtPlotCurve::Lines);
    curveOccupancy1->setRenderHint(QwtPlotItem::RenderAntialiased);
    curveOccupancy2->setPen(QColor(Qt::red));
    curveOccupancy2->setStyle(QwtPlotCurve::Lines);
    curveOccupancy2->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveOccupancyData = new QwtPlotCurve("DataConverted");
    curveOccupancyData->setPen(QColor(Qt::green));
    curveOccupancyData->setStyle(QwtPlotCurve::Lines);
    curveOccupancyData->setRenderHint(QwtPlotItem::RenderAntialiased);

    curveOccupancyVDL = new QwtPlotCurve("DataConverted");
    curveOccupancyVDL->setPen(QColor(Qt::green));
    curveOccupancyVDL->setStyle(QwtPlotCurve::Lines);
    curveOccupancyVDL->setRenderHint(QwtPlotItem::RenderAntialiased);

    ui->spinBoxStart->setMinimum(1);
    ui->spinBoxStart->setMaximum(max(1, L - viewing_window + 1));
    ui->spinBoxStart->setValue(position);
    ui->horizontalSlider->setMinimum(1);
    ui->horizontalSlider->setMaximum(max(1, L - viewing_window + 1));
    ui->horizontalSlider->setValue(position);
    ui->spinBoxWindow->setMinimum(1);
    ui->spinBoxWindow->setMaximum(L);
    ui->spinBoxWindow->setValue(viewing_window);

    QObject::connect(ui->spinBoxStart,      SIGNAL(valueChanged(int)), this, SLOT(PositionChangedFromSpinBox(int)));
    QObject::connect(ui->horizontalSlider,  SIGNAL(sliderMoved(int)),  this, SLOT(PositionChangedFromSlider(int)));
    QObject::connect(ui->spinBoxWindow,     SIGNAL(valueChanged(int)), this, SLOT(ViewingWindowChanged(int)));

    QObject::connect(ui->plainTextEditScript, SIGNAL(textChanged()),   this, SLOT(textChanged()));

    QObject::connect(ui->checkAtWish,       SIGNAL(stateChanged(int)), this, SLOT(replot(int)));
    QObject::connect(ui->checkBoxDataOcc,   SIGNAL(stateChanged(int)), this, SLOT(replot(int)));
    QObject::connect(ui->checkBoxGP1,       SIGNAL(stateChanged(int)), this, SLOT(replot(int)));
    QObject::connect(ui->checkBoxGP1occ,    SIGNAL(stateChanged(int)), this, SLOT(replot(int)));
    QObject::connect(ui->checkBoxGP2,       SIGNAL(stateChanged(int)), this, SLOT(replot(int)));
    QObject::connect(ui->checkBoxGP2occ,    SIGNAL(stateChanged(int)), this, SLOT(replot(int)));
    QObject::connect(ui->checkBoxVDL,       SIGNAL(stateChanged(int)), this, SLOT(replot(int)));
    QObject::connect(ui->checkBoxVDLocc,    SIGNAL(stateChanged(int)), this, SLOT(replot(int)));
    QObject::connect(ui->checkData,         SIGNAL(stateChanged(int)), this, SLOT(replot(int)));
    QObject::connect(ui->checkP1,           SIGNAL(stateChanged(int)), this, SLOT(replot(int)));
    QObject::connect(ui->checkP2,           SIGNAL(stateChanged(int)), this, SLOT(replot(int)));

    QObject::connect(ui->pushButtonScript,  SIGNAL(released()),        this, SLOT(doScript()));
}

PlotMainScreen::~PlotMainScreen()
{
    delete ui;
}

void PlotMainScreen::replot(int zz){
    zz++;   // avoid warning !!
    if(ui->checkBoxGP1->isChecked())    curveDensity1->setSamples(     &xpoints[position - 1], &Outdensity_p1[position - 1], min(viewing_window, max(0, (int) Outdensity_p1.size() - position + 1)));
        else                            curveDensity1->setSamples(     &xpoints[position - 1], &Outdensity_p1[position - 1], 0);
    if(ui->checkBoxGP2->isChecked())    curveDensity2->setSamples(     &xpoints[position - 1], &Outdensity_p2[position - 1], min(viewing_window, max(0, (int) Outdensity_p2.size() - position + 1)));
        else                            curveDensity2->setSamples(     &xpoints[position - 1], &Outdensity_p2[position - 1], 0);
    if(ui->checkBoxGP1occ->isChecked()) curveOccupancy1->setSamples(   &xpoints[position - 1], &Outoccupancy_p1[position - 1], min(viewing_window, max(0, (int) Outoccupancy_p1.size() - position + 1)));
        else                            curveOccupancy1->setSamples(   &xpoints[position - 1], &Outoccupancy_p1[position - 1], 0);
    if(ui->checkBoxGP2occ->isChecked()) curveOccupancy2->setSamples(   &xpoints[position - 1], &Outoccupancy_p2[position - 1], min(viewing_window, max(0, (int) Outoccupancy_p2.size() - position + 1)));
        else                            curveOccupancy2->setSamples(   &xpoints[position - 1], &Outoccupancy_p2[position - 1], 0);
    if(ui->checkData->isChecked())      curveData->setSamples(         &xpoints[position - 1], &Outloaded_data[position-1], min(viewing_window, max(0, (int) Outloaded_data.size() - position + 1)));
        else                            curveData->setSamples(         &xpoints[position - 1], &Outloaded_data[position-1], 0);
    if(ui->checkBoxDataOcc->isChecked())curveOccupancyData->setSamples(&xpoints[position - 1], &Outconverted_to_occupancy_data[position-1], min(viewing_window, max(0, (int) Outconverted_to_occupancy_data.size() - position + 1)));
        else                            curveOccupancyData->setSamples(&xpoints[position - 1], &Outconverted_to_occupancy_data[position-1], 0);
    if(ui->checkBoxVDL->isChecked())    curveDensityVDL->setSamples(   &xpoints[position - 1], &Outdensity_VDL[position-1], min(viewing_window, max(0, (int) Outdensity_VDL.size() - position + 1)));
        else                            curveDensityVDL->setSamples(   &xpoints[position - 1], &Outdensity_VDL[position-1],0);
    if(ui->checkBoxVDLocc->isChecked()) curveOccupancyVDL->setSamples( &xpoints[position - 1], &Outoccupancy_VDL[position-1], min(viewing_window, max(0, (int) Outoccupancy_VDL.size() - position + 1)));
        else                            curveOccupancyVDL->setSamples( &xpoints[position - 1], &Outoccupancy_VDL[position-1], 0);
    if(ui->checkP1->isChecked())        curvePotential1->setSamples(   &xpoints[position - 1], &OutPotential1[position-1], min(viewing_window, max(0, (int) OutPotential1.size() - position + 1)));
        else                            curvePotential1->setSamples(   &xpoints[position - 1], &OutPotential1[position-1], 0);
    if(ui->checkP2->isChecked())        curvePotential2->setSamples(   &xpoints[position - 1], &OutPotential2[position-1], min(viewing_window, max(0, (int) OutPotential2.size() - position + 1)));
        else                            curvePotential2->setSamples(   &xpoints[position - 1], &OutPotential2[position-1], 0);
    if(ui->checkAtWish->isChecked())    curveDataAtWish->setSamples(   &xpoints[position - 1], &Outdata_at_wish[position-1], min(viewing_window, max(0, (int) Outdata_at_wish.size() - position + 1)));
        else                            curveDataAtWish->setSamples(   &xpoints[position - 1], &Outdata_at_wish[position-1], 0);

    curveDensity1->attach(this->BigPlot);
    curveDensity2->attach(this->BigPlot);
    curveOccupancy1->attach(this->BigPlot);
    curveOccupancy2->attach(this->BigPlot);
    curveData->attach(this->BigPlot);
    curveOccupancyData->attach(this->BigPlot);
    curveDensityVDL->attach(this->BigPlot);
    curveOccupancyVDL->attach(this->BigPlot);
    curvePotential1->attach(this->BigPlot);
    curvePotential2->attach(this->BigPlot);
    curveDataAtWish->attach(this->BigPlot);

    //ui->qwtPlotMainPlot->setAxisScale(0, 0, 1, 0.2);
    //ui->qwtPlotMainPlot->setAxisScale(1, 0, 1, 0.2);
    this->BigPlot->setAxisScale(2, 1 + position - 1, max(1, min(L-1, position + viewing_window - 1)), viewing_window / 10);
    this->BigPlot->setAxisScale(3, 1 + position - 1, max(1, min(L-1, position + viewing_window - 1)), viewing_window / 10);

    this->BigPlot->replot();

}

void PlotMainScreen::textChanged(){
    ui->plainTextEditScript->setBackgroundRole(QPalette::Text);
    ui->plainTextEditScript->setStyleSheet("QPlainText { background-color: white; }");
}

void PlotMainScreen::writeToFile(QString location){
    ofstream fichier(location.toStdString().c_str(), ios::out);
    if(fichier){
        fichier << "Position\tPotential1\tPotential2\tData\tDataToOccopancy\tGraphP1\tGraphP2\tGP1occupancy\tGP2occupancy\tVDL\tVDLOccupancy" << endl;
        for(int i = 0; i < L; ++i){
            fichier << i + this->real_starting_position << "\t";
            fichier << setprecision (10) << Potential1[i] << "\t";
            fichier << setprecision (10) << Potential2[i] << "\t";
            fichier << setprecision (10) << loaded_data[i] << "\t";
            fichier << setprecision (10) << converted_to_occupancy_data[i] << "\t";
            fichier << setprecision (10) << density_p1[i] << "\t";
            fichier << setprecision (10) << density_p2[i] << "\t";
            fichier << setprecision (10) << occupancy_p1[i] << "\t";
            fichier << setprecision (10) << occupancy_p2[i] << "\t";
            fichier << setprecision (10) << density_VDL[i] << "\t";
            fichier << setprecision (10) << occupancy_VDL[i] << "\n";
            //cerr << setprecision (10) << density_p1[i] << "\t";
        }
        fichier.close();
    } else cerr << "ERR before launching R script(" << location.toStdString() << ") : Impossible d'ouvrir le fichier " << endl;
}

void PlotMainScreen::doScript(int zz){
    zz++; // avoid warning
    cerr << "Doing Script " << endl;
    QString location1(directory);
    location1.append("TablesIn.txt");

    cerr << location1.toStdString() << endl;
    //cerr << location1.toStdString() << endl;
    ofstream fichier(location1.toStdString().c_str(), ios::out);
    if(fichier){
        fichier << "Position\tPotential1\tPotential2\tData\tDataToOccopancy\tGraphP1\tGraphP2\tGP1occupancy\tGP2occupancy\tVDL\tVDLOccupancy" << endl;
        for(int i = 0; i < L; ++i){
            fichier << i + this->real_starting_position << "\t";
            fichier << setprecision (10) << Potential1[i] << "\t";
            fichier << setprecision (10) << Potential2[i] << "\t";
            fichier << setprecision (10) << loaded_data[i] << "\t";
            fichier << setprecision (10) << converted_to_occupancy_data[i] << "\t";
            fichier << setprecision (10) << density_p1[i] << "\t";
            fichier << setprecision (10) << density_p2[i] << "\t";
            fichier << setprecision (10) << occupancy_p1[i] << "\t";
            fichier << setprecision (10) << occupancy_p2[i] << "\t";
            fichier << setprecision (10) << density_VDL[i] << "\t";
            fichier << setprecision (10) << occupancy_VDL[i] << "\n";
            //cerr << setprecision (10) << density_p1[i] << "\t";
        }
        fichier.close();
    } else cerr << "ERR before launching R script(" << location1.toStdString() << ") : Impossible d'ouvrir le fichier " << endl;

    // If there is no script, skip the R calling
    if(ui->plainTextEditScript->toPlainText().toStdString().size() < 1){
        for(int i = 0; i < L; ++i){
            OutPotential1[i] = Potential1[i];
            OutPotential2[i] = Potential2[i];
            Outloaded_data[i] = loaded_data[i];
            Outconverted_to_occupancy_data[i] = converted_to_occupancy_data[i];
            Outdensity_p1[i] = density_p1[i];
            Outdensity_p2[i] = density_p2[i];
            Outoccupancy_p1[i] = occupancy_p1[i];
            Outoccupancy_p2[i] = occupancy_p2[i];
            Outdensity_VDL[i] = density_VDL[i];
            Outoccupancy_VDL[i] = occupancy_VDL[i];
            Outdata_at_wish[i] = 0;
        }
        cerr << "Info : Empty R-script, R not called\n";
        ui->plainTextEditScript->setBackgroundRole(QPalette::Button);
        ui->plainTextEditScript->setStyleSheet("QComboBox { background-color: grey; }");
        replot();
        return;
    }

    QString location2(directory);
    location2.append("Script.R");

    QString location3(directory);
    location3.append("TablesOut.txt");

    //cerr << location2.toStdString() << endl;
    ofstream fichier2(location2.toStdString().c_str(), ios::out);
    if(fichier2){
        fichier2 << "# ---------- R Script used to modify data/results before plotting ----------- : " << endl;
        fichier2 << "# --- first part, generated automatically, to load data" << endl;
        fichier2 << "BigTable <- read.table(\"" << location1.toStdString() << "\", header=TRUE);" << endl;
        fichier2 << "Data <- BigTable$Data" << endl;
        fichier2 << "Dataocc <- BigTable$DataToOccopancy" << endl;
        fichier2 << "P1 <- BigTable$Potential1" << endl;
        fichier2 << "P2 <- BigTable$Potential2" << endl;
        fichier2 << "VDL <- BigTable$VDL" << endl;
        fichier2 << "GP1 <- BigTable$GraphP1" << endl;
        fichier2 << "GP2 <- BigTable$GraphP2" << endl;
        fichier2 << "VDLocc <- BigTable$VDLOccupancy" << endl;
        fichier2 << "GP1occ <- BigTable$GP1occupancy" << endl;
        fichier2 << "GP2occ <- BigTable$GP2occupancy" << endl;

        fichier2 << "# --- by default, output values are taken as the same" << endl;
        fichier2 << "OutData <- BigTable$Data" << endl;
        fichier2 << "OutDataocc <- BigTable$DataToOccopancy" << endl;
        fichier2 << "OutP1 <- BigTable$Potential1" << endl;
        fichier2 << "OutP2 <- BigTable$Potential2" << endl;
        fichier2 << "OutVDL <- BigTable$VDL" << endl;
        fichier2 << "OutGP1 <- BigTable$GraphP1" << endl;
        fichier2 << "OutGP2 <- BigTable$GraphP2" << endl;
        fichier2 << "OutVDLocc <- BigTable$VDLOccupancy" << endl;
        fichier2 << "OutGP1occ <- BigTable$GP1occupancy" << endl;
        fichier2 << "OutGP2occ <- BigTable$GP2occupancy" << endl;
        fichier2 << "OutAtWish <- BigTable$Data" << endl;
        fichier2 << "L = " << L << endl;
        fichier2 << "Pos = " << this->real_starting_position << endl;

        fichier2 << "# --- Second part : customised by the user" << endl;
        fichier2 << ui->plainTextEditScript->toPlainText().toStdString() << endl;

        fichier2 << "# --- Third part : writing Out Tables into file" << endl;
        fichier2 << "Output = data.frame(Potential1 = OutP1, Potential2 = OutP2, Data = OutData, DataToOccopancy = OutDataocc, GraphP1 = OutGP1, GraphP2 = OutGP2, GP1occupancy = OutGP1occ, GP2occupancy = OutGP2occ, VDL = OutVDL, VDLOccupancy = OutVDLocc, DataAtWish = OutAtWish);" << endl;
        fichier2 << "write.table(Output, \"" << location3.toStdString() << "\");" << endl;
        /*

                Available as inputs in the script :

                L	Number of positions
                Pos	Real position of the first value

                Data	Table of loaded data


                P1	Table of potential (1st particles)
                P2	Table of potential (2nd particles)
                VDL	Starting density with Van Der Lick Algorithm
                GP1	Starting density (1st particles)
                GP2	Starting density (2nd particles)
                VDLocc	Occupancy with Van Der Lick Algorithm
                GP1occ	Occupancy (1st particles)
                GP2occ	Occupancy (2nd particles)


                Output tables that you must fill :

                OutData	Table of loaded data
                OutP1	Table of potential (1st particles)
                OutP2	Table of potential (2nd particles)
                OutVDL	Starting density with Van Der Lick Algorithm
                OutGP1	Starting density (1st particles)
                OutGP2	Starting density (2nd particles)
                OutVDLocc	Occupancy with Van Der Lick Algorithm
                OutGP1occ	Occupancy (1st particles)
                OutGP2occ	Occupancy (2nd particles)
                OutAtWish	Table for plotting an additional curve

                By default, every 'Out...' table is taken as the input corresponding
                table. Don't do anything for curves you don't want to change.
        */

        fichier2.close();
    } else cerr << "ERR before launching R script(" << location2.toStdString() << ") : Impossible d'ouvrir le fichier " << endl;

    QString RMcommand("rm ");
    RMcommand.append(location3);
    system(RMcommand.toStdString().c_str());

    QString Rcommand("R < ");
#ifdef R_COMMAND
    Rcommand.clear();
    Rcommand.append(R_COMMAND);
    Rcommand.append(" < ");
#endif

    Rcommand.append(location2);
    Rcommand.append(" --vanilla > Trash.txt\n");
    system(Rcommand.toStdString().c_str());


    //cerr << location1.toStdString() << endl;
    ifstream fichier3(location3.toStdString().c_str(), ios::in);
    string trash;
    if(fichier3){
        for(int i = 0; i < 11; ++i)
            fichier3 >> trash;
        int j = 0;
        while(fichier3 >> trash){    //position to trash
            //cerr << "trash " << trash << endl;

            //for(int i = 0; (i < 11) && (j < L); ++i){
                fichier3 >> OutPotential1[j];
                fichier3 >> OutPotential2[j];
                fichier3 >> Outloaded_data[j];
                fichier3 >> Outconverted_to_occupancy_data[j];
                fichier3 >> Outdensity_p1[j];
                //cerr << Outdensity_p1[j] << "\t";
                fichier3 >> Outdensity_p2[j];
                fichier3 >> Outoccupancy_p1[j];
                fichier3 >> Outoccupancy_p2[j];
                fichier3 >> Outdensity_VDL[j];
                fichier3 >> Outoccupancy_VDL[j];
                fichier3 >> Outdata_at_wish[j];
            //}
            j++;
        }
        fichier3.close();
    } else cerr << "ERR after launching R script(" << location3.toStdString() << ") : Impossible d'ouvrir le fichier " << endl;

    ui->plainTextEditScript->setBackgroundRole(QPalette::Button);
    ui->plainTextEditScript->setStyleSheet("QComboBox { background-color: grey; }");
    replot();
}

void PlotMainScreen::updateP1(int start, vector<double> &source){
    int givensize = source.size();
    //cerr << "updateP1 from " << start << endl;
    for(int i = max(0,start); i < min(start + givensize, L); ++i)
        Potential1[i] = source[i-start];
}

void PlotMainScreen::updateP2(int start, vector<double> &source){
    int givensize = source.size();
    //cerr << "updateP2" << endl;
    for(int i = max(0,start); i < min(start + givensize, L); ++i)
    //    {  cerr << source[i-start] << ", ";
        Potential2[i] = source[i-start];
    //     } cerr << endl;
}
void PlotMainScreen::updateDATA(int start, vector<double> &source){
    int givensize = source.size();
    //cerr << "updateData" << endl;
    for(int i = max(0,start); i < min(start + givensize, L); ++i)
    //    {  cerr << source[i-start] << ", ";
        loaded_data[i] = source[i-start];
    //     } cerr << endl;
}
void PlotMainScreen::updateDATAocc(int start, vector<double> &source){
    int givensize = source.size();
    //cerr << "updateDataOcc" << endl;
    for(int i = max(0,start); i < min(start + givensize, L); ++i)
    //    {  cerr << source[i-start] << ", ";
        converted_to_occupancy_data[i] = source[i-start];
    //     } cerr << endl;
}
void PlotMainScreen::updateGP1(int start, vector<double> &source){
    int givensize = source.size();
    //cerr << "updateGP1" << endl;
    for(int i = max(0,start); i < min(start + givensize, L); ++i)
    //    {  cerr << source[i-start] << ", ";
        density_p1[i] = source[i-start];
    //     } cerr << endl;
}
void PlotMainScreen::updateGP2(int start, vector<double> &source){
    int givensize = source.size();
    //cerr << "updateGP2" << endl;
    for(int i = max(0,start); i < min(start + givensize, L); ++i)
    //    {  cerr << source[i-start] << ", ";
        density_p2[i] = source[i-start];
    //     } cerr << endl;
}
void PlotMainScreen::updateGP1occ(int start, vector<double> &source){
    int givensize = source.size();
    //cerr << "updateGP1occ" << endl;
    for(int i = max(0,start); i < min(start + givensize, L); ++i)
    //    {  cerr << source[i-start] << ", ";
        occupancy_p1[i] = source[i-start];
    //     } cerr << endl;
}
void PlotMainScreen::updateGP2occ(int start, vector<double> &source){
    int givensize = source.size();
    //cerr << "updateGP2occ" << endl;
    for(int i = max(0,start); i < min(start + givensize, L); ++i)
    //    {  cerr << source[i-start] << ", ";
        occupancy_p2[i] = source[i-start];
    //     } cerr << endl;
}
void PlotMainScreen::updateVDL(int start, vector<double> &source){
    int givensize = source.size();
    //cerr << "updateVDL" << endl;
    for(int i = max(0,start); i < min(start + givensize, L); ++i)
    //    {  cerr << source[i-start] << ", ";
        density_VDL[i] = source[i-start];
    //     } cerr << endl;
}
void PlotMainScreen::updateVDLocc(int start, vector<double> &source){
    int givensize = source.size();
    //cerr << "updateVDLocc" << endl;
    for(int i = max(0,start); i < min(start + givensize, L); ++i)
    //    {  cerr << source[i-start] << ", ";
        occupancy_VDL[i] = source[i-start];
    //     } cerr << endl;
}

void PlotMainScreen::PositionChangedFromSlider(int new_one){
    position = new_one;
    ui->spinBoxStart->setValue(new_one);
    replot();
}

void PlotMainScreen::PositionChangedFromSpinBox(int new_one){
    position = new_one;
    ui->horizontalSlider->setValue(new_one);
    replot();
}

void PlotMainScreen::ViewingWindowChanged(int new_one){
    viewing_window = new_one;
    ui->spinBoxStart->setMaximum(L - new_one + 1);
    ui->horizontalSlider->setMaximum(L - new_one + 1);
    replot();
}



