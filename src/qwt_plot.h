#ifndef QWT_PLOT_H
#define QWT_PLOT_H

// because the 'graphical forms' are transformed into ui_xxx.h, which are looking for "qwt-plot.h"
#include "qwt6.1.0/qwt_plot.h"
#endif // QWT_PLOT_H
