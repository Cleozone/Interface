QT += printsupport

QT += opengl

QT += svg

OTHER_FILES += Long.txt \
    Numbers.txt \
    qwt6.1.0/src.pro \
    qwt6.1.0/src.pri
HEADERS += mainwindow.h \
    delegate.h \
    askpotential.h \
    function.h \
    4algos.h \
    VDL.h \
    common.h \
    plotmainscreen.h \
    qwt6.1.0/qwt_widget_overlay.h \
    qwt6.1.0/qwt_wheel.h \
    qwt6.1.0/qwt_transform.h \
    qwt6.1.0/qwt_thermo.h \
    qwt6.1.0/qwt_text_label.h \
    qwt6.1.0/qwt_text_engine.h \
    qwt6.1.0/qwt_text.h \
    qwt6.1.0/qwt_system_clock.h \
    qwt6.1.0/qwt_symbol.h \
    qwt6.1.0/qwt_spline.h \
    qwt6.1.0/qwt_slider.h \
    qwt6.1.0/qwt_series_store.h \
    qwt6.1.0/qwt_series_data.h \
    qwt6.1.0/qwt_scale_widget.h \
    qwt6.1.0/qwt_scale_map.h \
    qwt6.1.0/qwt_scale_engine.h \
    qwt6.1.0/qwt_scale_draw.h \
    qwt6.1.0/qwt_scale_div.h \
    qwt6.1.0/qwt_sampling_thread.h \
    qwt6.1.0/qwt_samples.h \
    qwt6.1.0/qwt_round_scale_draw.h \
    qwt6.1.0/qwt_raster_data.h \
    qwt6.1.0/qwt_point_polar.h \
    qwt6.1.0/qwt_point_mapper.h \
    qwt6.1.0/qwt_point_data.h \
    qwt6.1.0/qwt_point_3d.h \
    qwt6.1.0/qwt_plot_zoomer.h \
    qwt6.1.0/qwt_plot_zoneitem.h \
    qwt6.1.0/qwt_plot_tradingcurve.h \
    qwt6.1.0/qwt_plot_textlabel.h \
    qwt6.1.0/qwt_plot_svgitem.h \
    qwt6.1.0/qwt_plot_spectrogram.h \
    qwt6.1.0/qwt_plot_spectrocurve.h \
    qwt6.1.0/qwt_plot_shapeitem.h \
    qwt6.1.0/qwt_plot_seriesitem.h \
    qwt6.1.0/qwt_plot_scaleitem.h \
    qwt6.1.0/qwt_plot_rescaler.h \
    qwt6.1.0/qwt_plot_renderer.h \
    qwt6.1.0/qwt_plot_rasteritem.h \
    qwt6.1.0/qwt_plot_picker.h \
    qwt6.1.0/qwt_plot_panner.h \
    qwt6.1.0/qwt_plot_multi_barchart.h \
    qwt6.1.0/qwt_plot_marker.h \
    qwt6.1.0/qwt_plot_magnifier.h \
    qwt6.1.0/qwt_plot_legenditem.h \
    qwt6.1.0/qwt_plot_layout.h \
    qwt6.1.0/qwt_plot_item.h \
    qwt6.1.0/qwt_plot_intervalcurve.h \
    qwt6.1.0/qwt_plot_histogram.h \
    qwt6.1.0/qwt_plot_grid.h \
    qwt6.1.0/qwt_plot_glcanvas.h \
    qwt6.1.0/qwt_plot_directpainter.h \
    qwt6.1.0/qwt_plot_dict.h \
    qwt6.1.0/qwt_plot_curve.h \
    qwt6.1.0/qwt_plot_canvas.h \
    qwt6.1.0/qwt_plot_barchart.h \
    qwt6.1.0/qwt_plot_abstract_barchart.h \
    qwt6.1.0/qwt_plot.h \
    qwt6.1.0/qwt_pixel_matrix.h \
    qwt6.1.0/qwt_picker_machine.h \
    qwt6.1.0/qwt_picker.h \
    qwt6.1.0/qwt_panner.h \
    qwt6.1.0/qwt_painter_command.h \
    qwt6.1.0/qwt_painter.h \
    qwt6.1.0/qwt_null_paintdevice.h \
    qwt6.1.0/qwt_matrix_raster_data.h \
    qwt6.1.0/qwt_math.h \
    qwt6.1.0/qwt_magnifier.h \
    qwt6.1.0/qwt_legend_label.h \
    qwt6.1.0/qwt_legend_data.h \
    qwt6.1.0/qwt_legend.h \
    qwt6.1.0/qwt_knob.h \
    qwt6.1.0/qwt_interval_symbol.h \
    qwt6.1.0/qwt_interval.h \
    qwt6.1.0/qwt_graphic.h \
    qwt6.1.0/qwt_global.h \
    qwt6.1.0/qwt_event_pattern.h \
    qwt6.1.0/qwt_dyngrid_layout.h \
    qwt6.1.0/qwt_dial_needle.h \
    qwt6.1.0/qwt_dial.h \
    qwt6.1.0/qwt_date_scale_engine.h \
    qwt6.1.0/qwt_date_scale_draw.h \
    qwt6.1.0/qwt_date.h \
    qwt6.1.0/qwt_curve_fitter.h \
    qwt6.1.0/qwt_counter.h \
    qwt6.1.0/qwt_compat.h \
    qwt6.1.0/qwt_compass_rose.h \
    qwt6.1.0/qwt_compass.h \
    qwt6.1.0/qwt_column_symbol.h \
    qwt6.1.0/qwt_color_map.h \
    qwt6.1.0/qwt_clipper.h \
    qwt6.1.0/qwt_arrow_button.h \
    qwt6.1.0/qwt_analog_clock.h \
    qwt6.1.0/qwt_abstract_slider.h \
    qwt6.1.0/qwt_abstract_scale_draw.h \
    qwt6.1.0/qwt_abstract_scale.h \
    qwt6.1.0/qwt_abstract_legend.h \
    qwt6.1.0/qwt.h \
    spectre.h \
    viewsum.h \
    scanparameters.h \
    qwt_plot.h \
    multiple.h \
    onepart.h \
    potCompute.h \
    potcomputedialog.h

SOURCES += mainwindow.cpp \
    delegate.cpp \
    askpotential.cpp \
    function.cpp \
    4algos.cc \
    main.cpp \
    VDL.cpp \
    plotmainscreen.cpp \
    qwt6.1.0/qwt_widget_overlay.cpp \
    qwt6.1.0/qwt_wheel.cpp \
    qwt6.1.0/qwt_transform.cpp \
    qwt6.1.0/qwt_thermo.cpp \
    qwt6.1.0/qwt_text_label.cpp \
    qwt6.1.0/qwt_text_engine.cpp \
    qwt6.1.0/qwt_text.cpp \
    qwt6.1.0/qwt_system_clock.cpp \
    qwt6.1.0/qwt_symbol.cpp \
    qwt6.1.0/qwt_spline.cpp \
    qwt6.1.0/qwt_slider.cpp \
    qwt6.1.0/qwt_series_data.cpp \
    qwt6.1.0/qwt_scale_widget.cpp \
    qwt6.1.0/qwt_scale_map.cpp \
    qwt6.1.0/qwt_scale_engine.cpp \
    qwt6.1.0/qwt_scale_draw.cpp \
    qwt6.1.0/qwt_scale_div.cpp \
    qwt6.1.0/qwt_sampling_thread.cpp \
    qwt6.1.0/qwt_round_scale_draw.cpp \
    qwt6.1.0/qwt_raster_data.cpp \
    qwt6.1.0/qwt_point_polar.cpp \
    qwt6.1.0/qwt_point_mapper.cpp \
    qwt6.1.0/qwt_point_data.cpp \
    qwt6.1.0/qwt_point_3d.cpp \
    qwt6.1.0/qwt_plot_zoomer.cpp \
    qwt6.1.0/qwt_plot_zoneitem.cpp \
    qwt6.1.0/qwt_plot_xml.cpp \
    qwt6.1.0/qwt_plot_tradingcurve.cpp \
    qwt6.1.0/qwt_plot_textlabel.cpp \
    qwt6.1.0/qwt_plot_svgitem.cpp \
    qwt6.1.0/qwt_plot_spectrogram.cpp \
    qwt6.1.0/qwt_plot_spectrocurve.cpp \
    qwt6.1.0/qwt_plot_shapeitem.cpp \
    qwt6.1.0/qwt_plot_seriesitem.cpp \
    qwt6.1.0/qwt_plot_scaleitem.cpp \
    qwt6.1.0/qwt_plot_rescaler.cpp \
    qwt6.1.0/qwt_plot_renderer.cpp \
    qwt6.1.0/qwt_plot_rasteritem.cpp \
    qwt6.1.0/qwt_plot_picker.cpp \
    qwt6.1.0/qwt_plot_panner.cpp \
    qwt6.1.0/qwt_plot_multi_barchart.cpp \
    qwt6.1.0/qwt_plot_marker.cpp \
    qwt6.1.0/qwt_plot_magnifier.cpp \
    qwt6.1.0/qwt_plot_legenditem.cpp \
    qwt6.1.0/qwt_plot_layout.cpp \
    qwt6.1.0/qwt_plot_item.cpp \
    qwt6.1.0/qwt_plot_intervalcurve.cpp \
    qwt6.1.0/qwt_plot_histogram.cpp \
    qwt6.1.0/qwt_plot_grid.cpp \
    qwt6.1.0/qwt_plot_glcanvas.cpp \
    qwt6.1.0/qwt_plot_directpainter.cpp \
    qwt6.1.0/qwt_plot_dict.cpp \
    qwt6.1.0/qwt_plot_curve.cpp \
    qwt6.1.0/qwt_plot_canvas.cpp \
    qwt6.1.0/qwt_plot_barchart.cpp \
    qwt6.1.0/qwt_plot_axis.cpp \
    qwt6.1.0/qwt_plot_abstract_barchart.cpp \
    qwt6.1.0/qwt_plot.cpp \
    qwt6.1.0/qwt_pixel_matrix.cpp \
    qwt6.1.0/qwt_picker_machine.cpp \
    qwt6.1.0/qwt_picker.cpp \
    qwt6.1.0/qwt_panner.cpp \
    qwt6.1.0/qwt_painter_command.cpp \
    qwt6.1.0/qwt_painter.cpp \
    qwt6.1.0/qwt_null_paintdevice.cpp \
    qwt6.1.0/qwt_matrix_raster_data.cpp \
    qwt6.1.0/qwt_math.cpp \
    qwt6.1.0/qwt_magnifier.cpp \
    qwt6.1.0/qwt_legend_label.cpp \
    qwt6.1.0/qwt_legend_data.cpp \
    qwt6.1.0/qwt_legend.cpp \
    qwt6.1.0/qwt_knob.cpp \
    qwt6.1.0/qwt_interval_symbol.cpp \
    qwt6.1.0/qwt_interval.cpp \
    qwt6.1.0/qwt_graphic.cpp \
    qwt6.1.0/qwt_event_pattern.cpp \
    qwt6.1.0/qwt_dyngrid_layout.cpp \
    qwt6.1.0/qwt_dial_needle.cpp \
    qwt6.1.0/qwt_dial.cpp \
    qwt6.1.0/qwt_date_scale_engine.cpp \
    qwt6.1.0/qwt_date_scale_draw.cpp \
    qwt6.1.0/qwt_date.cpp \
    qwt6.1.0/qwt_curve_fitter.cpp \
    qwt6.1.0/qwt_counter.cpp \
    qwt6.1.0/qwt_compass_rose.cpp \
    qwt6.1.0/qwt_compass.cpp \
    qwt6.1.0/qwt_column_symbol.cpp \
    qwt6.1.0/qwt_color_map.cpp \
    qwt6.1.0/qwt_clipper.cpp \
    qwt6.1.0/qwt_arrow_button.cpp \
    qwt6.1.0/qwt_analog_clock.cpp \
    qwt6.1.0/qwt_abstract_slider.cpp \
    qwt6.1.0/qwt_abstract_scale_draw.cpp \
    qwt6.1.0/qwt_abstract_scale.cpp \
    qwt6.1.0/qwt_abstract_legend.cpp \
    spectre.cpp \
    viewsum.cpp \
    scanparameters.cpp \
    multiple.cpp \
    onepart.cpp \
    potCompute.cpp \
    potcomputedialog.cpp


FORMS += mainwindow.ui \
    askpotential.ui \
    function.ui \
    spectre.ui \
    viewsum.ui \
    scanparameters.ui \
    plotmainscreen.ui \
    multiple.ui \
    onepart.ui \
    potcomputedialog.ui
TARGET = Cleozone
TEMPLATE = app

unix{
INCLUDEPATH += /user/include/qwt6.1.0
DEPENDPATH += /user/lib
LIBS += -lqwt6.1.0
}
