#ifndef ASKPOTENTIAL_H
#define ASKPOTENTIAL_H

#include "common.h"

#ifdef QT4
#include <QtGui/QDialog>
#include <QtGui/QFileDialog>
#endif

#ifdef QT5
#include <QtWidgets/QDialog>
#include <QtWidgets/QFileDialog>
#endif

#include <vector>
#include "function.h"
using namespace std;

namespace Ui {
    class AskPotential;
}

class AskPotential : public QDialog {
    Q_OBJECT
public:
    AskPotential(vector<double>& _to_fill, int asked_size, QWidget *parent = 0, bool proposeFasta = false);
    ~AskPotential();

public slots:
    void LoadFromFile();
    void TextChanged(QString);
    void FunctionChanged(QString);
    void DoLoad();
    void updateSize(int i);
    void launchPotComputeDialog();

protected:
    void changeEvent(QEvent *e);

    QString FileName;
    vector<double>* to_fill;
    int chosen_size;

    Function* fct;

private:
    Ui::AskPotential *m_ui;
};

#endif // ASKPOTENTIAL_H
