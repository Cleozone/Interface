#include "askpotential.h"
#include "ui_askpotential.h"

#include "potcomputedialog.h"
#include <iostream>
#include <fstream>

using namespace std;

AskPotential::AskPotential(vector<double> &_to_fill, int asked_size,  QWidget *parent, bool proposeFasta) :
    QDialog(parent),
    m_ui(new Ui::AskPotential)
{
    m_ui->setupUi(this);
    fct = new Function(true, m_ui->widgetFunction);
    if(!proposeFasta) m_ui->radioButtonFileFASTA->hide();
    QObject::connect(m_ui->Find, SIGNAL(released()), this, SLOT(LoadFromFile()));
    QObject::connect(m_ui->buttonBox, SIGNAL(accepted()), this, SLOT(DoLoad()));
    QObject::connect(m_ui->lineEdit, SIGNAL(textChanged(QString)), this, SLOT(TextChanged(QString)));
    QObject::connect(m_ui->comboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(FunctionChanged(QString)));
    QObject::connect(m_ui->spinBoxSize, SIGNAL(valueChanged(int)), this, SLOT(updateSize(int)));

    QObject::connect(m_ui->radioButtonFileFASTA, SIGNAL(clicked()), this, SLOT(launchPotComputeDialog()));


    to_fill = &_to_fill;
    chosen_size = asked_size;
    if((chosen_size < 0) || (chosen_size > 10000000)) cerr << "ARG : Load Potential, ask way wrong size " << chosen_size << endl;
    m_ui->spinBoxSize->setValue(chosen_size);

    m_ui->comboBox->addItem(functionName(NB_FUNCTIONS));
    for(int i = 0; i < NB_FUNCTIONS; ++i){
        m_ui->comboBox->addItem(functionName((FunctionT) i));
    }


}

AskPotential::~AskPotential()
{
    delete m_ui;
    delete fct;
}

void AskPotential::updateSize(int i){
    chosen_size = i;
    fct->setSize(chosen_size);
}

void AskPotential::LoadFromFile(){
    FileName = QFileDialog::getOpenFileName();
    m_ui->lineEdit->setText(FileName);
    m_ui->radioButtonFile->setChecked(true);
}

void AskPotential::TextChanged(QString new_one){
    FileName = new_one; //m_ui->lineEdit->text();
    m_ui->radioButtonFile->setChecked(true);
}

void AskPotential::FunctionChanged(QString new_one){
    delete fct;
    fct = CreateFunctionAndPlot(functionID(new_one), m_ui->widgetFunction);
    fct->show();
    fct->setSize(chosen_size);

    m_ui->radioButtonFunction->setChecked(true);
}

void AskPotential::launchPotComputeDialog(){
    potComputeDialog* askPotCompute = new potComputeDialog(this);
    askPotCompute->show();
    //QEventLoop loop;
    //QObject::connect(askPotCompute, SIGNAL(accepted()), &loop, SLOT(quit()));
    //loop.exec();
    askPotCompute->setFocus(); // doens't work ???
    askPotCompute->setModal(true);
    askPotCompute->exec();

    chosen_size = min(chosen_size, (int) askPotCompute->currentProfile.size());
    to_fill->clear();
    to_fill->resize(chosen_size, 0);
    for(int i = 0; i < chosen_size; ++i){
        (*to_fill)[i] = (double) askPotCompute->currentProfile[i];
    }
    askPotCompute->hide();
    cerr << "Kill PotComputeDialog" << endl;
    delete askPotCompute;
}

void AskPotential::DoLoad(){
    if(chosen_size == 0) {
        cerr << "Load Potential : no action done (zero size asked)" << endl;
        return;
    }
    if(m_ui->radioButtonFile->isChecked()){

        ifstream fichier((const char*) FileName.toStdString().c_str(), ios::in);
        double mean = 0;
        double min_seen = 1e25;
        if(fichier){
                int cpt = 0;
                to_fill->clear();
                double value;
                while((cpt < chosen_size) && (fichier >> value)) {
                    cpt++;
                    to_fill->push_back(value);
                    min_seen = min(min_seen, value);
                    mean += value;
                }
                fichier.close(); 
                if(cpt == 0){
                     cerr << "ERR LoadPotential, file(" << FileName.toStdString() << ") : Nothing in file " << endl;
                     return;
                 }
                mean /= cpt;
                cout << "Potential loaded from " << FileName.toStdString() << ", size=" << cpt << "; mean=" << mean << "; min=" << min_seen << endl;
            } else {
                cerr << "ERR LoadPotential, file(" << FileName.toStdString() << ") : Impossible d'ouvrir le fichier " << endl;
            }

    } else if(m_ui->radioButtonFunction->isChecked()){
        fct->export_to_vector(to_fill);
    } else cerr << "Load Potential : no action done" << endl;
}

void AskPotential::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
