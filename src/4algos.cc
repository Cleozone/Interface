/*
 *  MultiProblems.Cpp
 *  Created by Robert Philippe on 18/03/11.
 *
 */

#include "4algos.h"

#include <string>
#include <sstream>


#define COUT_ON 0

#ifdef LAST_WAVE
//#include "../include/4algos.h";
	//#include "signals.h"
	//#include "lastwave.h"
#endif







void convert_to_occupancy(vector<double> & to_read, vector<double> & to_fill, int Vex, int starting_fill){
    /*for(int i = 0; i < (int) to_read.size(); ++i){
        cerr << to_read[i] << "\t";
    }
    cerr << endl;*/

    int Linit = to_read.size();
    int largersize = max(0,Linit + Vex - 1 - starting_fill);
    to_fill.resize(largersize, 0.0);
    for(int i = 0; i < largersize; ++i)
        to_fill[i] = 0.0;
    for(int i = 0; i < Linit; ++i){
        double starts_here = to_read[i];
        for(int j = 0; j < Vex; ++j){
            if(i+j >= starting_fill)
                to_fill[i+j - starting_fill] += starts_here;
        }
    }
}

void convert_to_occupancy(vector< vector <double> > & to_read, vector< vector <double> > & to_fill, vector<int> Vexs){
    if(to_read.size() < 1) return;
    int NP = to_read.size();
    int L = to_read[0].size();
    for(int i = 0; i < NP; ++i){
        if((int) to_read[i].size() != L) cerr << "ERR : convert_to_occupancy : you gave a non-square table (i.e. not the same length for each particle !!!) : line " << i+1 << " is of size " << to_read[i].size() << " while first line is of size " << L << endl;
    }

    to_fill.clear();
    to_fill.resize(NP);
    for(int i =0; i < NP; ++i){
        to_fill[i].resize(L, 0.0);
    }

    for(int i = 0; i < NP; ++i){
        convert_to_occupancy(to_read[i], to_fill[i], Vexs[i], 0);
    }
}

void convert_to_dyades(vector< vector <double> > & to_read, vector< vector <double> > & to_fill, vector<double> dyades){
    if(to_read.size() < 1) return;
    int NP = to_read.size();
    int L = to_read[0].size();
    for(int i = 0; i < NP; ++i){
        if((int) to_read[i].size() != L) cerr << "ERR : convert_to_dyades : you gave a non-square table (i.e. not the same length for each particle !!!) : line " << i+1 << " is of size " << to_read[i].size() << " while first line is of size " << L << endl;
    }

    to_fill.clear();
    to_fill.resize(NP);
    for(int i =0; i < NP; ++i){
        to_fill[i].resize(L, 0.0);
    }

    for(int p = 0; p < NP; ++p){
        // for particle p,
        double dyade = dyades[p];
        double dyade_int = ((double) ((int) dyade ));  // le choix se fera entre dyade_int et dyade_int + 1
        double pourc_left = 1.0 - (dyade - dyade_int);
        double pourc_right = 1.0 - pourc_left;
        // cerr << " dyade= " << dyade << ", dyade_int= " << dyade_int << ", p_left= " << pourc_left << ", p_right= " << pourc_right << ", si Xinit = 2, alors pos entre " << 2 + dyade_int-1 << " et " << 2 + dyade_int << endl;

        for(int j = 0; j < L; ++j){
            int p1 = j + ((int) dyade_int) - 1;
            int p2 = p1 + 1;
            if((p1 >= 0) && (p1 < L)){
                to_fill[p][p1] += pourc_left * to_read[p][j];
            }
            if((p2 >= 0) && (p2 < L)){
                to_fill[p][p2] += pourc_right * to_read[p][j];
            }
        }
    }
}


void convert_to_dyades(vector <double> & to_read,vector <double> & to_fill, double dyade){
    if(to_read.size() < 1) return;
    int L = to_read.size();

    to_fill.clear();
    to_fill.resize(L, 0.0);

        double dyade_int = ((double) ((int) dyade ));  // le choix se fera entre dyade_int et dyade_int + 1
        double pourc_left = 1.0 - (dyade - dyade_int);
        double pourc_right = 1.0 - pourc_left;
        // cerr << " dyade= " << dyade << ", dyade_int= " << dyade_int << ", p_left= " << pourc_left << ", p_right= " << pourc_right << ", si Xinit = 2, alors pos entre " << 2 + dyade_int-1 << " et " << 2 + dyade_int << endl;

        for(int j = 0; j < L; ++j){
            int p1 = j + ((int) dyade_int) - 1;
            int p2 = p1 + 1;
            if((p1 >= 0) && (p1 < L)){
                to_fill[p1] += pourc_left * to_read[j];
            }
            if((p2 >= 0) && (p2 < L)){
                to_fill[p2] += pourc_right * to_read[j];
            }
        }

}





/* --- class edge is already defined in the .h file --- */



/* --- class liste d'adjacence --- */

// liste := vector<edge>
int liste_adjacence::size(){
    return taille;
}
liste_adjacence::liste_adjacence() {taille = 0; table.clear();};
liste_adjacence::liste_adjacence(int _taille) {resize(_taille);};
void liste_adjacence::resize(int _taille){
    taille = _taille;
    table.resize(taille);
    for(int i = 0; i < taille; ++i){
        table[i] = new vector<edge>();
    }
}
liste & liste_adjacence::operator [](int i){		//passe la référence
    if((i < 0) || (i >= taille)) cerr << "Unauthorized call to adjacency table (index " << i << ")\n";
    return *(table[i]);
}
void liste_adjacence::add(int i, int j, valeur w){
    table[i]->push_back(edge(j, w));
}
liste_adjacence::~liste_adjacence(){
    for(int i = 0; i < taille; ++i){
        table[i]->clear();
        delete table[i];
    }
}



/* ---- class Graph ---- */

Graphe::Graphe(int _size){
    size = _size;

    t.resize(size);
    reverse.resize(size);

    Dpred.resize(size);
    Ddist.resize(size);
    Dvisited.resize(size);
    dijkstra_done = false;
    dijkstra_source = -1;
    dijkstra_dest = -1;

    Dpred_reverse.resize(size);
    Ddist_reverse.resize(size);
    Dvisited_reverse.resize(size);
    dijkstra_reverse_done = false;
    dijkstra_reverse_source = -1;
    dijkstra_reverse_dest = -1;

    Tpred_topo.resize(size);
    current_index_topo = 0;
    Tordre.resize(size);					// only the beginning used (0..current_index_topo - 1)
    tri_topologique_done = false;
    tri_topologique_source = -1;

    Tpred_topo_reverse.resize(size);
    current_index_topo_reverse = 0;
    Tordre_reverse.resize(size);			// only the beginning used (0..current_index_topo_reverse - 1)
    tri_topologique_reverse_done = false;
    tri_topologique_reverse_source = -1;

    cpt_interne = 0;
}

void Graphe::add_to_graph(int i, int j, valeur w){
    t[i].push_back(edge(j, w));
    reverse[j].push_back(edge(i,w));
    dijkstra_done = false;					// we could also reset the sources/destinations, but not necessary
    dijkstra_reverse_done = false;
    tri_topologique_done = false;
    tri_topologique_reverse_done = false;
}

int Graphe::nb_sommets(){
    return size;
}


void Graphe::tri_topologique(int start){
    if(tri_topologique_done && (start == tri_topologique_source)){
        cerr << "Tri Topologique already done from the same source. Not doing it again.\n";
        return;
    }
    if((start < 0) || (start >= size)) {
        cerr << "tri_topologique : out of range for source vertex number. Abort." << endl;
        return;
    }
    tri_topologique_done = false;

    for(int i = 0; i < size; ++i){
        Tordre[i] = 0;
        Tpred_topo[i] = -1;
    }
    Tpred_topo[start] = start;
    current_index_topo = 0;

    cpt_interne = 0;
    DFS(start);
        //cerr << "    -> DFS finished ; " << current_index_topo << " vertices seen\n";
    //if(current_index_topo < size) cerr << "WARNING : tri_topologique didn't manage to join all vertices of the graph. It may be not connex.\n";

    vector<int> cp = Tordre;
    for(int i = 0; i < current_index_topo; ++i){
        Tordre[i] = cp[current_index_topo - i - 1];
    }
    tri_topologique_done = true;
    tri_topologique_source = start;
}

void Graphe::DFS(int start){
    if(size > 10000) {
        if((cpt_interne % 1000) == 0) cerr << "... " << cpt_interne << " / " << size << " sommets visites\n";
    }
    liste::iterator it;
    for(it = t[start].begin(); it != t[start].end(); ++it){
        int d = it->dest;
        if(Tpred_topo[d] < 0){
            Tpred_topo[d] = start;
            cpt_interne++;
            DFS(d);
        }
    }
    Tordre[current_index_topo] = start;
    current_index_topo++;
}

void Graphe::tri_topologique_reverse(int start){
    if(tri_topologique_reverse_done && (start == tri_topologique_reverse_source)){
        cerr << "Tri Topologique (reverse) already done from the same source. Not doing it again.\n";
        return;
    }
    if((start < 0) || (start >= size)) {
        cerr << "tri_topologique_reverse : out of range for source vertex number. Abort." << endl;
        return;
    }
    tri_topologique_reverse_done = false;
    for(int i = 0; i < size; ++i){
        Tordre_reverse[i] = 0;
        Tpred_topo_reverse[i] = -1;
    }
    Tpred_topo_reverse[start] = start;
    current_index_topo_reverse = 0;

    cpt_interne = 0;
    DFS_reverse(start);
        ///cerr << "    -> DFS_reverse finished ; " << current_index_topo_reverse << " vertices seen\n";
    //if(current_index_topo_reverse < size) cerr << "WARNING : tri_topologique_reverse didn't manage to join all vertices of the graph. It may be not connex.\n";

    vector<int> cp = Tordre_reverse;
    for(int i = 0; i < current_index_topo_reverse; ++i){
        Tordre_reverse[i] = cp[current_index_topo_reverse - i - 1];
    }
    tri_topologique_reverse_done = true;
    tri_topologique_reverse_source = start;
}

void Graphe::DFS_reverse(int start){
    if(size > 10000) {
        if((cpt_interne % 1000) == 0) cerr << "... " << cpt_interne << " / " << size << " sommets visites\n";
    }
    liste::iterator it;
    for(it = reverse[start].begin(); it != reverse[start].end(); ++it){
        int d = it->dest;
        if(Tpred_topo_reverse[d] < 0){
            Tpred_topo_reverse[d] = start;
            cpt_interne++;
            DFS_reverse(d);
        }
    }
    Tordre_reverse[current_index_topo_reverse] = start;
    current_index_topo_reverse++;
}





bool Graphe::dijkstra(int start, int destination){
    if(dijkstra_done && (start == dijkstra_source) && (destination == dijkstra_dest)){
        cerr << "dijkstra already done from the same source and the same destination. Not doing it again." << endl;
    }
    if((start < 0) || (start >= size) || (destination < 0) || (destination >= size) ) {
        cerr << "dijkstra : out of range for source or destination vertex number. Abort." << endl;
        return false;
    }
    dijkstra_done = false;

    vector<bool> added(size, false);	// To remember who is in the queue without looking in all the queue
    valeur best_dist_found = 1e25;		// to stop when all the vertices of distance dist[destination] has been found (useless to go on then)
    cpt_interne = 0;					// to display a message all 1000 iterations if the graph is big

    int u;
    priority_queue <vert, vector<vert>, comp> q;	// in this queue, vertices will be sorted by distance from the source
    liste::iterator it;
    for(int i = 0; i < size; ++i){
        Ddist[i] = -1;
        Dpred[i] = -1;
        Dvisited[i] = false;
    }
    Ddist[start] = 0;
    q.push(vert(start,0));


    while(!q.empty()){

        /*  ---- Takes the first vertex out of the list. This is the closest unseen vertex from the source. --- */
        u = q.top().num;
        q.pop();

        /*  ---- to display something every 1000 iterations ---- */
        cpt_interne++;
        if(size > 10000){
            if((cpt_interne % 1000) == 0) cerr << "... " << cpt_interne << " / " << size << " sommets visites " << q.size() << "\n";
        }
        /*  ---- to stop when all vertices of distance <= dist[destination] has been found  --- */
        if(Ddist[u] > best_dist_found) {
            dijkstra_done = true;
            dijkstra_source = start;
            dijkstra_dest = destination;
            return true;
        }
        if(u == destination) best_dist_found = Ddist[u];


        /* ---- the big loop ---- */
        if(!Dvisited[u]){
            Dvisited[u] = true;
            for(it = t[u].begin() ; it != t[u].end(); ++it){
                int v = it->dest;
                valeur d2 = Ddist[u] + it->weight;		// does the distance of v is better going through source -> u -> v ?
                if((Ddist[v] < 0) || (Ddist[v] > d2)){	// if yes, (two cases : either v is unseen, either it is already seen (in the list), but we can improve its distance
                    Ddist[v] = d2;
                    Dpred[v] = u;
                    if(!Dvisited[v]) {
                        q.push(vert(v, Ddist[v]));
                        added[v] = true;
                    }
                }
                if(!Dvisited[v] && (!added[v])) {		// In the case v is already in the list (added[]), but is not improved, we don't repeat it in the list, in order to keep the list small.
                    q.push(vert(v, Ddist[v]));
                    added[v] = true;
                }
            }
        }
    }

    /* ---- if we arrive here, we have not managed to reach the destination ---- */
    dijkstra_done = true;
    dijkstra_source = start;
    dijkstra_dest = destination;
    return false;
}


bool Graphe::dijkstra_reverse(int start, int destination){
    if(dijkstra_reverse_done && (start == dijkstra_reverse_source) && (destination == dijkstra_reverse_dest)){
        cerr << "dijkstra_reverse already done from the same source and the same destination. Not doing it again." << endl;
    }
    if((start < 0) || (start >= size) || (destination < 0) || (destination >= size) ) {
        cerr << "dijkstra_reverse : out of range for source or destination vertex number. Abort." << endl;
        return false;
    }
    dijkstra_reverse_done = false;

    vector<bool> added(size, false);	// To remember who is in the queue without looking in all the queue
    valeur best_dist_found = 1e25;		// to stop when all the vertices of distance dist[destination] has been found (useless to go on then)
    cpt_interne = 0;					// to display a message all 1000 iterations if the graph is big

    int u;
    priority_queue <vert, vector<vert>, comp> q;	// in this queue, vertices will be sorted by distance from the source
    liste::iterator it;
    for(int i = 0; i < size; ++i){
        Ddist_reverse[i] = -1;
        Dpred_reverse[i] = -1;
        Dvisited_reverse[i] = false;
    }
    Ddist_reverse[start] = 0;
    q.push(vert(start,0));


    while(!q.empty()){

        /*  ---- Takes the first vertex out of the list. This is the closest unseen vertex from the source. --- */
        u = q.top().num;
        q.pop();

        /*  ---- to display something every 1000 iterations ---- */
        cpt_interne++;
        if(size > 10000){
            if((cpt_interne % 1000) == 0) cerr << "... " << cpt_interne << " / " << size << " sommets visites " << q.size() << "\n";
        }
        /*  ---- to stop when all vertices of distance <= dist[destination] has been found  --- */
        if(Ddist_reverse[u] > best_dist_found) {
            dijkstra_reverse_done = true;
            dijkstra_reverse_source = start;
            dijkstra_reverse_dest = destination;
            return true;
        }
        if(u == destination) best_dist_found = Ddist_reverse[u];


        /* ---- the big loop ---- */
        if(!Dvisited_reverse[u]){
            Dvisited_reverse[u] = true;
            for(it = reverse[u].begin() ; it != reverse[u].end(); ++it){
                int v = it->dest;
                valeur d2 = Ddist_reverse[u] + it->weight;		// does the distance of v is better going through source -> u -> v ?
                if((Ddist_reverse[v] < 0) || (Ddist_reverse[v] > d2)){	// if yes, (two cases : either v is unseen, either it is already seen (in the list), but we can improve its distance
                    Ddist_reverse[v] = d2;
                    Dpred_reverse[v] = u;
                    if(!Dvisited_reverse[v]) {
                        q.push(vert(v, Ddist_reverse[v]));
                        added[v] = true;
                    }
                }
                if(!Dvisited_reverse[v] && (!added[v])) {		// In the case v is already in the list (added[]), but is not improved, we don't repeat it in the list, in order to keep the list small.
                    q.push(vert(v, Ddist_reverse[v]));
                    added[v] = true;
                }
            }
        }
    }

    /* ---- if we arrive here, we have not managed to reach the destination ---- */
    dijkstra_reverse_done = true;
    dijkstra_reverse_source = start;
    dijkstra_reverse_dest = destination;
    return false;
}


int Graphe::pred(int sommet){
    if(dijkstra_done){
        if((sommet >= 0) && (sommet < size)){
            return Dpred[sommet];
        } else cerr << "Out of bounds, pred(" << sommet << ")" << endl;
    } else cerr << "Can't access predecessors before having done dijkstra." << endl;
    return -1;
}

valeur Graphe::dist(int sommet){
    if(dijkstra_done){
        if((sommet >= 0) && (sommet < size)){
            return Ddist[sommet];
        } else cerr << "Out of bounds, dist(" << sommet << ")" << endl;
    } else cerr << "Can't access distances before having done dijkstra." << endl;
    return -1;
}

int Graphe::pred_reverse(int sommet){
    if(dijkstra_reverse_done){
        if((sommet >= 0) && (sommet < size)){
            return Dpred_reverse[sommet];
        } else cerr << "Out of bounds, pred_reverse(" << sommet << ")" << endl;
    } else cerr << "Can't access predecessors (reverse) before having done dijkstra." << endl;
    return -1;
}

valeur Graphe::dist_reverse(int sommet){
    if(dijkstra_reverse_done){
        if((sommet >= 0) && (sommet < size)){
            return Ddist_reverse[sommet];
        } else cerr << "Out of bounds, dist_reverse(" << sommet << ")" << endl;
    } else cerr << "Can't access distances (reverse) before having done dijkstra." << endl;
    return -1;
}


int Graphe::pred_topo(int sommet){
    if(tri_topologique_done){
        if((sommet >= 0) && (sommet < size)){
            return Tpred_topo[sommet];
        } else cerr << "Out of bounds, pred_topo(" << sommet << ")" << endl;
    } else cerr << "Can't access predecessors (topological) before having done dijkstra." << endl;
    return -1;
}

int Graphe::nb_sommets_ordonnes(){
    if(tri_topologique_done){
        return current_index_topo;
    } else cerr << "Topological sorting not done. Can't call nb_sommets_ordonnes yet." << endl;
    return 0;
}

int Graphe::ordre(int nb){
    if(tri_topologique_done){
        if((nb >= 0) && (nb < current_index_topo)){
            return Tordre[nb];
        } else cerr << "Can't access the order of vertices whereas tri_topologique has not been done" << endl;
    } else cerr << "Topological sorting not done. Can't call nb_sommets_ordonnes yet." << endl;
    return 0;
}

int Graphe::number(int sommet){
    if((sommet < 0) || (sommet > size)) {
        cerr << "number() : Out of range in vertex index" << endl;
        return -1;
    }
    for(int i = 0; i < nb_sommets_ordonnes() ; ++i){
        if(ordre(i) == sommet) return i;
    }
    return -2; //case the vertex has not been seen by topological sorting
}

int Graphe::pred_topo_reverse(int sommet){
    if(tri_topologique_reverse_done){
        if((sommet >= 0) && (sommet < size)){
            return Tpred_topo_reverse[sommet];
        } else cerr << "Out of bounds, pred_topo_reverse(" << sommet << ")" << endl;
    } else cerr << "Can't access predecessors (topological, reverse) before having done dijkstra." << endl;
    return -1;
}

int Graphe::nb_sommets_ordonnes_reverse(){
    if(tri_topologique_reverse_done){
        return current_index_topo_reverse;
    } else cerr << "Reverse topological sorting not done. Can't call nb_sommets_ordonnes yet." << endl;
    return 0;
}

int Graphe::ordre_reverse(int nb){
    if(tri_topologique_reverse_done){
        if((nb >= 0) && (nb < current_index_topo_reverse)){
            return Tordre_reverse[nb];
        } else cerr << "Can't access the order of vertices whereas tri_topologique has not been done" << endl;
    } else cerr << "Topological sorting not done. Can't call nb_sommets_ordonnes yet." << endl;
    return 0;
}

int Graphe::number_reverse(int sommet){
    if((sommet < 0) || (sommet > size)) {
        cerr << "number_reverse() : Out of range in vertex index" << endl;
        return -1;
    }
    for(int i = 0; i < nb_sommets_ordonnes_reverse() ; ++i){
        if(ordre_reverse(i) == sommet) return i;
    }
    return -2; //case the vertex has not been seen by topological sorting
}


void Graphe::show_graph(void (* conv)(int ID)){
    cout << " ==============  Structure du graphe généré :  ============== \n";
    cout << "   - nb sommets : " << size << endl;
    if(dijkstra_done) cout << "    -> Dijkstra done from " << dijkstra_source << " to " << dijkstra_dest << endl;
    else cout << "    -> Dijkstra not done." << endl;
    if(dijkstra_reverse_done) cout << "    -> Dijkstra (in reverse) done from " << dijkstra_reverse_source << " to " << dijkstra_reverse_dest << endl;
    else cout << "    -> Dijkstra (reverse) not done." << endl;
    if(tri_topologique_done) cout << "    -> Topological sorting done from " << tri_topologique_source << endl;
    else cout << "    -> Topological sorting not done." << endl;
    if(tri_topologique_reverse_done) cout << "    -> Topological sorting (in reverse) done from " << tri_topologique_reverse_source << endl;
    else cout << "    -> Topological sorting (in reverse) not done." << endl;

    cout << "   - Displayed :\n";
    cout << "\tVertex_Number\n";
    if(dijkstra_done)					cout << "\tDdist := Dijkstra_distance_from " << dijkstra_source << "\tDpred := Dijkstra_predecessor\n";
    if(dijkstra_reverse_done)			cout << "\tDRdist:= Dijkstra_distance_IN_REVERSE_GRAPH_from" << dijkstra_reverse_source << "\tDpred := Dijkstra_predecessor_IN_REVERSE\n";
    if(tri_topologique_done)			cout << "\tTordre := number_of_this_vertex_in_topological_order_from" << tri_topologique_source<< "\n";
    if(tri_topologique_reverse_done)	cout << "\tTRordre:= number_of_this_vertex_in_REVERSE_topological_order_from " << tri_topologique_reverse_source << "\n";
    cout << "\tSons := neighbours in (oriented) graph(weight)\n";
    cout << "\tPredecessors := neighbours in REVERSE graph(weight)\n";
    cout << " ==============............................... ============== \n";

    for(int i = 0; i < size; ++i){
        if(conv) {conv(i); cout << endl;}
        else cout << "S" << i	<< "\n";
        if(dijkstra_done)					cout << "\tDdist  : " << dist(i) << "\tDpred : " << pred(i) << "\n";
        if(dijkstra_reverse_done)			cout << "\tDRdist : " << dist_reverse(i) << "\t DRpred:" << pred_reverse(i) << "\n";
        if(tri_topologique_done)			cout << "\tTordre : " << number(i) << "\n";
        if(tri_topologique_reverse_done)	cout << "\tTRordre: " << number_reverse(i) << "\n";
        cout << "\tSons : ";
                for(int j = 0; j < (int) t[i].size(); ++j){
            if(conv) {conv(t[i][j].dest); cout << "(" << t[i][j].weight << "),   ";}
            else cout << "S" << t[i][j].dest << "(" << t[i][j].weight << "),   ";
        }
        cout << endl;
        cout << "\tPredecessors : ";
                for(int j = 0; j < (int) reverse[i].size(); ++j){
            if(conv) {conv(reverse[i][j].dest); cout << "(" << reverse[i][j].weight << "),   ";}
            else cout << reverse[i][j].dest << "(" << reverse[i][j].weight << "),   ";
        }
        cout << endl;
    }
};




/* void Graphe::show_order(){
    cout << "Order of the " << current_index_topo << " vertices seen from the source (";
    showpos(source);
    cout << "): " << endl;
    for(int i = 0; i < current_index_topo; ++i){
        cout << i+1 << "\t";
        ordre[i];
        cout << endl;
    }
} */



/* ---- class grille 2D ---- */


grille::grille(int _largeur) : largeur(_largeur){
    source = -1;
    arrivee = -1;
        //showpostatic(_largeur);
};

int grille::ID(int i, int j){
    return (i - 1) * largeur + (j - 1);
}
int grille::pos_x(int ID){
    return (ID % largeur) + 1;
}
int grille::pos_y(int ID){
    return (ID / largeur) + 1;
}
void grille::showpos(int ID){
    cerr << ID << " (" << pos_y(ID) << "," << pos_x(ID) << ") ";
}

void grille::convert_to_positions_x(vector<int> &IDs, vector<int> &pos_to_fill){
    int lg = IDs.size();
    pos_to_fill.resize(lg);
    for(int i = 0; i < lg; ++i){
        pos_to_fill[i] = pos_x(IDs[i]);
    }
}

void grille::showpostatic(int ID){
    ID++;
        /*static int largeur_static = -2;
    if(largeur_static == -2) largeur_static = ID;
    else {
               cout << ID << "[" << (ID / largeur_static) + 1 << "," << (ID % largeur_static) + 1 << "]";
        }*/
}












/* ========================================= I - Common general mother class for managing data in the case of 1 particle kind ================================= */













/* ----------- I/O for the landscape values ----------------------- */
// N has to be known before

void Probleme::load_landscape_from_function(valeur (* _function_landscape) (int)){
    valeur mean = 0;
    for(int i = 1; i <= N; ++i){
        mean += _function_landscape(i);
    }
    mean_landscape = mean / (valeur) N;
    size_table_landscape = -1;				// to say values are stored in a function and not in the table
    function_landscape = _function_landscape;
}

void Probleme::load_landscape_from_file(char* file_landscape){
    ifstream fichier(file_landscape, ios::in);
    valeur mean = 0;
    if(fichier){
        table_landscape.clear();
        valeur value;
        while(fichier >> value) {
            table_landscape.push_back(value);
            mean += value;
        }
        size_table_landscape = table_landscape.size();
        fichier.close();
        if(size_table_landscape == 0) cerr << "ERR Load_landscape_from_file(" << file_landscape << ") : no information found. No external potential loaded.\n";
        else {mean_landscape = mean / (valeur) size_table_landscape;}
    } else cerr << "ERR Load_landscape_from_file(" << file_landscape << ") : Impossible d'ouvrir le fichier " << endl;
}

void Probleme::load_landscape_from_vector(vector<valeur> _landscape){
    valeur mean = 0;
    table_landscape.clear();
    table_landscape.resize(_landscape.size());
    int _size = _landscape.size();
    for(int i = 0; i < _size; ++i){
        table_landscape[i] = _landscape[i];
        mean += table_landscape[i];
    }
    size_table_landscape = table_landscape.size();
    if(size_table_landscape == 0) cerr << "ERR Load_landscape_from_vector : no information found. No exterbal potential loaded.\n";
    else { mean_landscape = mean / (valeur) size_table_landscape;}
}

valeur Probleme::landscape(int i){
    if(size_table_landscape == 0) cerr << "ERR Landscape() : no external potential has been loaded.\n";
    if(size_table_landscape == -1) return function_landscape(i);
    if(size_table_landscape > 0){
        i--; // because position 1 is at index 0
        if((i < 0) || (i >= size_table_landscape)){
            cerr << "ERR Landscape() : out of size of (table) loaded external potential \n size : " << size_table_landscape << " ; indice called (0..) : " << i << endl;
            return -1;
        }
        return table_landscape[i];
    }
        return -1;
}

/* ----------- I/O for the landscape values ----------------------- */
// N has to be known before

void Probleme::load_interaction_from_function(valeur (* _function_interaction) (int)){
    size_table_interaction = -1;
    function_interaction = _function_interaction;
}

void Probleme::load_interaction_from_vector(vector<valeur> _interaction){
    table_interaction.clear();
    table_interaction.resize(_interaction.size());
    int _size = _interaction.size();
    for(int i = 0; i < _size; ++i){
        table_interaction[i] = _interaction[i];
    }
    size_table_interaction = table_interaction.size();
    if(size_table_interaction == 0) cerr << "ERR Load_interaction_from_vector : no information found. No exterbal potential loaded.\n";
}

void Probleme::load_interaction_from_file(char* file_interaction){
    ifstream fichier(file_interaction, ios::in);
    if(fichier){
        table_interaction.clear();
        valeur value;
        while(fichier >> value) {
            table_interaction.push_back(value);
        }
        size_table_interaction = table_interaction.size();
        fichier.close();
        if(size_table_interaction == 0) cerr << "ERR Load_interaction_from_file(" << file_interaction << ") : no information found. No exterbal potential loaded.\n";
    } else cerr << "ERR Load_interaction_from_file(" << file_interaction << ") : Impossible d'ouvrir le fichier " << endl;
}

valeur Probleme::interaction(int i){
    if(size_table_interaction == 0) cerr << "ERR Interaction() : no interaction potential has been loaded.\n";
    if(size_table_interaction == -1) return function_interaction(i) + correct_to_interaction_function_only;
    if(size_table_interaction > 0){
        i--;
        if(i >= size_table_interaction) return 0;
        if(i < 0) {cerr << "ERR out of indices (negative) in call to interaction()\n";	return 0;}
        return table_interaction[i];
    }
        return -1;
}

/* -------------- different constructors ... ------------------------------- */

Probleme::Probleme(valeur (* _interaction)(int),	valeur (* _pot_ext)(int),	valeur _mu, int _N, int _portee, int _vol_exclus) : Mu(_mu), N(_N), portee(_portee), vol_exclus(_vol_exclus) {
    if(N < 0) cerr << "ERR: Probleme::Probleme() : l'option N = -1 est réservée aux fichiers (auquel cas N = taille fichier)\n";
    load_landscape_from_function(_pot_ext);
    load_interaction_from_function(_interaction);
    K = max(0,((N + vol_exclus - 1) / (vol_exclus)));
}
Probleme::Probleme(valeur (* _interaction)(int),	char* fichier_pot_ext,		valeur _mu, int _N, int _portee, int _vol_exclus) : Mu(_mu), N(_N), portee(_portee), vol_exclus(_vol_exclus) {
    load_landscape_from_file(fichier_pot_ext);
    load_interaction_from_function(_interaction);
    if(N < 0) N = size_table_landscape;
    K = max(0,((N + vol_exclus - 1) / (vol_exclus)));
    //K = max(0, ((N - 1) / (vol_exclus + 1)) + 1);
}
Probleme::Probleme(char* fichier_interaction,		valeur (* _pot_ext)(int),	valeur _mu, int _N, int _portee, int _vol_exclus) : Mu(_mu), N(_N), portee(_portee), vol_exclus(_vol_exclus) {
    if(N < 0) cerr << "ERR: Probleme::Probleme() : l'option N = -1 est réservée aux fichiers (auquel cas N = taille fichier)\n";
    load_landscape_from_function(_pot_ext);
    load_interaction_from_file(fichier_interaction);
    K = max(0,((N + vol_exclus - 1) / (vol_exclus)));
}
Probleme::Probleme(char* fichier_interaction,		char* fichier_pot_ext,		valeur _mu, int _N, int _portee, int _vol_exclus) : Mu(_mu), N(_N), portee(_portee), vol_exclus(_vol_exclus) {
    load_landscape_from_file(fichier_pot_ext);
    load_interaction_from_file(fichier_interaction);
    if(N < 0) N = size_table_landscape;
    K = max(0,((N + vol_exclus - 1) / (vol_exclus)));
}

Probleme::Probleme(vector<valeur> _interaction,		vector<valeur> _pot_ext,	valeur _mu, int _N, int _portee, int _vol_exclus) : Mu(_mu), N(_N), portee(_portee), vol_exclus(_vol_exclus) {
    load_landscape_from_vector(_pot_ext);
    load_interaction_from_vector(_interaction);
    if(N < 0) N = size_table_landscape;
    K = max(0,((N + vol_exclus - 1) / (vol_exclus)));
}


/* -------------functions to print a recap of the entries ------------------------- */
void Probleme::show_interaction(){
    cerr << "         ... Potentiel d'interaction entre nucléosomes : \n";
    cerr << "\t\t";
    for(int i = 1; i <= N; i++){
        if(i < vol_exclus) { cerr << "+inf\t"; }
                else if (i > portee + vol_exclus - 1) cerr << "0\t";
        else { cerr << (valeur) ((int) (interaction(i) * 100)) / 100.0 << "   ";}
    }
    cerr << endl;
}

void Probleme::show_landscape(){
    cerr << "         ... Profil énergétique extérieur en entrée (1..N) : \n";
    cerr << "\t\t";
    for(int i = 1; i <= N; ++i){
                cerr << (valeur) ((int) (landscape(i) * 100)) / 100.0 << "   ";
    }
    cerr << endl;
}

void Probleme::SumUp(){
    //cerr << "|===================================== Résumé des entrées ====================================|\n";
    cerr << "    -> Résumé des entrées ...\n";
    cerr << "         ... 1..N = " << N << " Positions ; K = " << K << "  particules au max ; Mu = " << Mu << " ; vol_exclus = " << vol_exclus << " (i.e. dist min " << vol_exclus + 1 << ")" << endl;
    show_landscape();
    show_interaction();
    cerr << "\n";
}

void Probleme::show_density(){
    cerr << "    Particles (starting point) density as function of position (1..N) : \n";
    cout << fixed;
    for(int i = 1; i <= N; ++i){
        cout << /* "\t\t" << i << "\t" << */ setprecision (10) << density[i] << "\n";
    }
}

/* ------------------ virtual function to be implemented differently regarding the algorithm/method (subclasses) -------------------- */
void Probleme::Init(){
    cerr << "If you see this message, it basically means that the init function has not be properly implemented in the sub problem class you are using.\n";
}















/* =================  I.1 : One particle, subclass for solving the Microcanonical problem, optimal configurations only =============================== */

void Microcanonique::Init(){
    SumUp();
    for(int i = 1; i < N ; ++i){
        if(interaction(i) < 0.0){cerr << "ERR : cannot launch microcanonical algorithm with an NEGATIVE INTERACTION POTENTIAL (ex: distance = " << i << ", value= " << interaction(i) << ")"<<  endl; 	exit(-1);}
        if(landscape(i) < 0.0){cerr << "ERR : cannot launch microcanonical algorithm with an NEGATIVE POTENTIALS (ex: position " << i << ", value = " << landscape(i) << ")" << endl; exit(-1);}
    }

    /* -------------- Creation du graphe --------------------- */

        /* Formulation of the inputs inside this class :
                N is the number of possible positions for the left side of a particle
                    i.e. N = L - vol_exclus + 1
                K is the maximum possible number of particles on a configuration
                portee is defined after the excluded volume (particles stuck together = portee = 1)
                    and is defined so I(portee) = 0
                    it is mode convenient to define :
                Dmax : the distance where interaction is 0 and will stay at 0.
                    Dmax = Vex + portee - 1 */
                int Dmax = vol_exclus + portee - 1;

    /* size of the graph : N columns for the positions + 1 column for gathering separately the configurations with 1, 2, 3 ... particles + source and arrivee */

    cerr << "    -> Graphe pre-created(empty), with " << graphe.nb_sommets() << " sommets" << endl;
    if(graphe.nb_sommets() != (N+1) * (K) + 2) cerr << "ERR : wrong size for graph, should be (N[=" << N << "] - 1 ) * K [=" << K << "] + 2" << endl;

    /* --------- to make sure that the weight of all edges will be positive, prepare a constant to add in the good place on the graph ... -------------------- */

    if(Mu <= 0) {CstAntiNeg = max((valeur) 1000.0, - K * Mu + 10);
        cerr << "    -> For information, " << CstAntiNeg << " will be added to every last edge for preserving positive edges even if negative mu.\n";}
    else CstAntiNeg = 0.0;

    /* defining the grid (already created with size K+1 x N) */

    G2D.source = G2D.ID(K+1,1);
    G2D.arrivee = G2D.ID(K+1,2);

    /* ---- Choix du premier nucléosome : la source lance sur la première ligne, coût landscape(x)	---- 	*/
    for(int j = 1; j <= N; ++j){
        graphe.add_to_graph(G2D.source, G2D.ID(1,j), landscape(j));
    }

    /* ---- d'une ligne �  l'autre, ID(i,j) pointe sur ID(i+1, j+k) avec poids potentiel(k) + landscape(j+k)	*/
    for(int i = 1; i <= K-1; ++i){
        for(int j = 1; j <= N; ++j){
            if(j % 100 == 0) cerr << "        ." << (i-1) * (N) + j << " / " << N*(K-1) << endl;
            for(int k = vol_exclus; k <= min(N - j, Dmax); ++k){
                graphe.add_to_graph(G2D.ID(i,j), G2D.ID(i+1,j+k), interaction(k) + landscape(j+k));
            }
            for(int k = min(N - j, Dmax) +1; k <= N-j; ++k){
                graphe.add_to_graph(G2D.ID(i,j), G2D.ID(i+1,j+k), landscape(j+k));
            }
        }
    }
    /*  et ID(I,J) pointe sur la fin de ligne avec poids i * mu 			*/
    for(int i = 1; i <= K; ++i){
        for(int j = 1; j <= N; ++j){
            graphe.add_to_graph(G2D.ID(i,j), G2D.ID(i,N+1),  CstAntiNeg + (Mu - mean_landscape) * (valeur) i);
        }
    }

    /* -> la source pointe aussi sur la dest (pas de nucléosome), poids 0		*/
    graphe.add_to_graph(G2D.source, G2D.arrivee, CstAntiNeg);
    for(int i = 1; i <= K; ++i){
        graphe.add_to_graph(G2D.ID(i, N+1), G2D.arrivee, 0.0);
    }

    cerr << "    -> Graphe construit.\n";

    all_paths_computed = false;
    nb_paths.clear();
    nb_paths_reverse.clear();
    big_table.clear();
    ensemble_solutions.clear();
    density.resize(N+2);

    cerr << "    -> Dijkstra ..." << endl;
    graphe.dijkstra(G2D.source, G2D.arrivee);
    cerr << "    -> Dijkstra reverse ..." << endl;
    graphe.dijkstra_reverse(G2D.arrivee, G2D.source);
    graphe.tri_topologique(G2D.source);
    cerr << "    -> Topological sorting ..." << endl;
    graphe.tri_topologique_reverse(G2D.arrivee);
    cerr << "    -> Reverse topological sorting ..." << endl;

    //graphe.show_graph(grille::showpostatic);
    compute_density_from_nb_paths();
    cerr << "------------------- ______________________________________________________ -------------------" << endl;

    show_density();

    cerr << endl << endl;
    show_one_best_solution();
    //export_graph();

    if(N < 201) {
        compute_density_from_all_paths();
        show_all_best_solutions();
        //show_density();
    }

    return;
}



void Microcanonique::compute_density_from_nb_paths(){
    int size = graphe.nb_sommets();
    nb_paths.clear();
    nb_paths.resize(size, 0);
    nb_paths_reverse.clear();
    nb_paths_reverse.resize(size, 0);
    density_graph.resize(size);
    density.resize(N+2);	//attention au +1 !!!

    nb_paths[G2D.source] = 1;
    nb_paths_reverse[G2D.arrivee] = 1;

    vector<bool> test_visited(size, false);			// attention, si on teste que les prédécesseurs ont été déj�  vus,
                                // ça ne marche pas parce qu'ileexiste des prédécesseurs non accessibles depuis la source.

    for(int i = 0; i < graphe.nb_sommets_ordonnes(); ++i){
        liste::iterator it;
        int u = graphe.ordre(i);
        test_visited[u] = true;
        for(it = graphe.reverse[u].begin(); it != graphe.reverse[u].end(); ++it){
            int prev = it->dest;
            if((test_visited[prev] == false) && (graphe.pred_topo(prev) >= 0)){		// this should never occur ...
                cerr << "ERR : topological order problem : vertex ";
                G2D.showpos(u);
                cerr << " has an unseen predecessor :";
                G2D.showpos(prev);
                cerr << " Please check the graph is acyclic\n";
            }
            if(egal(graphe.dist(prev) + it->weight, graphe.dist(u))){
                nb_paths[u] += nb_paths[prev];
            }
        }
    }
    test_visited.clear();
    test_visited.resize(size, false);

    for(int i = 0; i < graphe.nb_sommets_ordonnes_reverse(); ++i){
        liste::iterator it;
        int u = graphe.ordre_reverse(i);
        test_visited[u] = true;
        for(it = graphe.t[u].begin(); it != graphe.t[u].end(); ++it){
            int prev = it->dest;
            if((test_visited[prev] == false) && (graphe.pred_topo_reverse(prev) >= 0)) { //ie if also visited during topological algo
                cerr << "ERR : topological REVERSE order problem : vertex ";
                G2D.showpos(u);
                cerr << " has an unseen predecessor :";
                G2D.showpos(prev);
                cerr << " Please check the graph is acyclic\n";
            }
            if(egal(graphe.dist(prev) - it->weight, graphe.dist(u))){
                nb_paths_reverse[u] += nb_paths_reverse[prev];
            }
        }
    }

    int nb_paths_src_dest = nb_paths[G2D.arrivee];
    if(nb_paths_reverse[G2D.source] != nb_paths_src_dest) {cerr << "ERR : finds more paths in one way than in another\n";}

    if(nb_paths_src_dest == 0) cerr << "ERR compute_density_from_recursion() : destination not reached.\n";
    for(int i = 0; i < size; ++i){
        density_graph[i] = ((valeur) nb_paths[i] * (valeur) nb_paths_reverse[i] ) / ((valeur) nb_paths_src_dest);
    }

    /* maintenant, par position */
    for(int i = 1; i <= N; ++i){
        density[i] = 0.0;
    }
    for(int i = 0; i < size - 2; ++i){ //not to count source and destination (that has x = 1 et 2)
        density[G2D.pos_x(i)] += density_graph[i];
    }
}



void Microcanonique::trace_path(int dest){
    une_solution_optimale.clear();
    int i = dest;
    int cpt = 0;
    while ((graphe.pred(i) != -1) && (cpt < 100)){
        cpt++;
        if(DBG) G2D.showpos(i);
        if(cpt > 2) {
            if(DBG) cerr << "Particule en position " << G2D.pos_x(i);
            une_solution_optimale.push_back(G2D.pos_x(i));
        }
        if(DBG) cerr << endl;
        i = graphe.pred(i);
    }
    return;
}

void Microcanonique::show_one_best_solution(){

    trace_path(G2D.arrivee);
        cerr << "    => Coût optimal :" << (graphe.dist(G2D.arrivee)) - CstAntiNeg << endl;
        cerr << "    => Une solution optimale : (" << une_solution_optimale.size() << " particules) :\t";
                for(int i = 0; i < (int) une_solution_optimale.size(); ++i){
            cerr << une_solution_optimale[i] << "\t";
        }
        cerr << endl;
        int j = une_solution_optimale.size() - 1;
        cerr << "\t\t";
        for(int i = 0; i < N; ++i){
            if((j >= 0) && (une_solution_optimale[j] == i+1)){
                cerr << "N";
                j--;
            } else {
                cerr << ".";
            }
        }
        cerr << endl;
}



bool Microcanonique::egal(valeur a, valeur b){
    return (abs(a-b) < 0.000001);
}




/* -------------------- Structure for storing a path inside a graph --------------- */

void chemin::copy(chemin ch2){
    ch.clear();
    ch.resize(ch2.size());
    for(int i = 0; i < ch2.size(); ++i){
        ch[i] = ch2[i];
    }
}
int chemin::operator [] (int i) {
    return ch[i];
}
chemin::chemin(vector<int> ch2){
    ch.clear();
    ch.resize(ch2.size());
        for(int i = 0; i < (int) ch2.size(); ++i){
        ch[i] = ch2[i];
    }
}
chemin::chemin(){
    ch.clear();
}
int chemin::size(){
    return ch.size();
}
void chemin::push_back(int z){
    ch.push_back(z);
}
void chemin::show(){
    for(int i = 0; i < size(); ++i){
        cerr << ch[i] << ",   ";
    }
}



/* -------------------- Structure for manipulating a list of pathes inside a graph, with common operations on this listt ---------------- */

void ens_chemins::add(chemin &ch){
    size++;
    table.resize(size);
    table[size-1].copy(ch);
}
chemin ens_chemins::operator[](int i){
    return table[i];
}
ens_chemins::ens_chemins() {table.clear(); destination = 0; size = 0;};
void ens_chemins::set_destination(int _destination){
    destination = _destination;
}
void ens_chemins::clear(){
    size = 0;
    table.clear();
}
void ens_chemins::add(struct ens_chemins &ens){
    int to_add = ens.size;
    size = size + to_add;
    table.resize(size);
    for(int i = 0; i < to_add; ++i){
        table[size - to_add + i].copy(ens[i]);
        table[size - to_add + i].push_back(destination);
    }
}
void ens_chemins::set_to_source(){
    size = size + 1;
    table.resize(size);
    vector<int> start (1,destination);
    table[size - 1].copy(start);
}
void ens_chemins::show(){
    for(int i = 0; i < size; ++i){
        table[i].show();
        cerr << endl;
    }
}


/* ------- Exemple d'utilisation de la structure ens_chemins ---------
  l<-----	4 <----- 2 <----- 1
 l		^				  l
 6 <-l		l------- 3 <------l
 l                l
 l<----- 5 <------l

 ens_chemins big_table[MAX];
 int N = 10;
 for(int i = 0; i < N; ++i){
    big_table[i].set_destination(i);
 }
 big_table[1].set_to_source();
 big_table[2].add(big_table[1]);
 big_table[3].add(big_table[1]);
 big_table[4].add(big_table[2]);
 big_table[4].add(big_table[3]);
 big_table[5].add(big_table[3]);
 big_table[6].add(big_table[4]);
 big_table[6].add(big_table[5]);
 for(int i = 1; i <= 6; ++i){
    cout << "Chemins to " << i << " :\n";
    big_table[i].show();
    cout << "\n";
 }
 ----------------------------------------------------------------------- */


void Microcanonique::trace_all_paths(int from_arrivee){
    all_paths_computed = false;		//in case of fail
    int size = graphe.nb_sommets();

    big_table.clear();
    big_table.resize(size);
    for(int i = 0; i < size; ++i){
        big_table[i].set_destination(i);
    }
    big_table[from_arrivee].set_to_source();


    vector<bool> test_visited(size, false);			// attention, si on teste que les prédécesseurs ont été déj�  vus,
                                // ça ne marche pas parce qu'ileexiste des prédécesseurs non accessibles depuis la source.

    for(int i = 0; i < graphe.nb_sommets_ordonnes_reverse(); ++i){
        int u = graphe.ordre_reverse(i);
        test_visited[u] = true;

        liste::iterator it;
        for(it = graphe.t[u].begin(); it != graphe.t[u].end(); ++it){
            int prev = it->dest;
            if((test_visited[prev] == false) && (graphe.pred_topo_reverse(prev) >= 0)){
                cerr << "ERR : topological reverse order problem : vertex ";
                G2D.showpos(u);
                cerr << " has an unseen predecessor :";
                G2D.showpos(prev);
                cerr << " Please check the graph is acyclic\n";
            }
            if(egal(graphe.dist_reverse(prev) + it->weight, graphe.dist_reverse(u))){
                cerr << "." << flush;
                big_table[u].add(big_table[prev]);
                //nb_paths[u] += nb_paths[prev];
            }
        }
    }
    cerr << "," << endl;

    if(DBG) big_table[G2D.source].show();

    ensemble_solutions.clear();
    int TOT = big_table[G2D.source].size;
    ensemble_solutions.resize(TOT);

    for(int k = 0; k < TOT; ++k){
        int ch_size = big_table[G2D.source][k].size() - 2;	// remove arrivee and the state just before
        //cerr << "    .  solution optimale n " << k << " : " <<  ch_size << " particules) :\t";
        for(int i = 1; i < ch_size; ++i){		// remove source (first one).
            ensemble_solutions[k].push_back(G2D.pos_x(big_table[G2D.source][k][ch_size - i + 1]));
        }
    }
    all_paths_computed = true;
}


void Microcanonique::compute_density_from_all_paths(){

    int size = graphe.nb_sommets();
    density_graph.resize(size);
    density.resize(N+2);

    if(!all_paths_computed) {
        trace_all_paths(G2D.arrivee);
    }
    for(int i = 0; i < size; ++i) density_graph[i] = 0.0;

    /* Calcule la densité de chaque sommet parmi l'ensemble des solutions, il faudra alors sommer par position */
    valeur TOT = (valeur) big_table[G2D.source].size;
    if(TOT == 0){
        cerr << "ERR : Compute density called whereas no path computed.\n";
        return;
    }

    for(int i = 0; i < (int) TOT; ++i){
        int ch_size = big_table[G2D.source][i].size();
        for(int j = 0; j < ch_size; ++j){
            density_graph[big_table[G2D.source][i][j]] ++;
        }
    }
    //if(DBG) cerr << "\nDensité de chaque sommet du graphe, version exhaustive\n";
    for(int i = 0; i < size; ++i) {
        density_graph[i] = density_graph[i] / (valeur) TOT;
        if(DBG) cerr << density_graph[i] << "\t";
    }

    // maintenant, par position
    for(int i = 1; i <= N; ++i){
        density[i] = 0.0;
    }
    for(int i = 0; i < size - 2; ++i){ //not to count source and destination (that has x = 1 et 2)
        density[G2D.pos_x(i)] += density_graph[i];
    }
    //for(int i = 1; i <= N; ++i){
    //	cerr << density[i] << endl;
    //};

}


void Microcanonique::show_all_best_solutions(){
    if(!all_paths_computed) {
        trace_all_paths(G2D.arrivee);
    }
    int nb_solutions = ensemble_solutions.size();
    cerr << "    => Ensemble des solutions optimales (" << nb_solutions << "(" << endl;
    for(int k = 0; k < nb_solutions; ++k){
        int j =0;
        cerr << "\t\t";
        for(int i = 0; i < N; ++i){
                        if((j < (int) ensemble_solutions[k].size()) && (ensemble_solutions[k][j] == i+1)){
                cerr << "N";
                j++;
            } else {
                cerr << ".";
            }
        }
        cerr << "\t(" << ensemble_solutions[k].size() << " part.)\t";
                for(int i = 0; i < (int) ensemble_solutions[k].size(); ++i){
            cerr << ensemble_solutions[k][i] << ", ";
        }
        cerr << endl;
    }
}















/* =================  I.2 - GrandCanonical and Canonical resolution of the problem : possible to set fixed number of particles ============ */

/* Actually, this is the same things and possibilities than 'microcanonical'.
    However, here the graph is 'smarter' to avoid too much edges, so it's faster.
    There is no big advantage for microcanonique when enumerating all paths so the microcanonique is no reimplemented here,
    However, it's interesting to get faster the densities (canonical/grandcanonical)  */


valeur GrandCanonique::poids(valeur v){
    return std::exp(- v / kT);

}

void GrandCanonique::Init(){
    SumUp();

    /* the size of the graph is : 2 lines for the 1st / 2nd / ... particle (->2*K lines) and N columns for positions + 1 for gathering paths regarding their number of particles */
    cerr << "    -> Graphe pre-created(empty), with " << graphe.nb_sommets() << " sommets" << endl;
    if(graphe.nb_sommets() != (N+1) * (2 * K) + 2) cerr << "ERR : wrong size for graph" << endl;


    /* -------------- Creation du graphe --------------------- */

        /* Formulation of the inputs inside this class :
                N is the number of possible positions for the left side of a particle
                    i.e. N = L - vol_exclus + 1
                K is the maximum possible number of particles on a configuration
                portee is defined after the excluded volume (particles stuck together = portee = 1)
                    and is defined so I(portee) = 0
                    it is mode convenient to define :
                Dmax : the distance where interaction is 0 and will stay at 0.
                    Dmax = Vex + portee - 1 */
                int Dmax = vol_exclus + portee - 1;

    /* ---------- Assigning a source and well in the grid ------------ */
    G2D.source = G2D.ID(2*K+1,1);
    G2D.arrivee = G2D.ID(2*K+1,2);
    valeur Emean = mean_landscape;

    // Construction of graph edges
    graphe.add_to_graph(G2D.source, G2D.ID(1,1) , 0.0);
    graphe.add_to_graph(G2D.source, G2D.ID(2,1) ,  Mu - Emean + landscape(1));
    for(int i = 1; i <= K; ++i){
        for(int j = 1; j <= N-1; ++j){
            graphe.add_to_graph(G2D.ID(2*i - 1,j), G2D.ID(2*i - 1,j+1), 0.0);
            graphe.add_to_graph(G2D.ID(2*i - 1,j), G2D.ID(2*i,j+1),  Mu - Emean + landscape(j+1));
        }
    }
    for(int i = 1; i <= K-1; ++i){
        graphe.add_to_graph(G2D.ID(2*i-1, N), G2D.ID(2*i-1,N+1) , 0.0);
        graphe.add_to_graph(G2D.ID(2*i, N), G2D.ID(2*i+1,N+1) , 0.0);
    }
    graphe.add_to_graph(G2D.ID(2*K-1, N), G2D.ID(2*K-1,N+1) , 0.0);
//	graphe.add_to_graph(G2D.ID(2*K, N), G2D.ID(2*K,N+1) , 0.0); this one is already done below in the loop
    for(int j = 1; j <= N; ++j){
        graphe.add_to_graph(G2D.ID(2*K,j), G2D.ID(2*K, N+1), 0.0);
    }
    for(int i = 1; i <= K-1; ++i){
        for(int j = 1; j <= N-1; ++j){
            //if(j % 100 == 0) cerr << "        ." << (i-1) * (N) + j << " / " << N*(K-1) << endl;
            for(int k = vol_exclus; k <= min(N - j, Dmax); ++k){
                graphe.add_to_graph(G2D.ID(2*i,j), G2D.ID(2*i+2,j+k), interaction(k) + Mu - Emean + landscape(j+k));
            }
            if(j+Dmax <= N) graphe.add_to_graph(G2D.ID(2*i,j), G2D.ID(2*i+1,j+Dmax), 0.0);
            else graphe.add_to_graph(G2D.ID(2*i,j), G2D.ID(2*i+1, N+1), 0.0);
        }
    }
    for(int i = 1; i <= K; ++i){
        graphe.add_to_graph(G2D.ID(2*i - 1, N+1), G2D.arrivee, 0.0);
    }
    graphe.add_to_graph(G2D.ID(2*K, N+1), G2D.arrivee, 0.0);

    cerr << "    -> Graphe construit.\n";

    partition.clear();
    partition_reverse.clear();
    density_graph.resize(graphe.nb_sommets());
    density.resize(N+2);		// Très important le N+2 car il y a une ligne de plus que L dans la ligne e't qu'on commence �  1.

    //if(N < 20) graphe.show_graph(grille::showpostatic);

    graphe.tri_topologique(G2D.source);
    cerr << "    -> Topological sorting ..." << endl;
    graphe.tri_topologique_reverse(G2D.arrivee);
    cerr << "    -> Reverse topological sorting ..." << endl;


    //compute_density_from_recursion();
    //show_density();

    show_all_canonical_density_from_recursion(2);

    //export_graph();
    //void compute_canonical_density_from_recursion(int npart);
    return;
}


void GrandCanonique::compute_density_from_recursion(){
    int size = graphe.nb_sommets();
    partition.clear();
    partition.resize(size, 0.0);
    partition_reverse.clear();
    partition_reverse.resize(size, 0.0);
    density_graph.resize(size);
    density.resize(N+2);	//attention au +1 !!!

    partition[G2D.source] = 1.0;
    partition_reverse[G2D.arrivee] = 1.0;

    vector<bool> test_visited(size, false);			// attention, si on teste que les prédécesseurs ont été déj�  vus, ça ne marche pas parce qu'ileexiste des prédécesseurs non accessibles depuis la source.

    for(int i = 0; i < graphe.nb_sommets_ordonnes(); ++i){
        liste::iterator it;
        int u = graphe.ordre(i);
        test_visited[u] = true;
        for(it = graphe.reverse[u].begin(); it != graphe.reverse[u].end(); ++it){
            int prev = it->dest;
            if((test_visited[prev] == false) && (graphe.pred_topo(prev) >= 0)){
                cerr << "ERR : topological order problem : vertex ";
                G2D.showpos(u);
                cerr << " has an unseen predecessor :";
                G2D.showpos(prev);
                cerr << " Please check the graph is acyclic\n";
            }
            //if(egal(graphe.dist(prev) + it->weight, graphe.dist(u))){
                partition[u] += poids(it->weight) * partition[prev];
            //cerr << "p[";
            //G2D.showpos(u);
            //cout << "] += poids(" << it->weight << ")(=" << poids(it->weight) << ") * p[" << prev << "](=" << partition[prev] << ")   => new value " << partition[u] << endl;
            //}
        }
    }
    test_visited.clear();
    test_visited.resize(size, false);

    for(int i = 0; i < graphe.nb_sommets_ordonnes_reverse(); ++i){
        liste::iterator it;
        int u = graphe.ordre_reverse(i);
        test_visited[u] = true;
        for(it = graphe.t[u].begin(); it != graphe.t[u].end(); ++it){
            int prev = it->dest;
            if((test_visited[prev] == false) && (graphe.pred_topo_reverse(prev) >= 0)) { //ie if also visited during topological algo
                cerr << "ERR : topological REVERSE order problem : vertex ";
                G2D.showpos(u);
                cerr << " has an unseen predecessor :";
                G2D.showpos(prev);
                cerr << " Please check the graph is acyclic\n";
            }
            //if(egal(graphe.dist(prev) - it->weight, graphe.dist(u))){
                partition_reverse[u] +=  poids(it->weight) * partition_reverse[prev];

            //cout << "pR[";
            //G2D.showpos(u);
            //cout << "] += poids(" << it->weight << ")(=" << poids(it->weight) << ") * pR[" << prev << "](=" << partition_reverse[prev] << ")   => new value " << partition_reverse[u] << endl;
            //}
        }
    }

    valeur norm_factor = partition[G2D.arrivee];
    Z = norm_factor;

    if(norm_factor == 0) cerr << "ERR compute_density_from_recursion() : destination not reached.\n";
    if(abs(partition_reverse[G2D.source] / norm_factor - 1.0) > 0.00001 ) {cerr << "ERR : finds different normalisation factor in one way than in another(" << partition_reverse[G2D.source] << "!=" << norm_factor << "\n";}
    for(int i = 0; i < size; ++i){
        density_graph[i] = (partition[i] * partition_reverse[i] ) / (norm_factor);
    }

    /* maintenant, par position */
    for(int i = 1; i <= N; ++i){
        density[i] = 0.0;
    }
    for(int i = 0; i < size - 2; ++i){ //not to count source and destination (that has x = 1 et 2)
        if(G2D.pos_y(i) % 2 == 0) density[G2D.pos_x(i)] += density_graph[i];
    }

}


valeur GrandCanonique::compute_canonical_density_from_recursion(int nb_part){
    if((nb_part < 0) || (nb_part > K)){
        cerr << "compute_canonical_density_from_recursion : bad number of particles : " << nb_part << " asked while max number (K) = " << K << endl;
        return 0.0;
    }

    if(nb_part == 0){
        density.clear();
        density.resize(N+2, 0.0);
        return 1.0;
    }

    int new_source = 2*nb_part + 1;
    if(nb_part == K) new_source--;
    new_source = G2D.ID(new_source, N+1);

    int size = graphe.nb_sommets();
    // Partition : poids pour arriver �  ce sommet depuis la source
    // partition_reverse : poids pour aller �  a source depuis ce sommet
    partition.clear();
    partition.resize(size, 0.0);
    partition_reverse.clear();
    partition_reverse.resize(size, 0.0);
    density_graph.resize(size);
    density.resize(N+2);	//attention au +1 !!!

    partition[G2D.source] = 1.0;
    partition_reverse[new_source] = 1.0;
    graphe.tri_topologique_reverse(new_source);
    cerr << "    -> Reverse topological sorting from partial well (" << nb_part << " particles only)  ..." << endl;

    vector<bool> test_visited(size, false);			// attention, si on teste que les prédécesseurs ont été déj�  vus, ça ne marche pas parce qu'ileexiste des prédécesseurs non accessibles depuis la source.

    for(int i = 0; i < graphe.nb_sommets_ordonnes(); ++i){
        liste::iterator it;
        int u = graphe.ordre(i);
        test_visited[u] = true;
        for(it = graphe.reverse[u].begin(); it != graphe.reverse[u].end(); ++it){
            int prev = it->dest;
            if((test_visited[prev] == false) && (graphe.pred_topo(prev) >= 0)){
                cerr << "ERR : topological order problem : vertex ";
                G2D.showpos(u);
                cerr << " has an unseen predecessor :";
                G2D.showpos(prev);
                cerr << " Please check the graph is acyclic\n";
            }
            //if(egal(graphe.dist(prev) + it->weight, graphe.dist(u))){
            partition[u] += poids(it->weight) * partition[prev];
            //cerr << "p[";
            //G2D.showpos(u);
            //cout << "] += poids(" << it->weight << ")(=" << poids(it->weight) << ") * p[" << prev << "](=" << partition[prev] << ")   => new value " << partition[u] << endl;
            //}
        }
    }
    test_visited.clear();
    test_visited.resize(size, false);

    for(int i = 0; i < graphe.nb_sommets_ordonnes_reverse(); ++i){
        liste::iterator it;
        int u = graphe.ordre_reverse(i);
        test_visited[u] = true;
        for(it = graphe.t[u].begin(); it != graphe.t[u].end(); ++it){
            int prev = it->dest;
            if((test_visited[prev] == false) && (graphe.pred_topo_reverse(prev) >= 0)) { //ie if also visited during topological algo
                cerr << "ERR : topological REVERSE order problem : vertex ";
                G2D.showpos(u);
                cerr << " has an unseen predecessor :";
                G2D.showpos(prev);
                cerr << " Please check the graph is acyclic\n";
            }
            //if(egal(graphe.dist(prev) - it->weight, graphe.dist(u))){
            partition_reverse[u] +=  poids(it->weight) * partition_reverse[prev];

            //cout << "pR[";
            //G2D.showpos(u);
            //cout << "] += poids(" << it->weight << ")(=" << poids(it->weight) << ") * pR[" << prev << "](=" << partition_reverse[prev] << ")   => new value " << partition_reverse[u] << endl;
            //}
        }
    }

    valeur norm_factor = partition[new_source];

    if(norm_factor == 0) cerr << "ERR compute_density_from_recursion() : destination not reached.\n";
    if(abs(partition_reverse[G2D.source] / norm_factor - 1.0) > 0.00001 ) {cerr << "ERR : finds different normalisation factor in one way than in another(" << partition_reverse[G2D.source] << "!=" << norm_factor << "\n";}

    for(int i = 0; i < size; ++i){
        density_graph[i] = (partition[i] * partition_reverse[i] ) / (norm_factor);
    }

    /* maintenant, par position */
    for(int i = 1; i <= N; ++i){
        density[i] = 0.0;
    }
    for(int i = 0; i < size - 2; ++i){ //not to count source and destination (that has x = 1 et 2)
        if(G2D.pos_y(i) % 2 == 0) density[G2D.pos_x(i)] += density_graph[i];
    }

    return norm_factor;
}


valeur factorielle(valeur k){
    if(k <= 0.0) return 1.0;
    else return k * factorielle(k - 1.0);
}

void GrandCanonique::show_all_canonical_density_from_recursion(int TD){	//types of discernable particles to compare with
    // 0° colonne : grand-canonique ; colonne 1 ... K : répartition avec 1 ... K particules; K+1 : vérif en resommant avec pondération
    vector<vector<valeur> > stock;
    vector<valeur> templ(N, 0.0);
    stock.resize(K+2, templ);

    // Zn pour n de 1 �  L. Z0 = grandcano. Il doit y avoir 1 d'écart (pour aucun nucléosome)
    vector<valeur> repartition_particules;
    repartition_particules.resize(K+1);

    compute_density_from_recursion();
    for(int i = 0; i < N; ++i){
        stock[0][i] = density[i+1];
    }
    repartition_particules[0] = Z;

    for(int k = 1; k <= K; ++k){
        valeur Zpartial = compute_canonical_density_from_recursion(k);
        for(int i = 0; i < N; ++i){
            stock[k][i] = density[i+1];
        }
        repartition_particules[k] = Zpartial;
    }

    valeur ZWeighted = 1.0;
    for(int k = 1; k <= K; ++k){
        ZWeighted += repartition_particules[k] * (pow((valeur) TD, (valeur) k));
    }
    for(int i = 0; i < N; ++i){
        valeur sum = 0.0; // density = 0 for 0 particles
        for(int k = 1; k <= K; ++k){
            sum += stock[k][i] * repartition_particules[k] * (pow((valeur) TD, (valeur) k));
        }
        sum = sum / ZWeighted;
        stock[K+1][i] = sum / (valeur) TD;
    }


    cerr << "---------------------- _________________________________________________ ---------------------" << endl;
    cerr << " Densities for each position and each canonical number of particles : " << endl;
    cerr << "     Line : position (1 .. N=" << N << ")" << endl;
    cerr << "Ztot";
    for(int i = 1; i < K; ++i){
        cerr << "\t\tZ" << i;
    }
    cerr << "\t\tZ(Kmax=" << K << ")\tWeightedSumFor " << TD << " Discernables" << endl;
    cout << fixed;
    for(int i = 0; i < N; ++i){
        for(int k = 0; k <= K+1; ++k){
            cout << setprecision (10) << stock[k][i] << "\t";
        }
        cout << endl;
    }

    cerr << endl;
    cerr << "Repartition of weights between canonical solutions (partial Z) :" << endl;
    cerr << "GrandCanonical Z = " << Z << endl;
    valeur check_sum = 1.0;
    cerr << 0 << " particle    Z=\t" << 1 << "\tw= " << 1.0 / Z << endl;
    for(int i = 1; i <= K ; ++i){
        cerr << i << " particle(s) Z=\t" << repartition_particules[i] << "\tw= " << repartition_particules[i] / Z << endl;
        check_sum += repartition_particules[i];
    }
    cerr << "Check : sum of partial Z = " << check_sum << "\twhereas Z = " << Z << endl;
}

void GrandCanonique::show_partitions(){
        for(int i = 0; i < (int) partition.size(); ++i){
        G2D.showpos(i);
        cout << "P-> " << partition[i] << "\tP<- " << partition_reverse[i] << endl;
    }
}

























/* ================= I.3 : One particle, Fast Grand-Canonical density with simplest graph - no access to canonical densities ================== */



valeur FastGrandCanonique::poids(valeur v){
    return std::exp(- v / kT);

}

void FastGrandCanonique::Init(){

        // SumUp();  // since this is the fastest way to get densities, prints are suppressed

        // cerr << "    -> Graphe pre-created(empty), with " << graphe.nb_sommets() << " sommets" << endl;
    if(graphe.nb_sommets() != (N) * (2) + 2) cerr << "ERR : wrong size for graph" << endl;

    /* -------------- Creation du graphe --------------------- */

        /* Formulation of the inputs inside this class :
                N is the number of possible positions for the left side of a particle
                    i.e. N = L - vol_exclus + 1
                K is the maximum possible number of particles on a configuration
                portee is defined after the excluded volume (particles stuck together = portee = 1)
                    and is defined so I(portee) = 0
                    it is mode convenient to define :
                Dmax : the distance where interaction is 0 and will stay at 0.
                    Dmax = Vex + portee - 1 */
                int Dmax = vol_exclus + portee - 1;

    G2D.source = G2D.ID(3,1);
    G2D.arrivee = G2D.ID(3,2);
    valeur Emean = mean_landscape;


    /* ---- Choix du premier nucléosome : la source lance sur	---- 	*/

    graphe.add_to_graph(G2D.source, G2D.ID(1,1), Mu - Emean + landscape(1));
    graphe.add_to_graph(G2D.source, G2D.ID(2,1), 0.0);


    /* ---- d'une ligne �  l'autre, ID(i,j) pointe sur ID(i+1, j+k) avec poids potentiel(k) + landscape(j+k)	*/
        for(int j = 1; j < N; ++j){
            //if(j % 10000 == 0) cout << "        ." << (i-1) * (N) + j << " / " << N*(K-1) << endl;
            for(int k = vol_exclus; k <= min(N - j, Dmax); ++k){
                graphe.add_to_graph(G2D.ID(1,j), G2D.ID(1,j+k), interaction(k) + Mu - Emean + landscape(j+k));
            }
            if(j+vol_exclus + portee -1 <= N) graphe.add_to_graph(G2D.ID(1,j), G2D.ID(2,j+Dmax ), 0.0);
            else graphe.add_to_graph(G2D.ID(1,j), G2D.arrivee, 0.0);
            graphe.add_to_graph(G2D.ID(2,j), G2D.ID(2,j+1), 0.0);
            graphe.add_to_graph(G2D.ID(2,j), G2D.ID(1,j+1), Mu - Emean + landscape(j+1));
        }

    graphe.add_to_graph(G2D.ID(1,N), G2D.arrivee, 0.0);
    graphe.add_to_graph(G2D.ID(2,N), G2D.arrivee, 0.0);

        // cerr << "    -> Graphe construit.\n";

    partition.clear();
    partition_reverse.clear();
    density.resize(N+2);
    density_graph.resize(graphe.nb_sommets());

    //if(N < 20) graphe.show_graph(grille::showpostatic);
    graphe.tri_topologique(G2D.source);
        ///cerr << "    -> Topological sorting ..." << endl;
    graphe.tri_topologique_reverse(G2D.arrivee);
        ///cerr << "    -> Reverse topological sorting ..." << endl;

    compute_density_from_recursion();

        // cerr << "---------------------- _______________________________________________ -----------------------" << endl;

        // show_density();
    // export_graph();

    return;
}


void FastGrandCanonique::compute_density_from_recursion(){
    int size = graphe.nb_sommets();
    partition.clear();
    partition.resize(size, 0.0);
    partition_reverse.clear();
    partition_reverse.resize(size, 0.0);
    density_graph.resize(size);
    density.resize(N+2);	//attention au +1 !!!

    partition[G2D.source] = 1.0;
    partition_reverse[G2D.arrivee] = 1.0;

    vector<bool> test_visited(size, false);			// attention, si on teste que les prédécesseurs ont été déj�  vus,
                                // ça ne marche pas parce qu'ileexiste des prédécesseurs non accessibles depuis la source.

    for(int i = 0; i < graphe.nb_sommets_ordonnes(); ++i){
        liste::iterator it;
        int u = graphe.ordre(i);
        test_visited[u] = true;
        for(it = graphe.reverse[u].begin(); it != graphe.reverse[u].end(); ++it){
            int prev = it->dest;
            if((test_visited[prev] == false) && (graphe.pred_topo(prev) >= 0)){
                cerr << "ERR : topological order problem : vertex ";
                G2D.showpos(u);
                cerr << " has an unseen predecessor :";
                G2D.showpos(prev);
                cerr << " Please check the graph is acyclic\n";
            }
            partition[u] += poids(it->weight) * partition[prev];
            if(DBG){
                cout << "p[";
                G2D.showpos(u);
                cout << "] += poids(" << it->weight << ")(=" << poids(it->weight) << ") * p[" << prev << "](=" << partition[prev] << ")   => new value " << partition[u] << endl;
            }
        }
    }
    test_visited.clear();
    test_visited.resize(size, false);

    for(int i = 0; i < graphe.nb_sommets_ordonnes_reverse(); ++i){
        liste::iterator it;
        int u = graphe.ordre_reverse(i);
        test_visited[u] = true;
        for(it = graphe.t[u].begin(); it != graphe.t[u].end(); ++it){
            int prev = it->dest;
            if((test_visited[prev] == false) && (graphe.pred_topo_reverse(prev) >= 0)) { //ie if also visited during topological algo
                cerr << "ERR : topological REVERSE order problem : vertex ";
                G2D.showpos(u);
                cerr << " has an unseen predecessor :";
                G2D.showpos(prev);
                cerr << " Please check the graph is acyclic\n";
            }
            partition_reverse[u] +=  poids(it->weight) * partition_reverse[prev];
            if(DBG){
                cout << "pR[";
                G2D.showpos(u);
                cout << "] += poids(" << it->weight << ")(=" << poids(it->weight) << ") * pR[" << prev << "](=" << partition_reverse[prev] << ")   => new value " << partition_reverse[u] << endl;
            }
        }
    }

    valeur norm_factor = partition[G2D.arrivee];

    if(norm_factor == 0) cerr << "ERR compute_density_from_recursion() : destination not reached.\n";
    if(abs(partition_reverse[G2D.source] / norm_factor - 1.0) > 0.00001 ) {cerr << "ERR : finds different normalisation factor in one way than in another(" << partition_reverse[G2D.source] << "!=" << norm_factor << "\n";}
    for(int i = 0; i < size; ++i){
        density_graph[i] = (partition[i] * partition_reverse[i] ) / (norm_factor);
    }

    /* maintenant, par position */
    for(int i = 1; i <= N; ++i){
        density[i] = 0.0;
    }
    for(int i = 0; i < size - 2; ++i){ //not to count source and destination (that has x = 1 et 2)
        if(G2D.pos_y(i) == 1) density[G2D.pos_x(i)] += density_graph[i];
    }

}

void FastGrandCanonique::show_partitions(){
        for(int i = 0; i < (int) partition.size(); ++i){
        G2D.showpos(i);
        cout << "P-> " << partition[i] << "\tP<- " << partition_reverse[i] << endl;
    }
}










// ============= Exporting the constructed graph for all three cases ==============

#define sizing 250

void Microcanonique::export_graph(){

    cout << "digraph G {\n";
    cout << "\tgraph [bgcolor=lightyellow2, splines=true];\n";
    cout << "\tnode [color=yellow, style=filled, fontname=\"verdana\"];\n";

    int max_high = graphe.nb_sommets() / G2D.largeur + 1;

    for(int i = 0; i < graphe.nb_sommets(); ++i){
        cout << "A" << i << " [label=\"" << flush;
        G2D.showpos(i);
        //else cout << "S" << i;
        if(i < graphe.nb_sommets() - 2) cout << "\", pos=\"" << G2D.pos_x(i) * sizing << "," << (max_high - G2D.pos_y(i)) * sizing << "\" ";
        else if(i == graphe.nb_sommets() - 2) cout << "\", pos=\"" << 0 << "," << max_high * sizing << "\" ";
        else if(i == graphe.nb_sommets() - 1) cout << "\", pos=\"" << (G2D.largeur + 1) * sizing << "," << 0 << "\" ";
        //else cout << "\"";
        //if(((G2D.pos_y(i) / 2) % 2) == 0) cout << ", color=\"0.6 0.5 1.0\"";
        //else
        //	cout << "color=\"0.4 0.5 1.0\"";
        cout << "]\n";

                for(int j = 0; j < (int) graphe.t[i].size(); ++j){
            cout << "A" << i << " -> A" << graphe.t[i][j].dest << " [label = \"" << graphe.t[i][j].weight << "\"]" << endl;
        }
    }

    cout << "}\n";
};


void GrandCanonique::export_graph(){

    cout << "digraph G {\n";
    cout << "\tgraph [bgcolor=lightyellow2, splines=true];\n";
    cout << "\tnode [color=yellow, style=filled, fontname=\"verdana\"];\n";

    int max_high = graphe.nb_sommets() / G2D.largeur + 1;

    for(int i = 0; i < graphe.nb_sommets(); ++i){
        cout << "A" << i << " [label=\"" << flush;
        G2D.showpos(i);
        //else cout << "S" << i;
        if(i < graphe.nb_sommets() - 2) cout << "\", pos=\"" << G2D.pos_x(i) * sizing << "," << (max_high - G2D.pos_y(i)) * sizing << "\" ";
        else if(i == graphe.nb_sommets() - 2) cout << "\", pos=\"" << 0 << "," << max_high * sizing << "\" ";
        else if(i == graphe.nb_sommets() - 1) cout << "\", pos=\"" << (G2D.largeur + 1) * sizing << "," << 0 << "\" ";
        //else cout << "\"";
        if((((1 + G2D.pos_y(i)) / 2) % 2) == 0) cout << ", color=\"0.6 0.5 1.0\"";
        else cout << "color=\"0.4 0.5 1.0\"";
        cout << "]\n";

                for(int j = 0; j < (int) graphe.t[i].size(); ++j){
            cout << "A" << i << " -> A" << graphe.t[i][j].dest << " [label = \"" << graphe.t[i][j].weight << "\"]" << endl;
        }
    }

    cout << "}\n";
};


void FastGrandCanonique::export_graph(){

    cout << "digraph G {\n";
    cout << "\tgraph [bgcolor=lightyellow2, splines=true];\n";
    cout << "\tnode [color=yellow, style=filled, fontname=\"verdana\"];\n";

    int max_high = graphe.nb_sommets() / G2D.largeur + 1;

    for(int i = 0; i < graphe.nb_sommets(); ++i){
        cout << "A" << i << " [label=\"" << flush;
        G2D.showpos(i);
        //else cout << "S" << i;
        if(i < graphe.nb_sommets() - 2) cout << "\", pos=\"" << G2D.pos_x(i) * sizing << "," << (max_high - G2D.pos_y(i)) * sizing << "\" ";
        else if(i == graphe.nb_sommets() - 2) cout << "\", pos=\"" << 0 << "," << max_high * sizing << "\" ";
        else if(i == graphe.nb_sommets() - 1) cout << "\", pos=\"" << (G2D.largeur + 1) * sizing << "," << 0 << "\" ";
        //else cout << "\"";
        //if((((1 + G2D.pos_y(i)) / 2) % 2) == 0) cout << ", color=\"0.6 0.5 1.0\"";
        else cout << "color=\"0.4 0.5 1.0\"";
        cout << "]\n";

                for(int j = 0; j < (int) graphe.t[i].size(); ++j){
            cout << "A" << i << " -> A" << graphe.t[i][j].dest << " [label = \"" << graphe.t[i][j].weight << "\"]" << endl;
        }
    }

    cout << "}\n";
};



















/* =================================== I.4 One particle, resolution with Teif's method (transition matrix) ====================================== */





/* ------------------ structures to manipulate matrix, columns and lines ------------------------- */


#include <vector>
#include <iostream>
#include <map>
#include <cmath>
using namespace std;

//typedef struct horiz;
typedef struct vertic;
typedef struct matrice;

struct horiz {
    int size;
    vector<valeur> content;
    valeur operator[](int i);
    valeur operator *(vertic v);
    horiz(vector<valeur> _content);
    horiz(int _size, valeur fill);
    horiz(int _size);
    horiz();

    void operator =(horiz h){
        if(h.size != size) {
            cerr << "Erase vector size" << endl;
            size = h.size;
            content.resize(size);
        }
        for(int i = 0; i < size; ++i){
            content[i] = h[i];
        }
    }
};

struct vertic {
    int size;
    vector<valeur> content;
    valeur operator[](int i);
    valeur operator *(horiz v);
    vertic(vector<valeur> _content);
    vertic(int _size, valeur fill);
    vertic(int _size);
    vertic();

    void operator =(vertic h){
        if(h.size != size) {
            cerr << "Erase vector size" << endl;
            size = h.size;
            content.resize(size);
        }
        for(int i = 0; i < size; ++i){
            content[i] = h[i];
        }
    }
};


valeur horiz::operator [](int i){
    if((i < 0) || (i >= size)){
        cerr << "Out of bounds, horiz vector[" << i << "]" << endl;
        return 0;
    }
    return content[i];
}

valeur horiz::operator * (vertic v){
    if(v.size != size) {
        cerr << "try to multiply horiz and vertic vectors with different size !!" << endl;
        return ((valeur) 0);
    }
    valeur res = 0;
    for(int i = 0; i < size; ++i){
        res += v[i] * content[i];
    }
    return res;
}

horiz::horiz(vector<valeur> _content){
    size = _content.size();
    content.resize(size);
    for(int i = 0; i< size; ++i){
        content[i] = _content[i];
    }
}
horiz::horiz(int _size, valeur fill){
    size = _size;
    content.resize(size);
    for(int i = 0; i < size; ++i){
        content[i] = fill;
    }
}
horiz::horiz(int _size){
    size = _size;
    content.resize(_size);
    for(int i = 0; i < size; ++i){
        content[i] = (valeur) 0;
    }
}
horiz::horiz(){
    content.clear();
    size = 0;
}


valeur vertic::operator [](int i){
    if((i < 0) || (i >= size)){
        cerr << "Out of bounds, horiz vector[" << i << "]" << endl;
        return 0;
    }
    return content[i];
}

valeur vertic::operator * (horiz v){
    if(v.size != size) {
        cerr << "try to multiply horiz and vertic vectors with different size !!" << endl;
        return ((valeur) 0);
    }
    valeur res = 0;
    for(int i = 0; i < size; ++i){
        res += v[i] * content[i];
    }
    return res;
}

vertic::vertic(vector<valeur> _content){
    size = _content.size();
    content.resize(size);
    for(int i = 0; i< size; ++i){
        content[i] = _content[i];
    }
}
vertic::vertic(int _size, valeur fill){
    size = _size;
    content.resize(size);
    for(int i = 0; i < size; ++i){
        content[i] = fill;
    }
}
vertic::vertic(int _size){
    size = _size;
    content.resize(_size);
    for(int i = 0; i < size; ++i){
        content[i] = (valeur) 0;
    }
}
vertic::vertic(){
    content.clear();
    size = 0;
}


typedef int option;

struct matrice {
    int N, M;		// larg M haut N
    int type;		// 1 = function
    valeur (*f)(int i, int j, option opt);
    option opt;

    virtual valeur table(int i, int j);
    matrice(valeur (*_f)(int, int, option), int _N, int _M, option _opt);

    valeur operator ()(int i, int j);

};


matrice::matrice(valeur (*_f)(int, int, option), int _N, int _M, option _opt) : N(_N), M(_M), f(_f), opt(_opt) {
    type = 1;
}

valeur matrice::operator ()(int i, int j){
    return table(i,j);
}

valeur matrice::table(int i, int j){
    if((i < 0) || (i >= N) || (j < 0) || (j >= M)){
        cerr << "out of bounds, matrix, [" << i << "," << j << "]" << endl;
        return (valeur) 0;
    }
    return f(i,j, opt);
}




horiz operator *(horiz h, matrice M){
    horiz Res(h.size, (valeur) 0);
    if(M.N != h.size) {
        cerr << "Horiz * matrix : bad dimensions : horiz(" << h.size << " while matrix " << M.N << "lig x " << M.M << "col )" << endl;
        return Res;
    }
    if(M.M != h.size) {
        cerr << "Horiz * matrix : bad dimension for keeping vector size : horiz(" << h.size << " while matrix " << M.N << "lig x " << M.M << "col )" << endl;
        return Res;
    }

    for(int i = 0; i < h.size; ++i){
        for(int j = 0; j < h.size; ++j){
            Res.content[i] += h.content[j] * M(j,i);
        }
    }

    return Res;
}

vertic operator *(matrice M, vertic v){
    vertic Res(v.size, (valeur) 0);
    if(M.M != v.size) {
        cerr << "Horiz * matrix : bad dimensions : vertic(" << v.size << " while matrix " << M.N << "lig x " << M.M << "col )" << endl;
        return Res;
    }
    if(M.N != v.size) {
        cerr << "Horiz * matrix : bad dimension for keeping vector size : vertic(" << v.size << " while matrix " << M.N << "lig x " << M.M << "col )" << endl;
        return Res;
    }

    for(int i = 0; i < v.size; ++i){
        for(int j = 0; j < v.size; ++j){
            Res.content[i] += v.content[j] * M(i,j);
        }
    }

    return Res;
}



/* -------------------- Now, the subclass for solving with transition matrices ------------------- */

TeifGrandCanonique* _Teif;
valeur static_coeff = exp(-1);

//#define Staille _Teif->_vol_exclus() + 1
//#define Sportee _Teif->_portee()
//#define Spoids _Teif->poids()

int In(int pos){
    if((pos < 1) || (pos > _Teif->_vol_exclus() + 1)) return -1;
    return pos-1;}
int DP(int far){
    if((far < 1) || (far > _Teif->_portee())) return -150;
    return _Teif->_vol_exclus() + 1 + far-1;}
int HP(){
    return _Teif->_vol_exclus() + 1 + _Teif->_portee() + 1-1;}


valeur TeifGrandCanonique::poids(valeur v){
    return std::exp(- v / kT);
}


// Représente après atterrisage ici
valeur Qn(int i, int j, option pos){
    // les états :
    /*
     0 ... taille-1						In(prot, pos=1...tailleprot)
     taille ... taille + portee - 1		DP(prot, x=1...portee)
     HP()								taille + portee
     */

    // In(x < taille) -> In(x+1), 1.0
    if((i >= 0) && (i <= _Teif->_vol_exclus() + 1 - 2)){
        if(j == i+1) return (valeur) static_coeff;
        else return (valeur) 0;
    }

    //In(taille) -> DP(1), 1
    if((i == _Teif->_vol_exclus() + 1-1) && (j == _Teif->_vol_exclus() + 1)) return (valeur) static_coeff;

    // In(taille) -> In(1, E + I)
    if((i == _Teif->_vol_exclus() + 1 - 1) && (j == 0)) return static_coeff * _Teif->poids((valeur) _Teif->_Mu() -_Teif->_mean_landscape() + _Teif->landscape(pos) + _Teif->interaction(_Teif->_vol_exclus() + 1));

    // DP(x < taille + portee) -> DP(x+1), 1 ou In(1), Inter + landscape
    if((i >= _Teif->_vol_exclus() + 1) && (i <= _Teif->_vol_exclus() + 1 + _Teif->_portee() - 2)){
        if(j == i+1) return (valeur) static_coeff;
        if(j == 0) return static_coeff * _Teif->poids((valeur) _Teif->_Mu() - _Teif->_mean_landscape() + _Teif->landscape(pos) + _Teif->interaction(i+1));
    }

    // DP(taille + portee) -> In(1);
    if((i == _Teif->_vol_exclus() + 1 + _Teif->_portee() - 1) && (j == 0)) return static_coeff * _Teif->poids((valeur) _Teif->_Mu() - _Teif->_mean_landscape() + _Teif->landscape(pos));
    if((i == _Teif->_vol_exclus() + 1 + _Teif->_portee() - 1) && (j == _Teif->_vol_exclus() + 1 + _Teif->_portee())) return (valeur) static_coeff;

    // HP(x) -> HP(x+1)
    if((i == _Teif->_vol_exclus() + 1 + _Teif->_portee()) && (j == _Teif->_vol_exclus() + 1 + _Teif->_portee())) return (valeur) static_coeff;

    // HP(x) -> In(x+1), 1
    if((i == _Teif->_vol_exclus() + 1 + _Teif->_portee()) && (j == 0)) return static_coeff * _Teif->poids((valeur) _Teif->_Mu() - _Teif->_mean_landscape() + _Teif->landscape(pos));

    return (valeur) 0;
}

// Représente après atterrisage ici
valeur dQnn(int i, int j, option pos){
    // les états :
    /*
     0 ... taille-1						In(prot, pos=1...tailleprot)
     taille ... taille + portee - 1		DP(prot, x=1...portee)
     HP()								taille + portee
     */


    // In(taille) -> In(1, E + I)
    if((i == _Teif->_vol_exclus() + 1 - 1) && (j == 0)) return static_coeff * _Teif->poids((valeur) _Teif->_Mu() - _Teif->_mean_landscape() + _Teif->landscape(pos) + _Teif->interaction(_Teif->_vol_exclus() + 1));

    // DP(x < taille + portee) -> DP(x+1), 1 ou In(1), Inter + landscape
    if((i >= _Teif->_vol_exclus() + 1) && (i <= _Teif->_vol_exclus() + 1 + _Teif->_portee() - 2)){
        if(j == 0) return static_coeff * _Teif->poids((valeur) _Teif->_Mu() - _Teif->_mean_landscape() + _Teif->landscape(pos) + _Teif->interaction(i+1));
    }

    // DP(taille + portee) -> In(1);
    if((i == _Teif->_vol_exclus() + 1 + _Teif->_portee() - 1) && (j == 0)) return static_coeff * _Teif->poids((valeur) _Teif->_Mu() - _Teif->_mean_landscape() + _Teif->landscape(pos) );

    // HP(x) -> In(x+1), 1
    if((i == _Teif->_vol_exclus() + 1 + _Teif->_portee()) && (j == 0)) return static_coeff * _Teif->poids((valeur) _Teif->_Mu() - _Teif->_mean_landscape() + _Teif->landscape(pos));

    return (valeur) 0;
}





void TeifGrandCanonique::Init(){
    SumUp();
    _Teif = this;
    static_coeff = 1.0; //std::exp(((valeur) _Teif->_Mu() - _Teif->_mean_landscape()) / _Teif->_vol_exclus() );
    int L = N;
    int n = HP() + 1;

    density.resize(N+2, 0.0);

    vector<horiz> Afore;
    vector<horiz> dA0d;
    vector<vertic> Aback;
    vector<matrice*> MQn;
    vector<matrice*> MdQnn;

    horiz A0(n, (valeur) 0);
    vertic AN(n, (valeur) 1);
    A0.content[HP()] = (valeur) 1;
    //for(int i = taille -1; i <= HP(); ++i){
    //	AN.content[i] = (valeur) 1;
    //}

    Afore.resize(L+1, horiz(n, (valeur) 0));
    Aback.resize(L+2, vertic(n, (valeur) 0));

    MQn.resize(L+1);
    MdQnn.resize(L+1);
    cerr << "    -> Allocating ...\n";
    for(int i = 1; i <= L; ++i){
        MQn[i] = new matrice(&Qn, n, n, i);
        MdQnn[i] = new matrice(&dQnn, n, n, i);
        if(i % 200 == 0) cerr << ".";
    }
    cerr << endl;


    Afore[0] = A0;
    Aback[L+1] = AN;
    /*for(int i = 1; i <= L; ++i){
        Afore[i] = Afore[i-1] * (*MQn[i]);
        Aback[i] = (*MQn[i]) * Aback[i+1];
        if(i % 200 == 0) cerr << ".";
    }
    cerr << endl;*/


    cerr << "    -> Back and Foreward \n";
    for(int i = 1; i <= L; ++i){
        Afore[i] = Afore[i-1] * (*MQn[i]);
        Aback[L+1-i] = (*MQn[L+1-i]) * Aback[L+2-i];
        if(i % 200 == 0) cerr << ".";
    }
    cerr << endl;

    valeur test = 0;
    cout << "Forewards : " << endl;
    for(int i = 0; i < L+1; ++i){
        cout << "V" << i << "\t";
        for(int j = 0; j < n; ++j){
            if(i==L) test += Afore[i][j];
            cout << Afore[i][j] << ", ";
        }
        cout << endl;
    }

    cout << "Backwards : " << endl;
    for(int i = 0; i < L+1; ++i){
        cout << "V" << i << "\t";
        for(int j = 0; j < n; ++j){
            cout << Aback[i][j] << ", ";
        }
        cout << endl;
    }
    cout << "test : sum foreward : " << test << endl;

    valeur somme = Aback[1][HP()];

    cerr << "    -> Computing density \n";
    for(int i = 1; i <= L; ++i){
        horiz test = Afore[i-1] * (*MdQnn[i]);
        density[i] = (test*(Aback[i+1])) / somme;
        if(i % 200 == 0) cerr << ".";
    }

    cerr << "---------------------- _______________________________________________ -----------------------" << endl;

    show_density();
    return;
}
























/* =================================== II formulation of the problem with any number of particles =================================== */



















/* ----------------------- General mother class for andling inputs and data --------------------------- */


GeneralProblem::GeneralProblem(int _L, int _T, int _Dmax, Table &_interaction, Table &_pot_ext, vector<valeur> &_Mus, vector<int> _Volumes, vector<vector<int> > &_cross_or_how_to_read_interactions)
: L(_L), T(_T), Dmax(_Dmax) {
    Parse(_L, _T, _Dmax, _interaction, _pot_ext, _Mus, _Volumes, _cross_or_how_to_read_interactions);
}

GeneralProblem::GeneralProblem(int _L, int _T, int _Dmax, Table &_interaction, Table &_pot_ext, vector<valeur> &_Mus, vector<int> _Volumes)
: L(_L), T(_T), Dmax(_Dmax){
    vector<vector<int> > cross_table;
    cross_table.resize(T, vector<int>(T, 0));
    for(int i = 0; i < T; ++i){
        for(int j = 0; j < T; ++j){
            cross_table[i][j] = (i) * T + (j);
        }
    }
    Parse(_L, _T, _Dmax, _interaction, _pot_ext, _Mus, _Volumes, cross_table);
}

void GeneralProblem::Parse(int _L, int _T, int _Dmax, Table &_interaction, Table &_pot_ext, vector<valeur> &_Mus, vector<int> _Volumes, vector<vector<int> > &_cross_or_how_to_read_interactions){
///§§§§ ??????????
    if(_L < 1) {cerr << "ERR : GeneralProblem, incorrect number of positions (" << _L << ")\n";}
    if(_T < 1) {cerr << "ERR : GeneralProblem, incorrect number of species (" << _L << ")\n";}

        if(((int) _interaction.size() != T*T) || ((int) _interaction[0].size() < _Dmax)){
        cerr << "ERR : GeneralProblem, incorrect size of interaction table : \n    it should be TT (" << T*T << ") lines and >= Dmax (" << Dmax << ") rows , instead of " << _interaction.size()  << " x " << _interaction[0].size() << endl;
    }
        if(((int) _pot_ext.size() != T) || ((int) _pot_ext[0].size() != L)){
        cerr << "ERR : GeneralProblem, incorrect size of external potential (landscape) : \n    it should be T (" << T << ") lines and L (" << L << ") rows , instead of " << _pot_ext.size()  << " x " << _pot_ext[0].size() << endl;
    }
        if(((int) _cross_or_how_to_read_interactions.size() != T) || ((int) _cross_or_how_to_read_interactions[0].size() != T)){
        cerr << "ERR : GeneralProblem, incorrect size of matrix to read interaction table : \n    it should be T (" << T << ") lines and T (" << T << ") rows , instead of " << _cross_or_how_to_read_interactions.size()  << " x " << _cross_or_how_to_read_interactions[0].size() << endl;
    }
        if((int) _Mus.size() != T) {cerr << "ERR : GeneralProblem, incorrect size of Mus table : \n    it should be T (" << T << ") instead of " << _Mus.size() << endl;}
        if((int) _Volumes.size() != T) {cerr << "ERR : GeneralProblem, incorrect size of Volumes table : \n    it should be T (" << T << ") instead of " << _Volumes.size() << endl;}

        ///cerr << "     -> Loading interaction table" << endl;
    Interactions.clear();
    Interactions.resize(_interaction.size(), vector<valeur>(Dmax, 0.0));

    // checking the 'cross' is good
    for(int i = 0; i < T; ++i){
        for(int j = 0; j < T; ++j){
            //cerr << i << "," << j << "-" << _cross_or_how_to_read_interactions[i][j] << endl;
            if((_cross_or_how_to_read_interactions[i][j] < 0) || (_cross_or_how_to_read_interactions[i][j] >= (int) _interaction.size())) {cerr << "The HowtoReadInteraction file goes out of bounds of the interaction table : " << _cross_or_how_to_read_interactions[i][j] << " that should be in [0 .." << _interaction.size()-1 << endl;}
        }
    }
    // no need to do the cross here because it's implemented into the general problem
    for(int i = 0; i < (int) Interactions.size(); ++i){
            for(int k = 0; k < Dmax; ++k){
                Interactions[i][k] = _interaction[i][k];
                //cerr << i << "," << k << "\t" << Interactions[i][k] << endl;
            }

    }

    Volumes.clear();
    Volumes.resize(T, 0.0);
    for(int i = 0; i < T; ++i){
        Volumes[i] = _Volumes[i];
    }

    mean_potentials.clear();
    mean_potentials.resize(T, 0.0);
        ///cerr << "     -> Loading potentials table" << endl;
    Potentials.clear();
    Potentials.resize(L, vector<valeur>(T, 0.0));
    for(int i = 0; i < L; ++i){
        for(int j = 0; j < T; ++j){
            Potentials[i][j] = _pot_ext[j][i];
            if(i < L - Volume(j+1)) mean_potentials[j] += _pot_ext[j][i];
        }
    }
    for(int i = 0; i < T; ++i){
        mean_potentials[i] = mean_potentials[i] / (valeur) (L - Volume(i+1));
                ///cerr << "        Mean Type " << i+1 << " = " <<  mean_potentials[i] << endl;
    }

    TCross.clear();
    TCross.resize(T, vector<int>(T, 0));
    for(int i = 0; i < T; ++i){
        if((int) _cross_or_how_to_read_interactions[i].size() != T) {cerr << "ERR : the readInteraction is not square ...\n";}
        for(int j = 0; j < T; ++j){
            TCross[i][j] = _cross_or_how_to_read_interactions[i][j];
        }
    }

    Mus.clear();
    Mus.resize(T, 0.0);
    for(int i = 0; i < T; ++i){
        Mus[i] = _Mus[i];
    }

    densities.clear();
    densities.resize(L, vector<valeur>(T, 0.0));
}




int GeneralProblem::Cross(int I, int J){
    if((I < 1) || (J < 1) || (I > T) || (J > T)) {
        cerr << "ERR : Cross(" << I << "," << J << "), Out of bounds : (0.." << T << ", 0.." << T << ")" << endl;
    }
    return TCross[I-1][J-1];
}

valeur GeneralProblem::Interaction(int T1, int T2, int distance){
    if((T1 < 1) || (T2 < 1) || (T1 > T) || (T2 > T)) {
        cerr << "ERR : Interaction(" << T1 << "," << T2 << ", . ), Wrong species type ID" << endl;
        return (valeur) 0;
    }
    if((distance < 1) || (distance > min(L-1, Dmax))) {
        cerr << "ERR : Interaction, wrong distance " << distance << endl;
        return (valeur) 0;
    }
    //cerr << "I(" << T1 << "-" << T2 << ") dist " << distance << " = " << Interactions[Cross(T1,T2)][distance-1] << endl;
    return Interactions[Cross(T1,T2)][distance-1];
}

valeur GeneralProblem::Potential(int T1, int position){
    // if(position > L - Volume(T1)) cerr << "<USELESS> ";
    if((T1 < 1) || (T1 > T)) {
        cerr << "ERR : Potential(" << T1 << "," << position << "), Wrong species type ID" << endl;
        return (valeur) 0;
    }
    if((position < 1) || (position > L)) {
        cerr << "ERR : Potential(" << T1 << "," << position << "), wrong position " << endl;
        return (valeur) 0;
    }
    return Potentials[position-1][T1-1];
}






void  GeneralProblem::show_interactions(){
    cerr << "         ... Potentiel d'interaction entre particules de différents types : \n";
    for(int T1 = 1; T1 <= T; ++T1){
        for(int T2 = 1; T2 <= T; ++T2){
            cerr << "\n\tI(T" << T1 << " followed by T" << T2 <<	")\t\t";
            for(int i = 1; i <= min(L-1, Dmax); ++i){	// distance = L is impossible
                if(i < Volume(T1)) { cerr << "+inf\t"; }
                else { cerr << (valeur) ((int) (Interaction(T1, T2, i) * 100)) / 100.0 << ", ";}
            }
        }
    }
    cerr << endl;
}

void  GeneralProblem::show_potentials(){
    cerr << "         ... Profil énergétique extérieur en entrée (1..L) pour les différentes espèces : \n";
    for(int T1 = 1; T1 <= T; ++T1){
        cerr << "\tE(T" << T1 << ")=" << "\t";
        for(int i = 1; i <= L; ++i){
            cerr << (valeur) ((int) (Potential(T1, i) * 100)) / 100.0 << ", ";
        }
        cerr << endl;
    }
    cerr << endl;
}

void  GeneralProblem::SumUp(){
    //cerr << "|===================================== Résumé des entrées ====================================|\n";
    cerr << "     -> Résumé des entrées ...\n";
    cerr << "         ... 1..L = " << L << " Positions ; T = " << T << "  types de particules ; Mus[Type] = ";
    for(int T1 = 1; T1 <= T; ++T1) cerr << Mu(T1) << ", ";
    cerr << ") ; Volumes[Type] = ";
    for(int T1 = 1; T1 <= T; ++T1) cerr << Volume(T1) << ", ";
    cerr << "). i.e. distances min = volume + 1 each time" << endl;
    show_potentials();

    show_interactions();
    cerr << "\n";
}

void  GeneralProblem::show_density(){
    cerr << "    Particles (starting point) density for each kind of particle : \n";
    cout << fixed;
    for(int T1 = 1; T1 <= T; ++T1){
        cerr << "\t" << "Type " << T1 << "\t";
    }
    cerr << endl;
    for(int i = 1; i <= L; ++i){
        cout << i << "\t";
        for(int T1 = 1; T1 <= T; ++T1){
            cout << setprecision (10) << density(T1, i) << "\t";
        }
        cout << endl;
    }
}

void GeneralProblem::Init(){
    cerr << "If you see this message, it basically means that the init function has not be properly implemented in the sub problem class you are using.\n";
}






valeur GeneralFast::poids(valeur v){
    return std::exp(- v / kT);

}

void GeneralFast::Init(){
    //cerr << "kT = " << kT << endl;
        //SumUp();
    partition.clear();
    partition_reverse.clear();

        //cerr << "    -> Graphe pre-created(empty), with " << graphe.nb_sommets() << " sommets" << endl;
    if(graphe.nb_sommets() != (L) * (T+1) + 2) cerr << "ERR : wrong size for graph" << endl;
    density_graph.resize(graphe.nb_sommets());


    G2D.source = G2D.ID(T+2,1);
    G2D.arrivee = G2D.ID(T+2,2);


    /* ---- Choix du premier nucléosome : la source lance sur soit une particule de type i, soit rien	---- 	*/

    for(int i = 1; i <= T; ++i){
        graphe.add_to_graph(G2D.source, G2D.ID(i,1), Mu(i) - mean_potential(i) + Potential(i,1));
    }
    graphe.add_to_graph(G2D.source, G2D.ID(T+1,1), 0.0);


    /* ---- A partir d'une position libre d'interaction, soit on reste libre, soit on ajoute un nucléosome de type i ---- */
    for(int j = 1; j < L; ++j){
        graphe.add_to_graph(G2D.ID(T+1,j), G2D.ID(T+1,j+1), 0.0);
        for(int i = 1; i <= T; ++i){
            if(j <= L - Volume(i)) graphe.add_to_graph(G2D.ID(T+1,j), G2D.ID(i,j+1),  Mu(i) - mean_potential(i) + Potential(i,j+1));
        }
    }

    /* ---- A partir de la position de début d'une particule de type i, on peut soit ajouter une particule de type i', soit être libre de toute portée ---- */
    for(int j = 1; j <= L; ++j){
        for(int i = 1; i <= T; ++i){
            for(int i2 = 1; i2 <= T; ++i2){
                    /// pas de +1 ici			+1 ici
                //for(int k = Volume(i) + 1; k <= min(L - j - Volume(i2), Dmax); ++k){
                for(int k = Volume(i); k <= min(L - j - Volume(i2)+1, Dmax); ++k){
                    graphe.add_to_graph(G2D.ID(i,j), G2D.ID(i2,j+k), Mu(i2) - mean_potential(i2) + Potential(i2,j+k) + Interaction(i, i2, k));
                }
            }
            if(j + Dmax <= L)
                graphe.add_to_graph(G2D.ID(i,j), G2D.ID(T+1,j+ Dmax), 0);
            else // cas de la dernière colonne : OK go to well
                graphe.add_to_graph(G2D.ID(i,j), G2D.arrivee, 0);
        }
    }

    graphe.add_to_graph(G2D.ID(T+1,L), G2D.arrivee, 0.0);

        //cerr << "    -> Graphe construit.\n";
    //if(N < 20) graphe.show_graph(grille::showpostatic);
    graphe.tri_topologique(G2D.source);
        ///cerr << "    -> Topological sorting ..." << endl;
    graphe.tri_topologique_reverse(G2D.arrivee);
        ///cerr << "    -> Reverse topological sorting ..." << endl;

    compute_densities();

        ///cerr << "---------------------- _______________________________________________ -----------------------" << endl;
#ifndef USE_AS_LIBRARY
    show_density();
    SumUp();
#endif

    int Vmax = 0;
    for(int i = 0; i < T; ++i){
        Vmax = max(Vmax, Volume(i+1));
    }
    //compute_pair(3*Vmax,0, 0);
    //export_graph();
    return;
}


void GeneralFast::compute_densities(){
    int size = graphe.nb_sommets();
    partition.clear();
    partition.resize(size, 0.0);
    partition_reverse.clear();
    partition_reverse.resize(size, 0.0);
    density_graph.resize(size);

    partition[G2D.source] = 1.0;
    partition_reverse[G2D.arrivee] = 1.0;

    vector<bool> test_visited(size, false);			// attention, si on teste que les prédécesseurs ont été déj�  vus, ça ne marche pas parce qu'ileexiste des prédécesseurs non accessibles depuis la source.

    for(int i = 0; i < graphe.nb_sommets_ordonnes(); ++i){
        liste::iterator it;
        int u = graphe.ordre(i);
        test_visited[u] = true;
        for(it = graphe.reverse[u].begin(); it != graphe.reverse[u].end(); ++it){
            int prev = it->dest;
            if((test_visited[prev] == false) && (graphe.pred_topo(prev) >= 0)){
                cerr << "ERR : topological order problem : vertex ";
                G2D.showpos(u);
                cerr << " has an unseen predecessor :";
                G2D.showpos(prev);
                cerr << " Please check the graph is acyclic\n";
            }
            partition[u] += poids(it->weight) * partition[prev];
            if(DBG){
                cout << "p[";
                G2D.showpos(u);
                cout << "] += poids(" << it->weight << ")(=" << poids(it->weight) << ") * p[" << prev << "](=" << partition[prev] << ")   => new value " << partition[u] << endl;
            }
        }
    }
    test_visited.clear();
    test_visited.resize(size, false);

    for(int i = 0; i < graphe.nb_sommets_ordonnes_reverse(); ++i){
        liste::iterator it;
        int u = graphe.ordre_reverse(i);
        test_visited[u] = true;
        for(it = graphe.t[u].begin(); it != graphe.t[u].end(); ++it){
            int prev = it->dest;
            if((test_visited[prev] == false) && (graphe.pred_topo_reverse(prev) >= 0)) { //ie if also visited during topological algo
                cerr << "ERR : topological REVERSE order problem : vertex ";
                G2D.showpos(u);
                cerr << " has an unseen predecessor :";
                G2D.showpos(prev);
                cerr << " Please check the graph is acyclic\n";
            }
            partition_reverse[u] +=  poids(it->weight) * partition_reverse[prev];
            if(DBG){
                cout << "pR[";
                G2D.showpos(u);
                cout << "] += poids(" << it->weight << ")(=" << poids(it->weight) << ") * pR[" << prev << "](=" << partition_reverse[prev] << ")   => new value " << partition_reverse[u] << endl;
            }
        }
    }

    valeur norm_factor = partition[G2D.arrivee];

    if(norm_factor == 0) cerr << "ERR compute_density_from_recursion() : destination not reached.\n";
    if(abs(partition_reverse[G2D.source] / norm_factor - 1.0) > 0.00001 ) {cerr << "ERR : finds different normalisation factor in one way than in another(" << partition_reverse[G2D.source] << "!=" << norm_factor << "\n";}
    for(int i = 0; i < size; ++i){
        density_graph[i] = (partition[i] * partition_reverse[i] ) / (norm_factor);
    }

    /* maintenant, par position */
    for(int i = 1; i <= L; ++i){
        for(int j = 0; j < T; ++j){
            densities[i-1][j] = 0.0;
        }
    }

    for(int i = 0; i < size - 2; ++i){ //not to count source and destination (that has x = 1 et 2)
        if(G2D.pos_y(i) <= T) densities[G2D.pos_x(i)-1][G2D.pos_y(i) - 1] += density_graph[i];
    }

}

// éventuellement, calculer en même temps pour ne pas avoir �  tout mémoriser !!!!
valeur GeneralFast::getZ(int ID1, int ID2){
    if((ID1 == G2D.source) || (ID2 == G2D.source)) return 0.0;
    if((ID1 == G2D.arrivee) || (ID2 == G2D.arrivee)) return 0.0;
    if(!ZCreated) return 0.0;
    if(ID1 == ID2) return 1.0;
    //int i1 = G2D.pos_y(ID1);
    int j1 = G2D.pos_x(ID1);
    int i2 = G2D.pos_y(ID2);
    int j2 = G2D.pos_x(ID2);
    //cout << "read  " << ID1 << "("<< i1 << "," << j1 << ") -> " << ID2 << "(" << i2 << "," << j2 << ")" << endl;

    if(j1 >= j2) return 0.0;
    if(j1 < j2 - DeltaMax) return 0.0;	//ce cas n'est pas sensé arriver

    // But : stocker, pour ID2, le poids en provanance de ID1
    int delta = j2 - j1 - 1;	// entre 0 et DeltaMax - 1
    int pos_ZM = delta * (T+1) + i2 - 1;
    if(ZMatrix[ID1][pos_ZM] == -2.0) cerr << "ERR Call before computing Z" << endl;
    return ZMatrix[ID1][pos_ZM];


}
void GeneralFast::setZ(int ID1, int ID2, valeur v){
    if(!ZCreated) return;

    if((ID1 == G2D.source) || (ID2 == G2D.source)) return;
    if((ID1 == G2D.arrivee) || (ID2 == G2D.arrivee)) return;
    if(!ZCreated) return;
    if(ID1 == ID2) return;
    //int i1 = G2D.pos_y(ID1);
    int j1 = G2D.pos_x(ID1);
    int i2 = G2D.pos_y(ID2);
    int j2 = G2D.pos_x(ID2);

    if(j1 >= j2) return;
    if(j1 < j2 - DeltaMax) return;	//ce cas n'est pas sensé arriver

    // But : stocker, pour ID2, le poids en provanance de ID1
    int delta = j2 - j1 - 1;	// entre 0 et DeltaMax - 1
    int pos_ZM = delta * (T+1) + i2 - 1;
    if(ZMatrix[ID1][pos_ZM] != -2.0) cerr << "ERR Rewriting Z" << endl;
    ZMatrix[ID1][pos_ZM] = v;
}

void GeneralFast::compute_pair(int _DeltaMax, int Type1, int Type2, vector< vector<double> >* to_export){
        DeltaMax = _DeltaMax;                   // e
    if((Type1 < 0) || (Type1 > T)) return;	// authorize 0 as 'all'
    if((Type2 < 0) || (Type2 > T)) return;

    ZMatrix.clear();
    ZMatrix.resize(graphe.nb_sommets(), vector<valeur> (DeltaMax * (T+1), -2.0));
    ZCreated = true;

    for(int j2 = 1; j2 <= L; ++j2){
        for(int i2 = 1; i2 <= T+1; ++i2){
            for(int j = max(1, j2 - DeltaMax); j < j2; ++j){
                for(int i = 1; i <= T+1; ++i){
                    valeur Ztemp = 0.0;
                    liste::iterator it;
                    //cout << "compute Z"<< G2D.ID(i,j) << "(" << i << "," << j << ") -> " << G2D.ID(i2, j2) << "(" << i2 << "," << j2 << ")" << endl;
                    for(it = graphe.reverse[G2D.ID(i2,j2)].begin(); it != graphe.reverse[G2D.ID(i2,j2)].end(); ++it){
                        int pred = it->dest;
                        if(pred != -1) Ztemp += getZ(G2D.ID(i,j), pred) * poids(it->weight);
                        //cout << "   Add Z("<< G2D.ID(i,j) << "(" << i << "," << j << "), pred=" << pred << "(" << G2D.pos_y(pred) << "," << G2D.pos_x(pred) << ") * poids(pred->(i2,j2)) = " << getZ(G2D.ID(i,j), pred) << " * " << poids(it->weight) << endl;
                    }
                    //cout << "Res " << Ztemp << endl;
                    setZ(G2D.ID(i,j), G2D.ID(i2,j2), Ztemp);
                }
            }
        }
    }

    valeur norm_factor = partition[G2D.arrivee];
    if(norm_factor == 0) cerr << "Remind ERR compute_density_from_recursion() : destination not reached.\n";
    if(abs(partition_reverse[G2D.source] / norm_factor - 1.0) > 0.00001 ) {cerr << "Remind ERR : finds different normalisation factor in one way than in another(" << partition_reverse[G2D.source] << "!=" << norm_factor << "\n";}




    /*cout << "Ah" << getZ(G2D.ID(1, 1), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(1, 4)) << " -> " << getZ(G2D.ID(1, 4), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(1, 5)) << " -> " << getZ(G2D.ID(1, 5), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(1, 6)) << " -> " << getZ(G2D.ID(1, 6), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(2, 4)) << " -> " << getZ(G2D.ID(2, 4), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(2, 5)) << " -> " << getZ(G2D.ID(2, 5), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(2, 6)) << " -> " << getZ(G2D.ID(2, 6), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(3, 4)) << " -> " << getZ(G2D.ID(3, 4), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(3, 5)) << " -> " << getZ(G2D.ID(3, 5), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(3, 6)) << " -> " << getZ(G2D.ID(3, 6), G2D.ID(2, 7)) << endl;

    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(1, 4)) * getZ(G2D.ID(1, 4), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(1, 5)) * getZ(G2D.ID(1, 5), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(1, 6)) * getZ(G2D.ID(1, 6), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(2, 4)) * getZ(G2D.ID(2, 4), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(2, 5)) * getZ(G2D.ID(2, 5), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(2, 6)) * getZ(G2D.ID(2, 6), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(3, 4)) * getZ(G2D.ID(3, 4), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(3, 5)) * getZ(G2D.ID(3, 5), G2D.ID(2, 7)) << endl;
    cout << "§" << getZ(G2D.ID(1, 1), G2D.ID(3, 6)) * getZ(G2D.ID(3, 6), G2D.ID(2, 7)) << endl;


    exit(0)*/
    // Now, everything is computed. just displays as asked :

        // Change 18 juin 2012 ; pos < L et pas pos <=L (outof bounds)

    // 1st case : 1 particle against all other ones
    if((Type2 != 0) && (Type1 != 0)){
            if(to_export){
                to_export->resize(DeltaMax, vector<double>(L, 0.0));
            }
            int T1 = Type1;
            int T2 = Type2;
            if(COUT_ON) cerr << "Pair Function Between T" << T1 << " and T" << T2 << endl;
            if(COUT_ON) cerr << "Pos x=\tdensity(T" << T1 <<",x)\tPair(x, x+1)\tPair(x, x+2)\tPair(x, x+3)\t..." << endl;
            for(int pos = 1; pos <= L; ++pos){
                if(COUT_ON) cout << pos << "\t" << density(T1, pos) << "\t";
                for(int delta = 1; delta <= DeltaMax; ++delta){
                    if(delta <= min(L - pos - Volume(T1) + 1, DeltaMax)) {
                        double res = (partition[G2D.ID(T1, pos)] * getZ(G2D.ID(T1, pos), G2D.ID(T2, pos + delta)) * partition_reverse[G2D.ID(T2, pos + delta)]) / (norm_factor * density(T1, pos));
                        //cout << setprecision (10) << res << "\t";
                        if(to_export) (*to_export)[delta-1][pos-1] = res;
                    } else {
                        //cout << setprecision (10) << -1.0 << "\t";
                        if(to_export) (*to_export)[delta-1][pos-1] = -1.0;
                    }
                }
                //cout << endl;
            }
    }

    if((Type2 == 0) && (Type1 != 0)){
        int T1 = Type1;
        //for(int T1 = 1; T1 <= T; ++T1){
            for(int T2 = 1; T2 <= T; ++T2){
                if(COUT_ON) cerr << "Pair Function Between T" << T1 << " and T" << T2 << endl;
                if(COUT_ON) cerr << "Pos x=\tdensity(T" << T1 <<",x)\tPair(x, x+1)\tPair(x, x+2)\tPair(x, x+3)\t..." << endl;
                                for(int pos = 1; pos <= L; ++pos){
                    if(COUT_ON) cout << pos << "\t" << density(T1, pos) << "\t";
                    for(int delta = 1; delta <= DeltaMax; ++delta){
                                                if(delta <= min(L - pos- Volume(T1) + 1, DeltaMax)) cout << setprecision (10) << (partition[G2D.ID(T1, pos)] * getZ(G2D.ID(T1, pos), G2D.ID(T2, pos + delta)) * partition_reverse[G2D.ID(T2, pos + delta)]) / (norm_factor * density(T1, pos)) << "\t";
                        else if(COUT_ON) cout << setprecision (10) << -1.0 << "\t";
                    }
                    if(COUT_ON) cout << endl;
                }
            }
        //}
    }

    if((Type1 == 0) && (Type2 == 0)){
       for(int T1 = 1; T1 <= T; ++T1){
            for(int T2 = 1; T2 <= T; ++T2){
                if(COUT_ON) cerr << "Pair Function Between T" << T1 << " and T" << T2 << endl;
                if(COUT_ON) cerr << "Pos x=\tdensity(T" << T1 <<",x)\tPair(x, x+1)\tPair(x, x+2)\tPair(x, x+3)\t..." << endl;
                                for(int pos = 1; pos <= L; ++pos){
                    if(COUT_ON) cout << pos << "\t" << density(T1, pos) << "\t";
                    for(int delta = 1; delta <= DeltaMax; ++delta){
                                                if(delta <= min(L - pos- Volume(T1) + 1, DeltaMax)) cout << setprecision (10) << (partition[G2D.ID(T1, pos)] * getZ(G2D.ID(T1, pos), G2D.ID(T2, pos + delta)) * partition_reverse[G2D.ID(T2, pos + delta)]) / (norm_factor * density(T1, pos)) << "\t";
                        else if(COUT_ON) cout << setprecision (10) << -1.0 << "\t";
                    }
                    if(COUT_ON) cout << endl;
                }
            }
       }
    }

    return;
}



class Configuration {
public:
    // Code : 0 : free, 1..T : type	-1 = in excluded volume (go to left to see of whom)
    vector<int> content;
    int L;
    int T;		// Number of particle types
    valeur Energy;
public:
    valeur Energie(){
        return Energy;
    }
    Configuration(int _L, int _T): L(_L), T(_T) {
        Reset();
    }
    void Reset(){
        content.clear();
        content.resize(L, 0);
        Energy = 0;
    }
    int operator[](int i){
        if((i < 0) || (i >= L)) {cerr << "ERR : Configuration, Out of bounds (" << i << ")\n"; return -2;}
        return content[i];
    }
    int operator()(int pos){
        if((pos < 1) || (pos > L)) {cerr << "ERR : Configuration, Out of bounds (" << pos << ")\n"; return -2;}
        return content[pos-1];
    }

    bool isfree(int pos, int volume){
        if((pos < 1) || (pos > L)) {cerr << "ERR : Configuration, Out of bounds (" << pos << ")\n"; return -2;}
        if(pos + volume - 1 > L) return false;
        for(int p2 = pos; p2 <= pos + volume - 1; ++p2){
            if(content[p2-1] != 0) return false;
        }
        return true;
    }


#define MAXVAL 1e12
    int pos_pred(int pos, int maxRange = MAXVAL){
        if((pos < 1) || (pos > L)) {cerr << "ERR : Configuration, Out of bounds (" << pos << ")\n"; return -2;}
        for(int p2 = pos-1; p2 >= max(1, pos - maxRange); --p2){
            if(content[p2-1] > 0) return p2;
        }
        return -1;
    }

    int pos_next(int pos, int maxRange = MAXVAL){
        if((pos < 1) || (pos > L)) {cerr << "ERR : Configuration, Out of bounds (" << pos << ")\n"; return -2;}
        for(int p2 = pos+1; p2 <= min(L, pos + maxRange); ++p2){
            if(content[p2-1] > 0) return p2;
        }
        return -1;
    }

    void add_particle(int pos, int T1, int volume, valeur _Energy){
        if((pos < 1) || (pos > L)) { cerr << "ERR : add_particle, pos out of bounds " << pos << endl; return;}
        if((T1 < 1) || (T1 > T)) {cerr << "ERR : add_particle, type out of bounds " << T1 << " whereas max types number T= " << T << endl; return;}
        if(volume < 1) {cerr << "ERR : add_particle, wrong volume (< 1) : " << volume << endl; return; }
        Energy += _Energy;
        if((!isfree(pos, volume)) || (pos + volume - 1 > L)) {cerr << "ERR : add_particle, this place is not free/large enough !!! (pos = " << pos << ", volume = " << volume << ")." << endl; return; }
        content[pos-1] = T1;
        for(int p2 = pos+1; p2 <= min(L, pos+volume - 1); ++p2){
            content[p2-1] = -1;
        }
    }

    void remove_particle(int pos, int volume, valeur _Energy){
        if((pos < 1) || (pos > L)) { cerr << "ERR : remove_particle, pos out of bounds " << pos << endl; return;}
        //int T1 = content[pos-1];
        if(volume < 1) {cerr << "ERR : add_particle, wrong volume (< 1) : " << volume << endl; return; }
        Energy += _Energy;
        for(int p2 = pos+1; p2 <= min(L, pos+volume - 1); ++p2){
            if(content[p2-1] != -1) cerr << "ERR : remove_particle, you try to remove a particle with the wrong volume (for check)" << endl;
            content[p2-1] = 0;
        }
        content[pos-1] = 0;
    }

    void show_config(){
        for(int p2 = 1; p2 <= L; ++p2){
            cerr << content[p2-1] << " ";
        }
        cerr << "   E=" << Energy << endl;
    }
};



valeur MonteCarloDiscret::poids(valeur v){
    return std::exp(- v / kT) / 10.0;
}

// rand()%c donne un entier entre 0 et c-1
double randf(valeur a, valeur b){
    return ( rand()/(valeur)RAND_MAX ) * (b-a) + a;
}

void MonteCarloDiscret::Init(){
    Mult = 100;
    NSteps = 700;
    srand ( time(NULL) );

    // do Mult independant simulations
    Configuration a(L, T);
    for(int i = 1; i <= Mult; ++i){
        a.Reset();
        cerr << endl << "/ " << i << " of " << Mult << " /";

        // Do NSteps actions
        int j = 0;
        while(j < NSteps){

            // Chose a random position
            int rpos = 1 + (rand() % L);

            // If there is no particle here, (care for the else either both could happen)
            if(a.isfree(rpos,1)){

                // Try to put a particule : chose T times a times until it works
                bool found = false;
                for(int k = 0; (k < T) && (!found); ++k){

                    int rtype = 1 + (rand() % T);

                    // checks if there is space for this particular particle
                    if(a.isfree(rpos, Volume(rtype) + 1)){

                        // Do add this protein with probability exp(-(Mu+E+I) / kT)
                        int pred = a.pos_pred(rpos, Dmax);
                        int suiv = a.pos_next(rpos, Dmax);
                        valeur Energy = Mu(rtype) + Potential(rtype, rpos);
                        if(pred > 0) Energy += Interaction(a(pred), rtype, rpos - pred);
                        if(suiv > 0) Energy += Interaction(rtype, a(suiv), suiv - rpos);
                        valeur proba = poids(Energy);
                        if(randf(0.0, 1.0) <= proba){
                            a.add_particle(rpos, rtype, Volume(rtype) + 1, Energy);
                            //cerr << "adding\t";
                            //a.show_config();
                            j++;
                            if(j % 50 == 0) cerr << j << "->" << a.Energie() << endl;
                            cerr << ".";
                        }
                        found = true;		// even if not decided to position here
                    }
                }
            }
            else	// If there is a particle here
            {
                if(a(rpos) > 0){	// this particle starts here
                    int rtype = a(rpos);
                    int pred = a.pos_pred(rpos, Dmax);
                    int suiv = a.pos_next(rpos, Dmax);
                    valeur Energy = - Mu(rtype) - Potential(rtype, rpos);
                    if(pred > 0) Energy -= Interaction(a(pred), rtype, rpos - pred);
                    if(suiv > 0) Energy -= Interaction(rtype, a(suiv), suiv - rpos);
                    valeur proba = poids(Energy);
                    if(randf(0.0, 1.0) <= proba){
                        a.remove_particle(rpos, Volume(rtype) + 1, Energy);		// the volume is given for check
                        j++;
                        if(j % 50 == 0) cerr << j << "->" << a.Energie() << endl;
                        //cerr << "remove\t";
                        //a.show_config();

                        cerr << ".";
                    }
                }
            }
        }

        for(int pos = 1; pos <= L; ++pos){
            if(a(pos) > 0) densities[pos-1][a(pos) - 1] = densities[pos-1][a(pos) - 1] + 1;
        }
        a.show_config();
    }

    for(int i1 = 0; i1 < L; ++i1){
        for(int j = 0; j < T; ++j){
            densities[i1][j] /= (valeur) Mult;
        }
    }

    show_density();
}









valeur landscape(int i) {
    i++;
    return 1; //(valeur) (i % 10 + 1);
    //return 10 * sin((valeur) i / 10) + 15;
}

valeur potential(int i) {
    return max(6 - (valeur) i, (valeur) 0.0);
    //return abs (5.0 / ((valeur) i));
    //return 0.02 * max(250 - abs((valeur) i), 0.0);
}

void create_file_from_function(char* file_to_write, valeur (*fn)(int), int nb_lignes){
    cerr << "    -> Write in file " << file_to_write << " from function(1.." << nb_lignes <<")\n";
    ofstream fichier(file_to_write);
    if(fichier){
        for(int i = 1; i <= nb_lignes; ++i){
            fichier << fn(i) << endl;
        }
        fichier.close();
    } else cerr << "ERR : Unable to open " << file_to_write << endl;
}



valeur landscape_fixe_1(int i){
    i++;
    return 1.0;
}
valeur potential_triangle_5(int i) {
    return max(6 - (valeur) i, (valeur) 0.0);
}

valeur potential_triangle_180(int i) {
    return max(180 - (valeur) i, (valeur) 0.0) / 10.0;
}


// Précision en valeur : 17 chiffres significatifs env  ex :
//	cout << "args : " << file_landscape << " .. " << file_interaction << " .. " << N << " .. " << K << " .. " << vol_exclus << " .. "  << setprecision (20) << Mu << endl;
// ./a.out fich fich 1 2 3 0.1234567890123456789




// returns the size
int load_vector_from_file(char* file, vector<valeur> *dest, bool positive = false){
    int size = 0;
    ifstream fichier(file, ios::in);
    if(fichier){
        dest->clear();
        valeur value;
        valeur min_seen = 0.0;
        while(fichier >> value) {
            dest->push_back(value);
            min_seen = min(min_seen, value);
        }
        size = dest->size();
        fichier.close();

        if(size == 0) cerr << "ERR Load_vector_from_file(" << file << ") : no information found.\n";
        else {
            if(positive){
                if(min_seen <= 0.0){
                    cerr << "WARNING : File has negative values. (min = " << min_seen << ")\n";
                    cerr << "    -> " << - min_seen + 1.0 << " a été ajouté �  toutes les positions.\n";
                    for(int i = 0; i < size; ++i){
                        (*dest)[i] += - min_seen + 1.0;
                    }
                }
            }
        }
    } else {
        cerr << "ERR Load_vector_from_file(" << file << ") : Impossible d'ouvrir le fichier " << endl;
        return 0;
    }
    return size;
}

// returns the size
int load_vector_from_file(char* file, vector<int> *dest, bool positive = false){

    int size = 0;
    ifstream fichier(file, ios::in);
    if(fichier){
        dest->clear();
        int value;
        valeur min_seen = 0.0;
        while(fichier >> value) {
            dest->push_back(value);
            min_seen = min(min_seen, (valeur) value);
        }
        size = dest->size();
        fichier.close();

        if(size == 0) cerr << "ERR Load_vector_from_file(" << file << ") : no information found.\n";
        else {
            if(positive){
                cerr << "WRN : the use of 'positive = true' in load_vector_from ... is deprecated and useless !!" << endl;
                if(min_seen <= 0.0){
                    cerr << "WARNING : File has negative values. (min = " << min_seen << ")\n";
                    /*cerr << "    -> " << - min_seen + 1.0 << " a été ajouté �  toutes les positions.\n";
                    for(int i = 0; i < size; ++i){
                        (*dest)[i] += - min_seen + 1.0;
                    }*/
                }
            }
        }
    } else {
        cerr << "ERR Load_vector_from_file(" << file << ") : Impossible d'ouvrir le fichier " << endl;
        return 0;
    }
    return size;
}

// returns the size (nb of lines)
int load_table_from_file(char* file, Table *dest){
    int size = 0;
    ifstream fichier(file, ios::in);
    vector<valeur> tempLine;
    tempLine.clear();

    if(fichier){
        dest->clear();
        //valeur value;
        //valeur min_seen = 0.0;
    string z;
    while(getline(fichier, z)){
        tempLine.clear();
        istringstream ss(z);
        char buffer[256];
            while(ss >> buffer) {
                    tempLine.push_back((valeur) atof(buffer));
        }
        dest->push_back(tempLine);
    }

        size = dest->size();
        fichier.close();

        if(size == 0) cerr << "ERR Load_vector_from_file(" << file << ") : no information found.\n";

    } else {
        cerr << "ERR Load_vector_from_file(" << file << ") : Impossible d'ouvrir le fichier " << endl;
        return 0;
    }
    return size;
}

// returns the size (nb of lines)
int load_table_from_file(char* file, vector< vector<int> > *dest){
    int size = 0;
    ifstream fichier(file, ios::in);
    vector<int> tempLine;
    tempLine.clear();

    if(fichier){
        dest->clear();
        //int value;
        //valeur min_seen = 0.0;
    string z;
    while(getline(fichier, z)){
        tempLine.clear();
        istringstream ss(z);
        char buffer[256];
            while(ss >> buffer) {
                    tempLine.push_back((int) atoi(buffer));
        }
        dest->push_back(tempLine);
    }

        size = dest->size();
        fichier.close();

        if(size == 0) cerr << "ERR Load_vector_from_file(" << file << ") : no information found.\n";

    } else {
        cerr << "ERR Load_vector_from_file(" << file << ") : Impossible d'ouvrir le fichier " << endl;
        return 0;
    }
    return size;
}


#ifdef COMMAND_LINE

int main(int argc,char *argv[]) {

    if((argc != 14) && (argc != 8) && (argc != 3) && (argc != 10)){
        cerr << "      \n";
        cerr << "  ---------------------------------------- Welcome to Cleozone in command line mode -------------------------------------------- \n";
        cerr << "      \n";
        cerr << "      \n";
    cerr << "Usage 1 : For one kind of particles\n";
        cerr << "	" << argv[0] << "   InteractionFile   LandscapeFile   Mu   L   portee   vol_exclus   methode\n";
        cerr << "      \n";
        cerr << "Remarks:\n";
        cerr << "      - method: 	1 = Microcanonical;      (list all configurations)\n";
        cerr << "      			2 = (grand)canonical;    (densities for all number of particles)\n";
        cerr << "      			3 = Fast,GrandCanonical; (global densities)\n";
    cerr << "			4 = Teif, GrandCanonical.(idem, for checking. Slower method)\n";
        cerr << "      - L is the number of positions to simulate (length). you can put -1 for L so as to take the size given in landscape file\n";
        cerr << "      - InteractionFile : interaction for each distance between the start (left side) of 2 consecutive particles\n";
    cerr << "      		the interaction file can be incomplete (it will be then completed by 0).\n";
        cerr << "      - LandscapeFile : Affinity for each position in the sequence (negative = attractive)\n";
        cerr << "      - Mu : Nonspecific affinity for a particle to the matrix, whatever the position\n";
        cerr << "      - portee is the distance when there is no more interactions (even if interactions are longer in FileI)\n";
        cerr << "      - vol_exclus : Incompressible size a particle occupies on the matrix (no possible overlap) min = 2\n";
        cerr << "      \n";
    cerr << "      - the output is (depends on method): 1,3=>densities. 2=>all particles' number densities.\n";
    cerr << "      - feel free to redirect text output to a file adding '2> file.txt' or '> file.txt'. Redirect errors with '1> file.txt'\n";
        cerr << "      \n";
    cerr << "Have fun!      (300kb aproved, with vol_excl=146 and portee=50 !!!)\n";
        cerr << "      \n";
        cerr << "      \n";
        cerr << "      \n";
        cerr << "      \n";
        cerr << "Usage 2 : 2 or more kind of particles \n";
        cerr << "	" << argv[0] << "  L   2   Dmax   FileI1-1  FileI1-2     FileI2-2     FileE1    FileE2   Mu1  Mu2  Vex1  Vex2  methode\n";
        cerr << "	" << argv[0] << "  L   T   Dmax   FileI     FileForReadingI   FileE   FileMus   FilsVexs   methode\n";
        cerr << "      \n";
        cerr << "Remarks:\n";
        cerr << "      - L is the length of the sequence\n";
        cerr << "      - T is the number of different kinds of particles\n";
        cerr << "      		the version for L=2 assumes that I1-2 = I2-1 (symetrical interactions)\n";
        cerr << "      - Dmax is the distance when there is no more interactions (even if interactions are longer in FileI)\n";
        cerr << "      		note that the distance is defined between the start (left side) of 2 consecutive particles\n";
        cerr << "      - FileI is the interaction profiles. one line is one interaction profile ; the same can be used multiple times\n";
        cerr << "      		note that first interaction values are useless because of the excluded volume\n";
        cerr << "      - FileForReadingI associates a couple of particles to the line to read in FileI\n";
        cerr << "      		Ex:	(Second part.)	(P1)	(P2)	...	(P5)	\n";
        cerr << "      		(First part.)						\n";
        cerr << "      			(P1)		1	2	...	5	\n";
        cerr << "      			(P2)		6	7		8	\n";
        cerr << "      		In this example, interaction P1-P2 will be in line 2 of fileI while P2-P1 in line 7 \n";
        cerr << "      - FileE has one line of affinities per particle, in order\n";
        cerr << "      - fileMus has the list of Mus for each kind of particle\n";
        cerr << "      - fileVex has the list of excluded volumes for each kind of particle\n";
        cerr << "      - methode =\n";
        cerr << "      		1 for grandcanonical algorithm\n";
        cerr << "      		2 for simulation with a monte-carlo algorithm (for comparison but not trustable)\n";
        cerr << "      \n";
        cerr << "      \n";
        cerr << "Usage 3 : Draw examples\n";
        cerr << "	" << argv[0] << "  Example   0\n";
        cerr << "      		Creates some files in the current folder so you can play with it : \n";
        cerr << "      			LandCst1.txt	containing a landscape of L=10000 with 1.0 averywhere\n";
        cerr << "      			InterTrg5.txt	containing values of interactions looking like a triangle, portee = 5\n";
        cerr << "      			InterTrg180.txt	containing values of interactions triangle shape portee = 180\n";
        cerr << "      \n";
        cerr << "      		you can try this afterwards :\n";
        cerr << "      			./" << argv[0] << " InterTrg5.txt  LandCst1.txt  -1.0  100  5  3  1\n";
        cerr << "      			./" << argv[0] << " InterTrg5.txt  LandCst1.txt  -1.0  100  5  3  2\n";
        cerr << "      			./" << argv[0] << " InterTrg5.txt  LandCst1.txt  -1.0  100  5  3  3\n";
        cerr << "      			./" << argv[0] << " InterTrg5.txt  LandCst1.txt  -1.0  100  5  3  4\n";
        cerr << "      \n";
        cerr << "	" << argv[0] << "  Example   1\n";
        cerr << "      		Creates some files in the current folder so you can play with it : \n";
        cerr << "      			E1.txt	 containing a landscape of L=20\n";
        cerr << "      			I11.txt, I12.txt I22.txt   interactions (starting at -5 and up to 0 at distance 5)\n";
        cerr << "      \n";
        cerr << "      		you can try this afterwards :\n";
        cerr << "      			./" << argv[0] << " 20  2  5  I11.txt  I12.txt  E1  -1.0   -1.2   2   3   1\n";
        cerr << "      \n";
        cerr << "      \n";
        cerr << "	" << argv[0] << "  Example   2\n";
        cerr << "      		Creates some files in the current folder so you can play with it : \n";
        cerr << "      		Example with 4 particles, kind 1 and 2 cooperating and 3 and 4 cooperating while (1/2)-(3/4) repulsing each other\n";
        cerr << "      			Potentiels.txt \n";
        cerr << "      			Interactions.txt \n";
        cerr << "      			ReadInter.txt  \n";
        cerr << "      			Vexs.txt \n";
        cerr << "      			Mus.txt \n";
        cerr << "      			\n";
    cerr << "      		you can try this afterwards :\n";
        cerr << "      			" << argv[0] << " 40  4   25  Interactions.txt  ReadInter.txt   Potentiels.txt   Mus.txt   Vexs.txt   1\n";
        cerr << "      \n";
        cerr << "      \n";
        cerr << "      \n";
        cerr << "      		ADD '2> trash.txt' in the command to redirect error messages to this file and only keep results ! ...\n";

        //cerr << "                 2: " << argv[0] << "   numero_example\n";
        exit(-1);
    }

    if(argc == 8){
        char* file_interaction=argv[1];
        char* file_landscape=argv[2];
        valeur Mu = atof(argv[3]);
        int N = atoi(argv[4]);
        int portee = atoi(argv[5]);
        int vol_exclus = atoi(argv[6]);


        int trace = 0;
        if(argc > 7){
            int methode = atoi(argv[7]);
            if(methode == 1){
                cerr << "------------------- Microcanonical Algorithm (optimal configurations only) -------------------" << endl;
                Microcanonique firstway(file_interaction, file_landscape, Mu, N, portee, vol_exclus);
                firstway.Init();
            }
            if(methode == 2){
                cerr << "---------------------- Canonical Algorithm (for all number of particles) ---------------------" << endl;
                GrandCanonique firstway(file_interaction, file_landscape, Mu, N, portee, vol_exclus);
                firstway.Init();
                //firstway.show_partitions();
            }
            if(methode == 3){
                cerr << "---------------------- Grand Canonical Algorithm Only (Segal Inspired) -----------------------" << endl;
                FastGrandCanonique firstway(file_interaction, file_landscape, Mu, N, portee, vol_exclus);
                firstway.Init();
                firstway.show_density();
                //firstway.show_partitions();
            }
            if(methode == 4){
                cerr << "------------------- Fast Grand Canonical Algorithm (Teif Transfer Matrix) -------------------------" << endl;
                TeifGrandCanonique firstway(file_interaction, file_landscape, Mu, N, portee, vol_exclus);
                firstway.Init();
                //firstway.show_partitions();
            }
        }
    }

    if(argc == 3){
        int num_example = atoi(argv[2]);
        switch (num_example){
            case 0:{
                cerr << "Example 0 : creating different landscape and interaction files LandCst1.txt and InterTrg5.txt" << endl;
                create_file_from_function((char*) "LandCst1.txt", &landscape_fixe_1, 10000);
                create_file_from_function((char*) "InterTrg5.txt", &potential_triangle_5, 10);
                create_file_from_function((char*) "InterTrg180.txt", &potential_triangle_180, 200);
                break;
            }
            case 1:{
                int L = 20;
                {
                    ofstream fichier("I11.txt");	// attractif
                if(fichier){
                    for(int i = 1; i <= L; ++i){
                        fichier << min((valeur) 0.0, -5.0 + (valeur) i) << endl;
                    }
                    fichier.close();
                } else cerr << "ERR : Unable to open I11.txt" << endl;
                }

                {ofstream fichier("I12.txt");	// REPULSIF
                if(fichier){
                    for(int i = 1; i <= L; ++i){
                        fichier << max((valeur) 0.0, +5.0 - (valeur) i) << endl;
                    }
                    fichier.close();
                } else cerr << "ERR : Unable to open I12.txt" << endl;
                }
                {ofstream fichier("I22.txt");	// attractif
                if(fichier){
                    for(int i = 1; i <= L; ++i){
                        fichier << min((valeur) 0.0, -5.0 + (valeur) i) << endl;
                    }
                    fichier.close();
                } else cerr << "ERR : Unable to open I22.txt" << endl;
                }

                {ofstream fichier("E1.txt");
                if(fichier){
                    for(int i = 1; i <= L; ++i){
                        fichier << 1.0 << endl;
                    }
                    fichier.close();
                } else cerr << "ERR : Unable to open E1.txt" << endl;
                }
                break;
            }
            case 2:{
                int L = 40;

                {ofstream fichier("Potentiels.txt");
                if(fichier){
                    for(int p = 0; p < 4; ++p){
                        for(int i = 1; i <= L; ++i){
                            fichier << 0 << "\t";
                        }
                    fichier << "\n";
                    }
                    fichier.close();
                } else cerr << "ERR : Unable to open Potentiels.txt" << endl;
                }
                {ofstream fichier("Interactions.txt");

                if(fichier){
                    fichier << "0	0	0	0	0	0	0	0	0	0	-0.5	0	0	0	0	0	0	0	0	0	0	0	0	0	0\n0	0	0	0	0	0	0	0	0	0	-0.5	0	0	0	0	0	0	0	0	0	0	0	0	0	0\n0	0	0	0	0	0	0	0	0	0	0.5	0	0	0	0	0	0	0	0	0	0	0	0	0	0\n0	0	0	0	0	0	0	0	0	0	0.5	0	0	0	0	0	0	0	0	0	0	0	0	0	0\n0	0	0	0	0	0	0	0	0	0	0	0	0	0	-0.5	0	0	0	0	0	0	0	0	0	0\n0	0	0	0	0	0	0	0	0	0	0	0	0	0	-0.5	0	0	0	0	0	0	0	0	0	0\n0	0	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	0	0	0	0	0	0	0	0	0\n0	0	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	0	0	0	0	0	0	0	0	0\n0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	0	0	0\n0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0.5	0	0	0	0\n0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	-0.5	0	0	0	0\n0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	-0.5	0	0	0	0\n0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0.5\n0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0.5\n0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	-0.5\n0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	0	-0.5\n";
                    fichier.close();
                } else cerr << "ERR : Unable to open Interactions.txt" << endl;
                }
                {ofstream fichier("ReadInter.txt");
                if(fichier){
                    fichier << "0	1	2	3\n4	5	6	7\n8	9	10	11\n12	13	14	15\n";
                    fichier.close();
                } else cerr << "ERR : Unable to open ReadInter.txt" << endl;
                }


                {ofstream fichier("Mus.txt");
                if(fichier){
                    fichier << "0	-0.5	-1	-1.5\n";
                    fichier.close();
                } else cerr << "ERR : Unable to open Mus.txt" << endl;
                }
                {ofstream fichier("Vexs.txt");
                if(fichier){
                    fichier << "11	15	21	25\n";
                    fichier.close();
                } else cerr << "ERR : Unable to open Vexs.txt" << endl;
                }
                break;
            }
            default:{
            }
        };
    }

    if(argc == 14){
        int L =  atoi(argv[1]);
        int T =  atoi(argv[2]);
        if(T != 2) {
            cerr << "ERR : the command you chose is only for 2 kinds of particles ...\n";
            return -1;
        }
        int Dmax =  atoi(argv[3]);
        char* fileI11=argv[4];
        char* fileI12=argv[5];
        char* fileI22=argv[6];
        char* fileE1=argv[7];
        char* fileE2=argv[8];
        valeur Mu1 = atof(argv[9]);
        valeur Mu2 = atof(argv[10]);
        int vol_exclus1 = atoi(argv[11]);
        int vol_exclus2 = atoi(argv[12]);
        int methode = atoi(argv[13]);


        vector<valeur> Mus(2, 0.0);
        Mus[0] = Mu1; Mus[1] = Mu2;
        vector<int> Volumes(2, 0);
        Volumes[0] = vol_exclus1; Volumes[1] = vol_exclus2;
        vector<valeur> I11, I12, I22, E1, E2;

        Table Interactions(4);
        Table Potentiels(2);
        int V1 = load_vector_from_file(fileI11, &(Interactions[0]));
        int V2 = load_vector_from_file(fileI12, &(Interactions[1]));
        int V3 = load_vector_from_file(fileI12, &(Interactions[2]));
        int V2b = load_vector_from_file(fileI22, &(Interactions[3]));
        int V4 = load_vector_from_file(fileE1, &(Potentiels[0]));
        int V5 = load_vector_from_file(fileE2, &(Potentiels[1]));

        cerr << "GO !" << endl;
        if(methode == 2) {
            MonteCarloDiscret firstway(L, T, Dmax, Interactions, Potentiels, Mus, Volumes);
            firstway.Init();
        }
        if(methode == 1){
            GeneralFast firstway(L, T, Dmax, Interactions, Potentiels, Mus, Volumes);
            firstway.Init();
        }
    }

    if(argc == 10){
        //         cerr << "   ./" << argv[0] << "  L   T   Dmax   FileI     FileForReadingI   FileE   FileMus   FilsVexs   methode\n";
        int L =  atoi(argv[1]);
        int T =  atoi(argv[2]);
        int Dmax =  atoi(argv[3]);
        char* fileI=argv[4];
        char* fileReadingI=argv[5];
        char* fileE=argv[6];
        char* fileMus=argv[7];
        char* fileVexs=argv[8];

        int methode = atoi(argv[9]);

        if((L < 1) || (L > 1e12)) {cerr << "ERR : wrong size of sequence " << L << "\n"; return -1;}
        if((T < 1) || (T > 300)) {cerr << "ERR : wrong number of particles " << L << ", should be in [1 .. 300]\n"; return -1;}
        vector<valeur> Mus(T, 0.0);
        vector<int> Vexs(T, 0.0);
        //vector<string> Names(T, string(""));
        Table* Interactions = new Table();
        vector< vector<int> >* ReadInteractions = new vector< vector<int> >();
        Table* Potentiels = new Table();

        int V1 = load_vector_from_file(fileMus, &Mus);
        int V2 = load_vector_from_file(fileVexs, &Vexs);
    int V3 = load_table_from_file(fileI, Interactions);
    int V4 = load_table_from_file(fileReadingI, ReadInteractions);
    int V5 = load_table_from_file(fileE, Potentiels);


        cerr << "GO !" << endl;
        if(methode == 2) {
            MonteCarloDiscret firstway(L, T, Dmax, *Interactions, *Potentiels, Mus, Vexs, *ReadInteractions);
            firstway.Init();
        }
        if(methode == 1){
            GeneralFast firstway(L, T, Dmax, *Interactions, *Potentiels, Mus, Vexs, *ReadInteractions);
            firstway.Init();
        }

    delete Interactions;
    delete ReadInteractions;
    delete Potentiels;
    }


    // GeneralFast(int _L, int _T, int _Dmax, Table &_interaction, Table &_pot_ext, vector<valeur> &_Mus, vector<int> _Volumes, valeur _kT = 1.0)
    //	: GeneralProblem(_L, _T, _Dmax, _interaction, _pot_ext, _Mus, _Volumes) , G2D(L), graphe((L) * (T+1) + 2) {kT = _kT; ZCreated = false;};


    exit(0);

}

#endif


void Example1(){
    int Vex = 3;
    int L = 9;
    int N = L - Vex + 1;
    int portee = 3;

    vector<valeur> z(7, 0.0);
    z[0] = 0.0;
    z[1] = 0.0;
    z[2] = 0.0;
    z[3] = 0.0;
    z[4] = 0.0;
    z[5] = 0.0;
    z[6] = 0.0;

    vector<valeur> I(7,0.0);
    //I[0] = 0.0;	// useless because within volume
    //I[1] = 0.0;	// idem
    I[2] = 0.5;
    I[3] = 0.2;
    I[4] = 0.1;

    cerr << "\n\n\n-------------------- Microcano ---------------\n\n\n";
    valeur mu = -2.0;
    {Microcanonique m(I, z, mu, N, portee, Vex);
    m.Init();
    m.export_graph();
    }
cerr << "\n\n\n-------------------- Cano ---------------\n\n\n";
    {GrandCanonique m(I, z, mu, N, portee, Vex);
    m.Init();
m.export_graph();
    }

cerr << "\n\n\n-------------------- FastGrandocano ---------------\n\n\n";
    {FastGrandCanonique m(I, z, mu, N, portee, Vex);
    m.Init();
    m.show_density();
    m.export_graph();
    cerr << "I=" << m.interaction(4) << endl;
    }
}

void Example2(){

    vector<int> Vexs(2, 0);
    Vexs[0] = 3;
    Vexs[1] = 6;

    vector<valeur> Mus(2, 0.0);
    Mus[0] = -1.0;
    Mus[1] = -2.0;

    //int L = 12;
    //int Dmax = 10;

    vector< vector<valeur> > Pots;
    Pots.resize(2);
    Pots[0].resize(12, 0.0);
    Pots[1].resize(12, 0.0);

    /* Potentials for P1  */
    Pots[0][0] = 0.0;
    Pots[0][1] = 0.0;
    Pots[0][2] = 0.0;
    Pots[0][3] = 0.0;
    Pots[0][4] = 0.0;
    Pots[0][5] = 0.0;
    Pots[0][6] = 0.0;
    Pots[0][7] = 0.0;
    Pots[0][8] = 0.0;
    Pots[0][9] = 0.0;
    //Pots[0][10] = 0.0;
    //Pots[0][11] = 0.0;

    /* Potentials for P2   */
    Pots[1][0] = 0.0;
    Pots[1][1] = 0.0;
    Pots[1][2] = 0.0;
    Pots[1][3] = 0.0;
    Pots[1][4] = 0.0;
    Pots[1][5] = 0.0;
    Pots[1][6] = 0.0;
    //Pots[1][7] = 0.0;
    //Pots[1][8] = 0.0;
    //Pots[1][9] = 0.0;
    //Pots[1][10] = 0.0;
    //Pots[1][11] = 0.0;


    vector< vector<valeur> > Inter;
    Inter.resize(4);
    Inter[0].resize(12);
    Inter[1].resize(12);
    Inter[2].resize(12);
    Inter[3].resize(12);

    // useless values are commented (within volume or too far for L=12)
    // I1-1
    //Inter[0][0] = 0.0;
    //Inter[0][1] = 0.0;
    Inter[0][2] = -0.5;
    Inter[0][3] = -0.25;
    Inter[0][4] = -0.125;
    Inter[0][5] = 0.0;
    Inter[0][6] = 0.0;
    Inter[0][7] = 0.0;
    Inter[0][8] = 0.0;
    //Inter[0][9] = 0.0;
    //Inter[0][10] = 0.0;
    //Inter[0][11] = 0.0;

    // I1-2
    //Inter[1][0] = 0.0;
    //Inter[1][1] = 0.0;
    Inter[1][2] = 0.5;
    Inter[1][3] = 0.25;
    Inter[1][4] = 0.125;
    Inter[1][5] = 0.0;
    //Inter[1][6] = 0.0;
    //Inter[1][7] = 0.0;
    //Inter[1][8] = 0.0;
    //Inter[1][9] = 0.0;
    //Inter[1][10] = 0.0;
    //Inter[1][11] = 0.0;

    // I2-1
    //Inter[2][0] = 0.0;
    //Inter[2][1] = 0.0;
    //Inter[2][2] = 0.0;
    //Inter[2][3] = 0.0;
    //Inter[2][4] = 0.0;
    Inter[2][5] = 1;
    Inter[2][6] = 0.5;
    Inter[2][7] = 0.25;
    Inter[2][8] = 0.0;
    //Inter[2][9] = 0.0;
    //Inter[2][10] = 0.0;
    //Inter[2][11] = 0.0;

    // I2-2
    //Inter[3][0] = 0.0;
    //Inter[3][1] = 0.0;
    //Inter[3][2] = 0.0;
    //Inter[3][3] = 0.0;
    //Inter[3][4] = 0.0;
    Inter[3][5] = -0.5;
    //Inter[3][6] = 0.0;
    //Inter[3][7] = 0.0;
    //Inter[3][8] = 0.0;
    //Inter[3][9] = 0.0;
    //Inter[3][10] = 0.0;
    //Inter[3][11] = 0.0;

    vector< vector<int> > ToRead;
    ToRead.resize(2);
    ToRead[0].resize(2,0);
    ToRead[1].resize(2,0);

    ToRead[0][0] = 0;	// i.e. I1-1 is in Inter[0]
    ToRead[0][1] = 1;	// i.e. I1-2 is in Inter[1]
    ToRead[1][0] = 2;	// i.e. I2-1 is in Inter[2]
    ToRead[1][1] = 3;	// i.e. I2-2 is in Inter[3]


    GeneralFast m(12, 2, 10, Inter, Pots, Mus, Vexs, ToRead, 1.0);
    m.SumUp();
    m.Init();
    m.show_density();


}




#ifdef LAST_WAVE
void C_cleozone2(char** argv){
    char* file_interaction;
    char* file_landscape;
    double Mu;
    int N;
    int portee;
    int vol_exclus;
    int methode;

    argv = ParseArgv(argv, tSTR, &file_interaction, tSTR, &file_landscape, tDOUBLE, &Mu, tINT, &N, tINT, &portee, tINT, &vol_exclus, tINT, &methode, 0);
    //NoMoreArgs(argv);

    if(methode == 1){
        cerr << "------------------- Microcanonical Algorithm (optimal configurations only) -------------------" << endl;
        Microcanonique firstway(file_interaction, file_landscape, (valeur) Mu, N, portee, vol_exclus);
        firstway.Init();
    }
    if(methode == 2){
        cerr << "---------------------- Canonical Algorithm (for all number of particles) ---------------------" << endl;
        GrandCanonique firstway(file_interaction, file_landscape, (valeur) Mu, N, portee, vol_exclus);
        firstway.Init();
        //firstway.show_partitions();
    }
    if(methode == 3){
        cerr << "---------------------- Grand Canonical Algorithm Only (Segal Inspired) -----------------------" << endl;
        FastGrandCanonique firstway(file_interaction, file_landscape, (valeur) Mu, N, portee, vol_exclus);
        firstway.Init();
        //firstway.show_partitions();
    }
    if(methode == 4){
        cerr << "------------------- Fast Grand Canonical Algorithm (Teif Transfer Matrix) -------------------------" << endl;
        TeifGrandCanonique firstway(file_interaction, file_landscape, (valeur) Mu, N, portee, vol_exclus);
        firstway.Init();
        //firstway.show_partitions();
    }

}

void C_cleozone(char** argv){
    SIGNAL Sinteraction;
    SIGNAL Slandscape;

    double Mu;
    int N;
    int portee;
    int vol_exclus;
    int methode;

    argv = ParseArgv(argv, tSIGNALI, &Sinteraction, tSIGNALI, &Slandscape, tDOUBLE, &Mu, tINT, &N, tINT, &portee, tINT, &vol_exclus, tINT, &methode, 0);

    vector<valeur> Vinteraction(Sinteraction->size, 0.0);
    vector<valeur> Vlandscape(Slandscape->size, 0.0);
    for(int i = 0; i < Sinteraction->size; ++i){
        Vinteraction[i] = (valeur) Sinteraction->Y[i];
    }
    for(int i = 0; i < Slandscape->size; ++i){
        Vlandscape[i] = (valeur) Slandscape->Y[i];
    }

    Probleme* Current_problem = NULL;
    if(methode == 1){
        cerr << "------------------- Microcanonical Algorithm (optimal configurations only) -------------------" << endl;
        Microcanonique firstway(Vinteraction, Vlandscape, (valeur) Mu, N, portee, vol_exclus);
        Current_problem = &firstway;
        firstway.Init();
    }
    if(methode == 2){
        cerr << "---------------------- Canonical Algorithm (for all number of particles) ---------------------" << endl;
        GrandCanonique firstway(Vinteraction, Vlandscape, (valeur) Mu, N, portee, vol_exclus);
        Current_problem = &firstway;
        firstway.Init();
        //firstway.show_partitions();
    }
    if(methode == 3){
        cerr << "---------------------- Grand Canonical Algorithm Only (Segal Inspired) -----------------------" << endl;
        FastGrandCanonique firstway(Vinteraction, Vlandscape, (valeur) Mu, N, portee, vol_exclus);
        Current_problem = &firstway;
        firstway.Init();
        //firstway.show_partitions();
    }
    if(methode == 4){
        cerr << "------------------- Fast Grand Canonical Algorithm (Teif Transfer Matrix) -------------------------" << endl;
        TeifGrandCanonique firstway(Vinteraction, Vlandscape, (valeur) Mu, N, portee, vol_exclus);
        Current_problem = &firstway;
        firstway.Init();
        //firstway.show_partitions();
    }

    /*SIGNAL h=NewSignal();
    SizeSignal(h,12,YSIG);
    ZeroSig(h);


    SetResultValue(h);
    DeleteSignal(h);*/

    SIGNAL sigOut;
    sigOut = NewSignal();
    int _size = N;
    SizeSignal(sigOut,_size,YSIG);

    for (int i=0;i < _size;++i) {
        sigOut->Y[i] = (float) Current_problem->get_density(i+1);
    }

    SetResultValue(sigOut);

    DeleteSignal(sigOut);
}



/*

cleozone "/home/probert/Desktop/Cleozone/InterCst0.txt" "/home/probert/Desktop/Cleozone/LandCst1.txt" -2 15 5 5 3

SInter = <>
read SInter "/home/probert/Desktop/Cleozone/InterCst0.txt" -r
SLand = <>
read SLand "/home/probert/Desktop/Cleozone/LandCst1.txt" -r

 cleozone SInter SLand -2 15 5 5 3
*/



/*Set C_command interpretation parameters for LastWave*/
static CProc cleozoneProcs[] = {
"cleozone",C_cleozone,"{{{} {cleozone's proc}}}",
NULL,NULL,NULL
};

CProcTable cleozoneTable = {cleozoneProcs, "Cleozone", "4 algorithms for nucleosome prediction"};


#endif















