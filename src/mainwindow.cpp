#include "mainwindow.h"
#include "ui_mainwindow.h"


#include <iostream>
#include <sstream>
#include <cmath>
#include <string>
using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)


{
    ui->setupUi(this);
    ui->menuBar->hide();

    onepart = new OnePart(ui->tabOne);
    cerr << "One created" << endl;

    multiple = new Multiple(ui->tabMultiple);
    cerr << "Multiple created" << endl;
    //multiple->setStyleSheet("background-color:lightgray;");

}

MainWindow::~MainWindow(){
    delete ui;
}

