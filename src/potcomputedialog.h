#ifndef POTCOMPUTEDIALOG_H
#define POTCOMPUTEDIALOG_H

#include <QDialog>
#include <string>
#include <vector>
using namespace std;

namespace Ui {
class potComputeDialog;
}

class potComputeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit potComputeDialog(QWidget *parent = 0);
    ~potComputeDialog();

    string currentSequence;
    vector<float> currentProfile;

public slots:
    void recompute();
    void load();
    void clear();


private:
    Ui::potComputeDialog *ui;
};

#endif // POTCOMPUTEDIALOG_H
